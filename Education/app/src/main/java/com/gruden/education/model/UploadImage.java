package com.gruden.education.model;

import com.avos.avoscloud.AVFile;

/**
 * Created by yunjiansun on 16/1/25.
 */
public class UploadImage {
    public AVFile file;
    public String path;
    public String url;
    public boolean isDone;
}
