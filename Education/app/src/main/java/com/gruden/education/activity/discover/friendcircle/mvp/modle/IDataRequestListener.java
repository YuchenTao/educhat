package com.gruden.education.activity.discover.friendcircle.mvp.modle;
/**
 * 
* @ClassName: IDataRequestListener 
* @Description: 请求后台数据服务器响应后的回调 
* @author YuchenTao
* @date 2015-12-28 下午4:01:57 
*
 */
public interface IDataRequestListener {

	public void loadSuccess(Object object);
}
