package com.gruden.education.activity.conversation;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.avoscloud.leanchatlib.activity.AVChatActivity;
import com.gruden.education.R;

/**
 * Created by yunjiansun on 16/1/8.
 */
public class BaseChatActivity extends AVChatActivity {
    protected ActionBar actionBar;
    protected TextView txtTitile;
    protected TextView txtBack;
    protected ImageButton btnBack;
    protected Button btnRightTitle;
    protected ImageButton btnRightImage;
    protected ProgressDialog dialog;

    protected void alwaysShowMenuItem(MenuItem add) {
        add.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS
                | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
    }

    protected ProgressDialog showSpinnerDialog() {
        //activity = modifyDialogContext(activity);
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//    dialog.setCancelable(true);
        dialog.setMessage("加载中");
        if (!isFinishing()) {
            dialog.show();
        }
        return dialog;
    }


    protected void dismissSpinnerDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    protected ProgressDialog showSpinnerDialog(String message) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//    dialog.setCancelable(true);
        dialog.setMessage(message);
        if (!isFinishing()) {
            dialog.show();
        }
        return dialog;
    }



//  @Override
//  public boolean onOptionsItemSelected(MenuItem item) {
//    switch (item.getItemId()) {
//      case R.id.btnBack:
//        super.onBackPressed();
//        return true;
//    }
//    return super.onOptionsItemSelected(item);
//  }

    protected void hideSoftInputView() {
        if (getWindow().getAttributes().softInputMode !=
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
            InputMethodManager manager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                manager.hideSoftInputFromWindow(currentFocus.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    protected void setSoftInputMode() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void findBarViews(){

        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.base_actionbar);

        txtTitile = (TextView) actionBar.getCustomView().findViewById(R.id.txtBarTitle);
        btnBack = (ImageButton) actionBar.getCustomView().findViewById(R.id.btnBack);
        txtBack = (TextView) actionBar.getCustomView().findViewById(R.id.txtBack);
        btnRightTitle = (Button) actionBar.getCustomView().findViewById(R.id.btnRightTitle);
        btnRightImage = (ImageButton) actionBar.getCustomView().findViewById(R.id.btnRightImage);
    }

    protected void initActionBar() {
        initActionBar(null);
    }

    @Override
    protected void initActionBar(String title) {

        actionBar = getActionBar();


        if (actionBar != null) {
            findBarViews();

            if (title != null) {

                txtTitile.setText(title);


            }

        }
    }

    protected void initActionBar(int id) {
        initActionBar(getString(id));
    }


    public void showLeftBackButton(View.OnClickListener listener) {
        showLeftBackButton(null, listener);
    }
    public void showLeftBackButton(String backText) {
        showLeftBackButton(backText, null);
    }

    public void showLeftBackButton() {
        showLeftBackButton(null,null);
    }


    public void showLeftBackButton(String backText, View.OnClickListener listener) {

        btnBack.setVisibility(View.VISIBLE);
        txtBack.setVisibility(View.VISIBLE);

        if (backText!=null)
            txtBack.setText(backText);

        if (listener == null) {
            listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            };
        }

        btnBack.setOnClickListener(listener);
        txtBack.setOnClickListener(listener);
    }

    public void hideLeftBackButton(){
        btnBack.setVisibility(View.INVISIBLE);
        txtBack.setVisibility(View.INVISIBLE);
    }

    public void showRightImageButton(int rightResId, View.OnClickListener listener) {

        btnRightImage.setVisibility(View.VISIBLE);
        btnRightTitle.setVisibility(View.INVISIBLE);

        btnRightImage.setImageResource(rightResId);
        btnRightImage.setOnClickListener(listener);
    }

    public void showRightTextButton(String rightText, View.OnClickListener listener) {

        btnRightImage.setVisibility(View.INVISIBLE);
        btnRightTitle.setVisibility(View.VISIBLE);

        btnRightTitle.setText(rightText);
        btnRightTitle.setOnClickListener(listener);
    }

    public void hideRightButton(){
        btnRightImage.setVisibility(View.INVISIBLE);
        btnRightTitle.setVisibility(View.INVISIBLE);
    }


    protected void toast(String str) {
        Toast toast = Toast.makeText(this, str, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    protected void toast(Exception e){
        if(e!=null){
            toast(e.getMessage());
        }
    }

    protected void toast(int id) {
        Toast toast = Toast.makeText(this, id, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
