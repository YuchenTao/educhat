package com.gruden.education.viewholder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.event.NoticeItemClickEvent;
import com.gruden.education.model.Notice;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.greenrobot.event.EventBus;

/**
 * Created by yunjiansun on 16/1/12.
 */
public class NoticeItemHolder extends RecyclerView.ViewHolder {

    View lineTop;
    View lineBottom;
    TextView txtTime;
    TextView txtContent;
    String conversationId;


    public NoticeItemHolder(ViewGroup parent, String conversationId) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.notice_item, parent, false));
        this.conversationId = conversationId;
        initView();
    }

    public void initView() {
        lineTop = itemView.findViewById(R.id.lineTop);
        lineBottom = itemView.findViewById(R.id.lineBottom);
        txtTime = (TextView)itemView.findViewById(R.id.txtTime);
        txtContent = (TextView)itemView.findViewById(R.id.txtContent);
    }

    public void bindData(final Notice notice) {
        if (notice==null)
            return;

        if (getLayoutPosition()==0)
            lineTop.setVisibility(View.INVISIBLE);

        txtContent.setText(notice.content);

        txtTime.setText(notice.createdAt);

        if (notice.isSign){
            txtTime.setTextColor(Color.GRAY);
            txtContent.setTextColor(Color.GRAY);
        }else {
            txtTime.setTextColor(Color.BLACK);
            txtContent.setTextColor(Color.BLACK);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new NoticeItemClickEvent(notice.id,conversationId, notice.isCreatedByMe));

            }
        });

    }
}
