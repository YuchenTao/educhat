package com.gruden.education.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.model.ReportCardChild;
import com.gruden.education.model.ReportCardGroup;
import com.gruden.education.model.Subject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/1/15.
 */
public class ReportTeacherListAdapter extends BaseExpandableListAdapter {

    LayoutInflater mInflater;

    public ReportTeacherListAdapter(Context context){
        super();
        this.mInflater = LayoutInflater.from(context);


    }
    public List<ReportCardGroup> dataList = new ArrayList<ReportCardGroup>();

    public List<ReportCardGroup> getDataList() {
        return dataList;
    }

    public void setDataList(List<ReportCardGroup> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }
    public void addDataList(List<ReportCardGroup> datas) {
        dataList.addAll(datas);
    }

    @Override
    public int getGroupCount() {
        return dataList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return dataList.get(groupPosition).reportCardChildList.size();
    }

    @Override
    public ReportCardGroup getGroup(int groupPosition) {
        return dataList.get(groupPosition);
    }

    @Override
    public ReportCardChild getChild(int groupPosition, int childPosition) {
        return dataList.get(groupPosition).reportCardChildList.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ReportCardGroup reportCardGroup=dataList.get(groupPosition);
        ItemGroupHolder holder;
        if(convertView==null){
            holder = new ItemGroupHolder();
            convertView= mInflater.inflate(R.layout.report_exam_teacher_item, parent, false);
            holder.txtTitle= (TextView) convertView.findViewById(R.id.txtTitle);
            holder.imgExpanded= (ImageView) convertView.findViewById(R.id.imgExpanded);
            holder.lastDividerLayout=convertView.findViewById(R.id.lastDividerLayout);
            convertView.setTag(holder);
        }else{
            holder= (ItemGroupHolder) convertView.getTag();
        }
        holder.txtTitle.setText(reportCardGroup.examName);
        if(isExpanded){
            holder.imgExpanded.setImageResource(R.drawable.chengjidan_shouqi);
            convertView.setBackgroundResource(R.drawable.corner_item2_shape);
            holder.lastDividerLayout.setVisibility(View.VISIBLE);
        }else{
            holder.imgExpanded.setImageResource(R.drawable.chengjidan_zhankai);
            convertView.setBackgroundResource(R.drawable.corner_item3_shape);
            holder.lastDividerLayout.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ReportCardChild reportCardChild=dataList.get(groupPosition).reportCardChildList.get(childPosition);
        ItemChildHolder holder;
        if(convertView==null){
            holder = new ItemChildHolder();
            convertView= mInflater.inflate(R.layout.report_teacher_item, parent, false);
            holder.txtStudentCode= (TextView) convertView.findViewById(R.id.txtStudentCode);
            holder.txtStudentName= (TextView) convertView.findViewById(R.id.txtStudentName);
            holder.txtTotal= (TextView) convertView.findViewById(R.id.txtTotal);
            holder.txtSubject= (TextView) convertView.findViewById(R.id.txtSubject);
            holder.dividerLayout=convertView.findViewById(R.id.dividerLayout);
            holder.lastDividerLayout=convertView.findViewById(R.id.lastDividerLayout);
            holder.rightLayout=convertView.findViewById(R.id.rightLayout);
            holder.leftLayout=convertView.findViewById(R.id.leftLayout);
            convertView.setTag(holder);
        }else{
            holder= (ItemChildHolder) convertView.getTag();
        }
        holder.txtStudentCode.setText(reportCardChild.studentCode);
        holder.txtStudentName.setText(reportCardChild.studentName);
        holder.txtTotal.setText("总分：" +reportCardChild.total);

        String strSubject = "";
        for (int i=0; i<reportCardChild.subjectList.size();i++){
            Subject subject = reportCardChild.subjectList.get(i);
            strSubject = strSubject + subject.name + "：" + subject.score;
            if ((i+1)%2==0){
                strSubject = strSubject+"\n";
            }
            if((i+1)%2==1){
                strSubject = strSubject + " ";
            }
        }
        holder.txtSubject.setText(strSubject);
        if (isLastChild){

            holder.dividerLayout.setVisibility(View.GONE);
            holder.leftLayout.setVisibility(View.GONE);
            holder.rightLayout.setVisibility(View.GONE);
            holder.lastDividerLayout.setBackgroundResource(R.color.divider_line_color);
            convertView.setBackgroundResource(R.drawable.corner_item_shape);
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class ItemGroupHolder{
        TextView txtTitle;
        ImageView imgExpanded;
        View lastDividerLayout;
    }
    class ItemChildHolder{
        TextView txtStudentCode;
        TextView txtStudentName;
        TextView txtTotal;
        View lastDividerLayout;
        View dividerLayout;
        View rightLayout;
        View leftLayout;
        TextView txtSubject;

    }
}
