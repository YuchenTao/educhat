package com.gruden.education.viewholder;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by yunjiansun on 16/2/23.
 */
public class UserSignItemHolder {
    public ImageView imgAvatar;
    public TextView txtName;
    public TextView txtTime;
    public TextView txtSign;
}
