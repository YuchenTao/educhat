package com.gruden.education.customview;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gruden.education.R;
import com.gruden.education.activity.classmenu.FileListActivity;
import com.gruden.education.activity.classmenu.RedListActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClassMenu2 extends Fragment {

    protected Context ctx;
    public String conversationId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.class_menu2, container,false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ctx = getActivity();
    }
    @OnClick(R.id.layoutFlower)
    public void onFlowerClick(){
        Intent intent = new Intent(ctx, RedListActivity.class);
        intent.putExtra("id", conversationId);
        startActivity(intent);
    }
    @OnClick(R.id.layoutFile)
    public void onFileClick(){
        Intent intent = new Intent(ctx, FileListActivity.class);
        intent.putExtra("id", conversationId);
        startActivity(intent);
    }
}
