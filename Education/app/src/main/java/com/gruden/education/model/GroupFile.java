package com.gruden.education.model;

import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;

/**
 * Created by admin on 2016/3/9.
 */
public class GroupFile {
    public AVFile avfile;
    public AVObject avObject;
    public String fileName;
    public String creatdeAt;
    public String txtRemark;
    public String fileSize;
    public String fileUrl;
    public String fileId;
    public String downloadCount;
}
