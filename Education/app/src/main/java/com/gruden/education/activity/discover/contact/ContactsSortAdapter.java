package com.gruden.education.activity.discover.contact;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.Constants;
import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.gruden.education.R;
import com.gruden.education.customview.swipemenulistview.BaseSwipListAdapter;
import com.gruden.education.event.ContactItemClickEvent;
import com.gruden.education.event.ContactItemSelectEvent;
import com.nostra13.universalimageloader.core.ImageLoader;

import de.greenrobot.event.EventBus;

public class ContactsSortAdapter extends BaseSwipListAdapter implements SectionIndexer {
	private List<SortModel> mList;
	private List<SortModel> mSelectedList;
	private Activity mActivity;
	LayoutInflater mInflater;
	public boolean ifCheck;

	public ContactsSortAdapter(Activity activity, List<SortModel> list) {
		super();
		init(activity,list,false);
	}

	public ContactsSortAdapter(Activity activity, List<SortModel> list, boolean ifCheck) {
		super();
		init(activity,list,ifCheck);
	}

	private void init(Activity activity, List<SortModel> list, boolean ifCheck){

		this.mActivity = activity;
		this.ifCheck = ifCheck;
		mSelectedList = new ArrayList<SortModel>();

		if (list == null) {
			this.mList = new ArrayList<SortModel>();
		} else {
			this.mList = list;
		}

	}
	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * @param list
	 */
	public void  updateListView(List<SortModel> list) {
		if (list == null) {
			this.mList = new ArrayList<SortModel>();
		} else {
			this.mList = list;
		}
		notifyDataSetChanged();
	}

	public int getCount() {
		return this.mList.size();
	}

	@Override
	public int getViewTypeCount() {
		// menu type count
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		// current menu type
		//根据position获取分类的首字母的Char ascii值
//		int section = getSectionForPosition(position);

		//如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
//		if (position == getPositionForSection(section)) {
//			return 0;
//		} else {
//			return 1;
//		}

		if (mList.get(position).name==null){
			return 0;
		}else {
			return 1;
		}

	}

	@Override
	public boolean getSwipEnableByPosition(int position) {
		if(getItemViewType(position) == 0){
			return false;
		}
		return true;
	}

	public Object getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup arg2) {
		final ViewHolder viewHolder ;
		final SortModel mContent = mList.get(position);
		if (view == null) {
			viewHolder = new ViewHolder();
			view = LayoutInflater.from(mActivity).inflate(R.layout.item_contact, null);
			viewHolder.imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
			viewHolder.tvTitle = (TextView) view.findViewById(R.id.title);
			viewHolder.tvLetter = (TextView) view.findViewById(R.id.tvLetter);
			viewHolder.cbChecked = (CheckBox) view.findViewById(R.id.cbChecked);
			viewHolder.layoutContent = view.findViewById(R.id.layoutContent);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		if (getItemViewType(position)==0) {
			viewHolder.tvLetter.setVisibility(View.VISIBLE);
			viewHolder.layoutContent.setVisibility(View.GONE);
			viewHolder.tvLetter.setText(mContent.sortLetters);

		} else {
			viewHolder.tvLetter.setVisibility(View.GONE);
			viewHolder.layoutContent.setVisibility(View.VISIBLE);
			viewHolder.tvTitle.setText(mContent.name);
			ImageLoader.getInstance().displayImage(mContent.avatarUrl, viewHolder.imgAvatar, PhotoUtils.avatarImageOptions);

			if (ifCheck){
				viewHolder.cbChecked.setVisibility(View.VISIBLE);
				viewHolder.cbChecked.setChecked(isSelected(mContent));
				view.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						viewHolder.cbChecked.performClick();
						toggleChecked(position);
					}
				});
			}else {
				viewHolder.cbChecked.setVisibility(View.GONE);
				view.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(mActivity, FriendDetailActivity.class);
						intent.putExtra("id", mContent.userId);
						intent.putExtra("remarkName", mContent.name);
						intent.putExtra("position", position);
						intent.putExtra("backTitle", "通讯录");
						mActivity.startActivityForResult(intent,1);
					}
				});
			}

		}

		return view;

	}

	public static class ViewHolder {
		public ImageView imgAvatar;
		public TextView tvLetter;
		public TextView tvTitle;
		public TextView tvNumber;
		public CheckBox cbChecked;
		public View layoutContent;
	}

	/**
	 * 根据ListView的当前位置获取分类的首字母的Char ascii值
	 */

	public int getSectionForPosition(int position) {
		return mList.get(position).sortLetters.charAt(0);
	}

	/**
	 * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
	 */
	public int getPositionForSection(int section) {
		for (int i = 0; i < getCount(); i++) {
			String sortStr = mList.get(i).sortLetters;
			char firstChar = sortStr.toUpperCase(Locale.CHINESE).charAt(0);
			if (firstChar == section) {
				return i;
			}
		}

		return -1;
	}

	@Override
	public Object[] getSections() {
		return null;
	}

	private boolean isSelected(SortModel model) {
		return mSelectedList.contains(model);
		//return true;
	}

	public void toggleChecked(int position) {
		if (isSelected(mList.get(position))) {
			removeSelected(position);
		} else {
			setSelected(position);
		}

	}

	private void setSelected(int position) {
		if (mSelectedList.size()==0)
			EventBus.getDefault().post(new ContactItemSelectEvent(true));

		mSelectedList.add(mList.get(position));
	}

	private void removeSelected(int position) {

		mSelectedList.remove(mList.get(position));

		if (mSelectedList.size()==0)
			EventBus.getDefault().post(new ContactItemSelectEvent(false));
	}

	public List<SortModel> getSelectedList() {
		return mSelectedList;
	}
}