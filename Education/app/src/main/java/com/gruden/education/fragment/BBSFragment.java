package com.gruden.education.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.bbs.PostDetailActivity;
import com.gruden.education.activity.bbs.PublicPostActivity;
import com.gruden.education.activity.search.SearchActivity;
import com.gruden.education.adapter.BBSPostListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.event.PostListClickEvent;
import com.gruden.education.event.TopicSquareClickEvent;
import com.gruden.education.model.BBSPost;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;


public class BBSFragment extends BaseFragment {

    boolean hidden;
    int curPage = 0;
    BBSPostListAdapter postListAdapter;
    ArrayList<View> viewList = new ArrayList<View>();;

    View headerView;
    View layoutCommand;
    ViewPager viewPager;
    MyPagerAdapter pagerAdapter =  new MyPagerAdapter();
    CirclePageIndicator pageIndicator;
    ImageButton btnHot;
    ImageButton btnSquare;
    ImageButton btnAttention;
    View layoutHot,layoutSquare,layoutAttention;
    private boolean isRefreshing;
    private boolean isLoadMoreing;

    @Bind(R.id.bbs_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.bbsListView)
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postListAdapter = new BBSPostListAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.bbs_fragment, container, false);
        ButterKnife.bind(this, view);

        headerView = inflater.inflate(R.layout.bbs_header_layout, listView, false);
        layoutCommand = headerView.findViewById(R.id.layoutCommand);
        viewPager = (ViewPager)headerView.findViewById(R.id.viewpager);
        pageIndicator = (CirclePageIndicator)headerView.findViewById(R.id.pageIndicator);
        btnHot = (ImageButton)headerView.findViewById(R.id.btnHot);
        btnSquare = (ImageButton)headerView.findViewById(R.id.btnSquare);
        btnAttention = (ImageButton)headerView.findViewById(R.id.btnAttention);

        layoutHot=headerView.findViewById(R.id.layoutHot);
        layoutSquare=headerView.findViewById(R.id.layoutSquare);
        layoutAttention=headerView.findViewById(R.id.layoutAttention);

        layoutHot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new PostListClickEvent());
            }
        });

        layoutSquare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new TopicSquareClickEvent());
            }
        });
        layoutAttention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new PostListClickEvent("我的关注",null));
            }
        });

        listView.addHeaderView(headerView);
        listView.setAdapter(postListAdapter);

        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        Display display = wm.getDefaultDisplay();
        display.getSize(size);
        int width = size.x;
        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.height = (int) (width*0.25);
        viewPager.setLayoutParams(params);

        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                updateRecommedList();
                updatePostList(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                updatePostList(curPage+1);
            }
        });

        viewPager.setAdapter(pagerAdapter);
        pageIndicator.setViewPager(viewPager);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        headerLayout.showTitle("社区");

        headerLayout.showLeftImageButton(R.drawable.search, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                getActivity().startActivity(intent);
            }
        });

        headerLayout.showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, PublicPostActivity.class);
                ctx.startActivity(intent);
            }
        });


    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        this.hidden = hidden;
        if (!hidden) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }

    public void updateRecommedList(){

        RequestParams params = new RequestParams();
//        params.put("userId", LeanchatUser.getCurrentUserId());
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_FORUM +"post/list/recommed";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0){
                    Utils.toast(getActivity(), "暂时没有推荐给您的话题~");
                    layoutCommand.setVisibility(View.GONE);
                    return;
                }

                layoutCommand.setVisibility(View.VISIBLE);

                viewList.clear();

                for (int i=0; i<array.length(); i++){

                    JSONObject data = array.optJSONObject(i);
                    final String id = data.optString("id");
                    String url = data.optString("imgUrl");

                    View recommendPostView =  LayoutInflater.from(ctx).inflate(R.layout.bbs_recommend_imageview, viewPager, false);

                    TextView txtTitle = (TextView) recommendPostView.findViewById(R.id.txtTitle);
                    txtTitle.setText(data.optString("title"));

                    ImageView imageView = (ImageView)recommendPostView.findViewById(R.id.imageView);
                    ImageLoader.getInstance().displayImage(url, imageView, PhotoUtils.normalImageOptions);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), PostDetailActivity.class);
                            intent.putExtra("id", id);
                            getActivity().startActivity(intent);
                        }
                    });

                    viewList.add(recommendPostView);

                }

                pagerAdapter.notifyDataSetChanged();
                pageIndicator.notifyDataSetChanged();
                pageIndicator.setVisibility(viewList.size()>1?View.VISIBLE:View.INVISIBLE);

            }

            @Override
            public void handleSuccess(int code, String mesg) {
                toast( mesg);

            }

            @Override
            public void handleFailure(int error) {
                super.handleFailure(error);

            }

        });
    }

    public void updatePostList(final int page){

        RequestParams params = new RequestParams();
        params.put("curPage",page);
//        params.put("userId", LeanchatUser.getCurrentUserId());
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_FORUM +"post/list/latest";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0){
                    Utils.toast(getActivity(), "已经没有更多帖子了~");
                }else {
                    ArrayList<BBSPost> postList = new ArrayList<BBSPost>();

                    for (int i=0; i<array.length(); i++){

                        JSONObject data = array.optJSONObject(i);
                        BBSPost post = new BBSPost();
                        post.postId = data.optString("postId");
                        post.userId = data.optString("userId");
                        post.nickname = data.optString("nickname");
                        post.urlAvatar = data.optString("avatar");
                        post.title = data.optString("title");
                        post.replyCount = data.optInt("replyCount");
                        post.time = data.optString("createdAt");

                        postList.add(post);
                    }

                    if (page==0) {
                        postListAdapter.setDataList(postList);

                    }
                    else {
                        postListAdapter.addDataList(postList);
                    }

                    postListAdapter.notifyDataSetChanged();
                    curPage = page;
                }

                dissmissCircle();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }

        });
    }

    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public class MyPagerAdapter extends PagerAdapter {

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {

            return arg0 == arg1;
        }

        @Override
        public int getCount() {

            return viewList.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position,
                                Object object) {
            container.removeView(viewList.get(position));

        }

        @Override
        public int getItemPosition(Object object) {

            return super.getItemPosition(object);
        }



        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            container.addView(viewList.get(position));

            return viewList.get(position);
        }
    }

}
