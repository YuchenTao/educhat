package com.gruden.education.activity.conversation;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avoscloud.leanchatlib.event.ChatItemClickEvent;
import com.avoscloud.leanchatlib.utils.Constants;
import com.avoscloud.leanchatlib.utils.NotificationUtils;
import com.gruden.education.R;
import com.gruden.education.activity.discover.contact.FriendDetailActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

//import com.gruden.education.activity.LocationActivity;

/**
 * Created by lzw on 15/4/24.
 */
public class ChatRoomActivity extends BaseChatActivity {
  public static final int LOCATION_REQUEST = 100;
  public static final int GROUP_DETAIL_REQUEST = 101;
  public static final int QUIT_GROUP_RESULT= 200;
  public static final int NEW_NAME_RESULT= 201;

  private int identity = -1;
  private String conversationId;
  private String selfId;
  private boolean isCreator;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  protected void onResume() {
    NotificationUtils.cancelNotification(this);
    super.onResume();

  }

  @Override
  protected void updateConversation(final AVIMConversation conversation) {
    super.updateConversation(conversation);
    conversationId = conversation.getConversationId();
    selfId = LeanchatUser.getCurrentUser().getObjectId();
    if(selfId.equals(conversation.getCreator()))
      identity=2;

    showLeftBackButton();
    if (conversation.getMembers().size()==2){
      showRightImageButton(R.drawable.faxian_haoyoushenqing, new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          String userId=null;
          for (int i=0;i<conversation.getMembers().size();i++){
            if (!conversation.getMembers().get(i).equals(selfId))
               userId=conversation.getMembers().get(i);
          }
          Intent intent = new Intent(ChatRoomActivity.this, FriendDetailActivity.class);
          intent.putExtra("id", userId);
          intent.putExtra("isClass",1);
          startActivityForResult(intent, 1);
        }
      });
    }else{
      showRightImageButton(R.drawable.liaotian_qunzuxinxi, new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Intent intent = new Intent(ChatRoomActivity.this, ConversationDetailActivity.class);
          intent.putExtra(Constants.CONVERSATION_ID, conversationId);
          startActivityForResult(intent, GROUP_DETAIL_REQUEST);
        }
      });
    }


    getMembers();
  }


//  @Override
//  public boolean onCreateOptionsMenu(Menu menu) {
//    MenuInflater inflater = getMenuInflater();
//    inflater.inflate(R.menu.chat_ativity_menu, menu);
//    alwaysShowMenuItem(menu);
//    return super.onCreateOptionsMenu(menu);
//  }
//
//  @Override
//  public boolean onOptionsItemSelected(MenuItem item) {
//    int menuId = item.getItemId();
//    if (menuId == R.id.people) {
//      if (null != conversation) {
//        Intent intent = new Intent(ChatRoomActivity.this, ConversationDetailActivity.class);
//        intent.putExtra(Constants.CONVERSATION_ID, conversation.getConversationId());
//        startActivityForResult(intent, QUIT_GROUP_REQUEST);
//      }
//    }
//    return super.onOptionsItemSelected(item);
//  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
    super.onActivityResult(requestCode, resultCode, intent);
    if (requestCode == GROUP_DETAIL_REQUEST) {
      switch (resultCode) {
//        case LOCATION_REQUEST:
//          final double latitude = intent.getDoubleExtra(LocationActivity.LATITUDE, 0);
//          final double longitude = intent.getDoubleExtra(LocationActivity.LONGITUDE, 0);
//          final String address = intent.getStringExtra(LocationActivity.ADDRESS);
//          if (!TextUtils.isEmpty(address)) {
//            AVIMLocationMessage locationMsg = new AVIMLocationMessage();
//            locationMsg.setLocation(new AVGeoPoint(latitude, longitude));
//            locationMsg.setText(address);
//            chatFragment.sendMessage(locationMsg);
//          } else {
//            showToast(R.string.chat_cannotGetYourAddressInfo);
//          }
//          break;
        case QUIT_GROUP_RESULT: {
          setResult(QUIT_GROUP_RESULT);
          finish();
          break;
        }

        case NEW_NAME_RESULT: {
          if (txtTitile!=null){
            String newGroupName = intent.getStringExtra("new_group_name");
            if (!TextUtils.isEmpty(newGroupName))
              txtTitile.setText(newGroupName);
          }
          break;
        }
      }
    }
  }


  private void getMembers() {

    RequestParams params = new RequestParams();

    params.put("userId",selfId);
    params.put("groupId",conversationId);

    String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME+"members/list";

    MyHttpClient.get(url,params, new MyJsonResponseHandler() {

      @Override
      public void handleSuccess(int code, String mesg, JSONObject data) {


        JSONArray arrNames = data.names();
        if(arrNames!=null){
          for (int i=0; i<arrNames.length(); i++){

            JSONArray array = data.optJSONArray(arrNames.optString(i));

            for (int j=0; j<array.length(); j++){
              JSONObject object = array.optJSONObject(j);

              if (arrNames.optString(i).equals("headMaster")) {
                if (selfId.equals(object.optString("userId"))){
                  identity = 2;
                }
              }

            }
          }
        }

      }

      @Override
      public void handleSuccess(int code, String mesg, JSONArray data) {
        for (int i=0;i<data.length();i++){
          JSONObject object=data.optJSONObject(i);

          if ("0".equals(object.optString("identity"))){
            if (selfId.equals(object.optString("userId"))) {
              identity = 2;
            }
          }


        }
      }

      @Override
      public void handleSuccess(int code, String mesg) {
        toast(mesg);
      }

      @Override
      public void handleFailure(int error) {
        super.handleFailure(error);
      }

    });
  }

  public void onEvent(ChatItemClickEvent event) {
    Intent intent = new Intent(this, FriendDetailActivity.class);
    intent.putExtra("id", event.userId);
    intent.putExtra("remarkName", event.userName);
    intent.putExtra("conversationId", conversationId);
    intent.putExtra("isClass",1);
    if (event.isLeft)
      intent.putExtra("identity",identity);
    else
      intent.putExtra("identity",1);

    startActivityForResult(intent,1);
  }


//  public void onEvent(InputBottomBarLocationClickEvent event) {
//    LocationActivity.startToSelectLocationForResult(this, LOCATION_REQUEST);
//  }
//
//  public void onEvent(LocationItemClickEvent event) {
//    if (null != event && null != event.message && event.message instanceof AVIMLocationMessage) {
//      AVIMLocationMessage locationMessage = (AVIMLocationMessage) event.message;
//      LocationActivity.startToSeeLocationDetail(this, locationMessage.getLocation().getLatitude(),
//              locationMessage.getLocation().getLongitude());
//    }
//  }
}
