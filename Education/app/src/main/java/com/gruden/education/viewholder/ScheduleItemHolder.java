package com.gruden.education.viewholder;

import android.widget.TextView;

/**
 * Created by yunjiansun on 16/2/2.
 */
public class ScheduleItemHolder {

   public TextView txtSubject;
   public TextView txtTime;
}
