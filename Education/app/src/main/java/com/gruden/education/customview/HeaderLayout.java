package com.gruden.education.customview;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.gruden.education.R;

/**
 * Created by lzw on 14-9-17.
 */
public class HeaderLayout extends LinearLayout {
  LayoutInflater mInflater;
  RelativeLayout header;
  protected TextView txtTitile;
  protected TextView txtBack;
  protected ImageButton btnBack;
  protected ImageButton btnLeftImage;
  protected Button btnRightTitle;
  protected ImageButton btnRightImage;

  public HeaderLayout(Context context) {
    super(context);
    init();
  }

  public HeaderLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  private void init() {
    mInflater = LayoutInflater.from(getContext());

    header = (RelativeLayout) mInflater.inflate(R.layout.base_header, null, false);

    txtTitile = (TextView) header.findViewById(com.gruden.education.R.id.txtBarTitle);
    btnBack = (ImageButton)header.findViewById(com.gruden.education.R.id.btnBack);
    txtBack = (TextView) header.findViewById(com.gruden.education.R.id.txtBack);
    btnLeftImage = (ImageButton) header.findViewById(R.id.btnLeftImage);
    btnRightTitle = (Button) header.findViewById(com.gruden.education.R.id.btnRightTitle);
    btnRightImage = (ImageButton) header.findViewById(com.gruden.education.R.id.btnRightImage);

    addView(header);
  }

  public void showTitle(int titleId) {
    txtTitile.setText(titleId);
  }

  public void showTitle(String s) {
    txtTitile.setText(s);
  }

  public void showLeftBackButton(OnClickListener listener) {
    showLeftBackButton(null, listener);
  }
  public void showLeftBackButton(String backText) {
    showLeftBackButton(backText, null);
  }
  public void showLeftBackButton() {
    showLeftBackButton(null,null);
  }

  public void showLeftBackButton(String backText, OnClickListener listener) {

    btnBack.setVisibility(View.VISIBLE);
    txtBack.setVisibility(View.VISIBLE);

    if (backText!=null)
      txtBack.setText(backText);
    if (listener == null) {
      listener = new OnClickListener() {
        @Override
        public void onClick(View v) {
          ((FragmentActivity) getContext()).getSupportFragmentManager().popBackStack();
        }
      };
    }
    btnBack.setOnClickListener(listener);
    txtBack.setOnClickListener(listener);
  }

  public void hideLeftBackButton(){
    btnBack.setVisibility(View.INVISIBLE);
    txtBack.setVisibility(View.INVISIBLE);
  }


  public void showLeftImageButton(int rightResId, OnClickListener listener) {

    btnLeftImage.setVisibility(View.VISIBLE);

    btnLeftImage.setImageResource(rightResId);
    btnLeftImage.setOnClickListener(listener);
  }

  public void showRightImageButton(int rightResId, OnClickListener listener) {

    btnRightImage.setVisibility(View.VISIBLE);
    btnRightTitle.setVisibility(View.INVISIBLE);

    btnRightImage.setImageResource(rightResId);
    btnRightImage.setOnClickListener(listener);
  }

  public void showRightTextButton(String rightText, OnClickListener listener) {

    btnRightImage.setVisibility(View.INVISIBLE);
    btnRightTitle.setVisibility(View.VISIBLE);

    btnRightTitle.setText(rightText);
    btnRightTitle.setOnClickListener(listener);
  }

  public void hideRightButton(){
    btnRightImage.setVisibility(View.INVISIBLE);
    btnRightTitle.setVisibility(View.INVISIBLE);
  }
}
