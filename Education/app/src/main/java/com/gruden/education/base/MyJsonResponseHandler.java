package com.gruden.education.base;


import com.gruden.education.R;
import com.gruden.education.util.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;


/**
 * Created by yunjiansun on 15/11/10.
 */

public class MyJsonResponseHandler extends JsonHttpResponseHandler implements HandleResponse{

    public void handleSuccess(int code, String mesg) {
    }

    public void handleSuccess(int code, String mesg, JSONArray data) {

    }

    public void handleSuccess(int code, String mesg, JSONObject data) {

    }


    public void handleFailure(int error) {

        switch (error){
            case 1:{
                Utils.toast(R.string.error_internet);
                break;
            }

            case 2:{
                Utils.toast(R.string.error_server);
                break;
            }
            default:
                break;
        }

    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response){

        String url = this.getRequestURI().toString();
        System.out.printf("%s\n%s\n", url, response.toString());
        if (response.has("code")){

            if (response.has("data")){
                if (response.opt("data") instanceof JSONArray)
                    handleSuccess(response.optInt("code"), response.optString("mesg"), response.optJSONArray("data"));
                else
                    handleSuccess(response.optInt("code"), response.optString("mesg"), response.optJSONObject("data"));
            }else {
                handleSuccess(response.optInt("code"), response.optString("mesg"));
            }


        }else {
            handleFailure(2);
        }



    }


    @Override
    public void onFailure(int statusCode, Header[] headers,String  errorResponse,Throwable throwable){

        String url = this.getRequestURI().toString();
        System.out.println("Failed:"+ url);
        if (errorResponse!=null)
            System.out.println("ErrorResponse:"+ errorResponse);

        handleFailure(1);


    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse){

        String url = this.getRequestURI().toString();
        System.out.println("Failed:"+ url);
        if (errorResponse!=null)
            System.out.println("ErrorResponse:"+ errorResponse.toString());

        handleFailure(1);


    }


}
