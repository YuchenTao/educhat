package com.gruden.education.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.gruden.education.R;
import com.gruden.education.event.CommentItemClickEvent;
import com.gruden.education.model.Comment;
import com.gruden.education.viewholder.CommentItemHolder;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by yunjiansun on 16/2/5.
 */
public class PostCommentListAdapter extends BaseAdapter {
    LayoutInflater mInflater;
    Context context;

    public PostCommentListAdapter(Context context){
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);

    }

    public List<Comment> dataList = new ArrayList<Comment>();

    public List<Comment> getDataList() {
        return dataList;
    }

    public void setDataList(List<Comment> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }


    public void addDataList(List<Comment> datas) {
        dataList.addAll( datas);
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Comment getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Comment comment = dataList.get(position);
        CommentItemHolder holder;
        if (convertView == null) {
            holder = new CommentItemHolder();
            convertView = mInflater.inflate(R.layout.post_comment_item, parent, false);
            holder.imgAvatar = (ImageView) convertView.findViewById(R.id.imgAvatar);
            holder.txtUserName = (TextView)convertView.findViewById(R.id.txtUserName);
            holder.txtCreatedAt = (TextView)convertView.findViewById(R.id.txtCreatedAt);
            holder.txtContent = (TextView)convertView.findViewById(R.id.txtContent);
            holder.layoutReply = convertView.findViewById(R.id.layoutReply);
            holder.txtReplyUser = (TextView)convertView.findViewById(R.id.txtReplyUser);
            holder.txtReplyContent = (TextView)convertView.findViewById(R.id.txtReplyContent);
            convertView.setTag(holder);

        } else {
            holder = (CommentItemHolder) convertView.getTag();
        }

        ImageLoader.getInstance().displayImage(comment.avatarUrl, holder.imgAvatar, PhotoUtils.avatarImageOptions);
        holder.txtUserName.setText(comment.nickname);
        holder.txtCreatedAt.setText(comment.createdAt);
        holder.txtContent.setText(comment.content);
        if (!TextUtils.isEmpty(comment.replyTo.optString("content"))){
            holder.layoutReply.setVisibility(View.VISIBLE);
            holder.txtReplyUser.setText("“" + comment.replyTo.optString("nick_name") + " 发表于 " + comment.replyTo.optString("created_at") );
            holder.txtReplyContent.setText(comment.replyTo.optString("content")+"”");
        }else {
            holder.layoutReply.setVisibility(View.GONE);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new CommentItemClickEvent(comment,position));
            }
        });
        return convertView;
    }
}
