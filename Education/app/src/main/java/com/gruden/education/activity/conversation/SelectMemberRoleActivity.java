package com.gruden.education.activity.conversation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;

import butterknife.Bind;

/**
 * 群聊对话拉人页面
 * Created by lzw on 14-10-11.
 * TODO: ConversationChangeEvent
 */
public class SelectMemberRoleActivity extends BaseActivity {

  @Bind(R.id.layoutInvateTeacher)
  View layoutInvateTeacher;
  @Bind(R.id.layoutInvateParent)
  View layoutInvateParent;

  private String conversationId;
  private String className;
  private String code;
  private int type;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_select_member_role);
    Intent intent=getIntent();

    initActionBar(intent.getStringExtra("title"));
    conversationId=intent.getStringExtra("groupId");
    className=intent.getStringExtra("name");
    code=intent.getStringExtra("code");
    type=intent.getIntExtra("type",0);
    showLeftBackButton("完成");

    layoutInvateTeacher.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent=new Intent(SelectMemberRoleActivity.this, SelectFriendSourceActivity.class);
        intent.putExtra("groupId",conversationId);
        intent.putExtra("code",code);
        intent.putExtra("type",type);
        intent.putExtra("name",className);
        intent .putExtra("title","添加老师");
        intent.putExtra("status",1);
        startActivity(intent);
      }
    });
    layoutInvateParent.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent=new Intent(SelectMemberRoleActivity.this, SelectFriendSourceActivity.class);
        intent.putExtra("groupId",conversationId);
        intent.putExtra("code",code);
        intent.putExtra("type",type);
        intent.putExtra("name",className);
        intent .putExtra("title","添加家长");
        intent.putExtra("status",2);
        startActivity(intent);
      }
    });
  }



}