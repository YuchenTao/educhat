package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.HomeworkListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.event.HomeworkItemClickEvent;
import com.gruden.education.model.HomeWork;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

public class HomeworkListActivity extends BaseActivity {

    private int curPage;
    private String conversationId;

    private boolean isRefreshing;
    private boolean isLoadMoreing;

    private  HomeworkListAdapter listAdapter;

    @Bind(R.id.homework_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.listView)
    ListView listView;

    @Bind(R.id.txtPrompt)
    TextView txtPrompt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work_list);

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");

        initActionBar("作业");
        showLeftBackButton();
        LeanchatUser user = LeanchatUser.getCurrentUser();
        if(user.getRoles().contains(LeanchatUser.TEACHER)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN)){
            showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeworkListActivity.this, PublishHomeworkActivity.class);
                    intent.putExtra("id", conversationId);
                    startActivity(intent);
                }
            });
        }


        listAdapter = new HomeworkListAdapter(this, conversationId);
        listView.setAdapter(listAdapter);

        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                updateHomeworkList(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                updateHomeworkList(curPage+1);
            }

            @Override
            public void onfinish(){
                txtPrompt.setVisibility(listAdapter.getCount()>0?View.INVISIBLE:View.VISIBLE);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }

    void updateHomeworkList(final int page){
        RequestParams params = new RequestParams();
        params.put("groupId",conversationId);
        params.put("curPage",page);

        final LeanchatUser user = LeanchatUser.getCurrentUser();
        params.put("userId",user.getObjectId());
        params.put("role",(user.getRoles().contains(LeanchatUser.TEACHER)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN))?"1":"0");

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_MESSAGE +"homework";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0&&page>0){

//                    toast("已经没有更多作业了~");

                }else {
                    ArrayList<HomeWork> homeworkList = new ArrayList<HomeWork>();

                    for (int i=0; i<array.length(); i++){

                        JSONObject data = array.optJSONObject(i);
                        HomeWork homework1 = new HomeWork();
                        homework1.date = data.optString("date") + " " + data.optString("weekDay");
                        homeworkList.add(homework1);

                        JSONArray arr = data.optJSONArray("data");
                        for (int j=0; j<arr.length(); j++){

                            JSONObject subject = arr.optJSONObject(j);
                            HomeWork homework2 = new HomeWork();
                            homework2.id = subject.optString("id");

                            if (user.getObjectId().equals(subject.optString("creator")))
                                 homework2.isCreatedByMe = true;

                            homework2.groupId = subject.optString("groupId");
                            homework2.content = subject.optString("content");
                            homework2.subject = subject.optString("subject");
                            homework2.deadline = subject.optString("endDate");
                            homework2.createAt = subject.optString("time");
                            if (j==arr.length()-1)
                                homework2.isBottom = true;
                            homeworkList.add(homework2);
                        }
                    }

                    if (page==0) {
                        listAdapter.setDataList(homeworkList);
                    }
                    else {
                        listAdapter.addDataList(homeworkList);
                    }

                    listAdapter.notifyDataSetChanged();
                    curPage = page;
                }

                dissmissCircle();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }

        });
    }

    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }

    public void onEvent(HomeworkItemClickEvent event) {
        if (isLoadMoreing||isRefreshing )
            return;
        if (event.isCreatedByMe){
            Intent intent = new Intent(this, HomeworkTeacherActivity.class);
            intent.putExtra("id", event.id);
            intent.putExtra("conversationId", event.conversationId);
            startActivity(intent);
        }else {
            Intent intent = new Intent(this, HomeworkParentActivity.class);
            intent.putExtra("id", event.id);
            startActivity(intent);
        }
    }
}
