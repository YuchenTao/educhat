
package com.gruden.education.activity.discover.friendcircle;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.ProgressCallback;
import com.avos.avoscloud.SaveCallback;
import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.activity.discover.PublishLogActivity;
import com.gruden.education.activity.discover.friendcircle.adapter.CircleAdapter;
import com.gruden.education.activity.discover.friendcircle.bean.CircleItem;
import com.gruden.education.activity.discover.friendcircle.bean.CommentConfig;
import com.gruden.education.activity.discover.friendcircle.bean.CommentItem;
import com.gruden.education.activity.discover.friendcircle.bean.FavortItem;
import com.gruden.education.activity.discover.friendcircle.bean.User;
import com.gruden.education.activity.discover.friendcircle.listener.SwpipeListViewOnScrollListener;
import com.gruden.education.activity.discover.friendcircle.mvp.presenter.CirclePresenter;
import com.gruden.education.activity.discover.friendcircle.mvp.view.ICircleView;
import com.gruden.education.activity.discover.friendcircle.utils.CommonUtils;
import com.gruden.education.activity.discover.friendcircle.utils.DatasUtil;
import com.gruden.education.activity.discover.friendcircle.widgets.CommentListView;
import com.gruden.education.R;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.customview.poplistwindow.ListPopWindow;
import com.gruden.education.customview.poplistwindow.PopBean;
import com.gruden.education.model.Blog;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.PathUtils;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.datafans.android.common.helper.DipHelper;
import net.datafans.android.common.widget.imageview.CommonImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @ClassName: MainActivity
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author YuchenTao
 * @date 2015-12-28 下午4:21:18 
 *
 */
public class GrowthLogActivity extends BaseActivity implements  ICircleView, ListPopWindow.OnPopItemClickListener,ListPopWindow.OnBottomTextviewClickListener{

    protected static final String TAG = GrowthLogActivity.class.getSimpleName();
    private static final int PUBLISH_REQUEST = 1;
    private static final int CROP_REQUEST = 2;
    private ListView mCircleLv;
    private MaterialRefreshLayout mSwipeRefreshLayout;
    private CircleAdapter mAdapter;
    private LinearLayout mEditTextBody;
    private EditText mEditText;
    private ImageView sendIv;

    private ImageView coverView;

    protected ImageView userAvatarView;

    protected TextView userNickView;

    protected TextView signView;

    private int mScreenHeight;
    private int mEditTextBodyHeight;
    private int mCurrentKeyboardH;
    private int mSelectCircleItemH;
    private int mSelectCommentItemOffset;

    private CirclePresenter mPresenter;
    private CommentConfig mCommentConfig;

    ListPopWindow popWindow;
    private FrameLayout parentLayout;

    private String friendId;

    private Uri backgroundUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_growth_log);

        parentLayout = (FrameLayout) findViewById(R.id.parent);

        Intent intent = getIntent();
        friendId = intent.getStringExtra("friend_id");

        String outputPath = PathUtils.getAvatarTmpPath();
        backgroundUri = Uri.fromFile(new File(outputPath));

        initActionBar("成长日志");
        showLeftBackButton();

        if (TextUtils.isEmpty(friendId)){
            showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GrowthLogActivity.this, PublishLogActivity.class);
                    startActivityForResult(intent,PUBLISH_REQUEST);
                }
            });
        }

        LeanchatUser self = LeanchatUser.getCurrentUser();
        DatasUtil.curUser = new User(self.getObjectId(), self.getNickName(), self.getAvatarUrl());
        mPresenter = new CirclePresenter(this);

        initView();
        loadData(-1L);
    }


    @Override
    protected void onResume() {
        super.onResume();
//        mSwipeRefreshLayout.autoRefresh();
    }

    @SuppressLint({ "ClickableViewAccessibility", "InlinedApi" })
    private void initView() {
        mSwipeRefreshLayout = (MaterialRefreshLayout) findViewById(R.id.mRefreshLayout);
//        mSwipeRefreshLayout.setIsOverLay(true);
//        mSwipeRefreshLayout.setWaveShow(false);

        mCircleLv = (ListView) findViewById(R.id.circleLv);

        View header = getLayoutInflater().inflate(R.layout.header_growth_log, null);
        coverView = (ImageView) header.findViewById(net.datafans.android.timeline.R.id.cover);

//        ViewTreeObserver observer = coverView.getViewTreeObserver();
//        class SquareLayoutAdjuster
//                implements ViewTreeObserver.OnGlobalLayoutListener {
//            @Override
//            public void onGlobalLayout() {
//                ViewGroup.LayoutParams params = coverView.getLayoutParams();
//                params.height = params.width;
//                coverView.setLayoutParams(params);
//                coverView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//            }
//        }
//
//        observer.addOnGlobalLayoutListener(new SquareLayoutAdjuster());

        coverView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show("修改封面");
            }
        });

        userAvatarView = (ImageView) header.findViewById(net.datafans.android.timeline.R.id.userAvatar);

        if (TextUtils.isEmpty(friendId)){
            userAvatarView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(GrowthLogActivity.this, GrowthLogActivity.class);
                    intent.putExtra("friend_id", LeanchatUser.getCurrentUserId());
                    startActivity(intent);
                }
            });
        }

        userNickView = (TextView) header.findViewById(net.datafans.android.timeline.R.id.userNick);
        signView = (TextView) header.findViewById(net.datafans.android.timeline.R.id.sign);
        mCircleLv.addHeaderView(header);

//        mCircleLv.setOnScrollListener(new SwpipeListViewOnScrollListener(mSwipeRefreshLayout));
        mCircleLv.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mEditTextBody.getVisibility() == View.VISIBLE) {
                    //mEditTextBody.setVisibility(View.GONE);
                    //CommonUtils.hideSoftInput(MainActivity.this, mEditText);
                    updateEditTextBodyVisible(View.GONE, null);
                    return true;
                }
                return false;
            }
        });

        mSwipeRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
//                isRefreshing = true;
                loadData(-1L);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
//                isLoadMoreing = true;
                loadData(mAdapter.earliestTime);
            }
        });

        mAdapter = new CircleAdapter(this);
        mAdapter.friendId = friendId;
        mAdapter.setCirclePresenter(mPresenter);
        mCircleLv.setAdapter(mAdapter);

        mEditTextBody = (LinearLayout) findViewById(R.id.editTextBodyLl);
        mEditText = (EditText) findViewById(R.id.circleEt);
        sendIv = (ImageView) findViewById(R.id.sendIv);
        sendIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPresenter != null) {
                    //发布评论
                    String content = mEditText.getText().toString().trim();
                    if(TextUtils.isEmpty(content)){
                        Toast.makeText(GrowthLogActivity.this, "评论内容不能为空...", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mPresenter.addComment(content, mCommentConfig);
                }
                updateEditTextBodyVisible(View.GONE, null);
            }
        });

        setViewTreeObserver();

    }


    private void setViewTreeObserver() {

        final ViewTreeObserver swipeRefreshLayoutVTO = mSwipeRefreshLayout.getViewTreeObserver();
        swipeRefreshLayoutVTO.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                mSwipeRefreshLayout.getWindowVisibleDisplayFrame(r);
                int statusBarH =  getStatusBarHeight();//状态栏高度
                int screenH = mSwipeRefreshLayout.getRootView().getHeight();
                if(r.top != statusBarH ){
                    //在这个demo中r.top代表的是状态栏高度，在沉浸式状态栏时r.top＝0，通过getStatusBarHeight获取状态栏高度
                    r.top = statusBarH;
                }
                int keyboardH = screenH - (r.bottom - r.top);
                Log.d(TAG, "screenH＝ "+ screenH +" &keyboardH = " + keyboardH + " &r.bottom=" + r.bottom + " &top=" + r.top + " &statusBarH=" + statusBarH);

                if(keyboardH == mCurrentKeyboardH){//有变化时才处理，否则会陷入死循环
                    return;
                }

                mCurrentKeyboardH = keyboardH;
                mScreenHeight = screenH;//应用屏幕的高度
                mEditTextBodyHeight = mEditTextBody.getHeight();

                //偏移listview
                if(mCircleLv!=null && mCommentConfig != null){
                    int index = mCommentConfig.circlePosition==0?mCommentConfig.circlePosition:(mCommentConfig.circlePosition+mCircleLv.getHeaderViewsCount());
                    mCircleLv.setSelectionFromTop(index, getListviewOffset(mCommentConfig));
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case CROP_REQUEST:{
                popWindow.dismiss();
                if (data==null)
                    return;

                showSpinnerDialog();
                if (backgroundUri==null){
                    dismissSpinnerDialog();
                    toast("获取图片失败");
                    return;
                }

                String imagePath = Utils.getRealFilePath(this,backgroundUri);

                try {
                    final Bitmap bitmap  = BitmapFactory.decodeStream(getContentResolver().openInputStream(backgroundUri));
                    final AVFile avFile = AVFile.withAbsoluteLocalPath("image.jpg", imagePath);
                    avFile.saveInBackground(
                            new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    String url = avFile.getUrl();
                                    System.out.println("url: " + url);
                                    changeCoverImage(url,bitmap);

                                }
                            },
                            new ProgressCallback() {
                                @Override
                                public void done(Integer percentDone) {
                                    //打印进度
                                    System.out.println("uploading: " + percentDone);
                                }
                            });

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    dismissSpinnerDialog();
                    toast(e.getMessage());
                }
                break;
            }

            case PUBLISH_REQUEST:{
                loadData(-1L);
            }

            default:{
                break;
            }
        }

    }

    /**
     * 获取状态栏高度
     * @return
     */
    private int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }



    private void loadData(final long earliestTime) {

        RequestParams params = new RequestParams();
        params.put("selfId", LeanchatUser.getCurrentUser().getObjectId());

        if (!TextUtils.isEmpty(friendId)){
            params.put("friendId",friendId);
        }

        if (earliestTime!=-1L){
            params.put("earliestTime",earliestTime);
        }


        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER+"friends/log";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONObject object) {
                List<CircleItem> datas;

                if (earliestTime==-1L){

                    userNickView.setText(object.optString("nickname"));

                    ImageLoader.getInstance().displayImage(object.optString("avatar"), userAvatarView, PhotoUtils.avatarImageOptions);
                    ImageLoader.getInstance().displayImage(object.optString("circle_post_bgImg"), coverView, PhotoUtils.normalImageOptions);
                    datas = DatasUtil.createCircleDatas(object.optJSONArray("posts"),false);

                    mAdapter.setDatas(datas);
                    mAdapter.notifyDataSetChanged();
                }else {
                    if (object.optJSONArray("posts").length()>0) {
                        datas = DatasUtil.createCircleDatas(object.optJSONArray("posts"), true);
                        mAdapter.setDatas(datas);
                        mAdapter.notifyDataSetChanged();
                    }
                    else
                        toast("已经没有更多日志了~");
                }




                if (earliestTime==-1L){
                    mSwipeRefreshLayout.finishRefresh();
                }else {
                    mSwipeRefreshLayout.finishRefreshLoadMore();
                }

            }

            @Override
            public void handleSuccess(int code, String mesg) {
                if (earliestTime==-1L){
                    mSwipeRefreshLayout.finishRefresh();
                }else {
                    mSwipeRefreshLayout.finishRefreshLoadMore();
                }
                toast(mesg);
            }

            @Override
            public void handleFailure(int error) {
                if (earliestTime==-1L){
                    mSwipeRefreshLayout.finishRefresh();
                }else {
                    mSwipeRefreshLayout.finishRefreshLoadMore();
                }
                super.handleFailure(error);
            }

        });

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if(mEditTextBody != null && mEditTextBody.getVisibility() == View.VISIBLE){
                mEditTextBody.setVisibility(View.GONE);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void avatarClickEvent(String friendId){
        Intent intent = new Intent(this, GrowthLogActivity.class);
        intent.putExtra("friend_id", friendId);
        startActivity(intent);
    }

    @Override
    public void update2DeleteCircle(final String circleId) {

        AlertDialog alertDialog = new AlertDialog.Builder(this).
                setTitle("确定删除吗？").
                setPositiveButton("删除", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub

                        RequestParams params = new RequestParams();
                        params.put("userId",LeanchatUser.getCurrentUser().getObjectId());
                        params.put("postId",circleId);

                        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER+ "friends/log";
                        MyHttpClient.delete(url,params,new MyJsonResponseHandler(){

                            @Override
                            public void handleSuccess(int code, String mesg){

                                if (code==0){

                                    List<CircleItem> circleItems = mAdapter.getDatas();
                                    for(int i=0; i<circleItems.size(); i++){
                                        if(circleId.equals(circleItems.get(i).getId())){
                                            circleItems.remove(i);
                                            mAdapter.notifyDataSetChanged();
                                            return;
                                        }
                                    }

                                }else {
                                    toast(mesg);
                                }

                            }

                            @Override
                            public void handleFailure(int error) {
                                super.handleFailure(error);
                            }

                        });


                    }
                }).
                setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                }).
                create();

        alertDialog.show();

    }

    @Override
    public void update2AddFavorite(int circlePosition, FavortItem addItem) {
        if(addItem != null){
            mAdapter.getDatas().get(circlePosition).getFavorters().add(addItem);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void update2DeleteFavort(int circlePosition, String favortId) {
        List<FavortItem> items = mAdapter.getDatas().get(circlePosition).getFavorters();
        for(int i=0; i<items.size(); i++){
            if(favortId.equals(items.get(i).getId())){
                items.remove(i);
                mAdapter.notifyDataSetChanged();
                return;
            }
        }
    }

    @Override
    public void update2AddComment(final CommentConfig config, final CommentItem addItem) {

        if(addItem != null){
            //更新评论列表
            mAdapter.getDatas().get(config.circlePosition).getComments().add(addItem);
            mAdapter.notifyDataSetChanged();
            //后台发表评论
            RequestParams params = new RequestParams();
            params.put("userId",addItem.getUser().getId());
            params.put("postId",mAdapter.getDatas().get(config.circlePosition).getId());
            params.put("content",addItem.getContent());
            params.put("type",2);

            if (addItem.getToReplyUser()!=null){
                params.put("commentId",addItem.getToReplyUser().getId());
                params.put("commentCreatorId",mAdapter.getDatas().get(config.circlePosition).getComments().get(config.commentPosition).getId());
            }

            String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_FORUM+ "post/comment";
            MyHttpClient.post(url,params,new MyJsonResponseHandler(){

                @Override
                public void handleSuccess(int code, String mesg, JSONObject data){

                    List<CommentItem> items = mAdapter.getDatas().get(config.circlePosition).getComments();
                    int index=  items.indexOf(addItem);
                    items.get(index).setId(data.optString("commentId"));
                }

                @Override
                public void handleSuccess(int code, String mesg){
                    toast(mesg);
                    mAdapter.getDatas().get(config.circlePosition).getComments().remove(addItem);
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void handleFailure(int error) {
                    super.handleFailure(error);
                    mAdapter.getDatas().get(config.circlePosition).getComments().remove(addItem);
                    mAdapter.notifyDataSetChanged();
                }

            });

        }
        //清空评论文本
        mEditText.setText("");
    }

    @Override
    public void update2DeleteComment(final int circlePosition, final int commentPosition, final String commentId) {

        List<CommentItem> items = mAdapter.getDatas().get(circlePosition).getComments();
        final CommentItem comment= items.get(commentPosition);
        items.remove(commentPosition);
        mAdapter.notifyDataSetChanged();

        RequestParams params = new RequestParams();
        params.put("commentId",commentId);
        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_FORUM+ "post/comment";
        MyHttpClient.delete(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg){

                if (code!=0){
                    toast(mesg);
                    mAdapter.getDatas().get(circlePosition).getComments().add(commentPosition, comment);
                    mAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void handleFailure(int error) {
                super.handleFailure(error);
                mAdapter.getDatas().get(circlePosition).getComments().add(commentPosition, comment);
                mAdapter.notifyDataSetChanged();
            }

        });


    }

    @Override
    public void updateEditTextBodyVisible(int visibility, CommentConfig commentConfig) {
        mCommentConfig = commentConfig;
        mEditTextBody.setVisibility(visibility);

        measureCircleItemHighAndCommentItemOffset(commentConfig);

        if(View.VISIBLE==visibility){
            mEditText.requestFocus();
            //弹出键盘
            CommonUtils.showSoftInput(mEditText.getContext(), mEditText);

        }else if(View.GONE==visibility){
            //隐藏键盘
            CommonUtils.hideSoftInput(mEditText.getContext(), mEditText);
        }
    }


    /**
     * 测量偏移量
     * @param commentConfig
     * @return
     */
    private int getListviewOffset(CommentConfig commentConfig) {
        if(commentConfig == null)
            return 0;
        //这里如果你的listview上面还有其它占高度的控件，则需要减去该控件高度，listview的headview除外。
        int listviewOffset = mScreenHeight - mSelectCircleItemH - mCurrentKeyboardH - mEditTextBodyHeight -dp2px(60);
        if(commentConfig.commentType == CommentConfig.Type.REPLY){
            //回复评论的情况
            listviewOffset = listviewOffset + mSelectCommentItemOffset;
        }
        return listviewOffset;
    }

    private void measureCircleItemHighAndCommentItemOffset(CommentConfig commentConfig){
        if(commentConfig == null)
            return;

        int headViewCount = mCircleLv.getHeaderViewsCount();
        int firstPosition = mCircleLv.getFirstVisiblePosition();
        //只能返回当前可见区域（列表可滚动）的子项
        View selectCircleItem = mCircleLv.getChildAt(headViewCount + commentConfig.circlePosition - firstPosition);
        if(selectCircleItem != null){
            mSelectCircleItemH = selectCircleItem.getHeight();
            if(headViewCount >0 && firstPosition <headViewCount && commentConfig.circlePosition == 0){
                //如果有headView，而且head是可见的，并且处理偏移的位置是第一条动态，则将显示的headView的高度合并到第一条动态上
                for(int i=firstPosition; i<headViewCount; i++){
                    mSelectCircleItemH += mCircleLv.getChildAt(i).getHeight();
                }
            }
        }

        if(commentConfig.commentType == CommentConfig.Type.REPLY){
            //回复评论的情况
            CommentListView commentLv = (CommentListView) selectCircleItem.findViewById(R.id.commentList);
            if(commentLv!=null){
                //找到要回复的评论view,计算出该view距离所属动态底部的距离
                View selectCommentItem = commentLv.getChildAt(commentConfig.commentPosition);
                if(selectCommentItem != null){
                    //选择的commentItem距选择的CircleItem底部的距离
                    mSelectCommentItemOffset = 0;
                    View parentView = selectCommentItem;
                    do {
                        int subItemBottom = parentView.getBottom();
                        parentView = (View) parentView.getParent();
                        if(parentView != null){
                            mSelectCommentItemOffset += (parentView.getHeight() - subItemBottom);
                        }
                    } while (parentView != null && parentView != selectCircleItem);
                }
            }
        }
    }

    public void show(String text){
        List<PopBean> pops = new ArrayList<>();
        PopBean pop = new PopBean(text,0);
        pops.add(pop);
        popWindow = new ListPopWindow(GrowthLogActivity.this,this,this,parentLayout,pops,"取消",null);
        popWindow.showAtLocation(parentLayout, Gravity.CENTER| Gravity.BOTTOM,0,0);
    }

    private void changeCoverImage(String imageUrl, final Bitmap bitmap){

        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("bgImg",imageUrl);


        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER+"friends/log";

        MyHttpClient.put(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                coverView.setImageBitmap(bitmap);
                toast(mesg);
            }

            @Override
            public void handleFailure(int error) {

                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }

    @Override
    public void onBottomClick() {

        popWindow.dismiss();
    }

    @Override
    public void onPopItemClick(View view, int position) {


        Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);

        intent.setType("image/*");

        intent.putExtra("crop", "true");

        intent.putExtra("aspectX", 1);

        intent.putExtra("aspectY", 1);

        intent.putExtra("outputX", 600);

        intent.putExtra("outputY", 600);

        intent.putExtra("scale", true);

        intent.putExtra("return-data",false);

//        String outputPath = PathUtils.getAvatarTmpPath();
//        Uri imageUri = Uri.fromFile(new File(outputPath));
//
//        Log.e("outputPath",outputPath);
//        if (imageUri==null)
//            Log.e("imageUri","null");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, backgroundUri);

        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());

        intent.putExtra("noFaceDetection", true); // no face detection

        startActivityForResult(intent, CROP_REQUEST);


//        Intent intent = new Intent("com.android.camera.action.CROP");
//        intent.setDataAndType(uri, "image/*");
//        intent.putExtra("crop", "true");
//        intent.putExtra("aspectX", 1);
//        intent.putExtra("aspectY", 1);
//        intent.putExtra("outputX", 600);
//        intent.putExtra("outputY", 600);
//        intent.putExtra("scale", true);
//        String outputPath = PathUtils.getAvatarTmpPath();
//        Uri outputUri = Uri.fromFile(new File(outputPath));
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
//        intent.putExtra("return-data", true);
//        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG);
//        intent.putExtra("noFaceDetection", true); // no face detection
//        startActivityForResult(intent, CROP_REQUEST);
    }
}
