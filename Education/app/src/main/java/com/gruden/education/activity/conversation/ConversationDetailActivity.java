package com.gruden.education.activity.conversation;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.ProgressCallback;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avoscloud.leanchatlib.controller.ChatManager;
import com.avoscloud.leanchatlib.controller.RoomsTable;
import com.avoscloud.leanchatlib.utils.AVIMConversationCacheUtils;
import com.avoscloud.leanchatlib.utils.Constants;
import com.avoscloud.leanchatlib.utils.ConversationManager;
import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.activity.classmenu.GroupMembersActivity;
import com.gruden.education.activity.discover.contact.FriendDetailActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.PathUtils;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by lzw on 14-10-11.
 */
public class ConversationDetailActivity extends BaseActivity implements View.OnClickListener {
  private static final int IMAGE_PICK_REQUEST = 1;
  private static final int CROP_REQUEST = 2;
  public static final int QUIT_GROUP_RESULT= 200;
  public static final int NEW_NAME_RESULT= 201;


  @Bind(R.id.layoutGroupInfo)
  View layoutGroupInfo;
  @Bind(R.id.layoutConversationSet)
  View layoutConversationSet;

  @Bind(R.id.layoutInvateMember)
  View layoutInvateMember;

  @Bind(R.id.layoutClassMember)
  View layoutClassMember;

  @Bind(R.id.layoutChatLog)
  View layoutChatLog;

  @Bind(R.id.updateLayout)
  View updateLayout;



  @Bind(R.id.txtGroupCode)
  TextView  txtGroupCode;

  @Bind(R.id.txtClassMember)
  TextView  txtClassMember;

  @Bind(R.id.txtMemberName)
  TextView  txtMemberName;


  @Bind(R.id.btnSwitch)
  ImageButton btnSwitch;

  @Bind(R.id.groupAvatarView)
  ImageView avatarView;
  @Bind(R.id.imgUpdateName)
  ImageView imgUpdateName;

  @Bind(R.id.editUpdateName)
  EditText editUpdateName;

  @Bind(R.id.btn_finish)
  Button btn_finish;

  private String picUrl;
  private boolean isUpdate;


  String conversationId;
  private AVIMConversation conversation;
  private String className;
  private String classcode;
  private int type;
  private String newGroupName;



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_conversation_detail);
    conversationId = getIntent().getStringExtra(Constants.CONVERSATION_ID);
    conversation = AVIMClient.getInstance(ChatManager.getInstance().getSelfId()).getConversation(conversationId);

    initActionBar("班级信息");
    showLeftBackButton(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent data = new Intent();
        data.putExtra("new_group_name", newGroupName);
        setResult(NEW_NAME_RESULT, data);
        finish();
      }
    });

    initListener();

  }

  private void initListener() {

    layoutInvateMember.setOnClickListener(this);
    imgUpdateName.setOnClickListener(this);
    layoutClassMember.setOnClickListener(this);
    btn_finish.setOnClickListener(this);
//    editUpdateName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//
//      @Override
//      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//        if (actionId == EditorInfo.IME_ACTION_DONE) {
//
//          editUpdateName.setFocusable(false);
//          editUpdateName.setFocusableInTouchMode(false);
//          editUpdateName.setBackground(null);
//          InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//          imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//          btn_finish.setVisibility(View.GONE);
//          imgUpdateName.setVisibility(View.VISIBLE);
//          updateGroupName();
//          return true;
//
//        }
//        return false;
//      }
//    });
  }

  public void onContactsSwitch(View v) {

    btnSwitch.setSelected(btnSwitch.isSelected() ? false : true);
  }
  @Override
  public void onClick(View v) {
    Intent intent;
    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    switch (v.getId()){

      case R.id.layoutClassMember:
        intent = new Intent(this, GroupMembersActivity.class);
        intent.putExtra(Constants.CONVERSATION_ID, conversationId);
        startActivity(intent);
        break;
      case R.id.layoutInvateMember:
        intent =new Intent(this,SelectMemberRoleActivity.class);
        intent.putExtra("title","邀请新成员");
        intent.putExtra("groupId",conversationId);
        intent.putExtra("code",classcode);
        intent.putExtra("type",type);
        intent.putExtra("name",className);
        startActivity(intent);
        break;
      case R.id.imgUpdateName:
        editUpdateName.setFocusable(true);
        editUpdateName.setFocusableInTouchMode(true);
        editUpdateName.setBackgroundResource(R.drawable.edit_normal_shape);
        btn_finish.setVisibility(View.VISIBLE);
        imgUpdateName.setVisibility(View.GONE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
        break;
      case R.id.groupAvatarView:
        Intent i = new Intent(Intent.ACTION_PICK, null);
        i.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(i, IMAGE_PICK_REQUEST);
        break;
      case R.id.btn_finish:
        editUpdateName.setFocusable(false);
        editUpdateName.setFocusableInTouchMode(false);
        editUpdateName.setBackground(null);
        btn_finish.setVisibility(View.GONE);
        imgUpdateName.setVisibility(View.VISIBLE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        updateGroupName();
        break;
    }

  }

  @OnClick(R.id.layoutClassCard)
  public void ClassCard(){
  Intent intent=new Intent(this, FriendDetailActivity.class);
  intent.putExtra("id",LeanchatUser.getCurrentUser().getObjectId());
  intent.putExtra("conversationId",conversationId);
    intent.putExtra("remarkName",txtMemberName.getText().toString());
  startActivity(intent);
  }
  @Override
  protected void onResume() {
    super.onResume();
    initData();
  }

  private void initData() {
    btn_finish.setVisibility(View.GONE);
    imgUpdateName.setVisibility(View.VISIBLE);
    showSpinnerDialog();
    RequestParams params = new RequestParams();
    params.put("groupId", conversationId);
    params.put("userId", LeanchatUser.getCurrentUserId());

    String url = MyHttpClient.BASE_URL+MyHttpClient.MODEL_REALTIME+"group/detail";
    MyHttpClient.get(url, params, new MyJsonResponseHandler() {
      @Override
      public void handleSuccess(int code, String mesg, JSONObject data) {


        if (data.optString("avatar").length() > 0) {
          ImageLoader.getInstance().displayImage(data.optString("avatar"), avatarView);
        } else {
          avatarView.setImageResource(R.drawable.defaultgroup);
        }
        className=data.optString("name");
        editUpdateName.setText(className);
        classcode="班级群号："+data.optString("code");
        txtGroupCode.setText(classcode);
        type="班级群".equals(data.optString("type"))? 1:0;
        txtClassMember.setText(data.optString("memberCount") + "人");
        txtMemberName.setText(data.optString("myRemarkName"));
        if (LeanchatUser.getCurrentUserId().equals(data.optJSONObject("admin").optString("userId"))) {
          imgUpdateName.setVisibility(View.VISIBLE);
          avatarView.setOnClickListener(ConversationDetailActivity.this);
        } else {
          imgUpdateName.setVisibility(View.GONE);
        }

        dismissSpinnerDialog();
      }

      @Override
      public void handleSuccess(int code, String mesg) {
        dismissSpinnerDialog();
        toast(mesg);

      }

      @Override
      public void handleFailure(int error) {
        dismissSpinnerDialog();
        super.handleFailure(error);
      }
    });
  }


  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == Activity.RESULT_OK) {
      if (requestCode == IMAGE_PICK_REQUEST) {
        Uri uri = data.getData();
        startImageCrop(uri, 200, 200, CROP_REQUEST);
      } else if (requestCode == CROP_REQUEST) {
        final byte[]  pic = getImageByte(data);
        uploadGroupAvatar(pic);
      }
    }
  }

  public void startImageCrop(Uri uri, int outputX, int outputY,
                            int requestCode) {
    Intent intent = new Intent("com.android.camera.action.CROP");
    intent.setDataAndType(uri, "image/*");
    intent.putExtra("crop", "true");
    intent.putExtra("aspectX", 1);
    intent.putExtra("aspectY", 1);
    intent.putExtra("outputX", outputX);
    intent.putExtra("outputY", outputY);
    intent.putExtra("scale", true);
    String outputPath = PathUtils.getAvatarTmpPath();
    Uri outputUri = Uri.fromFile(new File(outputPath));
    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
    intent.putExtra("return-data", true);
    intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG);
    intent.putExtra("noFaceDetection", true); // face detection
    startActivityForResult(intent, requestCode);
  }

  private  byte[] getImageByte(Intent data) {
    Bundle extras = data.getExtras();
    Bitmap bitmap=null;
    byte[] result=null;

    if (extras != null) {
      bitmap = extras.getParcelable("data");
      if (bitmap != null) {
        bitmap = com.avoscloud.leanchatlib.utils.PhotoUtils.toRoundCorner(bitmap, 10);

//        avatarView.setImageBitmap(bitmap);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
        result = output.toByteArray();
        try {
           output.close();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    return result;
  }
  
  public void uploadGroupAvatar(final byte[] pic) {
    final AVFile file;
    try {
      showSpinnerDialog();
      file =new AVFile(editUpdateName.getText().toString()+".jpeg",pic);
      file.saveInBackground(new SaveCallback() {
        @Override
        public void done(AVException e) {
          picUrl = file.getUrl();
          if (picUrl!=null) {
            updateGroupPic();
          }
        }
      }, new ProgressCallback() {
        @Override
        public void done(Integer integer) {
          System.out.println("precentCode：" + integer);
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  //修改群图片
  private void updateGroupPic(){

    System.out.println("路径：" + picUrl);
    ImageLoader.getInstance().displayImage(picUrl, avatarView, PhotoUtils.groupImageOptions);
    ConversationManager.getInstance().updateAvatar(conversation,picUrl ,new AVIMConversationCallback(){
      @Override
      public void done( AVIMException e) {
        ImageLoader.getInstance().loadImage(picUrl,null);
        picUrl=null;
        dismissSpinnerDialog();

    }});

//    RequestParams params = new RequestParams();
//    params.put("groupId",conversationId);
//    params.put("avatar", picUrl);
//    String url= MyHttpClient.BASE_URL +  MyHttpClient.MODEL_REALTIME + "group";
//    MyHttpClient.put(url, params, new MyJsonResponseHandler() {
//      @Override
//      public void handleSuccess(int code, String mesg) {
//        dismissSpinnerDialog();
//        toast(mesg);
//        ImageLoader.getInstance().displayImage(picUrl, avatarView);
//        conversation.setAttribute("avatar",picUrl);
//
//
//        super.handleSuccess(code, mesg);
//      }
//
//      @Override
//      public void handleFailure(int error) {
//        dismissSpinnerDialog();
//        super.handleFailure(error);
//      }
//    });
//    picUrl=null;
  }
  //修改群名称
  private void updateGroupName(){

    newGroupName = editUpdateName.getText().toString();
    if (className.equals(newGroupName)){
      return;
    }
    if(TextUtils.isEmpty(newGroupName)){
      toast("群组名称不能为空");
      return;
    }
    showSpinnerDialog();
    ConversationManager.getInstance().updateName(conversation,newGroupName ,new AVIMConversationCallback(){
      @Override
      public void done( AVIMException e) {

        if (e!=null) {
          toast(e.getMessage());
          Log.e("updateConversationName",e.getMessage());
        }else {
          AVIMConversationCacheUtils.updateCacheConversation(conversation);
        }
        dismissSpinnerDialog();

      }});

//    RequestParams params = new RequestParams();
//    params.put("groupId",conversationId);
//    params.put("name",name );
//    String url= MyHttpClient.BASE_URL +  MyHttpClient.MODEL_REALTIME + "group";
//    MyHttpClient.put(url, params, new MyJsonResponseHandler() {
//      @Override
//      public void handleSuccess(int code, String mesg) {
//        dismissSpinnerDialog();
//        toast(mesg);
//        super.handleSuccess(code, mesg);
//      }
//
//      @Override
//      public void handleFailure(int error) {
//        dismissSpinnerDialog();
//        super.handleFailure(error);
//      }
//    });
  }

  @OnClick(R.id.layoutQuitClass)
  public void quitClass(){
    new AlertDialog.Builder(this).setMessage(R.string.conversation_quit_group_tip)
            .setPositiveButton(R.string.common_sure, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {

                showSpinnerDialog();
                RequestParams params = new RequestParams();
                params.put("groupId", conversationId);
                params.put("selfId", LeanchatUser.getCurrentUserId());

                String url = MyHttpClient.BASE_URL+MyHttpClient.MODEL_REALTIME+"members/quit";
                MyHttpClient.delete(url, params, new MyJsonResponseHandler() {

                  @Override
                  public void handleSuccess(int code, String mesg) {

                    dismissSpinnerDialog();
                    toast(mesg);

                    if (code==0){
                      RoomsTable roomsTable = ChatManager.getInstance().getRoomsTable();
                      roomsTable.deleteRoom(conversationId);
                      setResult(QUIT_GROUP_RESULT);
                      finish();
                    }

                  }

                  @Override
                  public void handleFailure(int error) {
                    dismissSpinnerDialog();
                    super.handleFailure(error);
                  }
                });

              }
            }).setNegativeButton(R.string.chat_common_cancel, null).show();

  }



}
