package com.gruden.education.activity.entry;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.gruden.education.R;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.customview.LocationPickerView;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class EntryInfoActivity extends EntryBaseActivity {

    @Bind(R.id.checkParent)
    CheckBox checkParent;

    @Bind(R.id.checkTeacher)
    CheckBox checkTeacher;

    EditText nicknameEdit;
    TextView locationText;
    Button btnEnsureInfo;
    LocationPickerView cityPicker;

    String provinceCode = "015";
    String cityCode = "015002";
    String strAreaCode = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_info_activity);

        initActionBar("完善资料");
        findView();

        cityPicker  = new LocationPickerView(EntryInfoActivity.this);
        cityPicker.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                //返回的分别是三个级别的选中位置
                strAreaCode = cityPicker.arrAreaCode.get(options3);
                locationText.setText("山东省 青岛市 "+cityPicker.options3Items.get(options1).get(option2).get(options3));
            }
        });

        locationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cityPicker!=null&&cityPicker.isOK) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

//                    if(imm.isActive()&&getCurrentFocus()!=null){
//                        if (getCurrentFocus().getWindowToken()!=null) {
//                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//                        }
//                    }
                    cityPicker.show();
                }
            }
        });

        btnEnsureInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePersonalInfo();
            }
        });
    }

    private void findView(){

        nicknameEdit = (EditText) findViewById(R.id.nicknameEdit);
        locationText = (TextView) findViewById(R.id.locationText);
        btnEnsureInfo = (Button) findViewById(R.id.btn_ensure_info);

    }


    private void updatePersonalInfo(){

        final String strNickName = nicknameEdit.getText().toString();
        if (TextUtils.isEmpty(strNickName)) {
            Utils.toast(this,"昵称不能为空");
            return;
        }

        if (strAreaCode==null){
            Utils.toast(this,"地区不能为空");
            return;
        }


        final List<String> arrRoles = new ArrayList<String>();
        if (checkParent.isChecked())
            arrRoles.add("1");
        if (checkTeacher.isChecked())
            arrRoles.add("2");

        if (arrRoles.size()==0) {
            Utils.toast(this, "身份不能为空");
            return;
        }


        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("userId",LeanchatUser.getCurrentUser().getObjectId());
        params.put("nickname", strNickName);
        params.put("provinceCode", provinceCode);
        params.put("cityCode", cityCode);
        params.put("areaCode", strAreaCode);
        params.put("position", locationText.getText().toString());

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "profile";
        url = AsyncHttpClient.getUrlWithQueryString(false, url, params);
        for (int i=0;i<arrRoles.size();i++){
            url = url +"&role=" +arrRoles.get(i);
        }

        MyHttpClient.put(url,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg) {

                switch (code){
                    case 0:{
                        int roleCode = 0;


                        List<String> arrRolesCode = new ArrayList<String>();
                        if (checkParent.isChecked())
                            arrRolesCode.add(LeanchatUser.PARENT);
                        if (checkTeacher.isChecked())
                            arrRolesCode.add(LeanchatUser.TEACHER);

                        LeanchatUser user = LeanchatUser.getCurrentUser();
                        user.setNickName(strNickName);
                        user.setRoles(arrRolesCode);
                        user.setPosition(locationText.getText().toString());
                        user.setProvince(provinceCode);
                        user.setCity(cityCode);
                        user.setArea(strAreaCode);
                        LeanchatUser.changeCurrentUser(user, true);
//                        MainActivity.goMainActivityFromActivity(EntryInfoActivity.this);
                        Intent intent = new Intent(EntryInfoActivity.this, EntryContactsMatchActivity.class);
                        startActivity(intent);
                        break;
                    }

                    default:{
                        toast(mesg);
                        break;
                    }
                }
                dismissSpinnerDialog();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }
}
