package com.gruden.education.activity.entry;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.gruden.education.R;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

public class EntryResetActivity extends EntryBaseActivity {
    String phoneNum;

    String smsCode;

    EditText newPasswordEdit;
    ImageButton eyeableButton;
    Button btnReset;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_reset);


        findView();
        initActionBar("重置密码");
        showLeftBackButton();

        btnReset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                reset();
            }
        });

        eyeableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                setEyeable();
            }
        });
        Intent intent =getIntent();
        phoneNum = intent.getStringExtra("phoneNum");
        smsCode = intent.getStringExtra("smsCode");
        if (smsCode==null)
            toast("验证码校验失败");
    }


    private void findView() {
        newPasswordEdit= (EditText) findViewById(R.id.newpasswordEdit);
        btnReset= (Button) findViewById(R.id.btnReset);
        eyeableButton= (ImageButton) findViewById(R.id.btnEyeable);
    }

    private void reset() {
        final String newpassword = newPasswordEdit.getText().toString();
        if (TextUtils.isEmpty(newpassword)) {
            Utils.toast(this, R.string.password_can_not_null);
            return;
        }
        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("username", phoneNum);
        params.put("password", newpassword);
        params.put("smsCode", smsCode);

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "forgetPwd";

        MyHttpClient.post(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {


                dismissSpinnerDialog();
                resetSuccess();


            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                resetSuccess();

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });

    }

    private void setEyeable() {
        if(eyeableButton.isSelected()){
            eyeableButton.setSelected(false);
            newPasswordEdit.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }else {
            eyeableButton.setSelected(true);
            newPasswordEdit.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
    }

    private void resetSuccess(){
        AlertDialog.Builder normalDia=new AlertDialog.Builder(EntryResetActivity.this);
        normalDia.setMessage("重置密码成功");

        normalDia.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(EntryResetActivity.this, EntryLoginActivity.class);
                startActivity(intent);
            }
        });

      normalDia.create().show();
    }
}
