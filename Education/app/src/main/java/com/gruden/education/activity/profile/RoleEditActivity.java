package com.gruden.education.activity.profile;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class RoleEditActivity extends BaseActivity {

    @Bind(R.id.checkParent)
    CheckBox checkParent;

    @Bind(R.id.checkTeacher)
    CheckBox checkTeacher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.role_edit_activity);
        initActionBar("完善资料");
        showLeftBackButton("取消");
        showRightTextButton("保存", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateRoles();
            }
        } );
    }


    @Override
    public void onResume() {
        super.onResume();
        LeanchatUser user = LeanchatUser.getCurrentUser();

        List<String> arrRolesCode = user.getRoles();
        if (arrRolesCode.contains(LeanchatUser.PARENT))
            checkParent.setChecked(true);
        if (arrRolesCode.contains(LeanchatUser.TEACHER))
            checkTeacher.setChecked(true);

    }

    void updateRoles(){


        final List<String> arrRoles = new ArrayList<String>();
        if (checkParent.isChecked())
            arrRoles.add("1");
        if (checkTeacher.isChecked())
            arrRoles.add("2");

        if (arrRoles.size()==0) {
            Utils.toast(this, "身份不能为空");
            return;
        }

        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("roles", arrRoles);

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "profile";
        MyHttpClient.put(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg) {

                dismissSpinnerDialog();
                switch (code){
                    case 0:{

                        LeanchatUser user = LeanchatUser.getCurrentUser();

                        List<String> arrRolesCode = new ArrayList<String>();
                        if (checkParent.isChecked())
                            arrRolesCode.add(LeanchatUser.PARENT);
                        if (checkTeacher.isChecked())
                            arrRolesCode.add(LeanchatUser.TEACHER);

                        user.setRoles(arrRolesCode);

                        LeanchatUser.changeCurrentUser(user, true);
                        onBackPressed();
                        break;
                    }

                    default:{
                        toast(mesg);
                        break;
                    }
                }
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });

    }
}
