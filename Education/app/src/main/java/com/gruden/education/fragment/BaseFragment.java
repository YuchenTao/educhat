package com.gruden.education.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.Toast;

import com.avoscloud.leanchatlib.model.Room;
import com.avoscloud.leanchatlib.utils.ConversationManager;
import com.gruden.education.R;
import com.gruden.education.adapter.ConversationListAdapter;
import com.gruden.education.customview.HeaderLayout;

public class BaseFragment extends Fragment {
  protected HeaderLayout headerLayout;
  protected Context ctx;
  protected ProgressDialog dialog;



  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    ctx = getActivity();
    headerLayout = (HeaderLayout) getView().findViewById(R.id.headerLayout);
  }

  protected void toast(String str) {
    Toast toast = Toast.makeText(this.getActivity(), str, Toast.LENGTH_LONG);
    toast.setGravity(Gravity.CENTER, 0, 0);
    toast.show();
  }

  protected void toast(int id) {
    Toast toast = Toast.makeText(this.getActivity(), id, Toast.LENGTH_LONG);
    toast.setGravity(Gravity.CENTER, 0, 0);
    toast.show();
  }

  protected boolean filterException(Exception e) {
    if (e != null) {
      Log.e("error",e.getMessage());
      return false;
    } else {
      return true;
    }
  }


  protected ProgressDialog showSpinnerDialog() {

    return showSpinnerDialog(null);

  }

  protected ProgressDialog showSpinnerDialog(String message) {

    if (dialog!=null&&dialog.isShowing())
      return dialog;

    if(dialog==null){
      dialog = new ProgressDialog(ctx);
      dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
      dialog.setCancelable(false);
    }

    if (message ==null)
        message = "加载中";
    dialog.setMessage(message);
    if (!getActivity().isFinishing()) {
      dialog.show();
    }
    return dialog;
  }

  protected void dismissSpinnerDialog() {
    if (dialog != null && dialog.isShowing())
      dialog.dismiss();
  }

  protected int dp2px(int dp) {
    return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
            getResources().getDisplayMetrics());
  }
}
