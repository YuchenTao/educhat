package com.gruden.education.activity.classmenu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by admin on 2016/3/3.
 */
public class AwardReasonActivity extends BaseActivity {


    Button btn_sendflower;
    GridView flowerlistGrid;
    FlowerListAdapter flowerListAdapter;

    private int curPage;
    private String conversationId;
    private ArrayList<String> childIds;
    private ArrayList<String> flowers=new ArrayList<String>();
    private  String flowerId;
    private  String flowerName;
    private boolean isRefreshing;
    private boolean isLoadMoreing;

    @Bind(R.id.flower_refresh)
    MaterialRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.award_reason_activity);
        initActionBar("奖励理由");
        showLeftBackButton();
        Intent i = getIntent();
        conversationId=i.getStringExtra("groupId");
        childIds=i.getStringArrayListExtra("childIds");

        showRightTextButton("添加红花", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AwardReasonActivity.this, AddFlowerActivity.class);
                intent.putExtra("groupId", conversationId);
                startActivityForResult(intent, 0);
            }
        });
        initView();


        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                initData(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                initData(curPage + 1);
            }


        });
        btn_sendflower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                distributeFlower();

            }
        });


//        flowerlistGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                //最后一个的item点击事件
//                if (position == flowerlistGrid.getChildCount() - 1) {
//                    Intent intent = new Intent(AwardReasonActivity.this, AddFlowerActivity.class);
//                    intent.putExtra("groupId", conversationId);
//                    startActivityForResult(intent, 0);
//
//                } else {
//                    if (view.findViewById(R.id.selectImg).getVisibility() == View.VISIBLE) {
//                        view.findViewById(R.id.selectImg).setVisibility(View.INVISIBLE);
//                    } else {
//                        view.findViewById(R.id.selectImg).setVisibility(View.VISIBLE);
//                    }
//
//                }
//            }
//        });

        flowerListAdapter=new FlowerListAdapter(this);
        flowerlistGrid.setAdapter(flowerListAdapter);

    }
    private void initView() {
        btn_sendflower= (Button) findViewById(R.id.btn_sendflower);
        flowerlistGrid= (GridView) findViewById(R.id.gridflowerlist);

    }

    @Override
    protected void onResume() {
        super.onResume();
        flowers.clear();
        refreshLayout.autoRefresh();
    }
    @Override
       protected void onActivityResult(int requestCode, int resultCode, Intent data) {
           super.onActivityResult(requestCode, resultCode, data);

      }



    /**
     * 初始化页面数据
     */
    private void initData(final int page) {
        RequestParams params = new RequestParams();
        params.put("groupId", conversationId);
        params.put("curPage", page);


        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"flower/list";
        MyHttpClient.get(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {
                if (array.length() == 0 && page > 0) {
//                    toast("已经没有更多红花~");
                } else {
                    ArrayList<FlowerInfo> flowerList = new ArrayList<FlowerInfo>();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject data = array.optJSONObject(i);
                        FlowerInfo flowerInfo = new FlowerInfo();
                        flowerInfo.id = data.optString("id");
                        flowerInfo.name = data.optString("name");
                        flowerInfo.isSelected=false;
                        flowerList.add(flowerInfo);
                    }

                    if (page == 0) {
                        flowerListAdapter.setDataList(flowerList);
                    } else {
                        flowerListAdapter.addDataList(flowerList);
                    }

                    flowerListAdapter.notifyDataSetChanged();
                    curPage = page;
                }
                dissmissCircle();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }
        });
    }
    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }


    /**
     * 点击发送红花的方法
     */
    private void distributeFlower() {
        flowerSelect();
        if (flowers.size()==0){
            toast("您还没有选择红花~");
            return ;
        }
        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("groupId", conversationId);
        params.put("childId",childIds);
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("flower",flowers);
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"flower/distribute";
        MyHttpClient.post(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
                Intent intent = new Intent(AwardReasonActivity.this, RedListActivity.class);
                intent.putExtra("id", conversationId);
                startActivity(intent);
                finish();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);

            }

        });

    }

    /**
     * 遍历选中的红花的方法
     */
    private void flowerSelect(){
        flowers.clear();
        for (int i=0;i<flowerListAdapter.getDataList().size();i++){
            FlowerInfo flowerInfo=flowerListAdapter.getItem(i);
            if(flowerInfo.isSelected){
                String flower=flowerInfo.id+"::"+flowerInfo.name;
                flowers.add(flower);
            }
        }
    }

    /**
     * 显示红花的自定义Adapter
     */
class FlowerListAdapter extends BaseAdapter {
    Context context;



    public FlowerListAdapter(Context context) {
        this.context = context;


    }
        public List<FlowerInfo> dataList = new ArrayList<FlowerInfo>();

        public List<FlowerInfo> getDataList() {
            return dataList;
        }

        public void setDataList(List<FlowerInfo> datas) {
            dataList.clear();
            if (null != datas) {
                dataList.addAll(datas);
            }
        }
        public void addDataList(List<FlowerInfo> datas) {
            dataList.addAll( datas);
        }


    @Override
    public int getCount() {
        return dataList.size()+1;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView= View.inflate(context,R.layout.distribute_student_item,null);
        final ImageView flowerImg= (ImageView) convertView.findViewById(R.id.avatar);
        final ImageView selectImg= (ImageView) convertView.findViewById(R.id.selectImg);
        flowerImg.setImageResource(R.drawable.honghua_liyou);
        TextView flowername= (TextView) convertView.findViewById(R.id.username);
        selectImg.setVisibility(View.GONE);

        if(position == getCount() - 1){
            flowerImg.setImageResource(R.drawable.add_pic);
            flowername.setText("添加红花");
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AddFlowerActivity.class);
                    intent.putExtra("groupId", conversationId);
                    startActivityForResult(intent, 0);
                }
            });
        }else {
            final FlowerInfo flowerInfo = dataList.get(position);
                flowername.setText(flowerInfo.name);
                flowername.setTag(flowerInfo.id);
            if (flowerInfo.isSelected){
                selectImg.setVisibility(View.VISIBLE);
            }else{
                selectImg.setVisibility(View.GONE);
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(flowerInfo.isSelected){
                        selectImg.setVisibility(View.GONE);
                        flowerInfo.isSelected=false;
                    }else{
                        selectImg.setVisibility(View.VISIBLE);
                        flowerInfo.isSelected=true;
                    }
                }
            });

        }
        return convertView;
    }
    @Override
    public FlowerInfo getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}
    class FlowerInfo{
        String id;
        String name;
        boolean isSelected;
    }

}
