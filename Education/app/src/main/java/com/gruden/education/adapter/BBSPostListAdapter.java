package com.gruden.education.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.gruden.education.R;
import com.gruden.education.activity.bbs.PostDetailActivity;
import com.gruden.education.event.BBSPostItemClickEvent;
import com.gruden.education.model.BBSPost;
import com.gruden.education.viewholder.BBSPostItemHolder;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by yunjiansun on 15/12/31.
 */
public class BBSPostListAdapter extends BaseAdapter {

    LayoutInflater mInflater;

    PrettyTime prettyTime;

    Context context;
    
    public BBSPostListAdapter(Context context){
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        prettyTime = new PrettyTime();
    }

    public List<BBSPost> dataList = new ArrayList<BBSPost>();

    public List<BBSPost> getDataList() {
        return dataList;
    }

    public void setDataList(List<BBSPost> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }


    public void addDataList(List<BBSPost> datas) {
        dataList.addAll( datas);
    }

    

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public BBSPost getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final BBSPost post = dataList.get(position);
        BBSPostItemHolder holder;
        if (convertView == null) {

            holder = new BBSPostItemHolder();

            convertView = mInflater.inflate(R.layout.bbs_post_item, parent, false);
            holder.txtTitle = (TextView)convertView.findViewById(R.id.txtTitle);
            holder.txtUserName = (TextView)convertView.findViewById(R.id.txtUserName);
            holder.txtReplyCount = (TextView)convertView.findViewById(R.id.txtReplyCount);
            holder.txtTime = (TextView)convertView.findViewById(R.id.txtTime);
            holder.imgAvatar = (ImageView) convertView.findViewById(R.id.imgAvatar);
            holder.btnComment = (ImageButton) convertView.findViewById(R.id.btnComment);

            convertView.setTag(holder);
        } else {
            holder = (BBSPostItemHolder) convertView.getTag();
        }

        ImageLoader.getInstance().displayImage(post.urlAvatar, holder.imgAvatar, PhotoUtils.avatarImageOptions);
        holder.txtTitle.setText(post.title);
        holder.txtUserName.setText(post.nickname);
        holder.txtReplyCount.setText(post.replyCount+"");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createdAt = format.parse(post.time,new ParsePosition(0));
        holder.txtTime.setText(this.prettyTime.format(createdAt));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PostDetailActivity.class);
                intent.putExtra("id", post.postId);
                context.startActivity(intent);
            }
        });
        
        return convertView;
    }

    
}
