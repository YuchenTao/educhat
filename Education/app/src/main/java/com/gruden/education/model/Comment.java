package com.gruden.education.model;

import org.json.JSONObject;

/**
 * Created by yunjiansun on 16/2/5.
 */
public class Comment {
    public String id;
    public String userId;
    public String avatarUrl;
    public String nickname;
    public JSONObject replyTo;
    public String content;
    public String createdAt;
    public int replyCount;
}
