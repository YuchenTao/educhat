package com.gruden.education.activity.entry;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCreatedCallback;
import com.avos.avoscloud.im.v2.messages.AVIMTextMessage;
import com.gruden.education.R;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;

import java.util.Arrays;

public class EntryRegisterActivity extends EntryBaseActivity {
  Button getSmsButton;
  ImageButton eyeableButton;
  EditText phoneNumEdit, passwordEdit;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    // TODO Auto-generated method stub
    super.onCreate(savedInstanceState);


    setContentView(R.layout.entry_register_activity);

    findView();
    initActionBar("新用户注册");
    showLeftBackButton();

    getSmsButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View arg0) {
        // TODO Auto-generated method stub
        getSms();
      }
    });

    eyeableButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View arg0) {
        // TODO Auto-generated method stub
        setEyeable();
      }
    });



  }

  private  void  testchat1(){
    // Tom 用自己的名字作为clientId，获取AVIMClient对象实例
    AVIMClient tom = AVIMClient.getInstance("55efca6860b2b52c4e3f07d9");
    // 与服务器连接
    tom.open(new AVIMClientCallback() {
      @Override
      public void done(AVIMClient client, AVIMException e) {
        if (e == null) {
          // 创建与 Jerry，Bob,Harry,William 之间的对话
          client.createConversation(Arrays.asList("564c27d460b2ed362068d60d", "564bdf7460b2260e5babcec2", "563c3be960b27cc278130c73", "5649cdcb60b259caed2f657b"), "Tom & Jerry & friedns", null,
                  new AVIMConversationCreatedCallback() {

                    @Override
                    public void done(AVIMConversation conversation, AVIMException e) {
                      if (e == null) {
                        Log.d("ConversationId",conversation.getConversationId().toString());
                        AVIMTextMessage msg = new AVIMTextMessage();
                        msg.setText("你们在哪儿？");
                        // 发送消息
                        conversation.sendMessage(msg, new AVIMConversationCallback() {

                          @Override
                          public void done(AVIMException e) {
                            if (e == null) {
                              Log.d("Tom & Jerry", "发送成功！");
                            }
                          }
                        });
                      }
                    }
                  });
        }
      }
    });

  }



  private void findView() {
    phoneNumEdit = (EditText) findViewById(R.id.phoneNumEdit);
    passwordEdit = (EditText) findViewById(R.id.passwordEdit);
    getSmsButton = (Button) findViewById(R.id.btnGetSms);
    eyeableButton = (ImageButton) findViewById(R.id.btnEyeable);
  }

  private void getSms() {
    final String phoneNum = phoneNumEdit.getText().toString();
    final String password = passwordEdit.getText().toString();
    if (TextUtils.isEmpty(phoneNum)) {
      Utils.toast(this,"手机号不能为空");
      return;
    }

    if (TextUtils.isEmpty(password)) {
      Utils.toast(this,R.string.password_can_not_null);
      return;
    }

    RequestParams params = new RequestParams();
    params.put("type", "0");
    params.put("mobilePhoneNumber", phoneNum);

    String url = MyHttpClient.BASE_URL + "sms/sendSms";
    showSpinnerDialog();

    MyHttpClient.post(url, params, new MyJsonResponseHandler() {

      @Override
      public void handleSuccess(int code, String mesg, JSONArray array){

        dismissSpinnerDialog();
        Intent intent = new Intent(EntryRegisterActivity.this, EntrySmsActivity.class);
        intent.putExtra("PhoneNum", phoneNum);
        intent.putExtra("Password", password);
        if (array.length()>0)
          intent.putExtra("SmsCode", array.optString(0));
        startActivity(intent);
      }

      @Override
      public void handleSuccess(int code, String mesg) {

        dismissSpinnerDialog();

        toast(mesg);
      }

      @Override
      public void handleFailure(int error) {
        dismissSpinnerDialog();
        super.handleFailure(error);
      }
    });

  }

  private void setEyeable(){
    if(eyeableButton.isSelected()){
      eyeableButton.setSelected(false);
      passwordEdit.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }else {
      eyeableButton.setSelected(true);
      passwordEdit.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
    }
  }
}
