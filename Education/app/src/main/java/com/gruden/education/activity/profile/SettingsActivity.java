package com.gruden.education.activity.profile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageButton;

import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.avoscloud.leanchatlib.controller.ChatManager;
import com.gruden.education.App;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.activity.entry.EntryLoginActivity;
import com.gruden.education.download.DownloadInfos;
import com.gruden.education.download.DownloadManager;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.service.PushManager;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.xutils.ex.DbException;

import java.io.File;

import butterknife.Bind;
import butterknife.OnClick;

public class SettingsActivity extends BaseActivity {

    ChatManager chatManager;
    @Bind(R.id.btnContactsSwitch)
    ImageButton btnContactsSwitch;
    Context context;
    LeanchatUser user=LeanchatUser.getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initActionBar("设置");
        showLeftBackButton();
        chatManager = ChatManager.getInstance();
        btnContactsSwitch.setSelected(user.getMatching());
    }

    public void onContactsSwitch(View v) {
        btnContactsSwitch.setSelected(btnContactsSwitch.isSelected() ? false : true);
        user.setMatching(btnContactsSwitch.isSelected());
        LeanchatUser.changeCurrentUser(user, true);

    }

    public void onLogoutClick(View v) {
//        EventBus.getDefault().post(new LogoutEvent());
        App.ctx.mainActivity.finish();
        chatManager.closeWithCallback(new AVIMClientCallback() {
            @Override
            public void done(AVIMClient avimClient, AVIMException e) {
            }
        });
        PushManager.getInstance().unsubscribeCurrentUserChannel();
        LeanchatUser.logOut();

        Intent intent = new Intent(this, EntryLoginActivity.class);
        startActivity(intent);
        finish();
    }
    @OnClick(R.id.layoutAbout)
    public void aboutOnclick(){
        Intent intent=new Intent(this,AboutActivity.class);
        startActivity(intent);

    }
    @OnClick(R.id.layoutClearCache)
    public void clearCacheOnclick(){
        AlertDialog.Builder normalDia=new AlertDialog.Builder(SettingsActivity.this);
        normalDia.setMessage("确定要清除缓存吗？");
        normalDia.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                System.out.println("清除缓存路径：" + App.ctx.getCacheDir().getAbsolutePath());
                cleanCache();
            }
        });
        normalDia.setNegativeButton("取消", null);
        normalDia.create().show();
    }

    public void cleanCache(){

        ImageLoader.getInstance().clearMemoryCache();
        ImageLoader.getInstance().clearDiskCache();
        cleanApplicationData(App.ctx);
        try {
            DownloadManager.getInstance(null).db.dropTable(DownloadInfos.class);
        } catch (DbException e) {
            e.printStackTrace();
        }

        toast("清除缓存成功！");
    }
    /** * 清除本应用内部缓存(/data/data/com.xxx.xxx/cache) * * @param context */
    public static void cleanInternalCache(Context context) {
            deleteFilesByDirectory(context.getCacheDir());
    }
    /**
     * * 清除本应用SharedPreference(/data/data/com.xxx.xxx/shared_prefs) * * @param
     * context
     */
    public static void cleanSharedPreference(Context context) {
        deleteFilesByDirectory(new File("/data/data/"
                + context.getPackageName() + "/shared_prefs"));
    }

    /** * 清除/data/data/com.xxx.xxx/files下的内容 * * @param context */
    public static void cleanFiles(Context context) {
        deleteFilesByDirectory(context.getFilesDir());
    }

    /**
     * * 清除外部cache下的内容(/mnt/sdcard/android/data/com.xxx.xxx/cache) * * @param
     * context
     */
    public static void cleanExternalCache(Context context) {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            deleteFilesByDirectory(context.getExternalCacheDir());
        }
    }

    /** * 清除自定义路径下的文件，使用需小心，请不要误删。而且只支持目录下的文件删除 * * @param filePath */
    public static void cleanCustomCache(String filePath) {
        deleteFilesByDirectory(new File(filePath));
    }

    /** * 清除本应用所有的数据 * * @param context * @param filepath */
    public static void cleanApplicationData(Context context, String... filepath) {
        cleanInternalCache(context);
        cleanExternalCache(context);
        cleanSharedPreference(context);
        cleanFiles(context);
        for (String filePath : filepath) {
            cleanCustomCache(filePath);
        }
    }

    /** * 删除方法 这里只会删除某个文件夹下的文件，如果传入的directory是个文件，将不做处理 * * @param directory */
    private static void deleteFilesByDirectory(File directory) {
        if (directory != null && directory.exists() && directory.isDirectory()) {
            for (File item : directory.listFiles()) {
                item.delete();
            }
        }
    }


}
