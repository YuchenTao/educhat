package com.gruden.education.activity.profile;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class NickNameEditActivity extends BaseActivity {

    @Bind(R.id.editText)
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_name_edit);
        initActionBar("完善资料");
        showLeftBackButton("取消");
        showRightTextButton("保存", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserName();
            }
        } );
    }

    void updateUserName(){
        final String strNickName = editText.getText().toString();
        if (TextUtils.isEmpty(strNickName)) {
            Utils.toast(this,"用户名不能为空");
            return;
        }

        showSpinnerDialog();


        LeanchatUser user = LeanchatUser.getCurrentUser();
        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("nickname", strNickName);

        List<String> arrRolesCode = user.getRoles();
        List<String> arrRoles = new ArrayList<String>();
        if (arrRolesCode.contains(LeanchatUser.PARENT))
            arrRoles.add("1");
        if (arrRolesCode.contains(LeanchatUser.TEACHER))
            arrRoles.add("2");
        params.put("roles", arrRoles);

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "profile";
        MyHttpClient.put(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg) {

                dismissSpinnerDialog();
                switch (code){
                    case 0:{
                        toast(mesg);
                        LeanchatUser user = LeanchatUser.getCurrentUser();
                        user.put("nickname",strNickName);
                        LeanchatUser.changeCurrentUser(user, true);
                        onBackPressed();
                        break;
                    }

                    default:{
                        toast(mesg);
                        break;
                    }
                }
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });

    }
}
