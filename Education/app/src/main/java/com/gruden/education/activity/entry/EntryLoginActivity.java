package com.gruden.education.activity.entry;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.gruden.education.R;
import com.gruden.education.activity.MainActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Child;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;


public class EntryLoginActivity extends EntryBaseActivity {

  @Bind(R.id.activity_login_et_username)
  public EditText userNameView;

  @Bind(R.id.activity_login_et_password)
  public EditText passwordView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    // TODO Auto-generated method stub
    super.onCreate(savedInstanceState);
    setContentView(R.layout.entry_login_activity);
  }

  @OnClick(R.id.activity_login_btn_login)
  public void onLoginClick(View v) {
      login();
  }

  @OnClick(R.id.activity_login_btn_register)
  public void onRegisterClick(View v) {
    Intent intent = new Intent(this, EntryRegisterActivity.class);
    startActivity(intent);
  }

  @OnClick(R.id.btn_forget)
  public void onForgetClick(View v) {
    Intent intent = new Intent(this, EntryForgetActivity.class);
    startActivity(intent);
  }

  private void login() {
    final String name = userNameView.getText().toString().trim();
    final String password = passwordView.getText().toString().trim();

    if (TextUtils.isEmpty(name)) {
      Utils.toast(R.string.username_cannot_null);
      return;
    }

    if (TextUtils.isEmpty(password)) {
      Utils.toast(R.string.password_can_not_null);
      return;
    }

    showSpinnerDialog();

    RequestParams params = new RequestParams();
    params.put("username",name);
    params.put("password", password);

    String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "login";

    MyHttpClient.get(url,params,new MyJsonResponseHandler(){

      @Override
      public void handleSuccess(int code, String mesg, JSONObject data) {

        dismissSpinnerDialog();

        int length = data.optJSONArray("roles").length();
        List<String> arrRolesCode = new ArrayList<String>();

        for (int i=0; i<length; i++){
          arrRolesCode.add(data.optJSONArray("roles").optJSONObject(i).optString("code"));
        }


        if (!(arrRolesCode.contains(LeanchatUser.PARENT)||arrRolesCode.contains(LeanchatUser.TEACHER))){
          toast("抱歉~手机客户端目前只开放给家长和老师使用");
          return;
        }

        List<Child> arrChildren = new ArrayList<Child>();
        for (int i=0;i<data.optJSONArray("children").length();i++){
          JSONObject object = data.optJSONArray("children").optJSONObject(i);
          Child child = new Child();
          child.groupId = object.optString("groupId");
          child.childId = object.optString("childId");
          child.studentCode = object.optString("studentCode");
          child.name = object.optString("name");
          arrChildren.add(child);
        }

        LeanchatUser user = new LeanchatUser();
        user.setObjectId(data.optString("userId"));
        user.setUsername(data.optString("username"));
        user.setMyPassword(password);
        user.setNickName(data.optString("nickname"));
        user.setGender(data.optInt("gender",0));
        user.setAvatarUrl(data.optString("avatar"));
        user.setSignture(data.optString("signature"));
        user.setMobilePhoneNumber(data.optString("mobilePhoneNumber"));
        user.setRoles(arrRolesCode);
        user.setPosition(data.optString("position"));
        user.setProvince(data.optString("provinceCode"));
        user.setCity(data.optString("cityCode"));
        user.setArea(data.optString("areaCode"));
        user.setChildren(arrChildren);
        LeanchatUser.changeCurrentUser(user, true);


        MainActivity.goMainActivityFromActivity(EntryLoginActivity.this);

      }

      @Override
      public void handleSuccess(int code, String mesg) {
        dismissSpinnerDialog();
        toast(mesg);
      }

      @Override
      public void handleFailure(int error) {
        dismissSpinnerDialog();
        super.handleFailure(error);
      }

    });


//    LeanchatUser.logInInBackground(name, password, new LogInCallback<LeanchatUser>() {
//      @Override
//      public void done(LeanchatUser avUser, AVException e) {
//        dialog.dismiss();
//        if (filterException(e)) {
//          MainActivity.goMainActivityFromActivity(EntryLoginActivity.this);
//        }
//      }
//    }, LeanchatUser.class);
  }
}
