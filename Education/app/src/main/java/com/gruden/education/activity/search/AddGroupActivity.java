package com.gruden.education.activity.search;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import butterknife.Bind;

/**
 * Created by admin on 2016/3/30.
 */
public class AddGroupActivity extends BaseActivity{
    @Bind(R.id.remarkNameEdit)
    EditText remarkNameEdit;

    @Bind(R.id.btn_register)
    Button btn_register;

    @Bind(R.id.status_radiogroup)
    RadioGroup status_radiogroup;

    @Bind(R.id.radioTeacher)
    RadioButton radioTeacher;

    @Bind(R.id.radioParent)
    RadioButton radioParent;

    String title;
    String groupId;
    int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);

        Intent intent = getIntent();
        initActionBar(intent.getStringExtra("title"));
        showLeftBackButton();
        remarkNameEdit.setHint(intent.getStringExtra("hint"));
        type=intent.getIntExtra("type",0);
        groupId=intent.getStringExtra("groupId");
        if(type==0){
            status_radiogroup.setVisibility(View.GONE);
        }

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applicationEntry();
            }
        });
    }
    private void applicationEntry(){
        if(remarkNameEdit.getText().toString()==null||remarkNameEdit.getText().toString().length()==0){
            toast("请输入备注名~");
            return;
        }
        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("groupId", groupId);
        params.put("type",type);
        params.put("userId", LeanchatUser.getCurrentUserId());
        params.put("remarkName",remarkNameEdit.getText().toString());
        if (type==1){
            params.put("identity",radioTeacher.isChecked()?1:2);
        }
        String url = MyHttpClient.BASE_URL+MyHttpClient.MODEL_REALTIME + "members/apply";
        MyHttpClient.post(url, params, new MyJsonResponseHandler() {


            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
                finish();

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }
        });
    }


}
