package com.gruden.education.model;

/**
 * Created by yunjiansun on 15/12/30.
 */
public class BBSPost {
    public String postId;
    public String userId;
    public String title;
    public String nickname;
    public int replyCount;
    public String time;
    public String urlAvatar;
}
