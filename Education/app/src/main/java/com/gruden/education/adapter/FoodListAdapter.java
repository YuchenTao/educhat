package com.gruden.education.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.gruden.education.R;
import com.gruden.education.activity.classmenu.FoodDetailActivity;
import com.gruden.education.model.Blog;
import com.gruden.education.viewholder.BlogItemHolder;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/2/18.
 */
public class FoodListAdapter extends BaseAdapter {
    LayoutInflater mInflater;
    Context context;

    public FoodListAdapter(Context context){
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);

    }

    public List<Blog> dataList = new ArrayList<Blog>();

    public List<Blog> getDataList() {
        return dataList;
    }

    public void setDataList(List<Blog> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }


    public void addDataList(List<Blog> datas) {
        dataList.addAll( datas);
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Blog getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Blog blog = dataList.get(position);
        BlogItemHolder holder;
        if (convertView == null) {

            holder = new BlogItemHolder();
            convertView = mInflater.inflate(R.layout.blog_item, parent, false);
            holder.dateLayout = convertView.findViewById(R.id.dateLayout);
            holder.blogLayout = convertView.findViewById(R.id.blogLayout);
            holder.lineBottom = convertView.findViewById(R.id.line_bottom);
            holder.txtDate = (TextView)convertView.findViewById(R.id.txtDate);
            holder.txtTitle = (TextView)convertView.findViewById(R.id.txtTitle);
            holder.txtCreateAt = (TextView)convertView.findViewById(R.id.txtCreateAt);
            holder.txtContent= (TextView)convertView.findViewById(R.id.txtContent);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(holder);

        } else {
            holder = (BlogItemHolder) convertView.getTag();
        }

        if (blog.id==null){
            holder.dateLayout.setVisibility(View.VISIBLE);
            holder.blogLayout.setVisibility(View.GONE);

            holder.txtDate.setText(blog.date);

            convertView.setOnClickListener(null);
        }else {
            holder.dateLayout.setVisibility(View.GONE);
            holder.blogLayout.setVisibility(View.VISIBLE);
            holder.lineBottom.setVisibility(blog.isBottom?View.GONE:View.VISIBLE);

            holder.txtTitle.setText(blog.title);
            holder.txtCreateAt.setText(blog.createAt);
            holder.txtContent.setText(blog.content);

            if (blog.imgUrl!=null&&blog.imgUrl.startsWith("http")) {
                holder.imageView.setVisibility(View.VISIBLE);
                ImageLoader.getInstance().displayImage(blog.imgUrl, holder.imageView, PhotoUtils.normalImageOptions);
            }
            else
                holder.imageView.setVisibility(View.INVISIBLE);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, FoodDetailActivity.class);
                    intent.putExtra("id", blog.id);
                    context.startActivity(intent);
                }
            });

        }
        return convertView;
    }
}
