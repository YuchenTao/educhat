package com.gruden.education.viewholder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.callback.AVIMSingleMessageQueryCallback;
import com.gruden.education.R;
import com.avoscloud.leanchatlib.controller.ConversationHelper;
import com.gruden.education.event.ClassDetailItemClickEvent;
import com.gruden.education.event.ClassItemClickEvent;
import com.gruden.education.event.MessageItemClickEvent;
import com.avoscloud.leanchatlib.model.ConversationType;
import com.avoscloud.leanchatlib.model.Room;
import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.avoscloud.leanchatlib.utils.ThirdPartUserUtils;
import com.avoscloud.leanchatlib.utils.Utils;
import com.avoscloud.leanchatlib.viewholder.CommonViewHolder;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.greenrobot.event.EventBus;

/**
 * Created by wli on 15/10/8.
 */
public class ConversationItemHolder extends CommonViewHolder {

  ImageView avatarView;
  TextView unreadView;
  TextView messageView;
  TextView timeView;
  TextView nameView;

  public ConversationItemHolder(ViewGroup root) {
    super(root.getContext(), root, R.layout.conversation_item);
    initView();
  }

  public void initView() {
    avatarView = (ImageView)itemView.findViewById(R.id.iv_recent_avatar);
    nameView = (TextView)itemView.findViewById(R.id.recent_name_text);
    timeView = (TextView)itemView.findViewById(R.id.recent_time_text);
    unreadView = (TextView)itemView.findViewById(R.id.recent_unread);
    messageView = (TextView)itemView.findViewById(R.id.recent_msg_text);
  }

  @Override
  public void bindData(Object o) {
    final Room room = (Room) o;
    final AVIMConversation conversation = room.getConversation();
    if (null != conversation) {

      final String conversationName = ConversationHelper.nameOfConversation(conversation);
      nameView.setText(conversation.getName());

      String avatar = null;
      if (conversation.getMembers().size()==2) {
        String userId = ConversationHelper.otherIdOfConversation(conversation);
        avatar = ThirdPartUserUtils.getInstance().getUserAvatar(userId);
        ImageLoader.getInstance().displayImage(avatar, avatarView, PhotoUtils.avatarImageOptions);

      } else {

        if (conversation.getAttribute("avatar")!=null) {
          avatar = conversation.getAttribute("avatar").toString();
        }
        System.out.println(conversation.getName() + ":" + avatar);
        ImageLoader.getInstance().displayImage(avatar, avatarView, PhotoUtils.groupImageOptions);
      }

      int num = room.getUnreadCount();
      unreadView.setText(num + "");
      unreadView.setVisibility(num > 0 ? View.VISIBLE : View.GONE);

      conversation.getLastMessage(new AVIMSingleMessageQueryCallback() {
        @Override
        public void done(AVIMMessage avimMessage, AVIMException e) {
          if (null != avimMessage) {
            Date date = new Date(avimMessage.getTimestamp());
            SimpleDateFormat format = new SimpleDateFormat("MM-dd HH:mm");
            timeView.setText(format.format(date));
            messageView.setText(Utils.getMessageeShorthand(getContext(), avimMessage));
          } else {
            timeView.setText("");
            messageView.setText("");
          }
        }
      });

      switch (room.itemType){

        case 0: {
          itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              EventBus.getDefault().post(new MessageItemClickEvent(room.getConversationId()));
            }
          });
          break;
        }

        case 1:{
          itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              EventBus.getDefault().post(new ClassItemClickEvent(room.getConversationId(),conversationName));
            }
          });
          break;
        }

        case 2:{
          itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              EventBus.getDefault().post(new ClassDetailItemClickEvent(room.getConversationId()));
            }
          });
          break;
        }
      }

    }
  }

  public static ViewHolderCreator HOLDER_CREATOR = new ViewHolderCreator<ConversationItemHolder>() {
    @Override
    public ConversationItemHolder createByViewGroupAndType(ViewGroup parent, int viewType) {
      return new ConversationItemHolder(parent);
    }
  };
}
