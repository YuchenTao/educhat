package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.ScheduleListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Subject;
import com.loopj.android.http.RequestParams;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.Bind;

public class ScheduleActivity extends BaseActivity implements OnDateSelectedListener, OnMonthChangedListener {

    @Bind(R.id.listView)
    ListView listView;

    private String conversationId;
    private int dayIndex;
    private ScheduleListAdapter listAdapter;
    private View headerView;
    private View layoutAM;
    private View layoutPM;
    private MaterialCalendarView calendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        initActionBar("课程表");
        showLeftBackButton();

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");
        listAdapter = new ScheduleListAdapter(this);

        headerView =  LayoutInflater.from(this).inflate(R.layout.schedule_header_layout, listView, false);
        calendarView = (MaterialCalendarView) headerView.findViewById(R.id.calendarView);
        layoutAM = headerView.findViewById(R.id.layoutAM);
        layoutPM = headerView.findViewById(R.id.layoutPM);

        listView.addHeaderView(headerView);
        listView.setAdapter(listAdapter);

        calendarView.setDynamicHeightEnabled(true);
        calendarView.setOnDateChangedListener(this);
        calendarView.setOnMonthChangedListener(this);

        Calendar calendar = Calendar.getInstance();
        calendarView.setSelectedDate(calendar.getTime());
        dayIndex = calendar.get(Calendar.DAY_OF_WEEK)-1;

        layoutAM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutAM.isSelected()){
                    return;
                }else {
                    layoutAM.setSelected(true);
                    layoutPM.setSelected(false);
                    getSchedule();
                }

            }
        });

        layoutPM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutPM.isSelected()){
                    return;
                }else {
                    layoutPM.setSelected(true);
                    layoutAM.setSelected(false);
                    getSchedule();
                }

            }
        });

        layoutAM.performClick();
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @Nullable CalendarDay date, boolean selected) {
        dayIndex = date.getCalendar().get(Calendar.DAY_OF_WEEK)-1;
        getSchedule();
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
    }

    void getSchedule(){

        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("groupId",conversationId);
        params.put("dayIndex",dayIndex);
        params.put("isAm",(layoutAM.isSelected()?1:0));

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"schedule";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0){

                    toast("哟~好像没有课哦");

                }else {
                    ArrayList<Subject> subjectList = new ArrayList<Subject>();
                    for (int i=0; i<array.length(); i++){
                        JSONObject obj = array.optJSONObject(i);
                        Subject s = new Subject();
                        s.name = obj.optString("subject");
                        s.period = obj.optString("period");
                        subjectList.add(s);
                    }

                    listAdapter.setDataList(subjectList);
                    listAdapter.notifyDataSetChanged();
                }

                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }


}
