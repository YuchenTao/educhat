package com.gruden.education.event;

/**
 * Created by wli on 15/10/9.
 * 因为 RecyclerView 没有 onItemClickListener，所以添加此事件
 */
public class MessageItemClickEvent {
  public String conversationId;
  public MessageItemClickEvent(String conversationId) {
    this.conversationId = conversationId;
  }
}
