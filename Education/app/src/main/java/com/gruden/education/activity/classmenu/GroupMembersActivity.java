package com.gruden.education.activity.classmenu;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;

import com.avoscloud.leanchatlib.utils.Constants;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.activity.discover.contact.CharacterParser;
import com.gruden.education.activity.discover.contact.Contact;
import com.gruden.education.activity.discover.contact.SortToken;
import com.gruden.education.adapter.GroupMembersAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.customview.swipemenulistview.SwipeMenu;
import com.gruden.education.customview.swipemenulistview.SwipeMenuCreator;
import com.gruden.education.customview.swipemenulistview.SwipeMenuItem;
import com.gruden.education.customview.swipemenulistview.SwipeMenuListView;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;

public class GroupMembersActivity extends BaseActivity {

    @Bind(R.id.searchView)
    SearchView searchView;

    @Bind(R.id.lv_contacts)
    SwipeMenuListView mListView;

    @Bind(R.id.btnBackground)
    Button btnBackground;

    private List<Contact> listMembers= new ArrayList<Contact>();
    private GroupMembersAdapter adapter;
    private String conversationId;
    private boolean isCreator;

    /**
     * 汉字转换成拼音的类
     */
    private CharacterParser characterParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_members);

        initActionBar("群成员");
        showLeftBackButton();

        conversationId = getIntent().getStringExtra(Constants.CONVERSATION_ID);
        characterParser = CharacterParser.getInstance();

        adapter = new GroupMembersAdapter(this);
        adapter.conversationId = conversationId;

        initSearchView();
        getMembers();

    }

    private void initSearchView(){

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText.length() > 0) {
                    ArrayList<Contact> fileterList = (ArrayList<Contact>) search(newText);
                    adapter.updateListView(fileterList);
                } else {
                    adapter.updateListView(listMembers);
                }
                mListView.setSelection(0);
                return true;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    btnBackground.setVisibility(View.VISIBLE);
                }
            }
        });

    }
    private void initListView(){

        /** 给ListView设置adapter **/
        mListView.setAdapter(adapter);

        if (isCreator){
            // step 1. create a MenuCreator
            SwipeMenuCreator creator = new SwipeMenuCreator() {

                @Override
                public void create(SwipeMenu menu) {
                    if (menu.getViewType()==1){
                        // create "open" item
                        SwipeMenuItem openItem = new SwipeMenuItem(GroupMembersActivity.this);
                        // set item background
                        openItem.setBackground(new ColorDrawable(Color.RED));
                        // set item width
                        openItem.setWidth(dp2px(70));
                        // set item title
                        openItem.setTitle("删除");
                        // set item title fontsize
                        openItem.setTitleSize(18);
                        // set item title font color
                        openItem.setTitleColor(Color.WHITE);
                        // add to menu
                        menu.addMenuItem(openItem);

                    }

                }
            };

            // set creator
            mListView.setMenuCreator(creator);

            // step 2. listener item click event
            mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                    new AlertDialog.Builder(GroupMembersActivity.this).setMessage("你确定要将 "+ listMembers.get(position).name + " 从该群移出吗?")
                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteMember(position);
                                }
                            }).setNegativeButton("取消", null).show();

                    return false;
                }
            });
        }

    }


    private void getMembers() {

        showSpinnerDialog();
        RequestParams params = new RequestParams();

        final LeanchatUser user = LeanchatUser.getCurrentUser();
        params.put("userId",user.getObjectId());
        params.put("groupId",conversationId);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME+"members/list";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {

                listMembers.clear();

                JSONArray arrNames = data.names();
                if(arrNames!=null){
                    for (int i=0; i<arrNames.length(); i++){
                        String sign = arrNames.optString(i);
                        Contact signLetter = new Contact();
                        signLetter.sortLetters = sign;
                        listMembers.add(signLetter);

                        JSONArray array = data.optJSONArray(arrNames.optString(i));

                        for (int j=0; j<array.length(); j++){
                            JSONObject object = array.optJSONObject(j);
                            Contact contact = new Contact();
                            contact.userId = object.optString("userId");
                            contact.name = object.optString("remarkName");
                            contact.sortToken = parseSortKey(contact.name);
                            contact.avatarUrl = object.optString("avatar");

                            listMembers.add(contact);

                            if (arrNames.optString(i).equals("headMaster")) {
                                if (user.getObjectId().equals(object.optString("userId"))){
                                    adapter.identity = 2;
                                    isCreator = true;
                                }
                            }

                        }
                    }
                }

                adapter.updateListView(listMembers);
                initListView();
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg, JSONArray data) {
                listMembers.clear();
                for (int i=0;i<data.length();i++){
                    JSONObject object=data.optJSONObject(i);
                    Contact contact = new Contact();
                    contact.userId = object.optString("userId");
                    contact.name = object.optString("remarkName");
                    contact.avatarUrl = object.optString("avatar");
                    contact.sortLetters=object.optString("identity");
                    listMembers.add(contact);

                    if ("0".equals(object.optString("identity"))){
                        if (user.getObjectId().equals(object.optString("userId")))
                            adapter.identity = 2;
                            isCreator = true;
                    }
                    
                }
                adapter.updateListView(listMembers);
                initListView();
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }



    private void deleteMember(final int position){
        showSpinnerDialog();
        RequestParams params = new RequestParams();

        final LeanchatUser user = LeanchatUser.getCurrentUser();
        params.put("selfId",user.getObjectId());
        params.put("groupId",conversationId);
        params.put("userId",listMembers.get(position).userId);
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME+"members/kick";

        MyHttpClient.delete(url,params, new MyJsonResponseHandler() {


            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);
                if (code==0) {
                    listMembers.remove(position);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }
    private List<Contact> search(String str) {
        List<Contact> filterList = new ArrayList<Contact>();// 过滤后的list
        for (Contact contact : listMembers) {
            if (contact.name != null) {
                //姓名全匹配,姓名首字母简拼匹配,姓名全字母匹配
                if (contact.name.toLowerCase(Locale.CHINESE).contains(str.toLowerCase(Locale.CHINESE))
                        || contact.sortToken.simpleSpell.toLowerCase(Locale.CHINESE).contains(str.toLowerCase(Locale.CHINESE))
                        || contact.sortToken.wholeSpell.toLowerCase(Locale.CHINESE).contains(str.toLowerCase(Locale.CHINESE))) {
                    if (!filterList.contains(contact)) {
                        filterList.add(contact);
                    }
                }
            }
        }
        return filterList;
    }


    String chReg = "[\\u4E00-\\u9FA5]+";//中文字符串匹配

//    String chReg="[^\\u4E00-\\u9FA5]";//除中文外的字符匹配
    /**
     * 解析sort_key,封装简拼,全拼
     * @param sortKey
     * @return
     */
    public SortToken parseSortKey(String sortKey) {
        SortToken token = new SortToken();
        if (sortKey != null && sortKey.length() > 0) {
            //其中包含的中文字符
            String pinyin = characterParser.getSelling(sortKey.replace(" ", ""));
            String[] enStrs = pinyin.split(chReg);
            for (int i = 0, length = enStrs.length; i < length; i++) {
                if (enStrs[i].length() > 0) {
                    //拼接简拼
                    token.simpleSpell += enStrs[i].charAt(0);
                    token.wholeSpell += enStrs[i];
                }
            }
        }
        return token;
    }


    @OnClick(R.id.btnBackground)
    public void hideKeyboard(){
        hideSoftInputView();
        btnBackground.setVisibility(View.GONE);
        searchView.clearFocus();
    }
}
