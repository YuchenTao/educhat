package com.gruden.education.activity.classmenu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Child;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by admin on 2016/3/3.
 */
public class DistributeFlowerActivity extends BaseActivity {

    @Bind(R.id.gridstudentlist)
    GridView gridstudentlist;
    @Bind(R.id.btn_nextstep)
    Button btn_nextstep;
    @Bind(R.id.txtPrompt)
    TextView txtPrompt;
    private String conversationId;
    private StudentListAdapter adapter;
    private boolean isAllSelected=true;

    ArrayList<String> selectstu=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.select_student_activity);
        initActionBar("选择学生");
        showLeftBackButton();
        Intent i = getIntent();
        conversationId=i.getStringExtra("groupId");
        adapter=new StudentListAdapter(this);
        gridstudentlist.setAdapter(adapter);
        initdata();


        showRightTextButton("全选", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button = (Button) v;
                if(isAllSelected) {
                    button.setText("取消");
                    for (int i = 0; i < adapter.getCount(); i++) {
                        Child child = adapter.getItem(i);
                        if (!child.isSelected)
                            child.isSelected = true;
                    }
                }else{
                    button.setText("全选");
                    for (int i = 0; i < adapter.getCount(); i++) {
                        Child child = adapter.getItem(i);
                        if (child.isSelected)
                            child.isSelected = false;
                    }
                }
                isAllSelected = !isAllSelected;
                adapter.notifyDataSetChanged();

            }
        });
        btn_nextstep.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                allcheck();
                if (selectstu.size() < 1) {
                    toast("您还没有选择学生");
                    return;
                }
//                toast(selectstu.size()+"");
                Intent intent = new Intent(DistributeFlowerActivity.this, AwardReasonActivity.class);
                intent.putExtra("groupId", conversationId);
                intent.putStringArrayListExtra("childIds", selectstu);
                startActivity(intent);
               finish();


            }
        });

    }

    /**
     * 初始化页面数据
     */
    private void initdata() {
        RequestParams param = new RequestParams();
        param.put("groupId",conversationId);
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"student";
        MyHttpClient.get(url, param, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {
                dismissSpinnerDialog();
                if (array.length() == 0&&array!=null) {
                    gridstudentlist.setVisibility(View.GONE);
                    txtPrompt.setVisibility(View.VISIBLE);
                }else{
                    ArrayList<Child> childList=new ArrayList<Child>();
                    for (int i=0;i<array.length();i++){
                        JSONObject data=array.optJSONObject(i);
                        Child child=new Child();
                        child.childId=data.optString("cid");
                        child.name=data.optString("studentName");
                        child.studentCode=data.optString("studentCode");
                        child.imgUrl=data.optString("avatar");
                        child.gender=data.optString("gender");
                        child.isSelected=false;
                        childList.add(child);
                    }
                    adapter.setDataList(childList);
                    adapter.notifyDataSetChanged();

                }


            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();

                super.handleFailure(error);
            }

        });
//        //点击选中/取消
//        gridstudentlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                if(view.findViewById(R.id.selectImg).getVisibility()==View.VISIBLE) {
//                    view.findViewById(R.id.selectImg).setVisibility(View.INVISIBLE);
//                }
//               else {
//                    view.findViewById(R.id.selectImg).setVisibility(View.VISIBLE);
//                }
//
//            }
//        });


    }

    /**
     * 遍历所有选中的学生
     */
    private void allcheck(){
        selectstu.clear();
       for(int i=0;i<adapter.getCount();i++){
          Child child = adapter.getItem(i);
           if(child.isSelected){
               selectstu.add(child.childId);
           }
       }
    }

    /**
     * 显示学生自定义Adapter
     */
    class StudentListAdapter extends BaseAdapter {
        Context context;

        ArrayList<Child> data=new ArrayList<Child>();

        public StudentListAdapter(Context context) {
            this.context = context;
        }
        public void setDataList(ArrayList<Child> datas) {
            data.clear();
            if (null != datas) {
                data.addAll(datas);
            }
        }
        public void addDataList(ArrayList<Child> datas) {
            data.addAll( datas);
        }


        @Override
        public int getCount() {
            return data.size();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Child child=data.get(position);
            convertView= View.inflate(context,R.layout.distribute_student_item,null);
            ImageView studentImg= (ImageView) convertView.findViewById(R.id.avatar);
            final ImageView selectImg= (ImageView) convertView.findViewById(R.id.selectImg);
            TextView studentname= (TextView) convertView.findViewById(R.id.username);

            //加载图片
            ImageLoader.getInstance().displayImage(child.imgUrl, studentImg);
            if(child.imgUrl.length()<1&&child.imgUrl!=null){
                studentImg.setImageResource(R.drawable.defaultface);
            }
            studentname.setText(child.name);
            studentname.setTag(child.childId);
            if (child.isSelected){
                selectImg.setVisibility(View.VISIBLE);
            }else{
                selectImg.setVisibility(View.INVISIBLE);
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(child.isSelected) {
                        selectImg.setVisibility(View.INVISIBLE);
                        child.isSelected=false;
                    }
                    else {
                        selectImg.setVisibility(View.VISIBLE);
                        child.isSelected=true;
                    }
                }
            });

            return convertView;
        }
        @Override
        public Child getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }
}
