package com.gruden.education.viewholder;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.avoscloud.leanchatlib.viewholder.CommonViewHolder;
import com.gruden.education.R;
import com.gruden.education.model.BBSPost;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.viewpagerindicator.CirclePageIndicator;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by yunjiansun on 15/12/30.
 */
public class BBSPostItemHolder  {

    public ImageView imgAvatar;
    public TextView txtTitle;
    public TextView txtUserName;
    public TextView txtReplyCount;
    public TextView txtTime;
    public ImageButton btnComment;

}
