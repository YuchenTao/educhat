package com.gruden.education.activity.entry;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class EntrySmsActivity extends EntryBaseActivity {

    String phoneNum;
    String password;
    String smsCode;

    EditText smsEdit;
    TextView txtTimer;
    Button btnRegister;

    private long timedown=3000;
    private Timer timer;
    private int second=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_sms_activity);

        initActionBar(R.string.register);
        showLeftBackButton();

        findView();

        btnRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                register();
            }
        });
        txtTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSms();
            }
        });

        Intent intent =getIntent();
        phoneNum = intent.getStringExtra( "PhoneNum" );
        password = intent.getStringExtra( "Password" );
        smsCode = intent.getStringExtra( "SmsCode" );
        if (smsCode==null) {
            toast("验证码发送失败");
            Message message = new Message();
            message.what = 1;
            handler.sendMessage(message);
        }else{
            settimer();
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        smsEdit.setText(smsCode);
    }

    private void findView() {
        smsEdit = (EditText) findViewById(R.id.smsEdit);
        btnRegister = (Button)findViewById(R.id.btn_register);
        txtTimer= (TextView) findViewById(R.id.txtTimer);
    }

    private void register() {

        final String strSmsCode = smsEdit.getText().toString();

        if (TextUtils.isEmpty(strSmsCode)) {
            toast("验证码不能为空");
            return;
        }

        if (!strSmsCode.equals(smsCode)){
            toast("验证码错误");
            return;
        }

        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("username",phoneNum);
        params.put("password", password);
        params.put("smsCode", smsCode);

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "register";

        MyHttpClient.post(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {


                LeanchatUser user = new LeanchatUser();
                user.setObjectId(data.optString("userId"));
                user.setUsername(data.optString("username"));
                user.setMobilePhoneNumber(phoneNum);
                user.setMyPassword(password);
                LeanchatUser.changeCurrentUser(user, true);

                dismissSpinnerDialog();

                Intent intent = new Intent(EntrySmsActivity.this, EntryInfoActivity.class);
                startActivity(intent);

            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });


        //    LeanchatUser.signUpByNameAndPwd(name, password, new SignUpCallback() {
//      @Override
//      public void done(AVException e) {
//        if (e != null) {
//          Utils.toast(App.ctx.getString(R.string.registerFailed) + e.getMessage());
//        } else {
//          Utils.toast(R.string.registerSucceed);
//          MainActivity.goMainActivityFromActivity(EntryRegisterActivity.this);
//        }
//      }
//    });
    }
    private void sendSms() {
        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("type", 0);
        params.put("mobilePhoneNumber", phoneNum);
        String url = MyHttpClient.BASE_URL +"sms/sendSms";

        MyHttpClient.post(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                dismissSpinnerDialog();
                if (array.length() > 0)
                    smsCode = array.optString(0);
                smsEdit.setText(smsCode);
                settimer();

            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }

    String sBuffer="";

    /**
     * 倒计时
     */
    private  void settimer(){
        txtTimer.setTextColor(0xffbbbaba);
        txtTimer.setEnabled(false);
        timer =new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timedown -= 1000;
                second = (int) (timedown / 1000 % 60);
                sBuffer = "";
                sBuffer += second;
                txtTimer.post(new Runnable() {
                    @Override
                    public void run() {
                        txtTimer.setText(sBuffer + "秒后，重新获取验证码");
                    }
                });
                if (timedown == 0) {
                    if (timer != null) {
                        timer.cancel();
                        timer = null;
                    }
                    timedown = 3000;
                    Message message = new Message();
                    message.what = 1;
                    handler.sendMessage(message);
                }


            }
        },1000,1000);


    }
    Handler handler = new Handler(){
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    txtTimer.setTextColor(0xff197cf8);
                    txtTimer.setText("点击重新获取验证码");
                    txtTimer.setEnabled(true);
                    break;
            }
            super.handleMessage(msg);
        }

    };
}

