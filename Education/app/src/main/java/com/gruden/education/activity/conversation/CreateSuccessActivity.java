package com.gruden.education.activity.conversation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;

import butterknife.Bind;

/**
 * Created by admin on 2016/5/5.
 */
public class CreateSuccessActivity extends BaseActivity {
    @Bind(R.id.btn_addMembers)
    Button btn_addMembers;

    @Bind(R.id.btn_finish)
    Button btn_finish;

    private String conversationId;
    private String className;
    private String code;
    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_success);

        initActionBar("创建成功");
        Intent intent=getIntent();
        conversationId=intent.getStringExtra("groupId");
        className=intent.getStringExtra("name");
        code=intent.getStringExtra("code");
        type=intent.getIntExtra("type",0);
        btn_addMembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CreateSuccessActivity.this, SelectMemberRoleActivity.class);
                intent.putExtra("groupId",conversationId);
                intent.putExtra("code",code);
                intent.putExtra("type",type);
                intent.putExtra("name",className);
                intent.putExtra("title","添加成员");
                startActivity(intent);

                finish();
            }
        });
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
