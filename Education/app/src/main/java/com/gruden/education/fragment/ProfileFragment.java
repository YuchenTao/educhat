package com.gruden.education.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.profile.ChildrenListActivity;
import com.gruden.education.activity.profile.InformationActivity;
import com.gruden.education.activity.profile.MyCollectionActivity;
import com.gruden.education.activity.profile.MyPostActivity;
import com.gruden.education.activity.profile.SettingsActivity;
import com.gruden.education.model.LeanchatUser;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lzw on 14-9-17.
 */
public class ProfileFragment extends BaseFragment {
  private static final int IMAGE_PICK_REQUEST = 1;
  private static final int CROP_REQUEST = 2;

  @Bind(R.id.profile_avatar_view)
  ImageView avatarView;

  @Bind(R.id.txtUsername)
  TextView txtUsername;

  @Bind(R.id.txtPhone)
  TextView txtPhone;


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.profile_fragment, container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    headerLayout.showTitle(R.string.profile_title);
//    headerLayout.hideLeftBackButton();
//    headerLayout.hideRightButton();
  }

  @Override
  public void onResume() {
    super.onResume();
    refresh();
  }

  private void refresh() {
    LeanchatUser curUser = LeanchatUser.getCurrentUser();
    txtUsername.setText(curUser.getNickName());
    txtPhone.setText("电话：" + curUser.getMobilePhoneNumber());
    ImageLoader.getInstance().displayImage(curUser.getAvatarUrl(), avatarView, com.avoscloud.leanchatlib.utils.PhotoUtils.avatarImageOptions);
  }


  @OnClick(R.id.profile_info_layout)
  public void onInfoClick() {
    Intent intent = new Intent(ctx, InformationActivity.class);
    ctx.startActivity(intent);
  }

  @OnClick(R.id.profile_children_layout)
  public void onChildrenClick() {
    Intent intent = new Intent(ctx, ChildrenListActivity.class);
    ctx.startActivity(intent);
  }

  @OnClick(R.id.profile_setting_layout)
  public void onSettingClick() {
    Intent intent = new Intent(ctx, SettingsActivity.class);
    ctx.startActivity(intent);
  }
  @OnClick(R.id.profile_favorite_layout)
  public void onFavoriteClick() {
    Intent intent = new Intent(ctx, MyCollectionActivity.class);
    ctx.startActivity(intent);
  }
  @OnClick(R.id.profile_post_layout)
  public void onPostClick() {
    Intent intent = new Intent(ctx, MyPostActivity.class);
    ctx.startActivity(intent);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

  }


}
