package com.gruden.education.activity.classmenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.ProgressCallback;
import com.avos.avoscloud.SaveCallback;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by admin on 2016/3/9.
 */
public class LocalFileUploadActivity extends BaseActivity  {

    @Bind(R.id.collection_listView)
    ListView fileListView;
    @Bind(R.id.txtallsize)
    TextView txtallsize;
    @Bind(R.id.btnsendfile)
    Button btnsendfile;

    private ArrayList<File> localFileList=new ArrayList<File>();// 本地文件列表
    private ArrayList<UploadFile> uploadFileList=new ArrayList<UploadFile>();//上传文件列表
    private ArrayList<String> mselectpath=new ArrayList<String>(); //上传文件的本地路径
    private boolean isAllSelected=true;
    private boolean isUploading;
    private String conversationId;
    private int size;
    private MyAdapter myAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.local_file_upload_activity);

        Intent intent=getIntent();
        conversationId=intent.getStringExtra("groupId");
        initActionBar("本地文件");
        showLeftBackButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (UploadFile file : uploadFileList) {
                    if (file.isDone) {
                        file.file.deleteInBackground();
                    } else {
                        file.file.cancel();
                    }
                }
                onBackPressed();
            }
        });
        myAdapter=  new MyAdapter(LocalFileUploadActivity.this);
        fileListView.setAdapter(myAdapter);
        showRightTextButton("全选", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button = (Button) v;
                uploadFileList.clear();
                mselectpath.clear();

                if (isAllSelected) {
                    button.setText("取消");
                    for (int i = 0; i < myAdapter.getCount(); i++) {
                        UploadFile file=myAdapter.getItem(i);
                        if (!file.isSelected) {
                            file.isSelected = true;
                            size += file.size;
                        }
                    }
                } else {
                    button.setText("全选");
                    for (int i = 0; i < myAdapter.getCount(); i++) {
                        UploadFile file=myAdapter.getItem(i);
                        if(file.isSelected)
                            file.isSelected = false;
                    }
                    size=0;
                    for (UploadFile file : uploadFileList) {
                        if (file.isDone) {
                            file.file.deleteInBackground();
                        } else {
                            file.file.cancel();
                        }
                    }
                }
                txtallsize.setText(size + "K");
                myAdapter.notifyDataSetChanged();
                selectedFiles();
                isAllSelected = !isAllSelected;
            }
        });
        //异步加载显示本地文件列表
        new AsyncTask<Integer, Integer, String[]>() {
            ProgressDialog dialog;
            ArrayList <UploadFile>list ;
            protected void onPreExecute() {
                dialog = ProgressDialog.show(LocalFileUploadActivity.this, "",
                        "正在扫描SD卡,请稍候....");
                super.onPreExecute();
            }
            protected String[] doInBackground(Integer... params) {
                if (!android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
                }
                else {

                        GetFiles(Environment.getExternalStorageDirectory());//getExternalStorageDirectory() 获取内部SD卡
                        GetFiles(new File(Environment.getExternalStorageDirectory() + "/DCIM")) ;
                        GetFiles(new File(Environment.getExternalStorageDirectory()+"/Download")) ;
                        GetFiles(new File(Environment.getExternalStorageDirectory()+"/Education")) ;
                        list = getData(localFileList);
                }
                return null;
            }
            protected void onPostExecute(String[] result) {
                dialog.dismiss();
                Toast.makeText(LocalFileUploadActivity.this, "扫描完毕", Toast.LENGTH_LONG).show();
                myAdapter.setDataList(list);
                myAdapter.notifyDataSetChanged();
                super.onPostExecute(result);
            }
        }.execute(0);

        btnsendfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUploadFiles();
            }
        });
    }

    //遍历所有选中的文件
    private ArrayList<String> selectedItem(){
        ArrayList<String> pathlist=new ArrayList<String>();
        for (int i = 0; i < myAdapter.getCount(); i++) {
            UploadFile file=myAdapter.getItem(i);
            if (file.isSelected) {
                pathlist.add(file.path);
            }
        }
        return pathlist;
    }
    //上传文件
    private void selectedFiles(){

        ArrayList<String> pathlist= selectedItem();
        for (int i=0; i<uploadFileList.size();i++){
            UploadFile file = uploadFileList.get(i);
            if (!pathlist.contains(file.path)){
                Log.e("remove File：", file.path);
                file.file.cancel();
                file.file.deleteInBackground();
                uploadFileList.remove(i);
            }
        }
        for (int i=0;i<pathlist.size();i++){
            if (!mselectpath.contains(pathlist.get(i))){
                Log.e("add file :", pathlist.get(i));
                try {
                    final UploadFile uploadFile=new UploadFile();
                    uploadFile.path= pathlist.get(i);
                    uploadFile.file=AVFile.withAbsoluteLocalPath(uploadFile.path.substring(pathlist.get(i).lastIndexOf("/") + 1, pathlist.get(i).length()), pathlist.get(i));
                    uploadFileList.add(uploadFile);
                    uploadFile.name=uploadFile.file.getOriginalName();
                    uploadFile.size=uploadFile.file.getSize();

                    uploadFile.file.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            uploadFile.url=uploadFile.file.getUrl();
                            uploadFile.isDone=true;

                            if (isUploading)
                                setUploadFiles();

                        }
                    }, new ProgressCallback() {
                        @Override
                        public void done(Integer integer) {
                            System.out.println("precentCode：" + integer);
                        }
                    });
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        mselectpath = pathlist;
    }
    /**
     * 后台添加上传文件的请求
     */
    private void setUploadFiles(){

        if (uploadFileList.size()==0||uploadFileList==null){
            toast("您没有选择上传的文件~") ;
            return;
        }
        showSpinnerDialog();
        for(UploadFile file : uploadFileList){
            if(!file.isDone){
                isUploading = true;
                return;
            }
        }
        isUploading = false;

        RequestParams params = new RequestParams();
        params.put("groupId",conversationId);
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());


        if (uploadFileList.size()>0){
            List<String> urlList = new ArrayList<String>();
            for(UploadFile file : uploadFileList){
                String url=file.url+"::"+file.name+"::"+file.size;
                urlList.add(url);
            }
            params.put("file",urlList);
        }

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_REALTIME + "filelist";
        MyHttpClient.post(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
                finish();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }
        });
    }

    /**
     * 得到本地文件数据方法
     * @param list
     * @return
     */
    private ArrayList<UploadFile> getData(ArrayList<File> list){

        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        ArrayList<UploadFile> data = new ArrayList<UploadFile>();
        int i = 0 ;
        for(i=0;i<list.size();i++){
            final   UploadFile uploadFile=new UploadFile();
            try {
                uploadFile.name=list.get(i).getName();
                FileInputStream fis = null;
                fis = new FileInputStream(list.get(i));
                uploadFile.size = fis.available()/1024;
                uploadFile.isSelected=false;
                uploadFile.path=list.get(i).toString();
                uploadFile.time=sdf.format(list.get(i).lastModified());
                data.add(uploadFile);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return data;
    }


    /**
     * 获取文件列表的方法
     *（递归调用）
     * @param filePath  文件的绝对路径
     */
    public void GetFiles(File filePath) {
        if (filePath.exists()) {
            File[] files = filePath.listFiles();
            if (files != null) {
                if (filePath.equals(Environment.getExternalStorageDirectory())) {
                    for (int i = 0; i < files.length; i++) {
                        String end = files[i].getName().substring(files[i].getName().lastIndexOf(".") + 1, files[i].getName().length()).toLowerCase();
                        if (end.equalsIgnoreCase("jpg") || end.equalsIgnoreCase("gif") || end.equalsIgnoreCase("png") ||
                                end.equalsIgnoreCase("jpeg") || end.equalsIgnoreCase("bmp") || end.equalsIgnoreCase("doc") ||
                                end.equalsIgnoreCase("docx") || end.equalsIgnoreCase("xls") || end.equalsIgnoreCase("xlsx") ||
                                end.equalsIgnoreCase("ppt") || end.equalsIgnoreCase("pptx") || end.equalsIgnoreCase("zip") ||
                                end.equalsIgnoreCase("mp3") || end.equalsIgnoreCase("mp4") || end.equalsIgnoreCase("xml") || end.equalsIgnoreCase("apk") || end.equalsIgnoreCase("html")
                                ) {
                            localFileList.add(files[i]);
                        }
                    }
                } else {
                    for (int i = 0; i < files.length; i++) {
                        if (files[i].isDirectory()) {
                            GetFiles(files[i]);
                        } else {
                            localFileList.add(files[i]);
                        }
                    }
                }
            }
        }

    }


    /**
     * 显示文件列表的Adapter
     */
    class MyAdapter extends BaseAdapter{
        Context context;
        int count=1;
        ArrayList<UploadFile> data=new  ArrayList<UploadFile>();
        public MyAdapter(Context context) {
            this.context = context;
        }

        public List<UploadFile> getDataList() {
            return data;
        }

        public void setDataList(List<UploadFile> datas) {
            data.clear();
            if (null != datas) {
                data.addAll(datas);
            }
        }
        public void addDataList(List<UploadFile> datas) {
            data.addAll(datas);
        }
        @Override
        public int getCount() {
            return data.size();
        }
        @Override
        public View getView(final int position,  View convertView, ViewGroup parent) {
            final UploadFile file=data.get(position);
            final FileHolder holder ;
            if(convertView==null){
                holder= new FileHolder();
                convertView=View.inflate(context, R.layout.local_file_item, null);
                holder.txtTitle= (TextView) convertView.findViewById(R.id.txtTitle);
                holder.txtFileTime= (TextView) convertView.findViewById(R.id.txtFileTime);
                holder.txtFileSize= (TextView) convertView.findViewById(R.id.txtFileSize);
                holder.img= (ImageView) convertView.findViewById(R.id.localfileImg);
                holder.checkBox = (CheckBox) convertView.findViewById(R.id.selectfileImg);
                convertView.setTag(holder);
            }else{
                holder= (FileHolder) convertView.getTag();
            }

            String name=file.name;
            String end=name.substring(name.lastIndexOf(".") + 1,name.length()).toLowerCase();
            if(end.equalsIgnoreCase("jpg")||end.equalsIgnoreCase("gif")||end.equalsIgnoreCase("png")||
                    end.equalsIgnoreCase("jpeg")||end.equalsIgnoreCase("bmp")) {
                ImageLoader.getInstance().displayImage("file://" + file.path, holder.img);
            }else if(end.equalsIgnoreCase("m4a")||end.equalsIgnoreCase("mp3")||end.equalsIgnoreCase("mid")||
                    end.equalsIgnoreCase("xmf")||end.equalsIgnoreCase("ogg")||end.equalsIgnoreCase("wav")){
//                img.setImageResource(R.drawable.filetype_mp3);
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_mp3, holder.img);
            }else if(end.equalsIgnoreCase("mp4")||end.equalsIgnoreCase("3gp")){
//                img.setImageResource(R.drawable.filetype_mov);
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_pdf, holder.img);
            }else if(end.equalsIgnoreCase("apk")){
//                img.setImageResource(R.drawable.filetype_apk);
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_apk, holder.img);
            }else if(end.equalsIgnoreCase("ppt")||end.equalsIgnoreCase("pptx")){
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_ppt, holder.img);
//                img.setImageResource(R.drawable.filetype_ppt);
            }else if(end.equalsIgnoreCase("xls")||end.equalsIgnoreCase("xlsx")){
//                img.setImageResource(R.drawable.filetype_xls);
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_xls, holder.img);
            }else if(end.equalsIgnoreCase("doc")||end.equalsIgnoreCase("docx")){
//                img.setImageResource(R.drawable.filetype_doc);
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_doc, holder.img);
            }else if(end.equalsIgnoreCase("pdf")){
//                img.setImageResource(R.drawable.filetype_pdf);
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_pdf, holder.img);
            }else if(end.equalsIgnoreCase("rar")){
//                img.setImageResource(R.drawable.filetype_rar);
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_rar, holder.img);
            }else if (end.equalsIgnoreCase("zip")){
//                img.setImageResource(R.drawable.filetype_zip);
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_zip, holder.img);
            }else if(end.equalsIgnoreCase("html")){
//                img.setImageResource(R.drawable.filetype_html);
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_html, holder.img);
            }else{
//                img.setImageResource(R.drawable.filetype_ics);
                ImageLoader.getInstance().displayImage("drawable://" + R.drawable.filetype_ics, holder.img);
            }
            holder.txtTitle.setText(file.name.toString());
            holder.txtFileTime.setText(file.time.toString());
            holder.txtFileSize.setText(file.size+"");
            holder.checkBox.setChecked(file.isSelected);

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(count<=5) {
                            holder.checkBox.performClick();
                        }else{
                            toast("上传的文件不能超过5个~");
                        }
                    }
                });
                holder.checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CheckBox checkBox= (CheckBox) v;
                        if (count<=5) {
                            if (!file.isSelected) {
                                file.isSelected = true;
                                size += file.size;
                                ++count;
                            } else {
                                size -= file.size;
                                file.isSelected = false;
                                --count;
                            }
                            txtallsize.setText(size + "K");
                            selectedFiles();
                        }else{
                            checkBox.setChecked(false);
                            toast("上传的文件不能超过5个~");
                        }
                    }
                });

            return convertView;
        }
        @Override
        public UploadFile getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

    }
    /**
     * 上传文件的类
     */
    class UploadFile{
        AVFile file;
        String url;
        String path;
        String name;
        Integer size;
        String time;
        boolean isSelected;
        boolean isDone;
    }
    class FileHolder{
        ImageView img;
        TextView txtTitle;
        TextView txtFileTime;
        TextView txtFileSize;
        CheckBox checkBox;

    }


}
