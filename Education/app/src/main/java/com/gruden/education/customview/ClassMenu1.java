package com.gruden.education.customview;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.Constants;
import com.gruden.education.R;
import com.gruden.education.activity.classmenu.BlogListActivity;
import com.gruden.education.activity.classmenu.FoodListActivity;
import com.gruden.education.activity.classmenu.GroupMembersActivity;
import com.gruden.education.activity.classmenu.HomeworkListActivity;
import com.gruden.education.activity.classmenu.LeaveListActivity;
import com.gruden.education.activity.classmenu.NoticeListActivity;
import com.gruden.education.activity.classmenu.ReportCardParentActivity;
import com.gruden.education.activity.classmenu.ReportCardTeacherActivity;
import com.gruden.education.activity.classmenu.ScheduleActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClassMenu1 extends Fragment {
    public static final int QUIT_GROUP_REQUEST = 200;
    protected Context ctx;

    public String conversationId;
    public String conversationName;
    LeanchatUser user=LeanchatUser.getCurrentUser();


    @Bind(R.id.layout_notice)
    View layout_notice;

    @Bind(R.id.layout_homework)
    View layout_homework;

    @Bind(R.id.layout_leave)
    View layout_leave;

    @Bind(R.id.noticeNum)
    TextView txtnoticeNum;

    @Bind(R.id.homeworkNum)
    TextView txthomeworkNum;

    @Bind(R.id.leaveNum)
    TextView txtleaveNum;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.class_menu1, container,false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ctx = getActivity();

    }

    @OnClick(R.id.layoutNotice)
    public void onNoticeClick(){
//        layout_notice.setVisibility(View.GONE);
        Intent intent = new Intent(ctx, NoticeListActivity.class);
        intent.putExtra("id", conversationId);
        startActivity(intent);
    }

    @OnClick(R.id.layoutHomework)
    public void onHomeworkClick(){
//        layout_homework.setVisibility(View.GONE);
        Intent intent = new Intent(ctx, HomeworkListActivity.class);
        intent.putExtra("id", conversationId);
        startActivity(intent);
    }

    @OnClick(R.id.layoutReport)
    public void onReportClick(){
        LeanchatUser user = LeanchatUser.getCurrentUser();
        if(user.getRoles().contains(LeanchatUser.PARENT)&&!user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN)) {
            Intent intent = new Intent(ctx, ReportCardParentActivity.class);
            intent.putExtra("id", conversationId);
            startActivity(intent);
        }else{
            Intent intent = new Intent(ctx, ReportCardTeacherActivity.class);
            intent.putExtra("id", conversationId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.layoutSchedule)
    public void onScheduleClick(){
        Intent intent = new Intent(ctx, ScheduleActivity.class);
        intent.putExtra("id", conversationId);
        startActivity(intent);
    }

    @OnClick(R.id.layoutMembers)
    public void onMembersClick(){
        Intent intent = new Intent(ctx, GroupMembersActivity.class);
        intent.putExtra(Constants.CONVERSATION_ID, conversationId);
        startActivityForResult(intent, QUIT_GROUP_REQUEST);
    }

    @OnClick(R.id.layoutBlog)
    public void onBlogClick(){
        Intent intent = new Intent(ctx, BlogListActivity.class);
        intent.putExtra("id", conversationId);
        startActivity(intent);
    }
    @OnClick(R.id.layoutLeave)
    public void onLeaveClick(){
        layout_leave.setVisibility(View.GONE);
        user.setClassStartTime(new Date().getTime());
        LeanchatUser.changeCurrentUser(user,true);
        Intent intent = new Intent(ctx, LeaveListActivity.class);
        intent.putExtra("id", conversationId);
        startActivity(intent);
    }
    @OnClick(R.id.layoutFood)
    public void onFoodClick(){
        Intent intent = new Intent(ctx, FoodListActivity.class);
        intent.putExtra("id", conversationId);
        startActivity(intent);
    }
    public void getUnreadCount(){
        RequestParams params=new RequestParams();
        params.put("groupId",conversationId);
        params.put("userId",user.getObjectId());
        params.put("startTime",user.getClassStartTime());
        String url= MyHttpClient.BASE_URL+MyHttpClient.MODEL_MESSAGE+"untreated";
        MyHttpClient.get(url,params,new MyJsonResponseHandler(){
            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {
                if (data.optInt("notice")>0){
                    layout_notice.setVisibility(View.VISIBLE);
                    txtnoticeNum.setText("+"+data.optInt("notice"));
                }else{
                    layout_notice.setVisibility(View.GONE);
                }
                if (data.optInt("homework")>0){
                    layout_homework.setVisibility(View.VISIBLE);
                    txthomeworkNum.setText("+"+data.optInt("homework"));
                }else{
                    layout_homework.setVisibility(View.GONE);
                }
                if (data.optInt("leave")>0){
                    layout_leave.setVisibility(View.VISIBLE);
                    txtleaveNum.setText("+"+data.optInt("leave"));
                }else{
                    layout_leave.setVisibility(View.GONE);
                }
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                super.handleSuccess(code, mesg);
            }

            @Override
            public void handleFailure(int error) {
                super.handleFailure(error);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getUnreadCount();
    }


}
