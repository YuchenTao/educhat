package com.gruden.education.activity.search;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.SearchAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.Search;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by admin on 2016/3/22.
 */
public class SearchTypeActivity extends BaseActivity{
    @Bind(R.id.searchView)
    SearchView searchView;

    @Bind(R.id.btn_cancel)
    Button btn_cancel;

    @Bind(R.id.list_search_post)
    ListView searchlist;

    @Bind(R.id.search_post_refresh)
    MaterialRefreshLayout refreshLayout;
    @Bind(R.id.txt_hearder)
    TextView header;

    SearchAdapter adapter;
    private boolean isRefreshing;
    private boolean isLoadMoreing;

    private String changeText="";
    private  int curPage=0 ;
    private int itemType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.activity_search_type);

        Intent i=getIntent();

        itemType=i.getIntExtra("type", 0);
        switch (itemType){
            case 0:
                searchView.setQueryHint("搜索联系人");
                break;
            case 1:
                searchView.setQueryHint("搜索群组");
                break;
            case 2:
                searchView.setQueryHint("搜索帖子");
                break;
            default:
                break;
        }

        if (i.getStringExtra("search")!=null){
            searchView.setQuery(i.getStringExtra("search"),false);
            changeText=i.getStringExtra("search");
        }

        adapter=new SearchAdapter(this);
        searchlist.setAdapter(adapter);

        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                searchInfo(changeText, 0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...

                isLoadMoreing = true;
                searchInfo(changeText, curPage + 1);
            }
        });
        initListener();


    }

    private void initListener() {
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (!TextUtils.isEmpty(newText)) {
                    adapter.setKeyWords(newText);
                    changeText = newText;
                    searchInfo(newText, 0);
                }
                return false;
            }
        });
    }
    private void searchInfo(String txt,int page){

        if(changeText.length()==0){
            return;
        }
        switch (itemType){
            case 0:
                adapter.setType(0);
                header.setText("联系人");
                searchContectInfo(txt,page);
                break;
            case 1:
                adapter.setType(1);
                header.setText("群组");
                searchGroupInfo(txt, page);
                break;
            case 2:
                adapter.setType(2);
                header.setText("帖子");
                searchPostInfo(txt,page);
                break;
            default:
                break;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            searchInfo(changeText,0);
        }

    }
    private void searchContectInfo(String txt,final int page){
        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUserId());
        params.put("content", txt);
        String url =MyHttpClient.BASE_URL+MyHttpClient.MODEL_USER+"friends/list";
        MyHttpClient.get(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length() == 0 && page > 0) {
                    toast("已经没有更多联系人~");
                } else {
                    header.setVisibility(View.VISIBLE);
                    ArrayList<Search> searchList = new ArrayList<Search>();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject data = array.optJSONObject(i);
                        Search search = new Search();
                        search.id = data.optString("userId");
                        search.name = data.optString("remarkName");
                        search.content="昵称 ："+data.optString("nickname");
                        search.imgUrl=data.optString("avatar");
                        searchList.add(search);
                    }
                    if(page==0){
                        adapter.setDataList(searchList);
                    }
                    adapter.notifyDataSetChanged();
                    curPage=page;
                }
                dissmissCircle();
            }
            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }
        });


    }
    private void searchGroupInfo(String txt,final int page){
        RequestParams params = new RequestParams();
        params.put("content", txt);
        params.put("curPage", page);
        String url =MyHttpClient.BASE_URL+MyHttpClient.MODEL_REALTIME+"group/list";
        MyHttpClient.get(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {
                if (array.length() == 0 && page > 0) {
                    toast("已经没有更多群~");
                } else {
                    header.setVisibility(View.VISIBLE);
                    ArrayList<Search> searchList = new ArrayList<Search>();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject data = array.optJSONObject(i);
                        Search search = new Search();
                        search.id = data.optString("groupId");
                        search.title = data.optString("name");
                        search.name = data.optString("type");
                        search.imgUrl=data.optString("avatar");
                        search.remarkName ="("+ data.optString("memberCount")+"人)";
                        searchList.add(search);
                    }

                    if (page == 0) {
                        adapter.setDataList(searchList);
                    } else {
                        adapter.addDataList(searchList);
                    }

                    adapter.notifyDataSetChanged();
                    curPage = page;
                }
                dissmissCircle();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }
        });
    }

    private void  searchPostInfo(String title,final int page ){
        RequestParams params = new RequestParams();
        params.put("order", "lasted");
        params.put("tcode", "all");
        params.put("operat", "query");
        params.put("curPage", page);
        params.put("title",title);
        String url =MyHttpClient.HOST +"topic/all";
        MyHttpClient.get(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length() == 0 && page > 0) {
//                    toast("已经没有更多帖子~");
                } else {
                    header.setVisibility(View.VISIBLE);
                    ArrayList<Search> postList = new ArrayList<Search>();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject data = array.optJSONObject(i);
                        Search search = new Search();
                        search.id = data.optString("id");
                        search.title = data.optString("title");
                        search.imgUrl=data.optString("avatar");
                        search.content=data.optString("content");
                        postList.add(search);
                    }

                    if (page == 0) {
                        adapter.setDataList(postList);
                    } else {
                        adapter.addDataList(postList);
                    }

                    adapter.notifyDataSetChanged();
                    curPage = page;
                }
                dissmissCircle();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }
        });
    }
    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }
}
