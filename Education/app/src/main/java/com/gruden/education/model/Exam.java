package com.gruden.education.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/1/15.
 */
public class Exam {

    public String examCode;
    public String examName;
    public int total;

    public List<Subject> subjectList  = new ArrayList<Subject>();


}
