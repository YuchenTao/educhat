package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.TabHost;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.RedGridViewAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;

/**
 * Created by admin on 2016/3/2.
 */
public class RedListActivity extends BaseActivity {
    private String conversationId;
    private String[] tabname = new String[]{"今天", "本周", "全部"};
    private String[] type = new String[]{"day", "week", "all"};
    private int[] gridids = new int[]{R.id.gridreddaylist, R.id.gridredweeklist, R.id.gridredalllist};
    LeanchatUser user;

    TabHost tabHost;
    GridView gridview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.red_list_activity);

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");

        findView();
        initActionBar("红花榜");
        showLeftBackButton();
        //tabHost初始化
        tabHost.setup();

        user = LeanchatUser.getCurrentUser();

       System.out.print("Roles:"+user.getRoles());
        if (user.getRoles().contains(LeanchatUser.TEACHER)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN)) {
            showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(RedListActivity.this, DistributeFlowerActivity.class);
                    intent.putExtra("groupId", conversationId);
                    startActivity(intent);
                    finish();
                }
            });
        }
        getTabTitles();
    }

    private void findView() {
        tabHost = (TabHost) findViewById(R.id.tabHost);
    }


    /**
     * 得到tabHost的三个标题和其所对应的页面的方法
     */
    private void getTabTitles() {

        showSpinnerDialog();
        for (int i = 0; i < tabname.length; i++) {
            LayoutInflater inflater = LayoutInflater.from(RedListActivity.this);
            View tab = inflater.inflate(R.layout.topic_tab_spec, null);
            TextView txtTitle = (TextView) tab.findViewById(R.id.txtTitle);
            txtTitle.setText(tabname[i]);
            TabHost.TabSpec spec = tabHost.newTabSpec("Tag" + i);

            spec.setContent(gridids[i]);
            spec.setIndicator(tab);
            tabHost.addTab(spec);
            gridview = (GridView) findViewById(gridids[i]);
            getTopicList(gridview, type[i]);

        }
    }

    /**
     * 得到tabHost的三个页面内容的方法
     * @param gridview 页面的控件
     * @param tagType  查询的参数（day，week，all）
     */
    private void getTopicList(final GridView gridview, String tagType) {
        RequestParams params = new RequestParams();
        params.put("groupId", conversationId);
        params.put("userId", user.getObjectId());
        params.put("role", (user.getRoles().contains(LeanchatUser.TEACHER)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN)) ? 1 : 0);
        params.put("type", tagType);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"flower";
        MyHttpClient.get(url,params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {
                if (array.length() == 0) {
//                    Toast.makeText(RedListActivity.this, "还没有发过红花~", Toast.LENGTH_SHORT).show();
                } else {

                    RedGridViewAdapter  redGridViewAdapter=new RedGridViewAdapter(RedListActivity.this,array);
                    gridview.setAdapter(redGridViewAdapter);
                    redGridViewAdapter.notifyDataSetChanged();
                }

                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);

            }

        });

    }


}
