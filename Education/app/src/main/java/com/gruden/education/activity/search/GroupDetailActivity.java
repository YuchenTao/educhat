package com.gruden.education.activity.search;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.Constants;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.activity.conversation.ChatRoomActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by admin on 2016/3/30.
 */
public class GroupDetailActivity extends BaseActivity {
    @Bind(R.id.txtgroupName)
    TextView txtGroupName;

    @Bind(R.id.txtGroupSize)
    TextView txtGroupSize;

    @Bind(R.id.txtPickNum)
    TextView txtPickNum;

    @Bind(R.id.txtPickMember)
    TextView txtPickMember;

    @Bind(R.id.txtAdminName)
    TextView txtAdminName;

    @Bind(R.id.txtbrief)
    TextView txtbrief;

    @Bind(R.id.groupAvatarView)
    ImageView avatarView;

    @Bind(R.id.layoutSendMsg)
    LinearLayout layoutSendMsg;

    @Bind(R.id.layoutAdd)
    LinearLayout layoutAdd;


    private String groupId;
    private int grouptype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);

        Intent intent = getIntent();
        groupId=intent.getStringExtra("groupId");
        grouptype=intent.getIntExtra("grouptype",0);

        initActionBar("群信息");
        showLeftBackButton();



    }

    @Override
    protected void onResume() {
        super.onResume();
        groupInfo();
    }

    private void groupInfo(){
        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("groupId", groupId);
        params.put("userId", LeanchatUser.getCurrentUserId());

        String url = MyHttpClient.BASE_URL+MyHttpClient.MODEL_REALTIME+"group/detail";
        MyHttpClient.get(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {

                if (data.optString("avatar").length() > 0) {
                    ImageLoader.getInstance().displayImage(data.optString("avatar"), avatarView);
                } else {
                    avatarView.setImageResource(R.drawable.defaultgroup);
                }
                txtGroupName.setText(data.optString("name"));

                txtGroupSize.setText(data.optString("type"));
                txtPickNum.setText(data.optString("code"));
                txtAdminName.setText(data.optJSONObject("admin").optString("remarkName"));
                txtPickMember.setText(data.optString("memberCount"));

                txtbrief.setText(data.optString("introduction"));

                if (data.optInt("isMember")==0) {
                    layoutSendMsg.setVisibility(View.GONE);
                    layoutAdd.setVisibility(View.VISIBLE);
                } else {
                    layoutSendMsg.setVisibility(View.VISIBLE);
                    layoutAdd.setVisibility(View.GONE);
                }
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }
        });
    }
    @OnClick(R.id.btnSendMsg)
    public void sendMsg(){

        Intent intent = new Intent(this, ChatRoomActivity.class);
        intent.putExtra(Constants.CONVERSATION_ID, groupId);
        startActivity(intent);

    }
    @OnClick(R.id.btnAddGroup)
    public void addGroup(){
        Intent intent = new Intent(this, AddGroupActivity.class);
        if (grouptype==1) {

           intent.putExtra("title","申请加入班级");
           intent.putExtra("hint","设置班级内备注");
            intent.putExtra("type", grouptype);
            intent.putExtra("groupId",groupId);
        }else{
            intent.putExtra("title","申请加入群组");
            intent.putExtra("hint","设置群内备注");
            intent.putExtra("type", grouptype);
            intent.putExtra("groupId",groupId);
        }
        startActivity(intent);
    }

}
