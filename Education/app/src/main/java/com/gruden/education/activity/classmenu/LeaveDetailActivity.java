package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.Bind;

public class LeaveDetailActivity extends BaseActivity {

    @Bind(R.id.txtChildName)
    TextView txtChildName;

    @Bind(R.id.txtDate)
    TextView txtDate;

    @Bind(R.id.txtContent)
    TextView txtContent;


    @Bind(R.id.btnDismiss)
    Button btnDismiss;

    @Bind(R.id.btnAgree)
    Button btnAgree;

    @Bind(R.id.layoutTeacher)
    LinearLayout layoutTeacher;


    private String id;
    private String creator;
    private String mobilePhone;
    private int state;
    final LeanchatUser user = LeanchatUser.getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_detail);

        initActionBar("请假");
        showLeftBackButton();
        if(user.getRoles().contains(LeanchatUser.TEACHER)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN)) {
            showRightTextButton("联系申请人", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    toast("" + mobilePhone);
                    Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+mobilePhone));
                    startActivity(intent);
                }
            });
        }
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        state=intent.getIntExtra("state",0);
        if ((user.getRoles().contains(LeanchatUser.PARENT)&&!user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN))||state!=0){
            layoutTeacher.setVisibility(View.GONE);
        }

        initListener();

    }

    private void initListener() {
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agreeLeave(0);
            }
        });
        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agreeLeave(1);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLeaveDetail();
    }

    void getLeaveDetail(){
        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("id",id);
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME+"leave";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0){

                    toast("请求失败");

                }else {
                    JSONObject object = array.optJSONObject(0);
                    txtChildName.setText(object.optString("childName"));
                    txtDate.setText(object.optString("startDate"));
                    txtContent.setText(object.optString("reason"));
                    creator=object.optString("creator");
                    mobilePhone=object.optString("mobilePhone");

                }
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }

    public void agreeLeave(int type){
        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("type",type);
        params.put("id",id);
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME+"leave";

        MyHttpClient.put(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
                finish();

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }
}
