package com.gruden.education.download;

import com.gruden.education.R;

import org.xutils.DbManager;
import org.xutils.common.Callback;
import org.xutils.common.task.PriorityExecutor;
import org.xutils.common.util.LogUtil;
import org.xutils.db.converter.ColumnConverterFactory;
import org.xutils.ex.DbException;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/**
 * Author: wyouflf
 * Date: 13-11-10
 * Time: 下午8:10
 */
public final class DownloadManager {

    static {
        // 注册DownloadState在数据库中的值类型映射
        ColumnConverterFactory.registerColumnConverter(DownloadState.class, new DownloadStateConverter());
    }
    private static DownloadManager instance;

    private final static int MAX_DOWNLOAD_THREAD = 2; // 有效的值范围[1, 3], 设置为3时, 可能阻塞图片加载.
    private final String groupId;
    public final DbManager db;
    private final Executor executor = new PriorityExecutor(MAX_DOWNLOAD_THREAD, true);
    public static final List<DownloadInfos> downloadInfoList = new ArrayList<DownloadInfos>();
    private final ConcurrentHashMap<DownloadInfos, DownloadCallback>
            callbackMap = new ConcurrentHashMap<DownloadInfos, DownloadCallback>(5);


    private DownloadManager(String groupId) {
        this.groupId=groupId;
        DbManager.DaoConfig daoConfig = new DbManager.DaoConfig()
                .setDbName("downloads")
                .setDbVersion(1);
        db = x.getDb(daoConfig);
        try {
            List<DownloadInfos> infoList = db.selector(DownloadInfos.class).where("groupId", "=", groupId).orderBy("createAt").findAll();
            if (infoList != null) {
                for (DownloadInfos info : infoList) {
                    System.out.println(info.toString());
                    downloadInfoList.add(info);
                }
            }
        } catch (DbException ex) {
            LogUtil.e(ex.getMessage(), ex);
        }
    }

    /*package*/
    public static DownloadManager getInstance(String gId) {
        downloadInfoList.clear();
        if (instance == null) {
            synchronized (DownloadManager.class) {
                if (instance == null) {
                    instance = new DownloadManager(gId);
                }
            }
        }
        return instance;
    }

    public void updateDownloadInfo(DownloadInfos info) throws DbException {
        db.update(info);
    }

    public int getDownloadListCount() {
        return downloadInfoList.size();
    }

    public DownloadInfos getDownloadInfo(int index) {
        return downloadInfoList.get(index);
    }
    public void showDownloadList(String fileId,String groupId,
                                              String url, String label, String savePath,long fileLength,int downloadCount,
                                              String creatAt,String remarkName,
                                 boolean autoResume, boolean autoRename) throws DbException {

        if (db.selector(DownloadInfos.class).where("fileId", "=", fileId).findFirst()!=null) {
            DownloadInfos downloadInfo=db.selector(DownloadInfos.class).where("fileId", "=", fileId).findFirst();
            downloadInfo.setIsDone(false);
            db.update(downloadInfo);
            downloadInfoList.add(downloadInfo);
        }else {
            String fileSavePath = new File(savePath).getAbsolutePath();
            DownloadInfos downloadInfo = new DownloadInfos();
            downloadInfo.setFileId(fileId);
            downloadInfo.setGroupId(groupId);
            downloadInfo.setUrl(url);
            downloadInfo.setLabel(label);
            downloadInfo.setFileSavePath(fileSavePath);
            downloadInfo.setFileLength(fileLength);
            downloadInfo.setDownloadCount(downloadCount);
            downloadInfo.setCreatedAt(creatAt);
            setImgResource(downloadInfo,label);
            downloadInfo.setRemarkName(remarkName);
            downloadInfo.setAutoRename(autoRename);
            downloadInfo.setAutoResume(autoResume);
            downloadInfo.setProgress(0);
            db.saveBindingId(downloadInfo);
            downloadInfoList.add(downloadInfo);
        }

    }
    private void setImgResource(DownloadInfos downloadInfo,String label){
        String end=label.substring(label.lastIndexOf(".") + 1,label.length()).toLowerCase();
        if(end.equalsIgnoreCase("jpg")||end.equalsIgnoreCase("gif")||end.equalsIgnoreCase("png")||
                end.equalsIgnoreCase("jpeg")||end.equalsIgnoreCase("bmp")) {
            downloadInfo.setImgUrl(R.drawable.filetype_jpg);
        }else if(end.equalsIgnoreCase("m4a")||end.equalsIgnoreCase("mp3")||end.equalsIgnoreCase("mid")||
                end.equalsIgnoreCase("xmf")||end.equalsIgnoreCase("ogg")||end.equalsIgnoreCase("wav")){
            downloadInfo.setImgUrl(R.drawable.filetype_mp3);
        }else if(end.equalsIgnoreCase("mp4")||end.equalsIgnoreCase("3gp")){
            downloadInfo.setImgUrl(R.drawable.filetype_mov);
        }else if(end.equalsIgnoreCase("apk")){
            downloadInfo.setImgUrl(R.drawable.filetype_apk);
        }else if(end.equalsIgnoreCase("ppt")||end.equalsIgnoreCase("pptx")){
            downloadInfo.setImgUrl(R.drawable.filetype_ppt);
        }else if(end.equalsIgnoreCase("xls")||end.equalsIgnoreCase("xlsx")){
            downloadInfo.setImgUrl(R.drawable.filetype_xls);
        }else if(end.equalsIgnoreCase("doc")||end.equalsIgnoreCase("docx")){
            downloadInfo.setImgUrl(R.drawable.filetype_doc);
        }else if(end.equalsIgnoreCase("pdf")){
            downloadInfo.setImgUrl(R.drawable.filetype_pdf);
        }else if(end.equalsIgnoreCase("rar")){
            downloadInfo.setImgUrl(R.drawable.filetype_rar);
        }else if (end.equalsIgnoreCase("zip")){
            downloadInfo.setImgUrl(R.drawable.filetype_zip);
        }else if(end.equalsIgnoreCase("html")){
            downloadInfo.setImgUrl(R.drawable.filetype_html);
        }else{
            downloadInfo.setImgUrl(R.drawable.filetype_ics);
        }

    }
    public synchronized void startDownload(String fileId,String groupId,String url,
//                                           String label, String savePath, boolean autoResume, boolean autoRename,
                                           DownloadViewHolder viewHolder) throws DbException {

//        String fileSavePath = new File(savePath).getAbsolutePath();
        DownloadInfos downloadInfo = db.selector(DownloadInfos.class)
                .where("fileId", "=", fileId)
                .and("groupId", "=", groupId)
                .findFirst();
        if (downloadInfo != null) {
            DownloadCallback callback = callbackMap.get(downloadInfo);
            if (callback != null) {
                if (viewHolder == null) {
                    viewHolder = new DefaultDownloadViewHolder(null, downloadInfo);
                }
                if (callback.switchViewHolder(viewHolder)) {
                    return;
                } else {
                    callback.cancel();
                }
            }
        }

         viewHolder.update(downloadInfo);
        DownloadCallback callback = new DownloadCallback(viewHolder);
        callback.setDownloadManager(this);
        callback.switchViewHolder(viewHolder);
        RequestParams params = new RequestParams(url);
        params.setAutoResume(downloadInfo.isAutoResume());
        params.setAutoRename(downloadInfo.isAutoRename());
        params.setSaveFilePath(downloadInfo.getFileSavePath());
        params.setExecutor(executor);
        params.setCancelFast(true);
        Callback.Cancelable cancelable = x.http().get(params, callback);
        callback.setCancelable(cancelable);
        callbackMap.put(downloadInfo, callback);
    }

    public void stopDownload(int index) {
        DownloadInfos downloadInfo = downloadInfoList.get(index);
        stopDownload(downloadInfo);
    }

    public void stopDownload(DownloadInfos downloadInfo) {
        Callback.Cancelable cancelable = callbackMap.get(downloadInfo);
        if (cancelable != null) {
            cancelable.cancel();
        }
    }

    public void stopAllDownload() {
        for (DownloadInfos downloadInfo : downloadInfoList) {
            Callback.Cancelable cancelable = callbackMap.get(downloadInfo);
            if (cancelable != null) {
                cancelable.cancel();
            }
        }
    }

    public void removeDownload(int index) throws DbException {
        DownloadInfos downloadInfo = downloadInfoList.get(index);
        db.delete(downloadInfo);
        stopDownload(downloadInfo);
        downloadInfoList.remove(index);
    }

    public void removeDownload(DownloadInfos downloadInfo) throws DbException {
        db.delete(downloadInfo);
        stopDownload(downloadInfo);
        downloadInfoList.remove(downloadInfo);
    }
}
