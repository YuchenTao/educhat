package com.gruden.education.activity.classmenu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.SignListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.UserSign;
import com.loopj.android.http.RequestParams;

import net.datafans.android.common.helper.DipHelper;
import net.datafans.android.timeline.view.imagegrid.ImageGridView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class HomeworkTeacherActivity extends BaseActivity {


    @Bind(R.id.sign_list_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.listView)
    ListView listView;

    private View headerView;
    private FrameLayout layoutContent;
    private TextView txtSubject;
    private TextView txtDate;
    private TextView txtContent;

    private TextView txtSigned;
    private TextView txtUnsigned;
    private ImageView imgSigned;
    private ImageView  imgUnsigned;

    private  View layoutSigned;
    private  View layoutUnsigned;
    private Button btnSendSms;

    private ImageGridView imageGridView;

    private String id;
    private String conversationId;

    private SignListAdapter listAdapter;
    private JSONArray signedIds = new JSONArray();
    private ArrayList<UserSign> signedList = new ArrayList<UserSign>();
    private ArrayList<UserSign> unsignedList = new ArrayList<UserSign>();

    private String strPhoneNums;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_teacher);

        initActionBar("作业详情");
        showLeftBackButton();

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        conversationId = intent.getStringExtra("conversationId");

        headerView =  LayoutInflater.from(this).inflate(R.layout.homework_teacher_header, listView, false);
        txtSubject = (TextView) headerView.findViewById(R.id.txtSubject);
        txtDate = (TextView) headerView.findViewById(R.id.txtDate);
        txtContent = (TextView) headerView.findViewById(R.id.txtContent);
        layoutContent = (FrameLayout) headerView.findViewById(R.id.layoutContent);
        txtSigned = (TextView) headerView.findViewById(R.id.txtSigned);
        txtUnsigned = (TextView) headerView.findViewById(R.id.txtUnsigned);
        imgSigned = (ImageView) headerView.findViewById(R.id.imgSigned);
        imgUnsigned = (ImageView) headerView.findViewById(R.id.imgUnsigned);
        btnSendSms = (Button) headerView.findViewById(R.id.btnSendSms);
        layoutSigned = headerView.findViewById(R.id.layoutSigned);
        layoutUnsigned = headerView.findViewById(R.id.layoutUnsigned);

        layoutSigned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signedOnclick();
            }
        });

        layoutUnsigned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("unsignedOnclick");
                unsignedOnclick();
            }
        });

        btnSendSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSms();
            }
        });


        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        Display display = wm.getDefaultDisplay();
        display.getSize(size);
        int width = size.x;
        float scale = DipHelper.px2dip(this, width) / (float) DipHelper.px2dip(this, 1080);
        imageGridView = new ImageGridView(this, DipHelper.dip2px(this, scale * 240));
        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutContent.addView(imageGridView, imageParams);

        listView.addHeaderView(headerView);
        listAdapter = new SignListAdapter(this);
        listView.setAdapter(listAdapter);

        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                updateSignedList(1);
            }


        });

        getHomeworkDetail();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }

    private void sendSms(){
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"+strPhoneNums));
        intent.putExtra("sms_body", txtContent.getText().toString());
        startActivity(intent);
    }

    private void signedOnclick(){

        imgSigned.setVisibility(View.VISIBLE);
        imgUnsigned.setVisibility(View.GONE);
        listAdapter.isSigned = true;
        listAdapter.setDataList(signedList);
        listAdapter.notifyDataSetChanged();
    }

    private void unsignedOnclick(){

        System.out.println("unsignedOnclick");
        imgSigned.setVisibility(View.GONE);
        imgUnsigned.setVisibility(View.VISIBLE);
        listAdapter.isSigned = false;
        listAdapter.setDataList(unsignedList);
        listAdapter.notifyDataSetChanged();
    }

    void getHomeworkDetail(){
        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("id",id);
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_MESSAGE+"homework";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0){

                    toast("请求失败");

                }else {
                    JSONObject object = array.optJSONObject(0);
                    txtSubject.setText(object.optString("subject"));
                    txtDate.setText(object.optString("endDate"));
                    txtContent.setText(object.optString("content"));


                    JSONArray arrUrls = object.optJSONArray("imgUrl");

                    if (arrUrls!=null&&arrUrls.length()>0){

                        List<String> listUrls = new ArrayList<String>();
                        for (int i=0; i<arrUrls.length(); i++){
                            listUrls.add(arrUrls.optString(i));
                        }
                        imageGridView.updateWithImage(listUrls);

                    }else {
                        layoutContent.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                refreshLayout.finishRefresh();
                toast( mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                refreshLayout.finishRefresh();
                super.handleFailure(error);
            }

        });
    }

    void updateSignedList(int type){
        RequestParams params = new RequestParams();
        params.put("objId",id);
        params.put("groupId",conversationId);
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("type",type);
        if (type==0)
            params.put("signedId",signedIds);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_MESSAGE+"sign";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {


            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {

                signedList.clear();

                JSONArray signedMembers = data.optJSONArray("signedMembers");
                if (signedMembers==null||signedMembers.length()==0){
                    UserSign sign = new UserSign();
                    sign.isNone = true;
                    signedList.add(sign);

                }else {
                    for (int i=0; i<signedMembers.length(); i++){
                        JSONObject object = signedMembers.optJSONObject(i);
                        UserSign sign = new UserSign();
                        sign.userId = object.optString("userId");
                        sign.remarkName = object.optString("remarkName");
                        sign.avatar = object.optString("avatar");
                        sign.phone = object.optString("phone");
                        sign.createdAt = object.optString("createdAt");
                        signedList.add(sign);
                    }
                }

                signedIds = data.optJSONArray("signedIds");

                txtSigned.setText(signedIds.length()+"");
                txtUnsigned.setText(data.optInt("notSignedCount")+"");

                signedOnclick();

                updateSignedList(0);
            }

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                unsignedList.clear();
                strPhoneNums = "";
                if (array==null||array.length()==0){
                    btnSendSms.setEnabled(false);
                    UserSign sign = new UserSign();
                    sign.isNone = true;
                    unsignedList.add(sign);

                }else {
                    for (int i=0; i<array.length(); i++){
                        btnSendSms.setEnabled(true);
                        JSONObject object = array.optJSONObject(i);
                        UserSign sign = new UserSign();
                        sign.userId = object.optString("userId");
                        sign.remarkName = object.optString("remarkName");
                        sign.avatar = object.optString("avatar");
                        sign.phone = object.optString("phone");
                        unsignedList.add(sign);
                        strPhoneNums = strPhoneNums + object.optString("phone");
                        if (i!=(array.length()-1))
                            strPhoneNums = strPhoneNums + ";";
                    }
                }

                dismissSpinnerDialog();
                refreshLayout.finishRefresh();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                refreshLayout.finishRefresh();
                toast( mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                refreshLayout.finishRefresh();
                super.handleFailure(error);

            }

        });
    }
}
