package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.ReportParentListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Child;
import com.gruden.education.model.Exam;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.Subject;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;

public class ReportCardParentActivity extends BaseActivity {

    int curPage;
    String conversationId;

    @Bind(R.id.tabHost)
    TabHost tabHost;

    ArrayList<String> arrStudentCode = new ArrayList<String>();
    ArrayList<ReportParentListAdapter> arrAdapter = new ArrayList<ReportParentListAdapter>();
    ArrayList<MaterialRefreshLayout> arrRefresh= new ArrayList<MaterialRefreshLayout>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_card_parent);

        initActionBar("成绩单");
        showLeftBackButton();

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");
        tabHost.setup();
        initTabHost();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private void initTabHost(){
        LeanchatUser user = LeanchatUser.getCurrentUser();
        List <Child>listChildren = user.getChildren();
        if (listChildren!=null&&listChildren.size()>0){

            LayoutInflater inflater = LayoutInflater.from(this);
            for (int i=0; i<listChildren.size(); i++) {

                final View tab = inflater.inflate(R.layout.topic_tab_spec, null);
                TextView txtTitle = (TextView) tab.findViewById(R.id.txtTitle);
                txtTitle.setText(listChildren.get(i).name);

                final ListView listView = new ListView(this);
                listView.setId(100 + i);
                listView.setDivider(null);
                ReportParentListAdapter listAdapter = new ReportParentListAdapter(this);
                listView.setAdapter(listAdapter);

                MaterialRefreshLayout refreshLayout = new MaterialRefreshLayout(this);
                refreshLayout.setId(200 + i);
                refreshLayout.setProgressColors(getResources().getIntArray(R.array.progress_colors));
                refreshLayout.setLoadMore(true);

                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                refreshLayout.addView(listView, params);

                arrStudentCode.add(listChildren.get(i).studentCode);

                refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
                    @Override
                    public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                        //下拉刷新...
                        updateReportList(0,arrStudentCode.get(tabHost.getCurrentTab()));

                    }

                    @Override
                    public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                        //上拉刷新...
                        updateReportList(curPage + 1,arrStudentCode.get(tabHost.getCurrentTab()));
                    }
                });

                tabHost.getTabContentView().addView(refreshLayout, params);
                tabHost.addTab(tabHost.newTabSpec("Tab" + i).setIndicator(tab).setContent(refreshLayout.getId()));

                arrAdapter.add(listAdapter);
                arrRefresh.add(refreshLayout);

                updateReportList(0,listChildren.get(i).studentCode);
            }

        }else {

                toast( "您还未添加孩子");
            }



    }
    void updateReportList(final int page, String studentCode){
        final int curTab = tabHost.getCurrentTab();
        if (studentCode==null){
            arrRefresh.get(curTab).finishRefresh();
            toast( "您还没有添加子女信息");
            return;
        }
        RequestParams params = new RequestParams();
        params.put("groupId",conversationId);
        params.put("studentCode",studentCode);
        params.put("curPage",page);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"score";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (page == 0) {
                    arrRefresh.get(curTab).finishRefresh();
                }else {
                    arrRefresh.get(curTab).finishRefreshLoadMore();
                }


                if (array.length()==0){

                    if(page==0)
                        toast("暂时还未上传任何成绩");
                    else
                        toast("已经没有更多报告了~");

                }else {
                    ArrayList<Exam> examList = new ArrayList<Exam>();

                    for (int i=0; i<array.length(); i++){

                        JSONObject data = array.optJSONObject(i);
                        Exam exam = new Exam();
                        exam.examCode = data.optString("examCode");
                        exam.examName = data.optString("examName");
                        exam.total = data.optInt("total");

                        JSONArray arrSubject = data.optJSONArray("data");
                        for (int j=0; j<arrSubject.length(); j++){
                            Subject subject = new Subject();
                            subject.name = arrSubject.optJSONObject(j).optString("subject");
                            subject.score = arrSubject.optJSONObject(j).optInt("score");
                            exam.subjectList.add(subject);
                        }

                        examList.add(exam);
                    }

                    ReportParentListAdapter listAdapter = arrAdapter.get(curTab);
                    if (page==0) {
                        listAdapter.setDataList(examList);
                        listAdapter.notifyDataSetChanged();
                    }
                    else {

                        listAdapter.addDataList(examList);
                        listAdapter.notifyDataSetChanged();
                    }

                    curPage = page;
                }


            }

            @Override
            public void handleSuccess(int code, String mesg) {
                if (page == 0) {
                    arrRefresh.get(curTab).finishRefresh();
                }else {
                    arrRefresh.get(curTab).finishRefreshLoadMore();
                }

                toast( mesg);

            }

            @Override
            public void handleFailure(int error) {
                if (page == 0) {
                    arrRefresh.get(curTab).finishRefresh();
                }else {
                    arrRefresh.get(curTab).finishRefreshLoadMore();
                }
                super.handleFailure(error);
            }

        });
    }



}
