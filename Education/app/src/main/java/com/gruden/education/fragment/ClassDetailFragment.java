package com.gruden.education.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.callback.AVIMConversationQueryCallback;
import com.avos.avoscloud.im.v2.callback.AVIMSingleMessageQueryCallback;
import com.avoscloud.leanchatlib.controller.ChatManager;
import com.avoscloud.leanchatlib.controller.ConversationHelper;
import com.avoscloud.leanchatlib.event.ConnectionChangeEvent;
import com.avoscloud.leanchatlib.event.ImTypeMessageEvent;
import com.avoscloud.leanchatlib.model.ConversationType;
import com.avoscloud.leanchatlib.model.Room;
import com.avoscloud.leanchatlib.utils.Constants;
import com.avoscloud.leanchatlib.utils.ConversationManager;
import com.gruden.education.App;
import com.gruden.education.R;
import com.gruden.education.activity.MainActivity;
import com.gruden.education.activity.conversation.ChatRoomActivity;
import com.gruden.education.adapter.ConversationListAdapter;
import com.gruden.education.customview.ClassMenu1;
import com.gruden.education.customview.ClassMenu2;
import com.gruden.education.customview.IndentItemDecoration;
import com.gruden.education.event.ClassDetailItemClickEvent;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.UserCacheUtils;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;


public class ClassDetailFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final int QUIT_GROUP_REQUEST = 200;

    // TODO: Rename and change types of parameters
    private String classConversationId;
    private String classConversationName;

    public ClassMenu1 class_menu1;
    public ClassMenu2 class_menu2;

    private List<Fragment> fragmentList;

    @Bind(R.id.im_client_state_view)
    View imClientStateView;

    @Bind(R.id.class_detail_srl_pullrefresh)
    protected SwipeRefreshLayout refreshLayout;

    @Bind(R.id.class_detail_srl_view)
    protected RecyclerView recyclerView;

    @Bind(R.id.viewpager)
    protected ViewPager viewPager;

    @Bind(R.id.page_indicator)
    protected CirclePageIndicator mIndicator;

    protected ConversationListAdapter<Room> itemAdapter;
    protected LinearLayoutManager layoutManager;

    public boolean hidden;
    private ConversationManager conversationManager;
    MyFragmentPageAdapter pagerAdapter;


    // TODO: Rename and change types and number of parameters
    public static ClassDetailFragment newInstance(String classConversationId, String classConversationName) {
        ClassDetailFragment fragment = new ClassDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, classConversationId);
        args.putString(ARG_PARAM2, classConversationName);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            classConversationId = getArguments().getString(ARG_PARAM1);
            classConversationName = getArguments().getString(ARG_PARAM2);
        }
//        View classMenu1=LayoutInflater.from(getContext()).inflate(R.layout.class_menu1,viewPager);
//        View layout_notice= classMenu1.findViewById(R.id.layout_notice);
//        View layout_leave=classMenu1.findViewById(R.id.layout_leave);
//        View layout_homework=classMenu1.findViewById(R.id.layout_homework);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.class_detail_fragment, container,false);

        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);

        conversationManager = ConversationManager.getInstance();
        refreshLayout.setEnabled(false);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new IndentItemDecoration(getActivity()));
        itemAdapter = new ConversationListAdapter<Room>();
        recyclerView.setAdapter(itemAdapter);


        class_menu1 = new  ClassMenu1();
        class_menu1.conversationId = classConversationId;
        class_menu1.conversationName = classConversationName;

        class_menu2 = new  ClassMenu2();
        class_menu2.conversationId = classConversationId;

        fragmentList = new ArrayList<Fragment>();
        fragmentList.add(class_menu1);
        fragmentList.add(class_menu2);

        pagerAdapter = new MyFragmentPageAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);

//        mIndicator = (CirclePageIndicator)view.findViewById(R.id.page_indicator);
        mIndicator.setViewPager(viewPager);


        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        headerLayout.showTitle(classConversationName);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();

                transaction.remove(App.ctx.mainActivity.classDetailFragment);
                transaction.show(App.ctx.mainActivity.classFragment);
                transaction.commit();

                App.ctx.mainActivity.classDetailFragment = null;
            }
        };
        headerLayout.showLeftBackButton(listener);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
//        Log.e("resultCode",resultCode+"");
//        if (resultCode ==QUIT_GROUP_REQUEST){
//            FragmentManager manager = getActivity().getSupportFragmentManager();
//            manager.popBackStack(MainActivity.FRAGMENT_TAG_CLASS_DETAIL,FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//            ((MainActivity)getActivity()).classDetailFragment = null;
//        }

    }



    public class MyFragmentPageAdapter extends FragmentPagerAdapter {



        public MyFragmentPageAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public Fragment getItem(int position) {

            return fragmentList.get(position);
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        this.hidden = hidden;
        if (!hidden) {
//            updateConversationList();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateConversationList();

    }

    private void initConversationList() {
        conversationManager.findAllConversationsIncludeMe(new AVIMConversationQueryCallback() {
            @Override
            public void done(List<AVIMConversation> conversations, AVIMException e) {
                Log.e("initConversationList","initConversationList");
                if (filterException(e)) {

                    for (AVIMConversation c : conversations){
                        Log.e("ConversationId",c.getConversationId());
                        ChatManager.getInstance().getRoomsTable().insertRoom(c.getConversationId(),Integer.parseInt(c.getAttribute("type").toString()));
                    }
                    updateConversationList();
                }
            }
        });


    }

    private void updateConversationList() {
        conversationManager.findAndCacheRoomById(classConversationId, new Room.SingleRoomCallback() {
            @Override
            public void done(Room room, AVException exception) {
                if (filterException(exception)) {
                    final List<Room> roomList = new ArrayList<>();
                    if (room==null){

                        FragmentManager manager = getActivity().getSupportFragmentManager();
                        manager.popBackStack(MainActivity.FRAGMENT_TAG_CLASS_DETAIL,FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        ((MainActivity)getActivity()).classDetailFragment = null;
                        return;

                    }

                    headerLayout.showTitle(room.getConversation().getName());
                    roomList.add(room);
                    updateLastMessage(roomList);
                    cacheRelatedUsers(roomList);
                    setItemType(roomList);

                    List<Room> sortedRooms = sortRooms(roomList);
                    itemAdapter.setDataList(sortedRooms);
                    itemAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    private void updateLastMessage(final List<Room> roomList) {
        for (final Room room : roomList) {
            AVIMConversation conversation = room.getConversation();
            if (null != conversation) {
                conversation.getLastMessage(new AVIMSingleMessageQueryCallback() {
                    @Override
                    public void done(AVIMMessage avimMessage, AVIMException e) {
                        if (filterException(e) && null != avimMessage) {
                            room.setLastMessage(avimMessage);
                            int index = roomList.indexOf(room);
                            itemAdapter.notifyItemChanged(index);
                        }
                    }
                });
            }
        }
    }

    private void cacheRelatedUsers(List<Room> rooms) {
        List<String> needCacheUsers = new ArrayList<String>();
        for(Room room : rooms) {
            AVIMConversation conversation = room.getConversation();
            if (ConversationHelper.typeOfConversation(conversation) == ConversationType.Private) {
                needCacheUsers.add(ConversationHelper.otherIdOfConversation(conversation));
            }
        }
        UserCacheUtils.fetchUsers(needCacheUsers, new UserCacheUtils.CacheUserCallback() {
            @Override
            public void done(List<LeanchatUser> userList, Exception e) {
                itemAdapter.notifyDataSetChanged();
            }
        });
    }

    private List<Room> sortRooms(final List<Room> roomList) {
        List<Room> sortedList = new ArrayList<Room>();
        if (null != roomList) {
            sortedList.addAll(roomList);
            Collections.sort(sortedList, new Comparator<Room>() {
                @Override
                public int compare(Room lhs, Room rhs) {
                    long value = lhs.getLastModifyTime() - rhs.getLastModifyTime();
                    if (value > 0) {
                        return -1;
                    } else if (value < 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            });
        }
        return sortedList;
    }

    private void setItemType(final List<Room> roomList) {
        for (Room room : roomList) {
            room.itemType = 2;
        }
    }

    public void onEvent(ConnectionChangeEvent event) {
        if(imClientStateView.getVisibility()==View.VISIBLE||(!event.isConnect)){
            updateConversationList();
        }
        imClientStateView.setVisibility(event.isConnect ? View.GONE : View.VISIBLE);
    }

    public void onEvent(ClassDetailItemClickEvent event) {
        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
        intent.putExtra(Constants.CONVERSATION_ID, event.conversationId);
        startActivity(intent);
    }

//    public void onEvent(QuitGroupEvent event) {
//
//        Log.e("QuitGroupEvent","QuitGroupEvent");
//
//        FragmentManager manager = getActivity().getSupportFragmentManager();
//        manager.popBackStack(MainActivity.FRAGMENT_TAG_CLASS_DETAIL,FragmentManager.POP_BACK_STACK_INCLUSIVE;
//
//        ((MainActivity)getActivity()).classDetailFragment = null;
//    }

    public void onEvent(ImTypeMessageEvent event) {
        updateConversationList();
    }

}
