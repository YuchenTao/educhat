package com.gruden.education.activity.discover.contact;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import butterknife.Bind;

public class RemarkNameEditActivity extends BaseActivity {

    @Bind(R.id.editText)
    EditText editText;

    private String id;
    private String conversationId;
    private int identity;
    private boolean isFriend;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remark_name_edit);
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        conversationId = intent.getStringExtra("groupId");
        identity = intent.getIntExtra("identity",-1);
        isFriend = intent.getBooleanExtra("isFriend",false);
        initActionBar("备注名");
        showLeftBackButton("取消");
        showRightTextButton("保存", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (conversationId==null){
                    updateRemarkName();

                }else{
                    updateGroupRemarkName();
                }

            }
        } );



    }

    void updateRemarkName(){

        final String strRemarkName = editText.getText().toString();

        if (!(identity>0||isFriend)){
            //数据是使用Intent返回
            Intent intent = new Intent();
            //把返回数据存入Intent
            intent.putExtra("remarkName", strRemarkName);
            //设置返回数据
            setResult(RESULT_OK, intent);
            //关闭Activity
            finish();
            return;
        }

        if (TextUtils.isEmpty(strRemarkName)) {
            Utils.toast(this,"备注名不能为空");
            return;
        }

        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("selfId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("friendId", id);
        params.put("remarkName", strRemarkName);


        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "friends/changeRemarkName";
        MyHttpClient.put(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg) {

                dismissSpinnerDialog();
                switch (code){
                    case 0:{
                        toast(mesg);
                        //数据是使用Intent返回
                        Intent intent = new Intent();
                        //把返回数据存入Intent
                        intent.putExtra("remarkName", strRemarkName);
                        //设置返回数据
                        setResult(RESULT_OK, intent);
                        //关闭Activity
                        finish();

                        break;
                    }

                    default:{
                        toast(mesg);
                        break;
                    }
                }
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });

    }
    void updateGroupRemarkName(){

        final String strRemarkName = editText.getText().toString();

        if (!(identity>0||isFriend)){
            //数据是使用Intent返回
            Intent intent = new Intent();
            //把返回数据存入Intent
            intent.putExtra("remarkName", strRemarkName);
            //设置返回数据
            setResult(RESULT_OK, intent);
            //关闭Activity
            finish();
            return;
        }

        if (TextUtils.isEmpty(strRemarkName)) {
            Utils.toast(this,"备注名不能为空");
            return;
        }

        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("groupId",conversationId);
        params.put("selfId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("userId", id);
        params.put("remarkName", strRemarkName);


        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_REALTIME + "members/changeRemarkName";
        MyHttpClient.put(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg) {

                dismissSpinnerDialog();
                switch (code){
                    case 0:{
                        toast(mesg);
                        //数据是使用Intent返回
                        Intent intent = new Intent();
                        //把返回数据存入Intent
                        intent.putExtra("remarkName", strRemarkName);
                        //设置返回数据
                        setResult(RESULT_OK, intent);
                        //关闭Activity
                        finish();

                        break;
                    }

                    default:{
                        toast(mesg);
                        break;
                    }
                }
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });

    }
}
