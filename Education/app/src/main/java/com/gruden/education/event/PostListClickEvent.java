package com.gruden.education.event;

/**
 * Created by yunjiansun on 15/12/31.
 */
public class PostListClickEvent {

    public String tagCode;
    public String topicTitle;

    public PostListClickEvent(){

    }

    public PostListClickEvent(String topicTitle, String tagCode){
        this.topicTitle = topicTitle;
        this.tagCode = tagCode;
    }
}
