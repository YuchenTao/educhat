package com.gruden.education.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.im.v2.AVIMClient;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMClientCallback;
import com.gruden.education.App;
import com.gruden.education.event.ClassItemClickEvent;
import com.gruden.education.R;
import com.gruden.education.event.PostListClickEvent;
import com.gruden.education.event.LogoutEvent;
import com.gruden.education.event.TopicSquareClickEvent;
import com.gruden.education.fragment.BBSFragment;
import com.gruden.education.fragment.ClassDetailFragment;
import com.gruden.education.fragment.ClassFragment;
import com.gruden.education.fragment.PostListFragment;
import com.gruden.education.fragment.TopicSquareFragment;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.event.LoginFinishEvent;
import com.gruden.education.fragment.MessageFragment;
import com.gruden.education.fragment.DiscoverFragment;
import com.gruden.education.fragment.ProfileFragment;
import com.gruden.education.util.UserCacheUtils;
import com.avoscloud.leanchatlib.controller.ChatManager;

import de.greenrobot.event.EventBus;
/**
 * Created by lzw on 14-9-17.
 */
public class MainActivity extends BaseActivity {
  public static final int FRAGMENT_N = 4;

  public static final int[] tabsNormalBackIds = new int[]{R.drawable.tabbar_chat,
          R.drawable.tabbar_contacts, R.drawable.tabbar_discover, R.drawable.tabbar_me};
  public static final int[] tabsActiveBackIds = new int[]{R.drawable.tabbar_chat_active,
          R.drawable.tabbar_contacts_active, R.drawable.tabbar_discover_active,
          R.drawable.tabbar_me_active};

  public static final String FRAGMENT_TAG_CLASS = "class";
  public static final String FRAGMENT_TAG_CLASS_DETAIL = "class_detail";
  public static final String FRAGMENT_TAG_MESSAGE = "message";
  public static final String FRAGMENT_TAG_BBS = "bbs";
  public static final String FRAGMENT_TAG_BBS_HOT = "bbs_hot";
  public static final String FRAGMENT_TAG_BBS_TOPIC = "bbs_topic";
  public static final String FRAGMENT_TAG_DISCOVER = "discover";
  public static final String FRAGMENT_TAG_PROFILE = "profile";
  private static final String[] fragmentTags = new String[]{
          FRAGMENT_TAG_CLASS,
          FRAGMENT_TAG_MESSAGE,
          FRAGMENT_TAG_BBS,
          FRAGMENT_TAG_DISCOVER,
          FRAGMENT_TAG_PROFILE,
          FRAGMENT_TAG_CLASS_DETAIL ,
          FRAGMENT_TAG_BBS_HOT,
          FRAGMENT_TAG_BBS_TOPIC};

  public  String FRAGMENT_TAG_CURRENT;

  private String [] permissions = {
          Manifest.permission.READ_EXTERNAL_STORAGE,
          Manifest.permission.WRITE_EXTERNAL_STORAGE,
          Manifest.permission.CAMERA,
          Manifest.permission.READ_CONTACTS,
          Manifest.permission.CALL_PHONE,
          Manifest.permission.SEND_SMS};


//  public LocationClient locClient;
//  public MyLocationListener locationListener;

  Button btnClass, btnMessage, btnBBS, btnDiscover, btnProfile;
  ImageView imgClass, imgMessage, imgBBS, imgDiscover, imgProfile;
  TextView txtClass, txtMessage, txtBBS, txtDiscover, txtProfile;

  View fragmentContainer;

  public ClassFragment classFragment;
  public ClassDetailFragment classDetailFragment;
  public MessageFragment messageFragment;
  public BBSFragment bbsFragment;
  public DiscoverFragment discoverFragment;
  public ProfileFragment profileFragment;
  public PostListFragment postListFragment;
  public TopicSquareFragment topicSquareFragment;

  Button[] tabs;
  View recentTips, contactTips;

  public static void goMainActivityFromActivity(Activity fromActivity) {
    EventBus eventBus = EventBus.getDefault();
    eventBus.post(new LoginFinishEvent());

    ChatManager chatManager = ChatManager.getInstance();
    chatManager.setupManagerWithUserId(fromActivity, LeanchatUser.getCurrentUserId());
    chatManager.openClient(new AVIMClientCallback() {
      @Override
      public void done(AVIMClient avimClient, AVIMException e) {
        if (e != null){
          Log.e("openClient",e.getMessage());
        }

      }
    });
    Intent intent = new Intent(fromActivity, MainActivity.class);
    fromActivity.startActivity(intent);

//    updateUserLocation();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_activity);
    App.ctx.mainActivity = this;
    findView();
    init();

    btnClass.performClick();
    //btnMessage.performClick();
//    btnBBS.performClick();
    //btnDiscover.performClick();
//    btnMySpace.performClick();

//    initBaiduLocClient();
//
    UserCacheUtils.cacheUser(LeanchatUser.getCurrentUser());
  }

  @Override
  protected void onResume() {
    super.onResume();
//    UpdateService updateService = UpdateService.getInstance(this);
//    updateService.checkUpdate();
  }

//  private void initBaiduLocClient() {
//    locClient = new LocationClient(this.getApplicationContext());
//    locClient.setDebug(true);
//    LocationClientOption option = new LocationClientOption();
//    option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
//    option.setScanSpan(5000);
//    option.setIsNeedAddress(false);
//    option.setCoorType("bd09ll");
//    option.setIsNeedAddress(true);
//    locClient.setLocOption(option);
//
//    locationListener = new MyLocationListener();
//    locClient.registerLocationListener(locationListener);
//    locClient.start();
//  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
//    Log.e("resultCode",resultCode+"");
//    FragmentManager manager = getSupportFragmentManager();
//    for (int i = 0; i < fragmentTags.length; i++) {
//      Fragment fragment = manager.findFragmentByTag(fragmentTags[i]);
//      if (fragment != null ) {
//        fragment.onActivityResult(requestCode, resultCode, data);
//      }
//    }
  }

  @Override
  public void onBackPressed(){

    if((classFragment!=null&&classFragment.isVisible())
            ||(messageFragment!=null&&messageFragment.isVisible())
            ||(bbsFragment!=null&&bbsFragment.isVisible())
            ||(discoverFragment!=null&&discoverFragment.isVisible())
            ||(profileFragment!=null&&profileFragment.isVisible())){

      finish();
      return;
    }

    FragmentManager manager = getSupportFragmentManager();
    FragmentTransaction transaction = manager.beginTransaction();

    if (classDetailFragment!=null&&classDetailFragment.isVisible()) {
      Log.e(FRAGMENT_TAG_CLASS_DETAIL,"onBackPressed");

      transaction.remove(classDetailFragment);
      transaction.show(classFragment);
      transaction.commit();

      classDetailFragment = null;
    }

    else if (postListFragment!=null&& postListFragment.isVisible()) {
      Log.e(FRAGMENT_TAG_BBS_HOT,"onBackPressed");

      transaction.remove(postListFragment);

      if (topicSquareFragment != null){
        transaction.show(topicSquareFragment);
      }else {
        transaction.show(bbsFragment);
      }

      transaction.commit();
      postListFragment = null;
    }

    else if (topicSquareFragment!=null&& topicSquareFragment.isVisible()) {
      Log.e(FRAGMENT_TAG_BBS_TOPIC,"onBackPressed");

      transaction.remove(topicSquareFragment);

      transaction.show(bbsFragment);

      transaction.commit();

      topicSquareFragment = null;
    }

    else
      super.onBackPressed();
  }

  private void init() {
    tabs = new Button[]{btnClass, btnMessage, btnBBS, btnDiscover, btnProfile};

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      checkPermission(permissions);
    }
  }



  private void findView() {
    btnClass = (Button) findViewById(R.id.btn_class);
    btnMessage = (Button) findViewById(R.id.btn_message);
    btnBBS = (Button) findViewById(R.id.btn_bbs);
    btnDiscover = (Button) findViewById(R.id.btn_discover);
    btnProfile = (Button) findViewById(R.id.btn_profile);

    imgClass = (ImageView) findViewById(R.id.img_class);
    imgMessage = (ImageView) findViewById(R.id.img_message);
    imgBBS = (ImageView) findViewById(R.id.img_bbs);
    imgDiscover = (ImageView) findViewById(R.id.img_discover);
    imgProfile = (ImageView) findViewById(R.id.img_profile);

    txtClass = (TextView) findViewById(R.id.txt_calss);
    txtMessage = (TextView) findViewById(R.id.txt_message);
    txtBBS = (TextView) findViewById(R.id.txt_bbs);
    txtDiscover = (TextView) findViewById(R.id.txt_discover);
    txtProfile = (TextView) findViewById(R.id.txt_profile);

    fragmentContainer = findViewById(R.id.fragment_container);

  }

  public void onTabSelect(View v) {
    int id = v.getId();
    setNormalBackgrounds();

    FragmentManager manager = getSupportFragmentManager();
    FragmentTransaction transaction = manager.beginTransaction();
    hideFragments(manager, transaction);

    switch (id){

      case R.id.btn_class:{


        if (classDetailFragment == null){

          if (classFragment == null) {
            classFragment = new ClassFragment();
            transaction.add(R.id.fragment_container, classFragment, FRAGMENT_TAG_CLASS);
//            transaction.addToBackStack(FRAGMENT_TAG_CLASS);
          }

          transaction.show(classFragment);

        }else {
          transaction.show(classDetailFragment);
        }


        imgClass.setSelected(true);
        txtClass.setSelected(true);
        break;
      }

      case R.id.btn_message:{

        if (messageFragment == null) {
          messageFragment = new MessageFragment();
          transaction.add(R.id.fragment_container, messageFragment, FRAGMENT_TAG_MESSAGE);
//          transaction.addToBackStack(FRAGMENT_TAG_MESSAGE);
        }
        transaction.show(messageFragment);

        imgMessage.setSelected(true);
        txtMessage.setSelected(true);
        break;
      }

      case R.id.btn_bbs:{

        imgBBS.setSelected(true);
        txtBBS.setSelected(true);

        if (postListFragment != null){
          transaction.show(postListFragment);
          break;
        }

        if (topicSquareFragment != null){
          transaction.show(topicSquareFragment);
          break;
        }

        if (bbsFragment == null) {
          bbsFragment = new BBSFragment();
          transaction.add(R.id.fragment_container, bbsFragment, FRAGMENT_TAG_BBS);
//          transaction.addToBackStack(FRAGMENT_TAG_BBS);
        }
        transaction.show(bbsFragment);

        break;
      }

      case R.id.btn_discover:{

        if (discoverFragment == null) {
          discoverFragment = new DiscoverFragment();
          transaction.add(R.id.fragment_container, discoverFragment, FRAGMENT_TAG_DISCOVER);
//          transaction.addToBackStack(FRAGMENT_TAG_DISCOVER);
        }
        transaction.show(discoverFragment);

        imgDiscover.setSelected(true);
        txtDiscover.setSelected(true);
        break;
      }

      case R.id.btn_profile:{

        if (profileFragment == null) {
          profileFragment = new ProfileFragment();
          transaction.add(R.id.fragment_container, profileFragment, FRAGMENT_TAG_PROFILE);
//          transaction.addToBackStack(FRAGMENT_TAG_PROFILE);
        }
        transaction.show(profileFragment);

        imgProfile.setSelected(true);
        txtProfile.setSelected(true);
        break;
      }
    }

    transaction.commit();


  }

  private void setNormalBackgrounds() {
    imgClass.setSelected(false);
    imgMessage.setSelected(false);
    imgBBS.setSelected(false);
    imgDiscover.setSelected(false);
    imgProfile.setSelected(false);

    txtClass.setSelected(false);
    txtMessage.setSelected(false);
    txtBBS.setSelected(false);
    txtDiscover.setSelected(false);
    txtProfile.setSelected(false);
  }


  private void hideFragments(FragmentManager fragmentManager, FragmentTransaction transaction) {
    for (int i = 0; i < fragmentTags.length; i++) {
      Fragment fragment = fragmentManager.findFragmentByTag(fragmentTags[i]);
      if (fragment != null && fragment.isVisible()) {
        transaction.hide(fragment);
      }
    }
  }

  public void onEvent(ClassItemClickEvent event) {

    classDetailFragment = ClassDetailFragment.newInstance(event.conversationId,event.conversationName);
    FRAGMENT_TAG_CURRENT = FRAGMENT_TAG_CLASS_DETAIL;
    FragmentManager manager = getSupportFragmentManager();
    FragmentTransaction transaction = manager.beginTransaction();
//    transaction.replace(R.id.fragment_container, classDetailFragment, FRAGMENT_TAG_CLASS_DETAIL);

    transaction.add(R.id.fragment_container, classDetailFragment, FRAGMENT_TAG_CLASS_DETAIL);
    transaction.hide(classFragment);
    transaction.show(classDetailFragment);
    transaction.commit();
  }

  public void onEvent(PostListClickEvent event) {

    postListFragment = new PostListFragment();
    postListFragment.topicTitle = event.topicTitle;
    postListFragment.tagCode = event.tagCode;

    FragmentManager manager = getSupportFragmentManager();
    FragmentTransaction transaction = manager.beginTransaction();
//    transaction.replace(R.id.fragment_container, classDetailFragment, FRAGMENT_TAG_CLASS_DETAIL);

    transaction.add(R.id.fragment_container, postListFragment, FRAGMENT_TAG_BBS_HOT);

    if (event.tagCode==null)
      transaction.hide(bbsFragment);
    else
      transaction.hide(topicSquareFragment);

    transaction.show(postListFragment);
    transaction.commit();
  }

  public void onEvent(TopicSquareClickEvent event) {

    topicSquareFragment = new TopicSquareFragment();

    FragmentManager manager = getSupportFragmentManager();
    FragmentTransaction transaction = manager.beginTransaction();
//    transaction.replace(R.id.fragment_container, classDetailFragment, FRAGMENT_TAG_CLASS_DETAIL);

    transaction.add(R.id.fragment_container, topicSquareFragment, FRAGMENT_TAG_BBS_TOPIC);
    transaction.hide(bbsFragment);
    transaction.show(topicSquareFragment);
    transaction.commit();
  }

  public void onEvent(LogoutEvent event) {
    System.out.println("LogoutEvent");

    this.finish();
  }

//  public static void updateUserLocation() {
//    PreferenceMap preferenceMap = PreferenceMap.getCurUserPrefDao(App.ctx);
//    AVGeoPoint lastLocation = preferenceMap.getLocation();
//    if (lastLocation != null) {
//      final LeanchatUser user = LeanchatUser.getCurrentUser();
//      final AVGeoPoint location = user.getAVGeoPoint(LeanchatUser.LOCATION);
//      if (location == null || !Utils.doubleEqual(location.getLatitude(), lastLocation.getLatitude())
//              || !Utils.doubleEqual(location.getLongitude(), lastLocation.getLongitude())) {
//        user.put(LeanchatUser.LOCATION, lastLocation);
//        user.saveInBackground(new SaveCallback() {
//          @Override
//          public void done(AVException e) {
//            if (e != null) {
//              LogUtils.logException(e);
//            } else {
//              AVGeoPoint avGeoPoint = user.getAVGeoPoint(LeanchatUser.LOCATION);
//              if (avGeoPoint == null) {
//                LogUtils.e("avGeopoint is null");
//              } else {
//                LogUtils.v("save location succeed latitude " + avGeoPoint.getLatitude()
//                        + " longitude " + avGeoPoint.getLongitude());
//              }
//            }
//          }
//        });
//      }
//    }
//  }
//
//  public class MyLocationListener implements BDLocationListener {
//
//    @Override
//    public void onReceiveLocation(BDLocation location) {
//      double latitude = location.getLatitude();
//      double longitude = location.getLongitude();
//      int locType = location.getLocType();
//      LogUtils.d("onReceiveLocation latitude=" + latitude + " longitude=" + longitude
//              + " locType=" + locType + " address=" + location.getAddrStr());
//      String currentUserId = LeanchatUser.getCurrentUserId();
//      if (!TextUtils.isEmpty(currentUserId)) {
//        PreferenceMap preferenceMap = new PreferenceMap(MainActivity.this, currentUserId);
//        AVGeoPoint avGeoPoint = preferenceMap.getLocation();
//        if (avGeoPoint != null && avGeoPoint.getLatitude() == location.getLatitude()
//                && avGeoPoint.getLongitude() == location.getLongitude()) {
//          updateUserLocation();
//          locClient.stop();
//        } else {
//          AVGeoPoint newGeoPoint = new AVGeoPoint(location.getLatitude(),
//                  location.getLongitude());
//          if (newGeoPoint != null) {
//            preferenceMap.setLocation(newGeoPoint);
//          }
//        }
//      }
//    }
//  }
}
