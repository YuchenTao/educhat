package com.gruden.education.activity.entry;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.gruden.education.R;
import com.gruden.education.activity.MainActivity;
import com.gruden.education.model.LeanchatUser;

import butterknife.Bind;

/**
 * Created by admin on 2016/4/22.
 */
public class EntryContactsMatchActivity extends EntryBaseActivity {

    @Bind(R.id.btn_openContacts)
    Button btn_openContacts;
    @Bind(R.id.btn_closeContacts)
    Button btn_closeContacts;
    LeanchatUser user=LeanchatUser.getCurrentUser();

//    ArrayList<Map<String,String>> phoneList=new ArrayList<Map<String,String>>();
//    ArrayList<String> phone=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_contacts_match_activity);
        initActionBar("通讯录匹配");
        showLeftBackButton();
        btn_openContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setMatching(true);
                LeanchatUser.changeCurrentUser(user, true);
                Intent intent=new Intent(EntryContactsMatchActivity.this,EntryContactsActivity.class);
//                intent.putExtra("phoneList",phoneList);
//                intent.putExtra("phone",phone);
                startActivity(intent);
            }
        });
        btn_closeContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setMatching(false);
                LeanchatUser.changeCurrentUser(user, true);
                MainActivity.goMainActivityFromActivity(EntryContactsMatchActivity.this);
            }
        });
    }


}
