package com.gruden.education.activity.conversation;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by admin on 2016/5/5.
 */
public class InviteNewMember extends BaseActivity {
    @Bind(R.id.phoneEdit)
    EditText phoneEdit;
    @Bind(R.id.remarkNameEdit)
    EditText remarkNameEdit;
    @Bind(R.id.status_radiogroup)
    RadioGroup status_radiogroup;
    @Bind(R.id.radioParent)
    RadioButton radioParent;
    @Bind(R.id.radioTeacher)
    RadioButton radioTeacher;

    private String conversationId;
    private String className;
    private String code;
    private int type;
    private int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_new_member);
        initActionBar("邀请新成员");
        showLeftBackButton();
        Intent intent=getIntent();
        conversationId=intent.getStringExtra("groupId");
        className=intent.getStringExtra("name");
        code=intent.getStringExtra("code");
        type=intent.getIntExtra("type",0);
        status=intent.getIntExtra("status",0);
        if (status==2){
            radioParent.setChecked(true);
        }else{
            radioTeacher.setChecked(true);
        }

    }

    @OnClick(R.id.btnRegister)
    public void onRegisterClick(){
        String phone=phoneEdit.getText().toString();
        if (TextUtils.isEmpty(phone)){
            toast("手机号不能为空~");
            return;
        }
       String nickname=remarkNameEdit.getText().toString();
        if (TextUtils.isEmpty(nickname)){
            toast("备注名不能为空~");
            return;
        }
        showSpinnerDialog();
        status_radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId==R.id.radioParent){
                    status=2;
                }else{
                    status=1;
                }
                toast(status+"");
            }
        });
        LeanchatUser user = LeanchatUser.getCurrentUser();
        RequestParams params = new RequestParams();
        params.put("groupId",conversationId);
        params.put("type",type);
        params.put("code",code);
        params.put("name",className);
        params.put("nickname",LeanchatUser.getCurrentUser().getNickName());
        params.put("member",phone+"::"+nickname+"::"+status);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME + "members/invite";

        MyHttpClient.post(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg) {

                dismissSpinnerDialog();
                toast(mesg);
                finish();

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });

    }
}
