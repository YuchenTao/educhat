package com.gruden.education.model;

/**
 * Created by yunjiansun on 16/1/12.
 */
public class Notice {
    public String id;
    public  String conversationId;
    public String content;
    public String groupId;
    public boolean isSign;
    public String createdAt;
    public String creator;
    public boolean isCreatedByMe;
}
