package com.gruden.education.activity.discover.friendcircle.mvp.view;

import com.gruden.education.activity.discover.friendcircle.bean.CommentConfig;
import com.gruden.education.activity.discover.friendcircle.bean.CommentItem;
import com.gruden.education.activity.discover.friendcircle.bean.FavortItem;
/**
 * 
* @ClassName: ICircleViewUpdateListener 
* @Description: view,服务器响应后更新界面 
* @author YuchenTao
* @date 2015-12-28 下午4:13:04 
*
 */
public interface ICircleView {

	public void update2DeleteCircle(String circleId);
	public void update2AddFavorite(int circlePosition, FavortItem addItem);
	public void update2DeleteFavort(int circlePosition, String favortId);
	public void update2AddComment(CommentConfig config, CommentItem addItem);
	public void update2DeleteComment(int circlePosition, int commentPosition, String commentId);

	public void updateEditTextBodyVisible(int visibility, CommentConfig commentConfig);

	public void avatarClickEvent(String friendId);
}
