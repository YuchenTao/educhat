package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.FoodListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Blog;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

public class FoodListActivity extends BaseActivity {

    private int curPage;
    private String conversationId;

    private boolean isRefreshing;
    private boolean isLoadMoreing;

    private FoodListAdapter listAdapter;

    @Bind(R.id.food_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.listView)
    ListView listView;

    @Bind(R.id.txtPrompt)
    TextView txtPrompt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");

        initActionBar("食谱");
        showLeftBackButton();
        LeanchatUser user = LeanchatUser.getCurrentUser();
        if(user.getRoles().contains(LeanchatUser.TEACHER)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN)) {
            showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FoodListActivity.this, PublishFoodActivity.class);
                    intent.putExtra("id", conversationId);
                    startActivity(intent);
                }
            });
        }

        listAdapter = new FoodListAdapter(this);
        listView.setAdapter(listAdapter);

        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                updateBlogList(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                updateBlogList(curPage+1);
            }

            @Override
            public void onfinish(){
                txtPrompt.setVisibility(listAdapter.getCount()>0?View.INVISIBLE:View.VISIBLE);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }

    void updateBlogList(final int page){
        RequestParams params = new RequestParams();
        params.put("groupId",conversationId);
        params.put("curPage",page);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_MESSAGE +"food";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0&&page>0){

//                    toast("已经没有更多食谱了~");

                }else {
                    ArrayList<Blog> blogList = new ArrayList<Blog>();

                    for (int i=0; i<array.length(); i++){

                        JSONObject data = array.optJSONObject(i);
                        Blog blog1 = new Blog();
                        blog1.date = data.optString("date") + " " + data.optString("weekDay");
                        blogList.add(blog1);

                        JSONArray arr = data.optJSONArray("data");
                        for (int j=0; j<arr.length(); j++){

                            JSONObject object = arr.optJSONObject(j);
                            Blog blog2 = new Blog();
                            blog2.id = object.optString("id");
                            blog2.title = object.optString("type");
                            blog2.content = object.optString("content");
                            JSONArray arrUrl = object.optJSONArray("imgUrl");

                            if (arrUrl!=null&&arrUrl.length()>0)
                                blog2.imgUrl = arrUrl.optString(0);

                            blog2.createAt = object.optString("time");
                            if (j==arr.length()-1)
                                blog2.isBottom = true;
                            blogList.add(blog2);
                        }
                    }

                    if (page==0) {
                        listAdapter.setDataList(blogList);
                    }
                    else {
                        listAdapter.addDataList(blogList);
                    }

                    listAdapter.notifyDataSetChanged();
                    curPage = page;
                }

                dissmissCircle();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }

        });

    }

    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }


}
