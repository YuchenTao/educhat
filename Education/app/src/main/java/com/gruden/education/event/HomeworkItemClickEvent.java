package com.gruden.education.event;

import com.gruden.education.model.LeanchatUser;

/**
 * Created by yunjiansun on 16/2/5.
 */
public class HomeworkItemClickEvent {
    public String id;
    public boolean isCreatedByMe;
    public String conversationId;

    public HomeworkItemClickEvent(String id,String conversationId, boolean isCreatedByMe){
        this.id = id;
        this.conversationId = conversationId;
        this.isCreatedByMe = isCreatedByMe;
    }
}
