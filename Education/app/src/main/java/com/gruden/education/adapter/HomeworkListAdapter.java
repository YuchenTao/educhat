package com.gruden.education.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.event.HomeworkItemClickEvent;
import com.gruden.education.model.HomeWork;
import com.gruden.education.viewholder.HomeworkItemHolder;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by yunjiansun on 16/1/27.
 */
public class HomeworkListAdapter extends BaseAdapter {

    LayoutInflater mInflater;
    Context context;
    String conversationId;

    public HomeworkListAdapter(Context context,String conversationId){
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.conversationId = conversationId;

    }

    public List<HomeWork> dataList = new ArrayList<HomeWork>();

    public List<HomeWork> getDataList() {
        return dataList;
    }

    public void setDataList(List<HomeWork> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }


    public void addDataList(List<HomeWork> datas) {
        dataList.addAll( datas);
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public HomeWork getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final HomeWork homeWork = dataList.get(position);
        HomeworkItemHolder holder;
        if (convertView == null) {

            holder = new HomeworkItemHolder();
            convertView = mInflater.inflate(R.layout.homework_item, parent, false);
            holder.dateLayout = convertView.findViewById(R.id.dateLayout);
            holder.subjectLayout = convertView.findViewById(R.id.subjectLayout);
            holder.lineBottom = convertView.findViewById(R.id.line_bottom);
            holder.txtDate = (TextView)convertView.findViewById(R.id.txtDate);
            holder.txtSubject = (TextView)convertView.findViewById(R.id.txtSubject);
            holder.txtDeadline = (TextView)convertView.findViewById(R.id.txtDeadline);
            holder.txtCreateAt = (TextView)convertView.findViewById(R.id.txtCreateAt);
            holder.txtContent= (TextView)convertView.findViewById(R.id.txtContent);
            convertView.setTag(holder);

        } else {
            holder = (HomeworkItemHolder) convertView.getTag();
        }

        if (homeWork.id==null){
            holder.dateLayout.setVisibility(View.VISIBLE);
            holder.subjectLayout.setVisibility(View.GONE);

            holder.txtDate.setText(homeWork.date);

            convertView.setOnClickListener(null);
        }else {
            holder.dateLayout.setVisibility(View.GONE);
            holder.subjectLayout.setVisibility(View.VISIBLE);
            holder.lineBottom.setVisibility(homeWork.isBottom?View.GONE:View.VISIBLE);

            holder.txtSubject.setText(homeWork.subject);
            holder.txtDeadline.setText("上交日期:" + homeWork.deadline);
            holder.txtCreateAt.setText(homeWork.createAt);
            holder.txtContent.setText(homeWork.content);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new HomeworkItemClickEvent(homeWork.id,conversationId, homeWork.isCreatedByMe));
                }
            });

        }
        return convertView;
    }
}
