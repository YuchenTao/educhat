package com.gruden.education.friends;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.widget.ListView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.NewFriendsAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Friend;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;

public class ContactNewFriendActivity extends BaseActivity {

  @Bind(R.id.friends_listView)
  ListView friendsList;

  private int curPage=0;
  private NewFriendsAdapter adapter;
  private SimpleDateFormat sdf;
  private ArrayList<Map<String,String>> phone2=new ArrayList<Map<String,String>>();
  private ArrayList<String> phone=new ArrayList<String>();
  private ArrayList<Friend> friendList = new ArrayList<Friend>();
  private ArrayList<String> userIdList = new ArrayList<String>();
  private LeanchatUser user;
  private Date localFirstTime;
  private static final String[] CONTACTOR_ION = new String[]{
          ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
          ContactsContract.Contacts.DISPLAY_NAME,
          ContactsContract.CommonDataKinds.Phone.NUMBER
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.contact_new_friend_activity);
    initActionBar("好友申请");
    showLeftBackButton();
    init();
    sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    adapter=new NewFriendsAdapter(this);
    friendsList.setAdapter(adapter);
    if (user.getNewFriends()==null||user.getNewFriends().size()==0){
      if (user.getMatching()) {
        matchContacts();
      }
      getFriends(0);
    }else if(user.getNewFriends().size()!=0){
      friendList.addAll(user.getNewFriends());
      adapter.setDataList(friendList);
      adapter.notifyDataSetChanged();

        for (int i = 0; i < user.getNewFriends().size(); i++) {
          Friend friend = user.getNewFriends().get(i);
          userIdList.add(friend.userId);
          for (int j = 0; j < phone.size(); j++) {
            if (friend.phone != null && phone.get(j).equals(friend.phone)) {
              phone.remove(j);
              phone2.remove(j);
            }
          }
        }
      if (user.getMatching()) {
        matchContacts();
      }
      getFriends2(0);
    }
  }

  private void init(){
    user=LeanchatUser.getCurrentUser();
    phone.clear();
    phone2.clear();
    friendList.clear();
    userIdList.clear();
    showSpinnerDialog();
    readAllContacts();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode==3){
      if (requestCode==3){
        adapter.notifyDataSetChanged();
      }
    }
  }

  //匹配通讯录
  private void matchContacts(){
    String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"contact";
    url = url + "?userId="+ LeanchatUser.getCurrentUser().getObjectId();
    for(int i=0;i<phone.size();i++){
      url+="&phone="+phone.get(i);
    }
    MyHttpClient.get(url, new MyJsonResponseHandler() {
      @Override
      public void handleSuccess(int code, String mesg, JSONArray data) {
        if (data.length() != 0 ) {
          for (int j = 0; j < phone2.size(); j++) {
            for (int i = 0; i < data.length(); i++) {
              JSONObject object = data.optJSONObject(i);
                if (phone2.get(j).containsKey(object.opt("phone"))) {
                  Friend friend = new Friend();
                  friend.userId = object.optString("uid");
                  friend.avatarUrl = object.optString("avatar");
                  friend.name=phone2.get(j).get(object.opt("phone"));
                  friend.nickname = object.optString("nickname") + "(" + phone2.get(j).get(object.opt("phone")) + ")";
                  friend.phone = object.optString("phone");
                  friend.dateTime = sdf.format(new Date());
                  friend.type = 1;
                  if (object.optInt("state") != 1) {
                    friend.state = object.optInt("state");
                    if (!userIdList.contains(object.optString("uid"))) {
                      friendList.add(friend);
                      userIdList.add(object.optString("uid"));
                    }
                  }
                }
              }
            }
        }
        SortComparator sortComparator = new SortComparator();
        Collections.sort(friendList,sortComparator);
        adapter.setDataList(friendList);
        adapter.notifyDataSetChanged();
        user.setNewFriends(friendList);
        LeanchatUser.changeCurrentUser(user, true);
      }
      @Override
      public void handleSuccess(int code, String mesg) {
        if(code==0) {
          toast(mesg);
        }
      }
      @Override
      public void handleFailure(int error) {
        super.handleFailure(error);
      }
    });
  }
  //初始化好友列表
  private void getFriends(final int page){
    RequestParams params = new RequestParams();
    params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
    params.put("type",0);
    params.put("curPage", page);
    String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"friends/apply";
    MyHttpClient.get(url, params, new MyJsonResponseHandler() {
      @Override
      public void handleSuccess(int code, String mesg, JSONArray array) {
        if (array.length() != 0 ) {
          for (int i = 0; i < array.length(); i++) {
            JSONObject data = array.optJSONObject(i);
            Friend friend = new Friend();
            friend.id = data.optString("id");
            friend.userId = data.optString("userId");
            friend.nickname = data.optString("nickname");
            friend.state = data.optInt("state");
            friend.dateTime = data.optString("dateTime");
            friend.avatarUrl=data.optString("avatar");
            friend.type = 0;
            friend.setDateTime(data.optString("dateTime"));
            if (!userIdList.contains(friend.userId)){
              userIdList.add(data.optString("userId"));
              friendList.add(friend);
            }

          }
          curPage=page;
          getFriends(curPage+1);
        }
        SortComparator sortComparator = new SortComparator();
        Collections.sort(friendList,sortComparator);
        adapter.setDataList(friendList);
        adapter.notifyDataSetChanged();
        user.setNewFriends(friendList);
        LeanchatUser.changeCurrentUser(user, true);
        dismissSpinnerDialog();
      }
      @Override
      public void handleSuccess(int code, String mesg) {
        dismissSpinnerDialog();
        toast(mesg);
      }
      @Override
      public void handleFailure(int error) {
        dismissSpinnerDialog();
        super.handleFailure(error);
      }
    });
  }
  //更新好友列表
  private void getFriends2(final int page){
    RequestParams params = new RequestParams();
    params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
    params.put("type",0);
    params.put("curPage", page);
    String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"friends/apply";
    MyHttpClient.get(url, params, new MyJsonResponseHandler() {
      @Override
      public void handleSuccess(int code, String mesg, JSONArray array) {
        try {
          localFirstTime=sdf.parse(user.getNewFriends().get(0).getDateTime());
          if (array.length() != 0 ) {
                for (int i = 0; i < array.length(); i++) {
                  JSONObject data = array.optJSONObject(i);
                  if ( localFirstTime.after(sdf.parse(array.optJSONObject(i).optString("dateTime")))||localFirstTime.equals(sdf.parse(array.optJSONObject(i).optString("dateTime")))) {
                    if(!userIdList.contains(data.optString("userId")) ) {
                      Friend friend = new Friend();
                      friend.id = data.optString("id");
                      friend.userId = data.optString("userId");
                      friend.nickname = data.optString("nickname");
                      friend.state = data.optInt("state");
                      friend.dateTime = data.optString("dateTime");
                      friend.avatarUrl = data.optString("avatar");
                      friend.type = 0;
                      friend.setDateTime(data.optString("dateTime"));
                      if (!userIdList.contains(friend.userId)){
                        userIdList.add(data.optString("userId"));
                        friendList.add(friend);
                      }
//                      friendList.add(friend);
                    }
                  }else {
                    for(int m=0;m<friendList.size();m++){
                       if(friendList.get(m).phone==null){
                         Friend friend=friendList.get(m);
                         friend.nickname = data.optString("nickname");
                         friend.state = data.optInt("state");
                         friend.avatarUrl = data.optString("avatar");
                         friend.dateTime = data.optString("dateTime");
                         friend.setDateTime(data.optString("dateTime"));
                       }
                    }
                  }
                }
                curPage = page;
               getFriends2(curPage + 1);
                SortComparator sortComparator = new SortComparator();
                Collections.sort(friendList, sortComparator);
                adapter.setDataList(friendList);
                adapter.notifyDataSetChanged();
                user.setNewFriends(friendList);
                LeanchatUser.changeCurrentUser(user, true);
                dismissSpinnerDialog();
            }
        } catch (ParseException e) {
          e.printStackTrace();
        }
        dismissSpinnerDialog();
      }
      @Override
      public void handleSuccess(int code, String mesg) {
        dismissSpinnerDialog();
        toast(mesg);
      }
      @Override
      public void handleFailure(int error) {
        dismissSpinnerDialog();
        super.handleFailure(error);
      }
    });
  }
  /*
   * 读取联系人的信息
   */

  public void readAllContacts(){
    phone.clear();
    phone2.clear();
    Cursor phones = null;
    ContentResolver cr = getContentResolver();
    try {
      phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                      , CONTACTOR_ION, null, null, "sort_key");

      if (phones != null) {
        final int contactIdIndex = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
        final int displayNameIndex = phones.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        final int phoneIndex = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        String phoneString, displayNameString, contactIdString;
        while (phones.moveToNext()) {
//          LinkManForm linkManForm = new LinkManForm();
          HashMap<String,String> map = new HashMap<String,String>();
          phoneString = phones.getString(phoneIndex);
          displayNameString = phones.getString(displayNameIndex);
          contactIdString = phones.getString(contactIdIndex);
          if (TextUtils.isEmpty(phoneString))
            continue;
          phone.add(phoneString);
          map.put(phoneString, displayNameString);
          phone2.add(map);

        }
      }
    } catch (Exception e) {

    } finally {
      if (phones != null)
        phones.close();
    }
  }
  //ArrayList按照时间排序
  class SortComparator implements Comparator {

    @Override
    public int  compare(Object lhs, Object rhs) {

      Friend friend1 = (Friend) lhs;
      Friend friend2 = (Friend) rhs;
      int flag = friend2.getDateTime().compareTo(friend1.getDateTime());
      return flag;
    }

  }
}
