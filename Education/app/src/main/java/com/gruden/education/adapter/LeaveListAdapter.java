package com.gruden.education.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.classmenu.LeaveDetailActivity;
import com.gruden.education.model.Leave;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/1/27.
 */
public class LeaveListAdapter extends BaseAdapter {

    LayoutInflater mInflater;
    Context context;
    String conversationId;

    public LeaveListAdapter(Context context, String conversationId){
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.conversationId = conversationId;

    }

    public List<Leave> dataList = new ArrayList<Leave>();

    public List<Leave> getDataList() {
        return dataList;
    }

    public void setDataList(List<Leave> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }


    public void addDataList(List<Leave> datas) {
        dataList.addAll( datas);
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Leave getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Leave leave = dataList.get(position);
        LeaveItemHolder holder;
        if (convertView == null) {

            holder = new LeaveItemHolder();
            convertView = mInflater.inflate(R.layout.leave_item, parent, false);
            holder.dateLayout = convertView.findViewById(R.id.dateLayout);
            holder.leaveLayout = convertView.findViewById(R.id.leaveLayout);
            holder.lineBottom = convertView.findViewById(R.id.line_bottom);
            holder.txtDate = (TextView)convertView.findViewById(R.id.txtDate);
            holder.txtChildName = (TextView)convertView.findViewById(R.id.txtChildName);
            holder.txtApplication = (TextView)convertView.findViewById(R.id.txtApplication);
            holder.txtCreateAt = (TextView)convertView.findViewById(R.id.txtCreateAt);
            holder.txtContent= (TextView)convertView.findViewById(R.id.txtContent);
            holder.txtLeaveDate= (TextView)convertView.findViewById(R.id.txtLeaveDate);
            convertView.setTag(holder);

        } else {
            holder = (LeaveItemHolder) convertView.getTag();
        }

        if (leave.id==null){
            holder.dateLayout.setVisibility(View.VISIBLE);
            holder.leaveLayout.setVisibility(View.GONE);
            holder.txtDate.setText(leave.date);
            convertView.setOnClickListener(null);
        }else {
            holder.dateLayout.setVisibility(View.GONE);
            holder.leaveLayout.setVisibility(View.VISIBLE);
            holder.lineBottom.setVisibility(leave.isBottom ? View.GONE : View.VISIBLE);
            holder.txtChildName.setText(leave.childName);
            if(leave.state==0){
                holder.txtApplication.setText("申请中...");
                holder.txtApplication.setTextColor(0xffffbb33);
            }else if(leave.state==1){
                holder.txtApplication.setText("已接受请假");
                holder.txtApplication.setTextColor(0xff696969);
            }else{
                holder.txtApplication.setText("已取消");
                holder.txtApplication.setTextColor(0xff696969);
            }

            holder.txtCreateAt.setText(leave.time);
            holder.txtContent.setText(leave.reason);
            holder.txtLeaveDate.setText(leave.startDate);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, LeaveDetailActivity.class);
                    intent.putExtra("id",leave.id);
                    intent.putExtra("state",leave.state);
                    context.startActivity(intent);
                }
            });

        }
        return convertView;
    }
    class LeaveItemHolder{
        View dateLayout;
        View leaveLayout;
        View lineBottom;
        TextView txtDate;
        TextView txtChildName;
        TextView txtApplication;
        TextView txtCreateAt;
        TextView txtContent;
        TextView txtLeaveDate;
    }
}
