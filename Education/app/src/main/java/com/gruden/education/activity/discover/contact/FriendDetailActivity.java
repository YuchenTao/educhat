package com.gruden.education.activity.discover.contact;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMConversationQuery;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.callback.AVIMConversationQueryCallback;
import com.avoscloud.leanchatlib.controller.ChatManager;
import com.avoscloud.leanchatlib.model.ConversationType;
import com.avoscloud.leanchatlib.utils.Constants;
import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.activity.conversation.ChatRoomActivity;
import com.gruden.education.activity.discover.friendcircle.GrowthLogActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.friends.ContactNewFriendActivity;
import com.gruden.education.model.Friend;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

public class FriendDetailActivity extends BaseActivity {

    @Bind(R.id.profile_avatar_view)
    ImageView avatarView;

    @Bind(R.id.txtUsername)
    TextView txtUsername;

    @Bind(R.id.txtPhone)
    TextView txtPhone;

    @Bind(R.id.txtRemark)
    TextView txtRemark;

    @Bind(R.id.txtPosition)
    TextView txtPosition;

    @Bind(R.id.imageView1)
    ImageView imageView1;

    @Bind(R.id.imageView2)
    ImageView imageView2;

    @Bind(R.id.imageView3)
    ImageView imageView3;

    @Bind(R.id.layoutRemark)
    View layoutRemark;

    @Bind(R.id.layout_sendMsg)
    View layoutSendMsg;

    @Bind(R.id.layout_delete)
    View layoutDelete;

    @Bind(R.id.layout_addFriend)
    View layoutAddFriend;

    @Bind(R.id.remark)
    TextView remark;

    @Bind(R.id.layoutLog)
    View layoutLog;

    @Bind(R.id.btnAddFriend)
    Button btnAddFriend;

    ArrayList<ImageView> arrImage = new ArrayList<ImageView>();
    LeanchatUser user=LeanchatUser.getCurrentUser();

    private static final int REMARK_EDIT_REQUEST = 1;
    private String id;
    private String conversationId;
    private String remarkName;
    private String opId;
    private int position;
    private int resultCode = 0;
    private int identity;
    private int friendState;
    private int friendType;
    private boolean isFriend;
    private int isClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_detail);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        conversationId = intent.getStringExtra("conversationId");
        opId=intent.getStringExtra("opId");
        remarkName = intent.getStringExtra("remarkName");


        position = intent.getIntExtra("position", -1);
        identity = intent.getIntExtra("identity",-1);
        friendState=intent.getIntExtra("FriendState", -1);
        friendType=intent.getIntExtra("FriendType",-1);
        isClass=intent.getIntExtra("isClass",0);
        System.out.println("身份验证："+identity+"朋友的类型:"+friendType+"备注名："+remarkName);
        initActionBar("详细资料");
        showLeftBackButton(intent.getStringExtra("backTitle"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("position", position);
                intent.putExtra("remarkName", remarkName);
                setResult(FriendDetailActivity.this.resultCode, intent);
                finish();
            }
        });

        arrImage.add(imageView1);
        arrImage.add(imageView2);
        arrImage.add(imageView3);


        layoutRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remark();
            }
        });

        getUserInfo();
    }

    void getUserInfo(){

        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("userId", id);
        params.put("selfId", user.getObjectId());
        if (conversationId!=null) {
            params.put("conversationId", conversationId);
        }

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER + "profile";

        MyHttpClient.get(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {

                ImageLoader.getInstance().displayImage(data.optString("avatar"), avatarView, PhotoUtils.avatarImageOptions);
                txtUsername.setText(TextUtils.isEmpty(data.optString("nickname")) ? data.optString("username") : data.optString("nickname"));
                txtPhone.setText(data.optString("mobilePhoneNumber"));
                txtPosition.setText(data.optString("position"));

                int length=data.optJSONArray("circlePost").length();

                for (int i = 0; i < length; i++) {
                      String imgUrl=data.optJSONArray("circlePost").optString(i);
                    if (!("null".equals(imgUrl)||TextUtils.isEmpty(imgUrl))) {
                        ImageLoader.getInstance().displayImage(data.optJSONArray("circlePost").optString(i), arrImage.get(i));
                    }
                }

                isFriend = (data.optInt("isFriend") == 1);
                if (isFriend ||(isClass==1&&user.getRoles().contains(LeanchatUser.TEACHER))) {

                    layoutLog.setVisibility(View.VISIBLE);
                    layoutAddFriend.setVisibility(View.GONE);
                    layoutSendMsg.setVisibility(View.VISIBLE);
                    layoutDelete.setVisibility(View.VISIBLE);

                    if (data.optString("remarkName").length() > 0)
                        remarkName = data.optString("remarkName");

                } else{

                    layoutLog.setVisibility(View.GONE);
                    layoutSendMsg.setVisibility(View.GONE);
                    layoutDelete.setVisibility(View.GONE);
                    layoutAddFriend.setVisibility(View.VISIBLE);
                    if (friendType==0&&friendState==0){
                        btnAddFriend.setText("通过验证");
                    }else {
                        btnAddFriend.setText("添加到通讯录");
                    }

                }
                if(remarkName!=null)
                    txtRemark.setText(remarkName);
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode==RESULT_OK){

            remarkName = data.getStringExtra("remarkName");
            txtRemark.setText(remarkName);
        }

        this.resultCode = resultCode;
    }

    public void remark(){

        Intent intent = new Intent(this, RemarkNameEditActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("identity", identity);
        intent.putExtra("isFriend",isFriend);
        if(conversationId!=null)
            intent.putExtra("groupId", conversationId);
        startActivityForResult(intent, REMARK_EDIT_REQUEST);

    }

    @OnClick(R.id.layoutLog)
    public void logOnClick(){
        Intent intent = new Intent(this, GrowthLogActivity.class);
        intent.putExtra("friend_id", id);
        startActivity(intent);
    }

    @OnClick(R.id.btnSendMsg)
    public void sendMsg(){

        showSpinnerDialog();
        AVIMConversationQuery query = ChatManager.getInstance().getImClient().getQuery();
        query.withMembers(Arrays.asList(id),true);
        //获取第一个对话
        query.findInBackground(new AVIMConversationQueryCallback() {
            @Override
            public void done(List<AVIMConversation> convs, AVIMException e) {
                if (e == null) {


                    if (convs != null && !convs.isEmpty()) {
                        dismissSpinnerDialog();
                        AVIMConversation conv = convs.get(0);
                        String convId = conv.getConversationId();
                        if (ChatManager.getInstance().getRoomsTable().getRoomWithId(convId)==null){
                            ChatManager.getInstance().getRoomsTable().insertRoom(convId,ConversationType.Private.getValue());

                        }

                        Intent intent = new Intent(FriendDetailActivity.this, ChatRoomActivity.class);
                        intent.putExtra(Constants.CONVERSATION_ID, convId);
                        intent.putExtra("title", remarkName);
                        startActivity(intent);
                        finish();

                    }else {
//                        LeanchatUser user = LeanchatUser.getCurrentUser();
                        RequestParams params = new RequestParams();
                        params.put("userId", user.getObjectId());
                        params.put("userNickName",TextUtils.isEmpty(user.getNickName())?user.getUsername():user.getNickName());
                        params.put("friendId",id);
                        params.put("friendNickName",txtUsername.getText().toString());

                        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME + "conversation/private";

                        MyHttpClient.post(url,params, new MyJsonResponseHandler() {

                            @Override
                            public void handleSuccess(int code, String mesg, JSONObject data) {
                                dismissSpinnerDialog();
                                String conversationId = data.optString("groupId");
                                ChatManager.getInstance().getRoomsTable().insertRoom(conversationId, ConversationType.Private.getValue());
                                Intent intent = new Intent(FriendDetailActivity.this, ChatRoomActivity.class);
                                intent.putExtra(Constants.CONVERSATION_ID, conversationId);
                                intent.putExtra("title", remarkName);
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void handleSuccess(int code, String mesg) {
                                dismissSpinnerDialog();
                                toast( mesg);


                            }

                            @Override
                            public void handleFailure(int error) {
                                dismissSpinnerDialog();
                                super.handleFailure(error);
                            }

                        });
                    }
                }
            }
        });

    }
    @OnClick(R.id.btnAddFriend)
    public void addFriend(){
        if (friendType==1) {
            String strRemarkName = txtRemark.getText().toString();
            showSpinnerDialog();
            RequestParams params = new RequestParams();
            params.put("friendId", id);
            params.put("selfId", user.getObjectId());
            params.put("type", 1);
            if (!TextUtils.isEmpty(strRemarkName)) {
                params.put("remarkName", strRemarkName);
            }

            String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER + "friends/apply";

            MyHttpClient.post(url, params, new MyJsonResponseHandler() {
                @Override
                public void handleSuccess(int code, String mesg, JSONObject data) {

                    txtRemark.setTag(data.optString("fid"));
                    ArrayList<Friend> changeState= (ArrayList<Friend>) user.getNewFriends();
                    changeState.get(position).state=2;
                    user.setNewFriends(changeState);
                    LeanchatUser.changeCurrentUser(user,true);
                    dismissSpinnerDialog();
                    toast(mesg);
                }
                @Override
                public void handleSuccess(int code, String mesg) {
                    dismissSpinnerDialog();
                    toast(mesg);

                }
                @Override
                public void handleFailure(int error) {
                    dismissSpinnerDialog();
                    super.handleFailure(error);
                }

            });
        }else if(friendType==0){
            RequestParams params = new RequestParams();
            params.put("userId", user.getObjectId());
            params.put("id", opId);
            params.put("op",1);
            String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"friends/verify";
            MyHttpClient.put(url, params, new MyJsonResponseHandler() {
                @Override
                public void handleSuccess(int code, String mesg) {
                    toast(mesg);
                    if (code==0) {

                        ArrayList<Friend> changeState= (ArrayList<Friend>) user.getNewFriends();
                        changeState.get(position).state=1;
                        user.setNewFriends(changeState);
                        LeanchatUser.changeCurrentUser(user,true);
                        layoutLog.setVisibility(View.VISIBLE);
                        layoutAddFriend.setVisibility(View.GONE);
                        layoutSendMsg.setVisibility(View.VISIBLE);
                        layoutDelete.setVisibility(View.VISIBLE);

                    }
                }

                @Override
                public void handleFailure(int error) {
                    super.handleFailure(error);
                }
            });
        }else{
            String strRemarkName = txtRemark.getText().toString();
            showSpinnerDialog();
            RequestParams params = new RequestParams();
            params.put("friendId", id);
            params.put("selfId", user.getObjectId());
            params.put("type", 1);
            if (!TextUtils.isEmpty(strRemarkName)) {
                params.put("remarkName", strRemarkName);
            }

            String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER + "friends/apply";

            MyHttpClient.post(url, params, new MyJsonResponseHandler() {
                @Override
                public void handleSuccess(int code, String mesg, JSONObject data) {
//                txtRemark.setText(data.optString("fname"));
                    txtRemark.setTag(data.optString("fid"));
                    dismissSpinnerDialog();
                    toast(mesg);
                }

                @Override
                public void handleSuccess(int code, String mesg) {
                    dismissSpinnerDialog();
                    toast(mesg);

                }

                @Override
                public void handleFailure(int error) {
                    dismissSpinnerDialog();
                    super.handleFailure(error);
                }

            });
        }
    }
    @OnClick(R.id.btnDelete)
    public void deleteFriend(){

        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("friendId",id);
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER + "friends/remove";

        MyHttpClient.put(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent=new Intent(this, ContactNewFriendActivity.class);
        setResult(3,intent);
    }
}
