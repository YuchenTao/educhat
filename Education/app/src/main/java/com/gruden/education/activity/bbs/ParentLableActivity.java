package com.gruden.education.activity.bbs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Topic;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by admin on 2016/5/10.
 */
public class ParentLableActivity extends BaseActivity {

    @Bind(R.id.lableListView)
    ListView listView;
    LableAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lable);
        initActionBar("话题标签");
        showLeftBackButton();
        adapter = new LableAdapter(ParentLableActivity.this);
        listView.setAdapter(adapter);
        getLable();
    }
    public  void getLable(){
        showSpinnerDialog();
        RequestParams params=new RequestParams();
        params.put("code","");
        String url= MyHttpClient.BASE_URL+MyHttpClient.MODEL_FORUM+"topics/tag";
        MyHttpClient.get(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {
                if (array.length()==0){
                    toast( "加载失败");
                }else {
                    ArrayList<Topic> topicList = new ArrayList<Topic>();
                    for (int i=0; i<array.length(); i++){

                        Topic topic = new Topic();
                        topic.strTitle = array.optJSONObject(i).optString("name");
                        topic.strTagCode = array.optJSONObject(i).optString("code");
                        topicList.add(topic);
                    }

                    adapter.setDataList(topicList);
                    adapter.notifyDataSetChanged();

                }

                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data==null){

        }else{
            Intent intent=new Intent(this,ParentLableActivity.class);
            intent.putExtra("tParentCode",data.getStringExtra("tParentCode"));
            intent.putExtra("title", data.getStringExtra("title"));
            intent.putExtra("tCode",data.getStringExtra("tCode"));
            setResult(RESULT_CANCELED ,intent);
            finish();
        }


    }
    class LableAdapter extends BaseAdapter{
        ArrayList<Topic> dataList=new ArrayList<Topic>();
        Context context;
        LayoutInflater mInflater;

        public void setDataList(ArrayList<Topic> datas) {
            dataList.clear();
            if (null != datas) {
                dataList.addAll(datas);
            }
        }
        public LableAdapter(Context context ){
            super();
            this.context = context;
            this.mInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public Topic getItem(int position) {
            return dataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final Topic topic = dataList.get(position);

            TopicItemHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.parent_lable_item, parent, false);
                holder = new TopicItemHolder();
                holder.txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
                holder.layoutLine1=convertView.findViewById(R.id.layoutLine1);
                holder.layoutLine2=convertView.findViewById(R.id.layoutLine2);
                convertView.setTag(holder);
            } else {
                holder = (TopicItemHolder) convertView.getTag();
            }
            if (position==dataList.size()-1){
                holder.layoutLine1.setVisibility(View.GONE);
                holder.layoutLine2.setVisibility(View.VISIBLE);
            }else{
                holder.layoutLine1.setVisibility(View.VISIBLE);
                holder.layoutLine2.setVisibility(View.GONE);
            }

            holder.txtTitle.setText(topic.strTitle);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context,ChildLableActivity.class);
                    intent.putExtra("tParentCode",topic.strTagCode);
                    intent.putExtra("title",topic.strTitle);
                    startActivityForResult(intent,0);
                }
            });

            return convertView;
        }

    }
    class TopicItemHolder{
        TextView txtTitle;
        View layoutLine1;
        View layoutLine2;
    }
}
