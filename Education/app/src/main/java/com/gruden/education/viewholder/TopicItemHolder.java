package com.gruden.education.viewholder;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by yunjiansun on 16/1/4.
 */
public class TopicItemHolder {
    public ImageView imgTopic;
    public TextView txtTitle;
    public TextView txtDetail;
    public Button btnAttention;
}
