package com.gruden.education.activity.discover;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.ProgressCallback;
import com.avos.avoscloud.SaveCallback;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.MyGridViewAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.UploadImage;
import com.loopj.android.http.RequestParams;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class PublishLogActivity extends BaseActivity {




    @Bind(R.id.editContent)
    EditText editContent;

    @Bind(R.id.imgDispalyGridView)
    GridView gridView;
    @Bind(R.id.txtLocation)
    TextView txtLocation;

    private static final int REQUEST_IMAGE = 2;
    private static final int REQUEST_LOCATION =1;
    private MyGridViewAdapter mAdapter;
    private List<UploadImage> imageList = new ArrayList<UploadImage>();
    private ArrayList<String> mSelectPath = new ArrayList<String>();

    private boolean isUploading;
    private String conversationId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish_log);

        initActionBar("");
        showLeftBackButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(UploadImage img : imageList){
                    if(img.isDone){
                        img.file.deleteInBackground();
                    }else {
                        img.file.cancel();
                    }
                }
                onBackPressed();
            }
        });
        showRightTextButton("发布", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publish();
            }
        });

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");

        mAdapter = new MyGridViewAdapter(this, mSelectPath);
        gridView.setAdapter(mAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == mSelectPath.size()) {//点击最后一张图片，打开相册
                    Intent intent = new Intent(PublishLogActivity.this, MultiImageSelectorActivity.class);
                    // 是否显示拍摄图片
                    intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);
                    // 最大可选择图片数量
                    intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, 9);
                    // 选择模式
                    intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, MultiImageSelectorActivity.MODE_MULTI);
                    // 默认选择
                    if(mSelectPath != null && mSelectPath.size()>0){
                        intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
                    }
                    startActivityForResult(intent, REQUEST_IMAGE);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_IMAGE){
            if(resultCode == RESULT_OK){

                ArrayList<String> pathList = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);

                for (int i=0; i<imageList.size();i++){
                    UploadImage img = imageList.get(i);
                    if (!pathList.contains(img.path)){
                        Log.e("remove image",  img.path);
                        img.file.cancel();
                        img.file.deleteInBackground();
                        imageList.remove(i);
                    }

                }

                for (int i=0; i< pathList.size(); i++){
                    if (!mSelectPath.contains(pathList.get(i))){
                        Log.e("add image",  pathList.get(i));
                        try {
                            final UploadImage img = new UploadImage();
                            img.path = pathList.get(i);
                            img.file = AVFile.withAbsoluteLocalPath("image.jpg", pathList.get(i));
                            imageList.add(img);
                            img.file.saveInBackground(
                                    new SaveCallback() {
                                        @Override
                                        public void done(AVException e) {
                                            img.url = img.file.getUrl();
                                            img.isDone = true;
                                            System.out.println("url: " + img.url);
                                            if (isUploading)
                                                publish();
                                        }
                                    },
                                    new ProgressCallback() {
                                        @Override
                                        public void done(Integer percentDone) {
                                            //打印进度
                                            System.out.println("uploading: " + percentDone);
                                        }
                                    });

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Log.e("Exception","FileNotFound");
                        }
                    }

                }

                mSelectPath = pathList;

                mAdapter.setDataList(mSelectPath);
                mAdapter.notifyDataSetChanged();
            }
        }
        if(requestCode == REQUEST_LOCATION){
            if(resultCode==2){
                txtLocation.setText(data.getStringExtra("location"));
            }

        }
    }

    void publish(){
        String strContent = editContent.getText().toString();
        if (TextUtils.isEmpty(strContent)) {
            toast("日志内容不能为空");
            return;
        }

        showSpinnerDialog();

        for(UploadImage img : imageList){
            if(!img.isDone){
                isUploading = true;
                return;
            }
        }

        isUploading = false;

        LeanchatUser user = LeanchatUser.getCurrentUser();
        RequestParams params = new RequestParams();
        params.put("userId",user.getObjectId());
        params.put("content",strContent);


        if (imageList.size()>0){
            List<String> urlList = new ArrayList<String>();
            for(UploadImage img : imageList){
                urlList.add(img.url);
            }
            params.put("imgUrl",urlList);
        }
        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "friends/log";
        MyHttpClient.post(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg){
                dismissSpinnerDialog();
                toast(mesg);
                if (code==0)
                    onBackPressed();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);

            }

        });
    }
//    @OnClick(R.id.layoutLocation)
//    public void location(){
//
//        Intent intent=new Intent(this,CurrentLocationActivity.class);
//        startActivityForResult(intent,REQUEST_LOCATION);
//    }
}
