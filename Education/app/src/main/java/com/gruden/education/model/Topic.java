package com.gruden.education.model;


/**
 * Created by yunjiansun on 16/1/4.
 */
public class Topic {
    public int tag;
    public int imgDraw;
    public String imgUrl;
    public String strTitle;
    public String strTagCode;
    public String strDetail;
    public int isConcerned;
}
