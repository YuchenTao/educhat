package com.gruden.education.base;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by yunjiansun on 15/11/10.
 */
public interface HandleResponse {

    void handleSuccess(int code, String mesg);
    void handleSuccess(int code, String mesg, JSONArray data);
    void handleSuccess(int code, String mesg, JSONObject data);

//    void handleSuccess(JSONObject jsonResponse);
    void handleFailure(int error);
//    void handleFailure(JSONObject jsonResponse);
}
