package com.gruden.education.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/1/15.
 */
public class ReportCardGroup {

    public String examCode;
    public String examName;
    public List<ReportCardChild> reportCardChildList  = new ArrayList<ReportCardChild>();


}
