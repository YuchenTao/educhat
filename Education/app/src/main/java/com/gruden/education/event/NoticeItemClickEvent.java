package com.gruden.education.event;

/**
 * Created by yunjiansun on 16/2/4.
 */
public class NoticeItemClickEvent {
    public String id;
    public String conversationId;
    public boolean isCreatedByMe;

    public NoticeItemClickEvent(String id,String conversationId, boolean isCreatedByMe){
        this.id = id;
        this.conversationId = conversationId;
        this.isCreatedByMe = isCreatedByMe;
    }
}
