package com.gruden.education.viewholder;

import android.view.View;
import android.widget.TextView;

/**
 * Created by yunjiansun on 16/1/27.
 */
public class HomeworkItemHolder {

    public View dateLayout;
    public View subjectLayout;
    public View lineBottom;
    public TextView txtDate;
    public TextView txtSubject;
    public TextView txtDeadline;
    public TextView txtCreateAt;
    public TextView txtContent;
}
