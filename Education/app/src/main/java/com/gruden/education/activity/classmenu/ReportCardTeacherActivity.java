package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ExpandableListView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.ReportTeacherListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.ReportCardChild;
import com.gruden.education.model.ReportCardGroup;
import com.gruden.education.model.Subject;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

public class ReportCardTeacherActivity extends BaseActivity {

    @Bind(R.id.reportCard_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.expandableListView)
    ExpandableListView listView;

    ReportTeacherListAdapter listAdapter;

    int curPage;
    String conversationId;

    private boolean isRefreshing;
    private boolean isLoadMoreing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_card_teacher);
        initActionBar("成绩单");
        showLeftBackButton();

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");

        listAdapter=new ReportTeacherListAdapter(this);
        listView.setAdapter(listAdapter);

        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                updateReportList(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                updateReportList(curPage+1);
            }


        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        updateReportList(0);
    }

    void updateReportList(final int page){

        RequestParams params = new RequestParams();
        params.put("groupId", conversationId);
        params.put("curPage", page);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"score";

        MyHttpClient.get(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length() == 0) {

                    if (page == 0)
                        toast("暂时还未上传任何成绩");
                    else
                        toast("已经没有更多报告了~");

                } else {
                    ArrayList<ReportCardGroup> reportCardGroupList = new ArrayList<ReportCardGroup>();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject data = array.optJSONObject(i);
                        ReportCardGroup reportCardGroup = new ReportCardGroup();
                        reportCardGroup.examCode = data.optString("examCode");
                        reportCardGroup.examName = data.optString("examName");

                        JSONArray arrStrudentCode = data.optJSONArray("data");
                        for (int j = 0; j < arrStrudentCode.length(); j++) {
                            ReportCardChild reportCardChild = new ReportCardChild();
                            reportCardChild.studentCode = arrStrudentCode.optJSONObject(j).optString("studentCode");
                            reportCardChild.studentName = arrStrudentCode.optJSONObject(j).optString("studentName");
                            reportCardChild.total= arrStrudentCode.optJSONObject(j).optInt("total");
                            JSONArray arrSubject = arrStrudentCode.optJSONObject(j).optJSONArray("data");
                            for (int t = 0; t < arrSubject.length(); t++) {
                                Subject subject = new Subject();
                                subject.name = arrSubject.optJSONObject(t).optString("subject");
                                subject.score = arrSubject.optJSONObject(t).optInt("score");
                                reportCardChild.subjectList.add(subject);
                            }
                            reportCardGroup.reportCardChildList.add(reportCardChild);

                        }
                        reportCardGroupList.add(reportCardGroup);
                    }

                    if (page == 0) {
                        listAdapter.setDataList(reportCardGroupList);
                    } else {
                        listAdapter.addDataList(reportCardGroupList);
                    }
                    listAdapter.notifyDataSetChanged();
                    curPage = page;
                }

                dissmissCircle();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }

        });

    }
    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }
}
