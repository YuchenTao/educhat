package com.gruden.education.event;

/**
 * Created by yunjiansun on 15/12/27.
 */
public class ClassDetailItemClickEvent {
    public String conversationId;
    public ClassDetailItemClickEvent(String conversationId) {
        this.conversationId = conversationId;
    }
}
