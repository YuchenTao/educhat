package com.gruden.education.model;

/**
 * Created by yunjiansun on 16/1/27.
 */
public class HomeWork {
    public String date;
    public String id;
    public String creator;
    public String groupId;
    public String subject;
    public String content;
    public String deadline;
    public String createAt;
    public boolean isCreatedByMe;
    public boolean isBottom;

}
