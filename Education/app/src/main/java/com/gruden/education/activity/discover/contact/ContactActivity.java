package com.gruden.education.activity.discover.contact;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.SaveCallback;
import com.avoscloud.leanchatlib.controller.ChatManager;
import com.avoscloud.leanchatlib.model.ConversationType;
import com.avoscloud.leanchatlib.utils.Constants;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.activity.conversation.ChatRoomActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.customview.swipemenulistview.SwipeMenu;
import com.gruden.education.customview.swipemenulistview.SwipeMenuCreator;
import com.gruden.education.customview.swipemenulistview.SwipeMenuItem;
import com.gruden.education.customview.swipemenulistview.SwipeMenuListView;
import com.gruden.education.event.ContactItemLongClickEvent;
import com.gruden.education.event.ContactItemSelectEvent;
import com.gruden.education.event.ContactRefreshEvent;
import com.gruden.education.friends.ContactAddFriendActivity;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.UserCacheUtils;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;

public class ContactActivity extends BaseActivity {

    @Bind(R.id.searchView)
    SearchView searchView;

    @Bind(R.id.lv_contacts)
    SwipeMenuListView mListView;

    @Bind(R.id.sidrbar)
    SideBar sideBar;

    @Bind(R.id.dialog)
    TextView dialog;

    @Bind(R.id.btnBackground)
    Button btnBackground;

    private boolean isCheck;
    private List<SortModel> mAllContactsList = new ArrayList<SortModel>();
    private ContactsSortAdapter adapter;
    /**
     * 汉字转换成拼音的类
     */
    private CharacterParser characterParser;

    /**
     * 根据拼音来排列ListView里面的数据类
     */
    private PinyinComparator pinyinComparator;
    private int type;
    private String conversationId;
    private String className;
    private String code;
    private int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Intent intent = getIntent();
        isCheck = intent.getBooleanExtra("isCheck",false);
        type=intent.getIntExtra("type",-1);
        status=intent.getIntExtra("status",-1);
        if (isCheck){
            if (type==0) {
                initActionBar("发起群聊");
                showRightTextButton("确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<String> memberIdList = new ArrayList<String>();
                        List<SortModel> mSelectedList = adapter.getSelectedList();
                        String groupName = "";
                        for (int i = 0; i < mSelectedList.size(); i++) {
                            SortModel user = mSelectedList.get(i);
                            memberIdList.add(user.userId);
                            if (i == (mSelectedList.size() - 1))
                                groupName = groupName + user.name;
                            else
                                groupName = groupName + user.name + "、";
                        }
                        LeanchatUser user = LeanchatUser.getCurrentUser();
                        RequestParams params = new RequestParams();
                        params.put("name", groupName);
                        params.put("type", 0);
                        params.put("creator", user.getObjectId() + "::" + user.getNickName());

                        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME + "group";

                        MyHttpClient.post(url, params, new MyJsonResponseHandler() {

                            @Override
                            public void handleSuccess(int code, String mesg, JSONObject data) {
                                dismissSpinnerDialog();
                                String conversationId = data.optString("groupId");
                                ChatManager.getInstance().getRoomsTable().insertRoom(conversationId, ConversationType.Private.getValue());
                                Intent intent = new Intent(ContactActivity.this, ChatRoomActivity.class);
                                intent.putExtra(Constants.CONVERSATION_ID, conversationId);
                                intent.putExtra("title", data.optString("name"));
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void handleSuccess(int code, String mesg) {
                                dismissSpinnerDialog();
                                toast(mesg);
                            }

                            @Override
                            public void handleFailure(int error) {
                                dismissSpinnerDialog();
                                super.handleFailure(error);
                            }

                        });
                    }
                });
                btnRightTitle.setEnabled(false);
            }else{
                initActionBar("好友列表");
                conversationId=intent.getStringExtra("groupId");
                code=intent.getStringExtra("code");
                className= intent.getStringExtra("name");

                showRightTextButton("确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        showSpinnerDialog();
                        ArrayList<String> memberIdList = new ArrayList<String>();
                        List<SortModel> mSelectedList = adapter.getSelectedList();
                        ArrayList<String> memberList=new ArrayList<String>();
                        for (int i = 0; i < mSelectedList.size(); i++) {
                            SortModel user = mSelectedList.get(i);
                            memberIdList.add(user.userId);
                            String member=user.userId+"::"+user.name+"::"+user.identity;
                            memberList.add(member);
                        }
                        LeanchatUser user = LeanchatUser.getCurrentUser();
                        RequestParams params = new RequestParams();
                        params.put("groupId",conversationId);
                        params.put("name", className);
                        params.put("type", type);
                        params.put("code", code);
                        params.put("nickname",user.getNickName());
                        params.put("member",memberList);

                        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME + "members/invite";

                        MyHttpClient.post(url, params, new MyJsonResponseHandler() {

                            @Override
                            public void handleSuccess(int code, String mesg) {
                                dismissSpinnerDialog();
                                toast(mesg);
                                finish();
                            }
                            @Override
                            public void handleFailure(int error) {
                                dismissSpinnerDialog();
                                super.handleFailure(error);
                            }
                        });
                    }
                });
                btnRightTitle.setEnabled(false);
            }
        }else {
            initActionBar("通讯录");
            showRightImageButton(R.drawable.faxian_haoyoushenqing, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ContactActivity.this, ContactAddFriendActivity.class);
                    startActivity(intent);
                }
            });
        }

        showLeftBackButton();

        sideBar.setTextView(dialog);

        characterParser = CharacterParser.getInstance();

//        AutoCompleteTextView textView = (AutoCompleteTextView) searchView.findViewById(R.id.search_src_text);
//        textView.setTextSize(getResources().getDimension(R.dimen.text_size_medium));
//        textView.setBackgroundColor(Color.WHITE);

        initListView();

        initListener();

        getMembers();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
//            int position = data.getIntExtra("position",-1);
//            if (position!=-1) {
//                mAllContactsList.get(position).name = data.getStringExtra("remarkName");
//                adapter.updateListView(mAllContactsList);
//            }

            getMembers();
        }

    }

    private void initListView(){

        /** 给ListView设置adapter **/

        mAllContactsList = new ArrayList<SortModel>();
        pinyinComparator = new PinyinComparator();
        Collections.sort(mAllContactsList, pinyinComparator);// 根据a-z进行排序源数据
        adapter = new ContactsSortAdapter(this, mAllContactsList,isCheck);
        mListView.setAdapter(adapter);

        if (!isCheck){

            // step 1. create a MenuCreator
            SwipeMenuCreator creator = new SwipeMenuCreator() {

                @Override
                public void create(SwipeMenu menu) {
                    if (menu.getViewType()==1){
                        // create "open" item
                        SwipeMenuItem openItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        openItem.setBackground(new ColorDrawable(getResources().getColor(R.color.theme_light_color)));
                        // set item width
                        openItem.setWidth(dp2px(70));
                        // set item title
                        openItem.setTitle("备注");
                        // set item title fontsize
                        openItem.setTitleSize(18);
                        // set item title font color
                        openItem.setTitleColor(Color.WHITE);
                        // add to menu
                        menu.addMenuItem(openItem);

//                    // create "delete" item
//                    SwipeMenuItem deleteItem = new SwipeMenuItem(
//                            getApplicationContext());
//                    // set item background
//                    deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
//                            0x3F, 0x25)));
//                    // set item width
//                    deleteItem.setWidth(dp2px(90));
//                    // set a icon
//                     deleteItem.setIcon(R.drawable.ic_delete);
//                    // add to menu
//                    menu.addMenuItem(deleteItem);
                    }

                }
            };

            // set creator
            mListView.setMenuCreator(creator);

            // step 2. listener item click event
            mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0: {

                            Intent intent = new Intent(ContactActivity.this, RemarkNameEditActivity.class);
                            intent.putExtra("id", mAllContactsList.get(position).userId);
                            startActivityForResult(intent, 1);
                            break;
                        }

                    }
                    return false;
                }
            });
        }

    }

    private void initListener() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (newText.length() > 0) {
                    ArrayList<SortModel> fileterList = (ArrayList<SortModel>) search(newText);
                    adapter.updateListView(fileterList);
                } else {
                    adapter.updateListView(mAllContactsList);
                }
                mListView.setSelection(0);
                return true;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    btnBackground.setVisibility(View.VISIBLE);
                }
            }
        });

        //设置右侧[A-Z]快速导航栏触摸监听
        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    mListView.setSelection(position);
                }
            }
        });


    }

    private void getMembers() {
        showSpinnerDialog();
        RequestParams params = new RequestParams();

        LeanchatUser user = LeanchatUser.getCurrentUser();
        params.put("userId",user.getObjectId());

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"friends/list";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {

                mAllContactsList.clear();


                JSONArray arrNames = data.names();
                List<String> userIds = new ArrayList<String>();
                if(arrNames!=null){
                    for (int i=0; i<arrNames.length(); i++){
                        String sign = arrNames.optString(i);
                        SortModel signLetter = new SortModel(null, sign);
                        signLetter.sortLetters = getSortLetterBySortKey(sign);
                        signLetter.sortToken = parseSortKey(sign);
                        mAllContactsList.add(signLetter);

                        JSONArray array = data.optJSONArray(arrNames.optString(i));

                        for (int j=0; j<array.length(); j++){
                            JSONObject object = array.optJSONObject(j);
                            SortModel contact = new SortModel(object.optString("remarkName"), sign);
                            contact.sortLetters = getSortLetterBySortKey(sign);
                            contact.sortToken = parseSortKey(contact.name);
                            contact.userId = object.optString("userId");
                            contact.avatarUrl = object.optString("avatar");
                            int length = object.optJSONArray("roles").length();
                            List<String> arrRolesCode = new ArrayList<String>();
                            for (int m=0; m<length; m++){
                                arrRolesCode.add(object.optJSONArray("roles").optJSONObject(m).optString("code"));
                            }
                            if (status<0) {
                                mAllContactsList.add(contact);
                                userIds.add(object.optString("userId"));
                            }
                            if (status==1&&arrRolesCode.contains("R_TEACHER")) {
                                contact.identity=1;
                                mAllContactsList.add(contact);
                                userIds.add(object.optString("userId"));
                            }
                            if (status==2&&arrRolesCode.contains("R_PARENT")){
                                contact.identity=2;
                                mAllContactsList.add(contact);
                                userIds.add(object.optString("userId"));
                            }

                        }
                    }
                }



                runOnUiThread(new Runnable() {
                    public void run() {
                        Collections.sort(mAllContactsList, pinyinComparator);
                        adapter.updateListView(mAllContactsList);
                        dismissSpinnerDialog();
                    }
                });

                UserCacheUtils.fetchUsers(userIds);

            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }


    /**
     * 名字转拼音,取首字母
     * @param name
     * @return
     */
    private String getSortLetter(String name) {
        String letter = "#";
        if (name == null) {
            return letter;
        }
        //汉字转换成拼音
        String pinyin = characterParser.getSelling(name);
        String sortString = pinyin.substring(0, 1).toUpperCase(Locale.CHINESE);

        // 正则表达式，判断首字母是否是英文字母
        if (sortString.matches("[A-Z]")) {
            letter = sortString.toUpperCase(Locale.CHINESE);
        }
        return letter;
    }

    /**
     * 取sort_key的首字母
     * @param sortKey
     * @return
     */
    private String getSortLetterBySortKey(String sortKey) {
        if (sortKey == null || "".equals(sortKey.trim())) {
            return null;
        }
        String letter = "#";
        //汉字转换成拼音
        String sortString = sortKey.trim().substring(0, 1).toUpperCase(Locale.CHINESE);
        // 正则表达式，判断首字母是否是英文字母
        if (sortString.matches("[A-Z]")) {
            letter = sortString.toUpperCase(Locale.CHINESE);
        }
        return letter;
    }

    /**
     * 模糊查询
     * @param str
     * @return
     */
    private List<SortModel> search(String str) {
        List<SortModel> filterList = new ArrayList<SortModel>();// 过滤后的list
        for (SortModel contact : mAllContactsList) {
            if (contact.name != null) {
                //姓名全匹配,姓名首字母简拼匹配,姓名全字母匹配
                if (contact.name.toLowerCase(Locale.CHINESE).contains(str.toLowerCase(Locale.CHINESE))
                        || contact.sortKey.toLowerCase(Locale.CHINESE).replace(" ", "").contains(str.toLowerCase(Locale.CHINESE))
                        || contact.sortToken.simpleSpell.toLowerCase(Locale.CHINESE).contains(str.toLowerCase(Locale.CHINESE))
                        || contact.sortToken.wholeSpell.toLowerCase(Locale.CHINESE).contains(str.toLowerCase(Locale.CHINESE))) {
                    if (!filterList.contains(contact)) {
                        filterList.add(contact);
                    }
                }
            }
        }
        return filterList;
    }

    String chReg = "[\\u4E00-\\u9FA5]+";//中文字符串匹配

//    String chReg="[^\\u4E00-\\u9FA5]";//除中文外的字符匹配
    /**
     * 解析sort_key,封装简拼,全拼
     * @param sortKey
     * @return
     */
    public SortToken parseSortKey(String sortKey) {
        SortToken token = new SortToken();
        if (sortKey != null && sortKey.length() > 0) {
            //其中包含的中文字符
            String pinyin = characterParser.getSelling(sortKey.replace(" ", ""));
            String[] enStrs = pinyin.split(chReg);
            for (int i = 0, length = enStrs.length; i < length; i++) {
                if (enStrs[i].length() > 0) {
                    //拼接简拼
                    token.simpleSpell += enStrs[i].charAt(0);
                    token.wholeSpell += enStrs[i];
                }
            }
        }
        return token;
    }

    public void showDeleteDialog(final String memberId) {
        new AlertDialog.Builder(this).setMessage(R.string.contact_deleteContact)
                .setPositiveButton(R.string.common_sure, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final ProgressDialog dialog1 = showSpinnerDialog();
                        LeanchatUser.getCurrentUser().removeFriend(memberId, new SaveCallback() {
                            @Override
                            public void done(AVException e) {
                                dialog1.dismiss();
                                if (filterException(e)) {
                                    getMembers();
                                }
                            }
                        });
                    }
                }).setNegativeButton(R.string.chat_common_cancel, null).show();
    }


    @OnClick(R.id.btnBackground)
    public void hideKeyboard(){
        hideSoftInputView();
        btnBackground.setVisibility(View.GONE);
        searchView.clearFocus();
    }

    public void onEvent(ContactRefreshEvent event) {
        getMembers();
    }


    public void onEvent(ContactItemSelectEvent event) {
        btnRightTitle.setEnabled(event.isSelected);
    }

    public void onEvent(ContactItemLongClickEvent event) {
        showDeleteDialog(event.memberId);
    }



}
