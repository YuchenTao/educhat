package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

/**
 * Created by admin on 2016/3/3.
 */
public class AddFlowerActivity extends BaseActivity {
    private String conversationId;
    EditText flowernameEdit;
    Button addflowerbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_flower_activity);
        findView();
        initActionBar("添加红花");
        showLeftBackButton();

        Intent i=getIntent();
        conversationId=i.getStringExtra("groupId");
        addflowerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFlower();
            }
        });
        
    }

    /**
     * 添加新红花的方法
     */
    private void addFlower() {
        final String flowerName = flowernameEdit.getText().toString();
        if (TextUtils.isEmpty(flowerName)) {
            Utils.toast(this, "红花名不能为空");
            return;
        }
        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("groupId", conversationId);
        params.put("name", flowerName);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"flower/add";

        MyHttpClient.post(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {
                dismissSpinnerDialog();
               //添加成功，自动跳转到奖励理由页面
                Intent intent=new Intent(AddFlowerActivity.this,AwardReasonActivity.class);
                setResult(1,intent);
                toast("添加成功");
                finish();//传完结束掉该Activity
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
                finish();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });

    }

    /**
     * 初始化View
     */
    private void findView() {
        flowernameEdit= (EditText) findViewById(R.id.flowernameEdit);
        addflowerbtn= (Button) findViewById(R.id.btnAddFlower);
    }

}
