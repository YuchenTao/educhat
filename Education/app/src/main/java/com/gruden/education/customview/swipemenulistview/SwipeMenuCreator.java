package com.gruden.education.customview.swipemenulistview;


import com.gruden.education.customview.swipemenulistview.*;


public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
