package com.gruden.education.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by yunjiansun on 16/2/5.
 */
public class CommentItemHolder {
    public ImageView imgAvatar;
    public TextView txtUserName;
    public TextView txtCreatedAt;
    public TextView txtContent;
    public View layoutReply;
    public TextView txtReplyUser;
    public TextView txtReplyContent;
}
