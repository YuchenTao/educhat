package com.gruden.education.model;

/**
 * Created by yunjiansun on 16/2/18.
 */
public class Blog {
    public String date;
    public String id;
    public String title;
    public String content;
    public String imgUrl;
    public String createAt;
    public boolean isBottom;
}
