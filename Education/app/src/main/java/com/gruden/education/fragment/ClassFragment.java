package com.gruden.education.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.callback.AVIMConversationQueryCallback;
import com.avos.avoscloud.im.v2.callback.AVIMSingleMessageQueryCallback;
import com.avoscloud.leanchatlib.controller.ChatManager;
import com.avoscloud.leanchatlib.controller.ConversationHelper;
import com.avoscloud.leanchatlib.event.ConnectionChangeEvent;
import com.avoscloud.leanchatlib.event.ImTypeMessageEvent;
import com.avoscloud.leanchatlib.model.ConversationType;
import com.avoscloud.leanchatlib.model.Room;
import com.avoscloud.leanchatlib.utils.ConversationManager;
import com.gruden.education.R;
import com.gruden.education.activity.conversation.CreateClassActivity;
import com.gruden.education.activity.search.SearchActivity;
import com.gruden.education.adapter.ConversationListAdapter;
import com.gruden.education.customview.IndentItemDecoration;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.UserCacheUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;


public class ClassFragment extends BaseFragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @Bind(R.id.im_client_state_view)
    View imClientStateView;

    @Bind(R.id.class_srl_pullrefresh)
    protected SwipeRefreshLayout refreshLayout;

    @Bind(R.id.class_srl_view)
    protected RecyclerView recyclerView;


    protected ConversationListAdapter<Room> itemAdapter;
    protected LinearLayoutManager layoutManager;

    private boolean hidden;
    private ConversationManager conversationManager;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        conversationManager = ConversationManager.getInstance();
        itemAdapter = new ConversationListAdapter<Room>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.class_fragment, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        refreshLayout.setEnabled(false);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new IndentItemDecoration(getActivity()));
        recyclerView.setAdapter(itemAdapter);

        initConversationList();


        return view;
    }



    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        headerLayout.showTitle("班级");

        headerLayout.showLeftImageButton(R.drawable.search, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                getActivity().startActivity(intent);
            }
        });
        LeanchatUser user=LeanchatUser.getCurrentUser();
        if (user.getRoles().contains(LeanchatUser.TEACHER)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN)) {
            headerLayout.showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ctx, CreateClassActivity.class);
                    ctx.startActivity(intent);
                }
            });
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        this.hidden = hidden;
        if (!hidden) {
            updateConversationList();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!hidden) {
            updateConversationList();
        }
    }



    public void onEvent(ImTypeMessageEvent event) {
        updateConversationList();
    }

    private void initConversationList() {
        conversationManager.findAllConversationsIncludeMe(new AVIMConversationQueryCallback() {
            @Override
            public void done(List<AVIMConversation> conversations, AVIMException e) {
                Log.e("initConversationList","initConversationList");
                if (filterException(e)) {

                    for (AVIMConversation c : conversations){
                        Log.e("ConversationId",c.getConversationId());
                        ChatManager.getInstance().getRoomsTable().insertRoom(c.getConversationId(),Integer.parseInt(c.getAttribute("type").toString()));
                    }
                    updateConversationList();
                }
            }
        });


    }

    private void updateConversationList() {
        conversationManager.findAndCacheGroupRooms(new Room.MultiRoomsCallback() {
            @Override
            public void done(List<Room> roomList, AVException exception) {
                if (filterException(exception)) {

                    updateLastMessage(roomList);
                    cacheRelatedUsers(roomList);
                    setItemType(roomList);

                    List<Room> sortedRooms = sortRooms(roomList);
                    itemAdapter.setDataList(sortedRooms);
                    itemAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    private void updateLastMessage(final List<Room> roomList) {
        for (final Room room : roomList) {
            AVIMConversation conversation = room.getConversation();
            if (null != conversation) {
                conversation.getLastMessage(new AVIMSingleMessageQueryCallback() {
                    @Override
                    public void done(AVIMMessage avimMessage, AVIMException e) {
                        if (filterException(e) && null != avimMessage) {
                            room.setLastMessage(avimMessage);
                            int index = roomList.indexOf(room);
                            itemAdapter.notifyItemChanged(index);
                        }
                    }
                });
            }
        }
    }

    private void cacheRelatedUsers(List<Room> rooms) {
        List<String> needCacheUsers = new ArrayList<String>();
        for(Room room : rooms) {
            AVIMConversation conversation = room.getConversation();
            if (ConversationHelper.typeOfConversation(conversation) == ConversationType.Private) {
                needCacheUsers.add(ConversationHelper.otherIdOfConversation(conversation));
            }
        }
        UserCacheUtils.fetchUsers(needCacheUsers, new UserCacheUtils.CacheUserCallback() {
            @Override
            public void done(List<LeanchatUser> userList, Exception e) {
                itemAdapter.notifyDataSetChanged();
            }
        });
    }

    private List<Room> sortRooms(final List<Room> roomList) {
        List<Room> sortedList = new ArrayList<Room>();
        if (null != roomList) {
            sortedList.addAll(roomList);
            Collections.sort(sortedList, new Comparator<Room>() {
                @Override
                public int compare(Room lhs, Room rhs) {
                    long value = lhs.getLastModifyTime() - rhs.getLastModifyTime();
                    if (value > 0) {
                        return -1;
                    } else if (value < 0) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            });
        }
        return sortedList;
    }


    private void setItemType(final List<Room> roomList) {
        for (Room room : roomList) {
            room.itemType = 1;
        }
    }

    public void onEvent(ConnectionChangeEvent event) {
        if(imClientStateView.getVisibility()==View.VISIBLE||(!event.isConnect)){
            updateConversationList();
        }
        imClientStateView.setVisibility(event.isConnect ? View.GONE : View.VISIBLE);

    }


//    public void onEvent(MessageItemClickEvent event) {
//        Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
//        intent.putExtra(Constants.CONVERSATION_ID, event.conversationId);
//        startActivity(intent);
//    }

}
