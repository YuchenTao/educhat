package com.gruden.education.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.gruden.education.R;
import com.gruden.education.util.ImageUtil;
import com.gruden.education.viewholder.BaseViewHolder;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/1/25.
 */
public class MyGridViewAdapter extends ArrayAdapter<String> {


    private final RelativeLayout.LayoutParams params;
    int rowHeight;
    int screenWidth;
    
    public List<String> dataList = new ArrayList<String>();

    public List<String> getDataList() {
        return dataList;
    }

    public void setDataList(List<String> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }


    public void addDataList(List<String> datas) {
        dataList.addAll( datas);
    }

    public MyGridViewAdapter(Context context, List<String> objects) {
        super(context, 0);
        setDataList(objects);
        screenWidth = ImageUtil.getScreenWidth(getContext());// 屏幕宽度px
        int itemPadding = (int) context.getResources().getDimension(R.dimen.gridPadding); // px
        // 计算每张图片大小：屏幕宽度 - GridView右边距 - GridView每个item的边距 * 4，结果除以4， 就是每张图片的大小（px）
        int imageSize = (screenWidth - itemPadding - itemPadding * 4) / 4;
        params = new RelativeLayout.LayoutParams(imageSize, imageSize);

        rowHeight = imageSize+itemPadding;
    }

    @Override
    public int getCount() {
        return dataList.size()+1;
    }

    @Override
    public String getItem(int position) {
        return dataList.get(position);
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        BaseViewHolder holder = BaseViewHolder.getViewHolder(getContext(), convertView, parent,R.layout.gridview_item_layout, position);
        ImageView image = (ImageView) holder.getView(R.id.image);
        ImageView addImg = (ImageView) holder.getView(R.id.addImg);
        image.setLayoutParams(params);// 设置图片大小
        addImg.setLayoutParams(params);// 设置图片大小
        if(position == getCount() - 1){// 最后一张图片显示添加图片
            image.setVisibility(View.GONE);
            addImg.setVisibility(View.VISIBLE);// 显示添加图片
        }else{
            image.setVisibility(View.VISIBLE);
            addImg.setVisibility(View.GONE);
            ImageLoader.getInstance().displayImage("file://" + getItem(position), image, PhotoUtils.normalImageOptions);
        }

        int rowCount = getCount()/4+1;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(screenWidth,rowHeight*rowCount );
        parent.setLayoutParams(params);

        return holder.getConvertView();
    }

    @Override public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }


}
