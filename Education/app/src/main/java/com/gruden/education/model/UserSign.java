package com.gruden.education.model;

import org.json.JSONArray;

/**
 * Created by yunjiansun on 16/2/23.
 */
public class UserSign {

    public String userId;
    public String remarkName;
    public String avatar;
    public String phone;
    public String createdAt;
    public boolean isNone;

}
