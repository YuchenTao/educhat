package com.gruden.education.activity.profile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.ProgressCallback;
import com.avos.avoscloud.SaveCallback;
import com.bigkoo.pickerview.OptionsPickerView;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.customview.LocationPickerView;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.PathUtils;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;

import butterknife.Bind;
import butterknife.OnClick;

public class InformationActivity extends BaseActivity {

    private static final int IMAGE_PICK_REQUEST = 1;
    private static final int CROP_REQUEST = 2;

    @Bind(R.id.avatar_view)
    ImageView avatarView;

    @Bind(R.id.txtNickName)
    TextView txtNickName;

    @Bind(R.id.txtPosition)
    TextView txtPosition;

    @Bind(R.id.txtRole)
    TextView txtRole;

    @Bind(R.id.txtPhone)
    TextView txtPhone;

    @Bind(R.id.txtPassword)
    TextView txtPassword;

    LocationPickerView cityPicker;
    String provinceCode = "015";
    String cityCode = "015002";
    String areaCode = null;
    String strPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        initActionBar("完善资料");
        showLeftBackButton();

        cityPicker  = new LocationPickerView(this);
        cityPicker.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                //返回的分别是三个级别的选中位置
                areaCode = cityPicker.arrAreaCode.get(options3);
                strPosition = "山东省 青岛市 " + cityPicker.options3Items.get(options1).get(option2).get(options3);
                updatePosition();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh() {
        LeanchatUser curUser = LeanchatUser.getCurrentUser();
        ImageLoader.getInstance().displayImage(curUser.getAvatarUrl(), avatarView, com.avoscloud.leanchatlib.utils.PhotoUtils.avatarImageOptions);
        txtNickName.setText(curUser.getNickName());
        txtPosition.setText(curUser.getPosition());

        String role = "家长";
        if (curUser.getRoles().contains(LeanchatUser.TEACHER))
            role = "老师";

        txtRole.setText(role);

        txtPhone.setText(curUser.getMobilePhoneNumber());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == IMAGE_PICK_REQUEST) {
                Uri uri = data.getData();
                startImageCrop(uri, 200, 200, CROP_REQUEST);
            } else if (requestCode == CROP_REQUEST) {

                final byte[]  pic = getImageByte(data);
                uploadUserAvatar(pic);
            }
        }
    }

    public Uri startImageCrop(Uri uri, int outputX, int outputY,
                              int requestCode) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("scale", true);
        String outputPath = PathUtils.getAvatarTmpPath();
        Uri outputUri = Uri.fromFile(new File(outputPath));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
        intent.putExtra("return-data", true);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG);
        intent.putExtra("noFaceDetection", false); // face detection
        startActivityForResult(intent, requestCode);
        return outputUri;
    }

    private  byte[] getImageByte(Intent data) {
        Bundle extras = data.getExtras();
        Bitmap bitmap=null;
        byte[] result=null;

        if (extras != null) {
            bitmap = extras.getParcelable("data");
            if (bitmap != null) {
//                bitmap = com.avoscloud.leanchatlib.utils.PhotoUtils.toRoundCorner(bitmap, 10);

//                Drawable drawable = new BitmapDrawable(getResources(),bitmap);
//                avatarView.setImageDrawable(drawable);
//                avatarView.setImageBitmap(bitmap);
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
                result = output.toByteArray();
                try {
                    output.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }


    public void uploadUserAvatar(final byte[] pic) {
        final AVFile file;
        try {
            showSpinnerDialog();
            file =new AVFile(LeanchatUser.getCurrentUser().getObjectId()+".jpeg",pic);
            file.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    String picUrl = file.getUrl();
                    if (picUrl!=null) {
                        updateUserAvatar(picUrl );
                    }
                }
            }, new ProgressCallback() {
                @Override
                public void done(Integer integer) {
                    System.out.println("precentCode：" + integer);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //修改修改头像
    private void updateUserAvatar(final String picUrl ) {

        ImageLoader.getInstance().displayImage(picUrl, avatarView, com.avoscloud.leanchatlib.utils.PhotoUtils.avatarImageOptions);

        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("avatar",picUrl);
        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "profile";
        MyHttpClient.put(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
//                ImageLoader.getInstance().loadImage(picUrl,null);
                System.out.println("头像URL：" + picUrl);
                LeanchatUser user = LeanchatUser.getCurrentUser();
                user.setAvatarUrl(picUrl);
                LeanchatUser.changeCurrentUser(user, true);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });


    }

    void updatePosition(){

        if (areaCode==null){
            toast("地区不能为空");
        }

        showSpinnerDialog();

        LeanchatUser user = LeanchatUser.getCurrentUser();
        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("roles", user.getRoles());

        params.put("provinceCode", provinceCode);
        params.put("cityCode", cityCode);
        params.put("areaCode", areaCode);
        params.put("position", strPosition);

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "profile";
        MyHttpClient.put(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg) {

                dismissSpinnerDialog();
                switch (code) {
                    case 0: {
                        toast(mesg);
                        txtPosition.setText(strPosition);
                        LeanchatUser user = LeanchatUser.getCurrentUser();
                        user.setPosition(strPosition);
                        user.setProvince(provinceCode);
                        user.setCity(cityCode);
                        user.setArea(areaCode);
                        LeanchatUser.changeCurrentUser(user, true);
                        break;
                    }

                    default: {
                        toast(mesg);
                        break;
                    }
                }
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });

    }
    @OnClick(R.id.avatar_layout)
    public void onAvatarClick() {
        Intent intent = new Intent(Intent.ACTION_PICK, null);
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, IMAGE_PICK_REQUEST);
    }

    @OnClick(R.id.username_layout)
    public void onUsernameClick() {
        Intent intent = new Intent(this, NickNameEditActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.position_layout)
    public void onPositionClick(){
        if (cityPicker!=null&&cityPicker.isOK)
            cityPicker.show();
    }

    @OnClick(R.id.role_layout)
    public void onRoleClick() {
        Intent intent = new Intent(this, RoleEditActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.password_layout)
    public void onPasswordClick(){
        Intent intent = new Intent(this, PasswordRevise.class);
        startActivity(intent);
    }


}
