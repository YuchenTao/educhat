package com.gruden.education.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.App;
import com.gruden.education.R;
import com.gruden.education.activity.MainActivity;
import com.gruden.education.activity.bbs.PublicPostActivity;
import com.gruden.education.adapter.BBSPostListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.BBSPost;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostListFragment extends BaseFragment {

    public String tagCode;
    public String topicTitle;
    int curPage ;
    BBSPostListAdapter postListAdapter;

    private boolean isRefreshing;
    private boolean isLoadMoreing;

    @Bind(R.id.hot_post_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.hot_post_listview)
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postListAdapter = new BBSPostListAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.hot_post_fragment, container, false);

        ButterKnife.bind(this, view);

        listView.setAdapter(postListAdapter);


        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                if (topicTitle!=null&&tagCode==null)
                    concernedPostList(0);
                else
                    updatePostList(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                if (topicTitle!=null&&tagCode==null)
                    concernedPostList(curPage+1);
                else
                    updatePostList(curPage+1);
            }
        });

        return  view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (topicTitle==null)
            headerLayout.showTitle("热门话题");
        else
            headerLayout.showTitle(topicTitle);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.remove(App.ctx.mainActivity.postListFragment);

                if (App.ctx.mainActivity.topicSquareFragment != null){
                    transaction.show(App.ctx.mainActivity.topicSquareFragment);
                }else {
                    transaction.show(App.ctx.mainActivity.bbsFragment);
                }

                transaction.commit();

                App.ctx.mainActivity.postListFragment = null;

            }
        };
        headerLayout.showLeftBackButton(listener);
        headerLayout.showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, PublicPostActivity.class);
                ctx.startActivity(intent);
            }
        });

        refreshLayout.autoRefresh();
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    public void updatePostList(final int page){

        RequestParams params = new RequestParams();
        params.put("curPage",page);
        if (tagCode!=null)
            params.put("tagCode",tagCode);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_FORUM +"post/list/hot";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0){
                    Utils.toast(getActivity(), "已经没有更多帖子了~");
                }else {
                    ArrayList<BBSPost> postList = new ArrayList<BBSPost>();

                    for (int i=0; i<array.length(); i++){

                        JSONObject data = array.optJSONObject(i);
                        BBSPost post = new BBSPost();
                        post.postId = data.optString("postId");
                        post.userId = data.optString("userId");
                        post.nickname = data.optString("nickname");
                        post.urlAvatar = data.optString("avatar");
                        post.title = data.optString("title");
                        post.replyCount = data.optInt("replyCount");
                        post.time = data.optString("createdAt");

                        postList.add(post);
                    }

                    if (page==0) {
                        postListAdapter.setDataList(postList);
                    }
                    else {
                        postListAdapter.addDataList(postList);
                    }

                    postListAdapter.notifyDataSetChanged();
                    curPage = page;
                }

                dissmissCircle();

            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }

        });
    }
    public void concernedPostList(final int page){

        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("curPage",page);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_FORUM +"post/list/concerned";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0){
                    Utils.toast(getActivity(), "已经没有更多帖子了~");
                }else {
                    ArrayList<BBSPost> postList = new ArrayList<BBSPost>();

                    for (int i=0; i<array.length(); i++){

                        JSONObject data = array.optJSONObject(i);
                        BBSPost post = new BBSPost();
                        post.postId = data.optString("postId");
                        post.userId = data.optString("userId");
                        post.nickname = data.optString("nickname");
                        post.urlAvatar = data.optString("avatar");
                        post.title = data.optString("title");
                        post.replyCount = data.optInt("replyCount");
                        post.time = data.optString("createdAt");

                        postList.add(post);
                    }

                    if (page==0) {
                        postListAdapter.setDataList(postList);
                    }
                    else {
                        postListAdapter.addDataList(postList);
                    }

                    postListAdapter.notifyDataSetChanged();
                    curPage = page;
                }

                dissmissCircle();

            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }

        });
    }

    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }
}
