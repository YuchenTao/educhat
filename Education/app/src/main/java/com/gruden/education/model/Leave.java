package com.gruden.education.model;

/**
 * Created by yunjiansun on 16/1/27.
 */
public class Leave {
    public String date;
    public String groupId;
    public String id;
    public String childId;
    public String childName;
    public String reason;
    public String creator;
    public String startDate;
    public int state;
    public String time;
    public boolean isCreatedByMe;
    public boolean isBottom;
}
