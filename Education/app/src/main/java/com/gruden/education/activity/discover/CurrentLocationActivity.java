package com.gruden.education.activity.discover;

import com.gruden.education.activity.BaseActivity;

/**
 * Created by admin on 2016/4/21.
 */
public class CurrentLocationActivity extends BaseActivity  {



//    MapView mMapView;
//    BaiduMap mBaiduMap;
//    ProgressBar mLoadBar;
//    ImageView mSelectImg;
//
//
//    // 定位
//    LocationClient mLocationClient = null;
//    MyBDLocationListner mListner = null;
//    BitmapDescriptor mCurrentMarker = null;
//
//    // 当前经纬度
//    double mLantitude;
//    double mLongtitude;
//    LatLng mLoactionLatLng;
//
//    // 设置第一次定位标志
//    boolean isFirstLoc = true;
//
//    // MapView中央对于的屏幕坐标
//    Point mCenterPoint = null;
//
//    // 地理编码
//    GeoCoder mGeoCoder = null;
//
//    // 位置列表
//    ListView mListView;
//    LocNearAddressAdapter mAdapter;
//    ArrayList<PoiInfo> mInfoList;
//    PoiInfo mCurentInfo;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_current_location);
//
//        initView();
//    }
//
//    /**
//     * 初始化界面
//     */
//    private void initView() {
//// 初始化地图
//        mMapView = (MapView) findViewById(R.id.mapView_location);
////        mMapView.showZoomControls(false);
//        mBaiduMap = mMapView.getMap();
//        MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(17.0f);
//        mBaiduMap.setMapStatus(msu);
////        mBaiduMap.setOnMapTouchListener(touchListener);
//
//// 初始化POI信息列表
//        mInfoList = new ArrayList<PoiInfo>();
//
//// 初始化当前MapView中心屏幕坐标，初始化当前地理坐标
//        mCenterPoint = mBaiduMap.getMapStatus().targetScreen;
//        mLoactionLatLng = mBaiduMap.getMapStatus().target;
//
//// 定位
//        mBaiduMap.setMyLocationEnabled(true);
//        mLocationClient = new LocationClient(this);
//        mListner = new MyBDLocationListner();
//        mLocationClient.registerLocationListener(mListner);
//        LocationClientOption option = new LocationClientOption();
//        option.setOpenGps(true);// 打开gps
//        option.setCoorType("bd09ll"); // 设置坐标类型
//        option.setScanSpan(1000);
//        mLocationClient.setLocOption(option);
//        mLocationClient.start();
//
//// 地理编码
//        mGeoCoder = GeoCoder.newInstance();
//        mGeoCoder.setOnGetGeoCodeResultListener(GeoListener);
//
//// 周边位置列表
//
//
////        mListView.setOnItemClickListener(itemClickListener);
//        mAdapter = new LocNearAddressAdapter(this,mInfoList);
//        mListView.setAdapter(mAdapter);
//
//        mSelectImg = new ImageView(this);
//    }
//
//    public void turnBack(View view) {
//// 实现动画跳转
//        MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(mLoactionLatLng);
//        mBaiduMap.animateMapStatus(u);
//
//        mBaiduMap.clear();
//// 发起反地理编码检索
//        mGeoCoder.reverseGeoCode((new ReverseGeoCodeOption())
//                .location(mLoactionLatLng));
//
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mLocationClient.stop();
//        mGeoCoder.destroy();
//    }
//
//    // 定位监听器
//    private class MyBDLocationListner implements BDLocationListener {
//
//        @Override
//        public void onReceiveLocation(BDLocation location) {
//// map view 销毁后不在处理新接收的位置
//            if (location == null )
//                return;
//            MyLocationData data = new MyLocationData.Builder()//
//// .direction(mCurrentX)//
//                    .accuracy(location.getRadius())//
//                    .latitude(location.getLatitude())//
//                    .longitude(location.getLongitude())//
//                    .build();
//            mBaiduMap.setMyLocationData(data);
//// 设置自定义图标
////            MyLocationConfiguration config = new MyLocationConfiguration(
////                    MyLocationConfigeration.LocationMode.NORMAL, true, null);
////            mBaiduMap.setMyLocationConfigeration(config);
//
//            mLantitude = location.getLatitude();
//            mLongtitude = location.getLongitude();
//
//            LatLng currentLatLng = new LatLng(mLantitude, mLongtitude);
//            mLoactionLatLng = new LatLng(mLantitude, mLongtitude);
//
//// 是否第一次定位
//            if (isFirstLoc) {
//                isFirstLoc = false;
//// 实现动画跳转
//                MapStatusUpdate u = MapStatusUpdateFactory
//                        .newLatLng(currentLatLng);
//                mBaiduMap.animateMapStatus(u);
//
//                mGeoCoder.reverseGeoCode((new ReverseGeoCodeOption())
//                        .location(currentLatLng));
//                return;
//            }
//
//        }
//
//    }
//
//    // 地理编码监听器
//    OnGetGeoCoderResultListener GeoListener = new OnGetGeoCoderResultListener() {
//        public void onGetGeoCodeResult(GeoCodeResult result) {
//            if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
//// 没有检索到结果
//            }
//// 获取地理编码结果
//        }
//
//        @Override
//        public void onGetReverseGeoCodeResult(ReverseGeoCodeResult result) {
//            if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
//// 没有找到检索结果
//            }
//// 获取反向地理编码结果
//            else {
//// 当前位置信息
//                mCurentInfo = new PoiInfo();
//                mCurentInfo.address = result.getAddress();
//                mCurentInfo.location = result.getLocation();
//                mCurentInfo.name = "[位置]";
//                mInfoList.clear();
//                mInfoList.add(mCurentInfo);
//
//// 将周边信息加入表
//                if (result.getPoiList() != null) {
//                    mInfoList.addAll(result.getPoiList());
//                }
//// 通知适配数据已改变
//                mAdapter.notifyDataSetChanged();
//                mLoadBar.setVisibility(View.GONE);
//
//            }
//        }
//    };
//
//    // 地图触摸事件监听器
//    BaiduMap.OnMapClickListener touchListener = new BaiduMap.OnMapClickListener() {
//
//
//        @Override
//        public void onMapClick(LatLng latLng) {
//
//        }
//
//        @Override
//        public boolean onMapPoiClick(MapPoi mapPoi) {
//            return false;
//        }
//
//        public void onTouch(MotionEvent event) {
//            if (event.getAction() == MotionEvent.ACTION_UP) {
//
//                if (mCenterPoint == null) {
//                    return;
//                }
//
//// 获取当前MapView中心屏幕坐标对应的地理坐标
//                LatLng currentLatLng;
//                currentLatLng = mBaiduMap.getProjection().fromScreenLocation(
//                        mCenterPoint);
//                System.out.println("----" + mCenterPoint.x);
//                System.out.println("----" + currentLatLng.latitude);
//// 发起反地理编码检索
//                mGeoCoder.reverseGeoCode((new ReverseGeoCodeOption())
//                        .location(currentLatLng));
//                mLoadBar.setVisibility(View.VISIBLE);
//
//            }
//        }
//    };

//    // listView选项点击事件监听器
//    AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
//
//        @Override
//        public void onItemClick(AdapterView parent, View view, int position,
//                                long id) {
//
//// 通知是适配器第position个item被选择了
//            mAdapter.setNotifyTip(position);
//
////            BitmapDescriptor mSelectIco = BitmapDescriptorFactory
////                    .fromResource(R.drawable.icon_geo);
//            mBaiduMap.clear();
//            PoiInfo info = (PoiInfo) mAdapter.getItem(position);
//            LatLng la = info.location;
//
//// 动画跳转
//            MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(la);
//            mBaiduMap.animateMapStatus(u);
//
//// 添加覆盖物
//            OverlayOptions ooA = new MarkerOptions().position(la)
//                    .icon(mSelectIco).anchor(0.5f, 0.5f);
//            mBaiduMap.addOverlay(ooA);
//
//// 选中项打勾
////            mSelectImg.setBackgroundResource(R.drawable.greywhite);
////            mSelectImg = (ImageView) view.findViewById(R.id.place_select);
////            mSelectImg.setBackgroundResource(R.drawable.ic_select);
//
//// Uri mUri = Uri.parse("geo:39.940409,116.355257");
//// Intent mIntent = new Intent(Intent.ACTION_VIEW,mUri);
//// startActivity(mIntent);
//
//        }
//
//    };




//class  LocNearAddressAdapter extends BaseAdapter{
//        Context context;
//        ArrayList<PoiInfo> data=new ArrayList<PoiInfo>();
//
//        public LocNearAddressAdapter(Context context, ArrayList<PoiInfo> data) {
//            this.context = context;
//            this.data = data;
//        }
//
//        @Override
//        public int getCount() {
//            return data.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return data.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            PoiInfo poiInfo=data.get(position);
//            LocationHolder holder = null;
//            if (convertView==null){
//                holder=new LocationHolder();
//                convertView=View.inflate(context,R.layout.current_location_item,null);
//                holder.txtCurLocation= (TextView) convertView.findViewById(R.id.txtCurLocation);
//                holder.txtLocation= (TextView) convertView.findViewById(R.id.txtLocation);
//                convertView.setTag(holder);
//
//            }else{
//                holder= (LocationHolder) convertView.getTag();
//            }
//            holder.txtCurLocation.setText(poiInfo.name);
//            holder.txtLocation.setText(poiInfo.address);
//            return convertView;
//        }
//    }
//    class LocationHolder{
//        TextView txtCurLocation;
//        TextView txtLocation;
//    }

}
