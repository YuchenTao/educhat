package com.gruden.education.activity.entry;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;

import java.util.Timer;
import java.util.TimerTask;

public class EntryForgetActivity extends EntryBaseActivity {
    private long timedown=3000;
    private Timer timer;
    private int second=0;
    EditText phoneNumEdit,smsCodeEdit;
    Button sendCodebtn,ensurebtn;
    TextView codeText;
    String phoneNum,smsCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_forget);


        findView();
        initActionBar("忘记密码");
        showLeftBackButton();

        sendCodebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                getCode();
            }
        });

        ensurebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                getEnsure();
            }
        });
    }

    private void getCode() {
        phoneNum = phoneNumEdit.getText().toString();
        if (TextUtils.isEmpty(phoneNum)) {
            codeText.setText( "手机号不能为空");
            return;
        }


        settimer();

        RequestParams params = new RequestParams();
        params.put("type", "1");
        params.put("mobilePhoneNumber", phoneNum);
        String url = MyHttpClient.BASE_URL + "sms/sendSms";
//        showSpinnerDialog();

        MyHttpClient.post(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

//                dismissSpinnerDialog();
                if (array.length() > 0)
                    smsCode = array.optString(0);
                smsCodeEdit.setText(smsCode);
            }

            @Override
            public void handleSuccess(int code, String mesg) {

//                dismissSpinnerDialog();

                toast(mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }
        });


    }

    private void getEnsure() {
        phoneNum = phoneNumEdit.getText().toString();
        final String strSmsCode = smsCodeEdit.getText().toString();

        if (TextUtils.isEmpty(phoneNum)) {
            codeText.setText( "手机号不能为空");
            return;
        }

        if (TextUtils.isEmpty(smsCode)) {
            codeText.setText("验证码不能为空");
            return;
        }
        if (!strSmsCode.equals(smsCode)){
            codeText.setText("验证码错误,请重新输入");
            return;
        }


        Intent intent = new Intent(EntryForgetActivity.this, EntryResetActivity.class);
        intent.putExtra("phoneNum", phoneNum);
        intent.putExtra("smsCode", smsCode);
        startActivity(intent);



    }

    private void findView() {
        phoneNumEdit = (EditText) findViewById(R.id.phoneNumEdit);
        smsCodeEdit = (EditText) findViewById(R.id.smsWordEdit);
        sendCodebtn = (Button) findViewById(R.id.btnSendCode);
        ensurebtn = (Button) findViewById(R.id.btnEnsure);
        codeText= (TextView) findViewById(R.id.textWrongCode);

    }

    String sBuffer="";

    /**
     * 倒计时
     */
    private  void settimer(){
        sendCodebtn.setBackgroundResource(R.drawable.common_btn_bg_pressed);
        sendCodebtn.setTextColor(0xff90a3a5);
        timer =new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timedown -= 1000;
                second = (int) (timedown / 1000 % 60);
                sBuffer = "";
                sBuffer += second;
                sendCodebtn.post(new Runnable() {
                    @Override
                    public void run() {
                        sendCodebtn.setText("已发送（" + sBuffer + "s)");
                    }
                });
                if (timedown == 0) {
                    if (timer != null) {
                        timer.cancel();
                        timer = null;
                    }
                    timedown = 3000;
                    Message message = new Message();
                    message.what = 1;
                    handler.sendMessage(message);
                }


            }
        },1000,1000);


    }
    Handler handler = new Handler(){
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    sendCodebtn.setBackgroundResource(R.drawable.common_btn_bg_normal);
                    sendCodebtn.setTextColor(0xff82d3cc);
                    sendCodebtn.setText("重新获取");
                    break;
            }
            super.handleMessage(msg);
        }

    };


}
