package com.gruden.education.download;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * Author: wyouflf
 * Date: 13-11-10
 * Time: 下午8:11
 */
@Table(name = "downloads", onCreated = "CREATE UNIQUE INDEX realative_unique ON downloads(fileId,groupId)")
public class DownloadInfos {

    public DownloadInfos() {
    }

    @Column(name = "id", isId = true)
    private long  id;

    @Column(name = "state")
    private DownloadState state = DownloadState.WAITING;

    @Column(name = "url")
    private String url;

    @Column(name = "label")
    private String label;

    @Column(name = "fileSavePath")
    private String fileSavePath;

    @Column(name = "progress")
    private int progress;

    @Column(name = "fileLength")
    private long fileLength;

    @Column(name = "autoResume")
    private boolean autoResume;

    @Column(name = "autoRename")
    private boolean autoRename;

    @Column(name = "remarkName")
    private String  remarkName;

    @Column(name = "downloadCount")
    private int  downloadCount;

    @Column(name = "createdAt")
    private String createdAt;

    @Column(name = "fileId")
    private String fileId;

    @Column(name = "groupId")
    private String groupId;

    @Column(name = "imgUrl")
    private int imgUrl;

    @Column(name = "isDone")
    private boolean  isDone=false;

    public int getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(int imgUrl) {
        this.imgUrl = imgUrl;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DownloadState getState() {
        return state;
    }

    public void setState(DownloadState state) {
        this.state = state;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getFileSavePath() {
        return fileSavePath;
    }

    public void setFileSavePath(String fileSavePath) {
        this.fileSavePath = fileSavePath;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public boolean isAutoResume() {
        return autoResume;
    }

    public void setAutoResume(boolean autoResume) {
        this.autoResume = autoResume;
    }

    public boolean isAutoRename() {
        return autoRename;
    }

    public void setAutoRename(boolean autoRename) {
        this.autoRename = autoRename;
    }

    public String getRemarkName() {
        return remarkName;
    }

    public void setRemarkName(String remarkName) {
        this.remarkName = remarkName;
    }

    public int getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(int downloadCount) {
        this.downloadCount = downloadCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DownloadInfos)) return false;

        DownloadInfos that = (DownloadInfos) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public String toString() {
        return "DownloadInfos{" +
                "id=" + id +
                ", state=" + state +
                ", url='" + url + '\'' +
                ", label='" + label + '\'' +
                ", fileSavePath='" + fileSavePath + '\'' +
                ", progress=" + progress +
                ", fileLength=" + fileLength +
                ", autoResume=" + autoResume +
                ", autoRename=" + autoRename +
                ", remarkName='" + remarkName + '\'' +
                ", downloadCount=" + downloadCount +
                ", createdAt='" + createdAt + '\'' +
                ", fileId='" + fileId + '\'' +
                ", groupId='" + groupId + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return  (int) (id ^ (id >>> 32));
    }
}
