package com.gruden.education.activity.profile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.ChildrenListAdapter;
import com.gruden.education.model.Child;
import com.gruden.education.model.LeanchatUser;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class ChildrenListActivity extends BaseActivity {

    ChildrenListAdapter listAdapter;
    List<Child> childrenList;


    @Bind(R.id.listView)
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_children_list);

        initActionBar("我的孩子");
        showLeftBackButton();
//        showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(ChildrenListActivity.this, ChildDetailActivity.class);
//                startActivity(intent);
//            }
//        });

        listAdapter = new ChildrenListAdapter(this);
        listView.setAdapter(listAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    void refresh(){

        LeanchatUser user = LeanchatUser.getCurrentUser();
        childrenList = user.getChildren();
        if (childrenList!=null&&childrenList.size()>0){
            listAdapter.setDataList(childrenList);
            listAdapter.notifyDataSetChanged();
        }
    }
}
