package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.LeaveListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.Leave;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

public class LeaveListActivity extends BaseActivity {

    private int curPage;
    private String conversationId;

    private boolean isRefreshing;
    private boolean isLoadMoreing;

    private LeaveListAdapter listAdapter;

    @Bind(R.id.homework_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.listView)
    ListView listView;

    @Bind(R.id.txtPrompt)
    TextView txtPrompt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work_list);

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");

        initActionBar("请假");
        showLeftBackButton();
        LeanchatUser user = LeanchatUser.getCurrentUser();
        if(user.getRoles().contains(LeanchatUser.PARENT)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN)){
            showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(LeaveListActivity.this, WriteLeaveNoteActivity.class);
                    intent.putExtra("id", conversationId);
                    startActivity(intent);
                }
            });
        }


        listAdapter = new LeaveListAdapter(this, conversationId);
        listView.setAdapter(listAdapter);

        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                updateLeaveList(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                updateLeaveList(curPage + 1);
            }

            @Override
            public void onfinish(){
                txtPrompt.setVisibility(listAdapter.getCount()>0?View.INVISIBLE:View.VISIBLE);
                txtPrompt.setText("暂时没有请假~");
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }

    void updateLeaveList(final int page){
        RequestParams params = new RequestParams();
        params.put("groupId",conversationId);
        params.put("curPage",page);

        final LeanchatUser user = LeanchatUser.getCurrentUser();
        params.put("userId",user.getObjectId());
        params.put("role",(user.getRoles().contains(LeanchatUser.TEACHER)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN))?"1":"0");

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"leave";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0&&page>0){

                    toast("已经没有更多假条了~");

                }else {
                    ArrayList<Leave> leaveList = new ArrayList<Leave>();
                    for (int i=0; i<array.length(); i++){

                        JSONObject data = array.optJSONObject(i);
                        Leave leave1 = new Leave();
                        leave1.date = data.optString("date") + " " + data.optString("weekDay");
                        leaveList.add(leave1);

                        JSONArray arr = data.optJSONArray("data");
                        for (int j=0; j<arr.length(); j++){

                            JSONObject leave = arr.optJSONObject(j);
                            Leave leave2 = new Leave();
                            leave2.id = leave.optString("id");

                            if (user.getObjectId().equals(leave.optString("creator")))
                                 leave2.isCreatedByMe = true;

                            leave2.groupId = conversationId;
                            leave2.reason = leave.optString("reason");
                            leave2.childName = leave.optString("childName");
                            leave2.childId = leave.optString("childId");
                            leave2.startDate = leave.optString("startDate");
                            leave2.state = leave.optInt("state");
                            leave2.time=leave.optString("time");
                            if (j==arr.length()-1)
                                leave2.isBottom = true;
                            leaveList.add(leave2);
                        }
                    }

                    if (page==0) {
                        listAdapter.setDataList(leaveList);
                    }
                    else {
                        listAdapter.addDataList(leaveList);
                    }

                    listAdapter.notifyDataSetChanged();
                    curPage = page;
                }

                dissmissCircle();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }

        });
    }

    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }


}
