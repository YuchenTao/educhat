package com.gruden.education.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.CountCallback;
import com.gruden.education.R;
import com.gruden.education.activity.discover.contact.ContactActivity;
import com.gruden.education.activity.discover.friendcircle.GrowthLogActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.event.InvitationEvent;
import com.gruden.education.friends.AddRequestManager;
import com.gruden.education.friends.ContactNewFriendActivity;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by lzw on 14-9-17.
 */
public class DiscoverFragment extends BaseFragment {

  @Bind(R.id.request_tips)
  ImageView requestTipsView;
  private LeanchatUser user=LeanchatUser.getCurrentUser();

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.discover_fragment, container, false);
    ButterKnife.bind(this, view);

    EventBus.getDefault().register(this);

    return view;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    headerLayout.showTitle("发现");
//    refresh();

  }



  @Override
  public void onResume() {
    super.onResume();
//    updateNewRequestBadge();
    getUntreatedApplyCount();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    EventBus.getDefault().unregister(this);
  }


  private void updateNewRequestBadge() {
    requestTipsView.setVisibility(
            AddRequestManager.getInstance().hasUnreadRequests() ? View.VISIBLE : View.GONE);
  }

  private void refresh() {
    AddRequestManager.getInstance().countUnreadRequests(new CountCallback() {
      @Override
      public void done(int i, AVException e) {
        updateNewRequestBadge();
      }
    });
  }

  public void onEvent(InvitationEvent event) {
    AddRequestManager.getInstance().unreadRequestsIncrement();
    updateNewRequestBadge();
  }

  @OnClick(R.id.logLayout)
  public void onLogClick(){
    Intent intent = new Intent(ctx, GrowthLogActivity.class);
    ctx.startActivity(intent);
  }

  @OnClick(R.id.contactLayout)
  public void onContactClick(){
    Intent intent = new Intent(ctx, ContactActivity.class);
    ctx.startActivity(intent);
  }

  @OnClick(R.id.newFriendLayout)
  public void onNewFriendClick(){
    requestTipsView.setVisibility(View.INVISIBLE);
    Date startTime=new Date();
    user.setFriendStartTime(startTime.getTime());
    LeanchatUser.changeCurrentUser(user, true);
    Intent intent = new Intent(ctx, ContactNewFriendActivity.class);
    ctx.startActivity(intent);
  }
  public void getUntreatedApplyCount(){
    RequestParams params = new RequestParams();
    params.put("userId", user.getObjectId());
    params.put("startTime",user.getFriendStartTime());

    String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"friends/untreatedApplyCount";

    MyHttpClient.get(url,params, new MyJsonResponseHandler() {

      @Override
      public void handleSuccess(int code, String mesg, JSONObject data) {
        if(data.optInt("count")>0) {
          requestTipsView.setVisibility(View.VISIBLE);
        }else {
          requestTipsView.setVisibility(View.INVISIBLE);
        }
      }
      @Override
      public void handleSuccess(int code, String mesg) {

      }

      @Override
      public void handleFailure(int error) {

        super.handleFailure(error);
      }

    });
  }


}
