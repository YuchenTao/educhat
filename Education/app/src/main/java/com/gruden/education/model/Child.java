package com.gruden.education.model;

/**
 * Created by yunjiansun on 16/1/18.
 */
public class Child {
    public String groupId;
    public String childId;
    public String studentCode;
    public String name;
    public String grade;
    public String school;
    public String birthday;
    public String imgUrl;
    public String  gender;
    public boolean isSelected;


}
