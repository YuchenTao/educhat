package com.gruden.education.model;

/**
 * Created by admin on 2016/4/13.
 */
public class Friend {
    public  String id;
    public String userId;
    public String nickname;
    public int state;
    public String dateTime;
    public String phone;
    public int type;
    public String avatarUrl;
    public String name;

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
