package com.gruden.education.activity.profile;

import android.os.Bundle;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;

/**
 * Created by lzw on 14-9-24.
 */
public class ProfileNotifySettingActivity extends BaseActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.profile_setting_notify_layout);
    setTitle(R.string.profile_notifySetting);
  }

}
