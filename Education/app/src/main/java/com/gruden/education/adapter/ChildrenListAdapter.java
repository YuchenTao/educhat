package com.gruden.education.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.profile.ChildDetailActivity;
import com.gruden.education.model.Child;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/1/20.
 */
public class ChildrenListAdapter extends BaseAdapter {
    LayoutInflater mInflater;
    Context context;

    public ChildrenListAdapter(Context context){
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    public List<Child> dataList = new ArrayList<Child>();

    public List<Child> getDataList() {
        return dataList;
    }

    public void setDataList(List<Child> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }

    public void addDataList(List<Child> datas) {
        dataList.addAll( datas);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String name = dataList.get(position).name;
        TextView txtName;
        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.child_item, parent, false);
            txtName = (TextView)convertView.findViewById(R.id.txtName);
            convertView.setTag(txtName);
        } else {
            txtName = (TextView) convertView.getTag();
        }

        txtName.setText(name);
        final String childId = dataList.get(position).childId;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChildDetailActivity.class);

                intent.putExtra("childId", childId);
                context.startActivity(intent);
            }
        });

        return convertView;
    }
}
