package com.gruden.education.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.gruden.education.R;
import com.gruden.education.model.UserSign;
import com.gruden.education.viewholder.UserSignItemHolder;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/2/23.
 */
public class SignListAdapter extends BaseAdapter {

    public boolean isSigned;
    LayoutInflater mInflater;
    Context context;

    public SignListAdapter(Context context){
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);

    }

    public List<UserSign> dataList = new ArrayList<UserSign>();

    public List<UserSign> getDataList() {
        return dataList;
    }

    public void setDataList(List<UserSign> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }


    public void addDataList(List<UserSign> datas) {
        dataList.addAll( datas);
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public UserSign getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UserSign sign = dataList.get(position);
        UserSignItemHolder holder;
        if (convertView == null) {
            holder = new UserSignItemHolder();
            convertView = mInflater.inflate(R.layout.sign_item, parent, false);
            holder.imgAvatar = (ImageView) convertView.findViewById(R.id.imageView);
            holder.txtName = (TextView)convertView.findViewById(R.id.txtName);
            holder.txtTime = (TextView)convertView.findViewById(R.id.txtTime);
            holder.txtSign = (TextView)convertView.findViewById(R.id.txtSign);
            convertView.setTag(holder);

        } else {
            holder = (UserSignItemHolder) convertView.getTag();
        }

        if (sign.isNone){

            holder.imgAvatar.setVisibility(View.INVISIBLE);

            holder.txtTime.setText("");
            holder.txtSign.setText("");

            if (isSigned){
                holder.txtName.setText("暂时还没有人签收~");
            }else {
                holder.txtName.setText("所有人已经签收！");
            }

        }else {

            holder.imgAvatar.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(sign.avatar, holder.imgAvatar, PhotoUtils.avatarImageOptions);
            holder.txtName.setText(sign.remarkName);

            if (isSigned){
                holder.txtTime.setVisibility(View.VISIBLE);
                holder.txtTime.setText(sign.createdAt);
                holder.txtSign.setText("已签收");
            }else {
                holder.txtTime.setVisibility(View.GONE);
                holder.txtSign.setText("未签收");
            }
        }

        return convertView;
    }
}
