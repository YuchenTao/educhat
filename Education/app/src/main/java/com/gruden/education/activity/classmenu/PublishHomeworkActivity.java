package com.gruden.education.activity.classmenu;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.ProgressCallback;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avoscloud.leanchatlib.controller.ConversationHelper;
import com.avoscloud.leanchatlib.utils.AVIMConversationCacheUtils;
import com.bigkoo.pickerview.TimePickerView;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.MyGridViewAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.UploadImage;
import com.loopj.android.http.RequestParams;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class PublishHomeworkActivity extends BaseActivity {

    @Bind(R.id.editContent)
    EditText editContent;

    @Bind(R.id.imgDispalyGridView)
    GridView gridView;


    @Bind(R.id.layoutSubject)
    View layoutSubject;

    @Bind(R.id.editSubject)
    EditText editSubject;

    @Bind(R.id.layoutDate)
    View layoutDate;

    @Bind(R.id.txtDate)
    TextView txtDate;

    @Bind(R.id.layoutClass)
    View layoutClass;

    @Bind(R.id.txtClass)
    TextView txtClass;

    private static final int REQUEST_IMAGE = 2;

    private String [] permissions = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    private MyGridViewAdapter mAdapter;
    private List<UploadImage> imageList = new ArrayList<UploadImage>();
    private ArrayList<String> mSelectPath = new ArrayList<String>();;

    private TimePickerView datePickerView;

    private boolean isUploading;
    private String conversationId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish_homework);
        initActionBar("发布作业");
        showLeftBackButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isUploading = false;
                for(UploadImage img : imageList){
                    if(img.isDone){
                        img.file.deleteInBackground();
                    }else {
                        img.file.cancel();
                    }
                }
                onBackPressed();
            }
        });
        showRightTextButton("发布", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publish();
            }
        });

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");

        AVIMConversation conversation = AVIMConversationCacheUtils.getCacheConversation(conversationId);
        txtClass.setText(ConversationHelper.nameOfConversation(conversation));

        mAdapter = new MyGridViewAdapter(this, mSelectPath);
        gridView.setAdapter(mAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == mSelectPath.size()) {//点击最后一张图片，打开相册

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)&&hasPermission(Manifest.permission.CAMERA)) {
                            pickImage();
                        }else {

                            checkPermission(permissions);
                        }
                    }else {

                        pickImage();
                    }
                }
            }
        });


        //时间选择器
        datePickerView = new TimePickerView(this, TimePickerView.Type.ALL);
        //控制时间范围
        Calendar calendar = Calendar.getInstance();
        datePickerView.setRange(calendar.get(Calendar.YEAR), calendar.get(Calendar.YEAR)+10);
        datePickerView.setTime(new Date());
        datePickerView.setCyclic(false);
        datePickerView.setCancelable(true);
        //时间选择后回调
        datePickerView.setOnTimeSelectListener(new TimePickerView.OnTimeSelectListener() {

            @Override
            public void onTimeSelect(Date date) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                txtDate.setText( format.format(date) );
            }
        });

        //弹出时间选择器
        layoutDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                datePickerView.show();
            }
        });

    }

    private void pickImage(){
        Intent intent = new Intent(PublishHomeworkActivity.this, MultiImageSelectorActivity.class);
        // 是否显示拍摄图片
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);
        // 最大可选择图片数量
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, 9);
        // 选择模式
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, MultiImageSelectorActivity.MODE_MULTI);
        // 默认选择
        if(mSelectPath != null && mSelectPath.size()>0){
            intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
        }
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_IMAGE){
            if(resultCode == RESULT_OK){

                ArrayList<String> pathList = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);

                for (int i=0; i<imageList.size();i++){
                    UploadImage img = imageList.get(i);
                    if (!pathList.contains(img.path)){
                        Log.e("remove image",  img.path);
                        img.file.cancel();
                        if(img.isDone){
                            img.file.deleteInBackground();
                        }
                        imageList.remove(i);
                    }

                }

                for (int i=0; i< pathList.size(); i++){
                    if (!mSelectPath.contains(pathList.get(i))){
                        Log.e("add image",  pathList.get(i));
                        try {
                            final UploadImage img = new UploadImage();
                            img.path = pathList.get(i);
                            img.file = AVFile.withAbsoluteLocalPath("image.jpg", pathList.get(i));
                            imageList.add(img);
                            img.file.saveInBackground(
                                    new SaveCallback() {
                                        @Override
                                        public void done(AVException e) {
                                            img.url = img.file.getUrl();
                                            img.isDone = true;
                                            System.out.println("url: " + img.url);
                                            if (isUploading)
                                                publish();
                                        }
                                    },
                                    new ProgressCallback() {
                                        @Override
                                        public void done(Integer percentDone) {
                                            //打印进度
                                            System.out.println("uploading: " + percentDone);
                                        }
                                    });

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Log.e("Exception","FileNotFound");
                        }
                    }

                }

                mSelectPath = pathList;

                mAdapter.setDataList(mSelectPath);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    void publish(){

        String strSubject = editSubject.getText().toString();
        if (TextUtils.isEmpty(strSubject)) {
            toast("科目名称不能为空");
            return;
        }

        String strContent = editContent.getText().toString();
        if (TextUtils.isEmpty(strSubject)) {
            toast("作业内容不能为空");
            return;
        }


        String strDate = txtDate.getText().toString();
        if (TextUtils.isEmpty(strSubject)) {
            toast("上交日期不能为空");
            return;
        }

        showSpinnerDialog("发布中");

        for(UploadImage img : imageList){
            if(!img.isDone){
                isUploading = true;
                return;
            }
        }

        isUploading = false;

        LeanchatUser user = LeanchatUser.getCurrentUser();
        RequestParams params = new RequestParams();
        params.put("userId",user.getObjectId());
        params.put("groupId",conversationId);
        params.put("endDate",strDate+ ":00");
        params.put("content",strContent);
        params.put("subject",strSubject);

        if (imageList.size()>0){
            List<String> urlList = new ArrayList<String>();
            for(UploadImage img : imageList){
                urlList.add(img.url);
            }
            params.put("imgUrl",urlList);
        }


        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_MESSAGE + "homework";
        MyHttpClient.post(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg){
                dismissSpinnerDialog();
                toast(mesg);
                if (code==0)
                    onBackPressed();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);

            }

        });



    }
}
