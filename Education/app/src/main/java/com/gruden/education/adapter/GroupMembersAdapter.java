package com.gruden.education.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.gruden.education.R;
import com.gruden.education.activity.discover.contact.Contact;
import com.gruden.education.activity.discover.contact.FriendDetailActivity;
import com.gruden.education.customview.swipemenulistview.BaseSwipListAdapter;
import com.gruden.education.model.LeanchatUser;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by yunjiansun on 16/3/29.
 */
public class GroupMembersAdapter extends BaseSwipListAdapter {

    private List<Contact> listMembers;
    private LayoutInflater mInflater;
    private Activity mActivity;
    public int identity = -1;
    public String conversationId;


    public GroupMembersAdapter(Activity activity){
        super();
        this.mActivity = activity;
        this.mInflater = LayoutInflater.from(activity);

    }

    public GroupMembersAdapter(Activity activity,List<Contact> listMembers){
        super();
        this.mActivity = activity;
        this.mInflater = LayoutInflater.from(activity);
        this.listMembers = listMembers;

    }


    public void  updateListView(List<Contact> list) {
        if (list == null) {
            this.listMembers.clear();
        } else {
            this.listMembers = list;
        }
    }

    public static class GroupMembersViewHolder {
        public ImageView imgAvatar;
        public TextView txtLetter;
        public TextView txtName;
        public View layoutContent;
    }

    @Override
    public int getCount() {
        return listMembers.size();
    }

    @Override
    public Contact getItem(int position) {
        return listMembers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        // menu type count
        return 2;
    }

    @Override
    public int getItemViewType(int position) {

        if (listMembers.get(position).name==null){
            return 0;
        }else {
            return 1;
        }
    }

    @Override
    public boolean getSwipEnableByPosition(int position) {
        if(getItemViewType(position) == 0){
            return false;
        }
        return true;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        GroupMembersViewHolder holder;
        final Contact contact = listMembers.get(position);
        if (convertView == null) {
            holder = new GroupMembersViewHolder();
            convertView = mInflater.inflate(R.layout.item_contact, parent, false);
            holder.imgAvatar = (ImageView) convertView.findViewById(R.id.imgAvatar);
            holder.txtName = (TextView) convertView.findViewById(R.id.title);
            holder.txtLetter = (TextView) convertView.findViewById(R.id.tvLetter);
            holder.layoutContent = convertView.findViewById(R.id.layoutContent);
            convertView.setTag(holder);
        } else {
            holder = (GroupMembersViewHolder) convertView.getTag();
        }

        if (getItemViewType(position)==0) {
            holder.txtLetter.setVisibility(View.VISIBLE);
            holder.layoutContent.setVisibility(View.GONE);
            String sectionTitle = contact.sortLetters;
            if (sectionTitle.equals("headMaster"))
                sectionTitle = "班主任";
            if (sectionTitle.equals("teachers"))
                sectionTitle = "任课老师";
            if (sectionTitle.equals("parents"))
                sectionTitle = "家长";
            holder.txtLetter.setText(sectionTitle);
            convertView.setOnClickListener(null);
        } else {
            holder.txtLetter.setVisibility(View.GONE);
            holder.layoutContent.setVisibility(View.VISIBLE);
            holder.txtName.setText(contact.name);
            ImageLoader.getInstance().displayImage(contact.avatarUrl, holder.imgAvatar, PhotoUtils.avatarImageOptions);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, FriendDetailActivity.class);
                    intent.putExtra("id", contact.userId);
                    intent.putExtra("remarkName", contact.name);
                    intent.putExtra("conversationId", conversationId);
                    intent.putExtra("position", position);
                    intent.putExtra("backTitle", "群成员");
                    intent.putExtra("isClass",1);
                    LeanchatUser user = LeanchatUser.getCurrentUser();
                    if (user.getObjectId().equals(contact.userId))
                        intent.putExtra("identity",1);
                    else
                        intent.putExtra("identity",identity);
                    mActivity.startActivityForResult(intent,1);
                }
            });
        }
        return convertView;
    }

}
