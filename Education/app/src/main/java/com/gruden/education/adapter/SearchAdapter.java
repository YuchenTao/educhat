package com.gruden.education.adapter;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.bbs.PostDetailActivity;
import com.gruden.education.activity.discover.contact.FriendDetailActivity;
import com.gruden.education.activity.search.GroupDetailActivity;
import com.gruden.education.model.Search;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2016/3/25.
 */
public class SearchAdapter extends BaseAdapter{


    String keywords;
    private Activity mActivity;
    int type;
    public List<Search> dataList = new ArrayList<Search>();
    LayoutInflater inflater;
    final int TYPE_1 = 0;
    final int TYPE_2 = 1;
    final int TYPE_3 = 2;
    public SearchAdapter(Activity mActivity) {
        this.mActivity = mActivity;
    }
    public List<Search> getDataList() {
        return dataList;
    }
    public void setKeyWords(String keywords) {
        this.keywords = keywords;
    }
    public void setDataList(List<Search> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }
    public void addDataList(List<Search> datas) {
        dataList.addAll(datas);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder1 holder1 = null;
        ViewHolder2 holder2 = null;
        ViewHolder3 holder3 = null;
        final Search search=dataList.get(position);
        int type = getType();
        if (convertView == null) {
            inflater = LayoutInflater.from(mActivity);
            // 按当前所需的样式，确定new的布局
            switch (type) {
                case TYPE_1:
                    convertView = inflater.inflate(R.layout.item_contact2, parent, false);
                    holder1 = new ViewHolder1();
                    holder1.img1 = (ImageView) convertView.findViewById(R.id.imgAvatar);
                    holder1.name = (TextView) convertView.findViewById(R.id.title);
                    holder1.content=(TextView) convertView.findViewById(R.id.txt_name);
                    convertView.setTag(holder1);
                    break;
                case TYPE_2:
                    convertView = inflater.inflate(R.layout.item_group, parent, false);
                    holder2 = new ViewHolder2();
                    holder2.img2 = (ImageView) convertView.findViewById(R.id.imgAvatar);
                    holder2.title2 = (TextView) convertView.findViewById(R.id.title);
                    holder2.name= (TextView) convertView.findViewById(R.id.txt_name);
                    holder2.remarkname= (TextView) convertView.findViewById(R.id.txtRemarkName);
                    convertView.setTag(holder2);
                    break;
                case TYPE_3:
                    convertView = inflater.inflate(R.layout.item_post,parent, false);
                    holder3 = new ViewHolder3();
                    holder3.img3 = (ImageView) convertView.findViewById(R.id.imgAvatar);
                    holder3.title3 = (TextView) convertView.findViewById(R.id.title);
                    holder3.content= (TextView) convertView.findViewById(R.id.txt_content);
                    convertView.setTag(holder3);
                    break;
                default:
                    break;
            }

        } else {
            switch (type) {
                case TYPE_1:
                    holder1 = (ViewHolder1) convertView.getTag();
                    break;
                case TYPE_2:
                    holder2 = (ViewHolder2) convertView.getTag();
                    break;
                case TYPE_3:
                    holder3 = (ViewHolder3) convertView.getTag();
                    break;
            }
        }
        // 设置资源
        switch (type) {
            case TYPE_1:
                if (search.imgUrl.length()>0) {
                    ImageLoader.getInstance().displayImage(search.imgUrl,  holder1.img1);
                }else{
                    holder1.img1.setImageResource(R.drawable.defaultface);
                }
                holder1.name.setText(search.name);
                holder1.name.setTag(search.id);
                holder1.content.setText(search.content);
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(mActivity, FriendDetailActivity.class);
                        intent.putExtra("id", search.id);
                        intent.putExtra("remarkName", search.name);
                        intent.putExtra("position", position);
                        mActivity.startActivityForResult(intent, 3);
                    }
                });
                break;
            case TYPE_2:

                if (search.imgUrl.length()>0) {
                    ImageLoader.getInstance().displayImage(search.imgUrl,  holder2.img2);
                }else{
                    holder2.img2.setImageResource(R.drawable.defaultgroup);
                }
                holder2.title2.setText(search.title);
                keywords(search.title, holder2.title2);
                holder2.title2.setTag(search.id);
                holder2.name.setText(search.name);
                holder2.remarkname.setText(search.remarkName);
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mActivity, GroupDetailActivity.class);
                        intent.putExtra("groupId", search.id);
                        intent.putExtra("grouptype",search.name.equals("班级群")?1:0);
                        mActivity.startActivity(intent);
                    }
                });
                break;
            case TYPE_3:
                if (search.imgUrl.length()>0) {
                    ImageLoader.getInstance().displayImage(search.imgUrl,  holder3.img3);
                }else{
                    holder3.img3.setImageResource(R.drawable.defaultface);
                }
                holder3.title3.setText(search.title);
                holder3.title3.setTag(search.id);
                keywords(search.title,holder3.title3);
                holder3.content.setText(Html.fromHtml(search.content));

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mActivity, PostDetailActivity.class);
                        intent.putExtra("id", search.id);
                        mActivity.startActivity(intent);
                    }
                });

                break;
        }

        return convertView;
    }
    private void keywords(String txt,TextView view){
        if (!TextUtils.isEmpty(keywords)) {
            char[] titles = txt.toCharArray();
            char[] keys = keywords.toCharArray();
            StringBuffer temp=new StringBuffer();
            StringBuffer temp2=new StringBuffer();

            temp.append("");

            if (txt.contains(keywords)) {

                for (int i = 0; i < titles.length; i++) {
                    String t = titles[i] + "";
                    for (int j = 0; j < keys.length; j++) {
                        String k = keys[j] + "";
                        if (k.equals(t))
                            t = "<font color='#82d3cc'><b>" + t + "</b></font>";
                    }

                    temp.append(t);
                }
                view.setText(Html.fromHtml(temp.toString()));
            }

        } else {
            view.setText(txt);

        }

    }

    public class ViewHolder1 {
            TextView name;
            TextView content;
            ImageView img1;

    }

    public class ViewHolder2 {
        ImageView img2;
        TextView title2;
        TextView name;
        TextView remarkname;
    }

    public class ViewHolder3 {
        ImageView img3;
        TextView title3;
        TextView content;
    }

}