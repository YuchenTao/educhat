package com.gruden.education.friends;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ListView;
import android.widget.SearchView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.SearchAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Search;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * 查找好友页面
 */
public class ContactAddFriendActivity extends BaseActivity {

  @Bind(R.id.list_search)
  ListView searchlist;

  @Bind(R.id.searchView)
  SearchView searchView;

  @Bind(R.id.search_refresh)
  MaterialRefreshLayout refreshLayout;

  SearchAdapter adapter;
  private boolean isRefreshing;
  private boolean isLoadMoreing;

  private String changeText="";
  private  int curPage=0 ;



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.contact_add_friend_activity);
    initActionBar("查找好友");
    showLeftBackButton();
    searchView.setQueryHint("搜索联系人");

    adapter=new SearchAdapter(this);
    adapter.setType(0);
    searchlist.setAdapter(adapter);

    refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
      @Override
      public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
        //下拉刷新...
        isRefreshing = true;
        searchContectInfo(changeText, 0);
      }

      @Override
      public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
        //上拉刷新...

        isLoadMoreing = true;
        searchContectInfo(changeText, curPage + 1);
      }
    });
    initListener();

  }

  private void initListener(){
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        return true;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        if (!TextUtils.isEmpty(newText)) {
          adapter.setKeyWords(newText);
          changeText = newText;
          searchContectInfo(newText, 0);
        }
        return false;
      }
    });

  }

  private void searchContectInfo(final String txt,final int page){
    RequestParams params = new RequestParams();
    params.put("curPage", page);
    params.put("content", txt);
    String url = MyHttpClient.BASE_URL+MyHttpClient.MODEL_USER+"list";
    MyHttpClient.get(url, params, new MyJsonResponseHandler() {
      @Override
      public void handleSuccess(int code, String mesg, JSONArray array) {
        if (array.length() == 0 && page ==0 && txt.length()>0) {
          toast("没有该联系人~");
        }
        if (array.length() == 0 && page > 0) {
          toast("已经没有更多联系人~");
        } else {

          ArrayList<Search> searchList = new ArrayList<Search>();

          for (int i = 0; i < array.length(); i++) {

            JSONObject data = array.optJSONObject(i);
            Search search = new Search();
            search.id = data.optString("userId");
            search.name = data.optString("nickname").length()==0?data.optString("username"):data.optString("nickname");
            search.content="手机号 ： "+data.optString("username");
            search.imgUrl = data.optString("avatar");
            searchList.add(search);
          }
          if (page == 0) {
            adapter.setDataList(searchList);
          } else {
            adapter.addDataList(searchList);
          }
          adapter.notifyDataSetChanged();
          curPage = page;
        }
        dissmissCircle();
      }

      @Override
      public void handleSuccess(int code, String mesg) {
        dissmissCircle();
        toast(mesg);

      }

      @Override
      public void handleFailure(int error) {
        dissmissCircle();
        super.handleFailure(error);
      }
    });


  }

  void dissmissCircle(){
    if (isRefreshing) {
      refreshLayout.finishRefresh();
      isRefreshing = false;
    }

    if (isLoadMoreing) {
      refreshLayout.finishRefreshLoadMore();
      isLoadMoreing = false;
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshLayout.autoRefresh();
  }
}
