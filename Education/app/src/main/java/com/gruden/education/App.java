package com.gruden.education;

import android.content.Context;
import android.os.StrictMode;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVObject;
import com.avoscloud.leanchatlib.controller.ChatManager;
import com.avoscloud.leanchatlib.controller.ConversationEventHandler;
import com.avoscloud.leanchatlib.utils.ThirdPartUserUtils;
import com.gruden.education.activity.MainActivity;
import com.gruden.education.friends.AddRequest;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.UpdateInfo;
import com.gruden.education.service.PushManager;
import com.gruden.education.util.LeanchatUserProvider;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import org.xutils.x;


public class App extends MultiDexApplication {
    public static boolean debug = true;
    public static App ctx;
    public MainActivity mainActivity;
    @Override
    public void onCreate() {
        super.onCreate();
        ctx = this;
        x.Ext.init(this);
        x.Ext.setDebug(BuildConfig.DEBUG);
//        Utils.fixAsyncTaskBug();


        String appId = "z5y2YfPRlvNSRtt0q1mHLqJr";
        String appKey = "QpbiXgaMU9C0SixBw3UU3u9p";

        AVObject.registerSubclass(AddRequest.class);
        AVObject.registerSubclass(UpdateInfo.class);
        LeanchatUser.alwaysUseSubUserClass(LeanchatUser.class);

        AVOSCloud.initialize(this, appId, appKey);

    // 节省流量
      AVOSCloud.setLastModifyEnabled(true);

        PushManager.getInstance().init(ctx);

//      AVOSCloud.setDebugLogEnabled(debug);
//      AVAnalytics.enableCrashReport(this, !debug);

        ThirdPartUserUtils.setThirdPartUserProvider(new LeanchatUserProvider());
        initChatManager();

     initImageLoader(ctx);
//      initBaiduMap();
//      if (App.debug) {
//      openStrictMode();
//    }
//



//         if (App.debug) {
//         Logger.level = Logger.VERBOSE;
//         } else {
//             Logger.level = Logger.NONE;
//         }
  }

    private void initChatManager() {
        final ChatManager chatManager = ChatManager.getInstance();
        chatManager.init(this);
        String currentUserId = LeanchatUser.getCurrentUserId();
        if (!TextUtils.isEmpty(currentUserId)) {
            chatManager.setupManagerWithUserId(this, currentUserId);
        }
        chatManager.setConversationEventHandler(ConversationEventHandler.getInstance());
        ChatManager.setDebugEnabled(App.debug);
    }

    public void openStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()   // or .detectAll() for all detectable problems
                .penaltyLog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                //.penaltyDeath()
                .build());
    }

    /**
     * 初始化ImageLoader
     */
    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context)
                .threadPoolSize(3).threadPriority(Thread.NORM_PRIORITY - 2)
                //.memoryCache(new WeakMemoryCache())
                .denyCacheImageMultipleSizesInMemory()
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();
        ImageLoader.getInstance().init(config);
    }

//    private void initBaiduMap() {
//        SDKInitializer.initialize(this);
//    }


}
