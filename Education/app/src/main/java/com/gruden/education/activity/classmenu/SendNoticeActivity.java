package com.gruden.education.activity.classmenu;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.ProgressCallback;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avoscloud.leanchatlib.controller.ConversationHelper;
import com.avoscloud.leanchatlib.utils.AVIMConversationCacheUtils;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.MyGridViewAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.UploadImage;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class SendNoticeActivity extends BaseActivity {

    @Bind(R.id.editText)
    EditText editText;

    @Bind(R.id.imgDispalyGridView)
    GridView gridView;

    @Bind(R.id.txtGroupName)
    TextView txtGroupName;

    @Bind(R.id.radioCommon)
    RadioButton radioCommon;

    private static final int IMAGE_PICK_REQUEST = 1;
    private static final int REQUEST_IMAGE = 2;

    private String [] permissions = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    private MyGridViewAdapter mAdapter;
    private List<UploadImage> imageList = new ArrayList<UploadImage>();
    private ArrayList<String> mSelectPath = new ArrayList<String>();;

    private boolean isUploading;
    private String conversationId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_notice);
        initActionBar("发布通知");
        showLeftBackButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isUploading = false;
                for(UploadImage img : imageList){
                    if(img.isDone){
                        img.file.deleteInBackground();
                    }else {
                        img.file.cancel();
                    }
                }
                onBackPressed();
            }
        });
        showRightTextButton("发布", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publish();
            }
        });

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");

        AVIMConversation conversation = AVIMConversationCacheUtils.getCacheConversation(conversationId);
        txtGroupName.setText(ConversationHelper.nameOfConversation(conversation));

        mAdapter = new MyGridViewAdapter(this, mSelectPath);
        gridView.setAdapter(mAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == mSelectPath.size()) {//点击最后一张图片，打开相册

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)&&hasPermission(Manifest.permission.CAMERA)) {
                            pickImage();
                        }else {

                            checkPermission(permissions);
                        }
                    }else {

                        pickImage();
                    }
                }
            }
        });


    }

    private void pickImage(){
        Intent intent = new Intent(SendNoticeActivity.this, MultiImageSelectorActivity.class);
        // 是否显示拍摄图片
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);
        // 最大可选择图片数量
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, 9);
        // 选择模式
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, MultiImageSelectorActivity.MODE_MULTI);
        // 默认选择
        if(mSelectPath != null && mSelectPath.size()>0){
            intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
        }
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_IMAGE){
            if(resultCode == RESULT_OK){

                ArrayList<String> pathList = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);

                for (int i=0; i<imageList.size();i++){
                    UploadImage img = imageList.get(i);
                    if (!pathList.contains(img.path)){
                        Log.e("remove image",  img.path);
                        img.file.cancel();
                        if(img.isDone){
                            img.file.deleteInBackground();
                        }
                        imageList.remove(i);
                        }

                }

                for (int i=0; i< pathList.size(); i++){
                    if (!mSelectPath.contains(pathList.get(i))){
                        Log.e("add image",  pathList.get(i));
                        try {
                            final UploadImage img = new UploadImage();
                            img.path = pathList.get(i);
                            img.file = AVFile.withAbsoluteLocalPath("image.jpg", pathList.get(i));
                            imageList.add(img);
                            img.file.saveInBackground(
                                    new SaveCallback() {
                                        @Override
                                        public void done(AVException e) {
                                            img.url = img.file.getUrl();
                                            img.isDone = true;
                                            System.out.println("url: " + img.url);
                                            if (isUploading)
                                                publish();
                                        }
                                    },
                                    new ProgressCallback() {
                                        @Override
                                        public void done(Integer percentDone) {
                                            //打印进度
                                            System.out.println("uploading: " + percentDone);
                                        }
                            });

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Log.e("Exception","FileNotFound");
                        }
                    }

                }

                mSelectPath = pathList;

                mAdapter.setDataList(mSelectPath);
                mAdapter.notifyDataSetChanged();
            }
        }
    }


    void publish(){

        String strNotice = editText.getText().toString();
        if (TextUtils.isEmpty(strNotice)) {
            toast("通知内容不能为空");
            return;
        }

        showSpinnerDialog("发布中");

        for(UploadImage img : imageList){
            if(!img.isDone){
                isUploading = true;
                return;
            }
        }

        isUploading = false;

        LeanchatUser user = LeanchatUser.getCurrentUser();
        RequestParams params = new RequestParams();
        params.put("userId",user.getObjectId());
        params.put("groupId",conversationId);
        params.put("content",strNotice);

        if (imageList.size()>0){
            List<String> urlList = new ArrayList<String>();
            for(UploadImage img : imageList){
                urlList.add(img.url);
            }
            params.put("imgUrl",urlList);
        }


        if (radioCommon.isChecked())
            params.put("type",0);
        else
            params.put("type",1);

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_MESSAGE + "notice";
        MyHttpClient.post(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg){
                dismissSpinnerDialog();
                toast(mesg);
                if (code==0)
                    onBackPressed();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);

            }

        });

    }

    @OnClick(R.id.imagePickLayout)
    public void onAvatarClick() {

        Intent intent = new Intent(Intent.ACTION_PICK, null);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_REQUEST);
    }

//    void uploadFile(String path) throws FileNotFoundException {
//
//        showSpinnerDialog();
//
//        RequestParams params = new RequestParams();
//        File file = new File(path);
//        params.put("image",file,"application/octet-stream");
//
//        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_FILE + "upload";
//        MyHttpClient.post(url,params,"application/octet-stream",new MyJsonResponseHandler(){
//
//            @Override
//            public void handleSuccess(int code, String mesg, JSONArray data) {
//                urlList.add(data.optJSONObject(0).optString("url"));
//            }
//
//            @Override
//            public void handleSuccess(int code, String mesg){
//                toast(mesg);
//            }
//
//            @Override
//            public void handleFailure(int error) {
//                super.handleFailure(error);
//            }
//
//        });
//
//    }


}
