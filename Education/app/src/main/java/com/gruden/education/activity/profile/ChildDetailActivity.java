package com.gruden.education.activity.profile;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.Notice;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;

public class ChildDetailActivity extends BaseActivity {

    private String childId;

    private TimePickerView pickerView;

    @Bind(R.id.editName)
    EditText editName;

    @Bind(R.id.txtBirthday)
    TextView txtBirthday;

    @Bind(R.id.txtGrade)
    TextView txtGrade;

    @Bind(R.id.txtSchool)
    TextView txtSchool;

    @Bind(R.id.txtStudentCode)
    TextView txtStudentCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_detail);
        initActionBar("我的孩子");
        showLeftBackButton();

        Intent intent = getIntent();
        childId = intent.getStringExtra("childId");
        if (childId!=null){
            showRightTextButton("保存", new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    updateChildDetail();
                }
            });
        }

        //时间选择器
        pickerView = new TimePickerView(this, TimePickerView.Type.YEAR_MONTH_DAY);
        //控制时间范围
        Calendar calendar = Calendar.getInstance();
        pickerView.setRange(calendar.get(Calendar.YEAR) - 20, calendar.get(Calendar.YEAR));
        pickerView.setTime(new Date());
        pickerView.setCyclic(false);
        pickerView.setCancelable(true);
        //时间选择后回调
        pickerView.setOnTimeSelectListener(new TimePickerView.OnTimeSelectListener() {

            @Override
            public void onTimeSelect(Date date) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                txtBirthday.setText( format.format(date));
            }
        });
        //弹出时间选择器
        txtBirthday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                hideSoftInputView();
                pickerView.show();
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        if (childId!=null)
            getChildDetail();
    }

    void getChildDetail(){

        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("childId",childId);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"children";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()>0){
                    JSONObject object = array.optJSONObject(0);
                    editName.setText(object.optString("name"));
                    txtBirthday.setText(object.optString("birthday"));
                    txtGrade.setText(object.optString("grade"));
                    txtSchool.setText(object.optString("school"));
                    txtStudentCode.setText(object.optString("studentCode"));
                }
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }

    void updateChildDetail(){

        String name = editName.getText().toString();
        String birthday = txtBirthday.getText().toString();
        if (TextUtils.isEmpty(name)) {
            toast("姓名不能为空");
            return;
        }

        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("childId",childId);
        params.put("name",name);

        if (!birthday.equals("选填")){
            params.put("birthday",birthday);
        }


        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"children";

        MyHttpClient.put(url,params, new MyJsonResponseHandler() {


            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);
                onBackPressed();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }
}
