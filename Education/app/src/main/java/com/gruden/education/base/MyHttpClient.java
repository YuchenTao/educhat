package com.gruden.education.base;

import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;



public class MyHttpClient {

    //Test
//    public static final String BASE_URL = "https://stg-education.leanapp.cn/api/";

    //Live
    public static final String HOST = "https://education2.leanapp.cn/";
    public static final String BASE_URL = HOST +"api/";

    public static final String MODEL_DATA = "data/";
    public static final String MODEL_FORUM = "forum/";
    public static final String MODEL_MESSAGE = "message/";
    public static final String MODEL_REALTIME = "realtime/";
    public static final String MODEL_FILE = "file/";
    public static final String MODEL_USER = "user/";
    public static enum Type {
        GET, POST, PUT, DELETE;
    }



    private static AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);



    static  {
        //client.setTimeout(11000);
        //client.isUrlEncodingEnabled();
        client.setURLEncodingEnabled(false);
        client.setEnableRedirects(true);
    }

    public static AsyncHttpClient getClient(){
        return client;
    }

    // 用一个完整url获取一个string对象
    public static void get(String urlString, AsyncHttpResponseHandler res)
    {

        client.get(urlString, res);

    }

    // url里面带参数
    public static void get(String url, RequestParams params,AsyncHttpResponseHandler responseHandler){

        client.get(url,params,responseHandler);
    }


    // 不带参数，获取json对象或者数组
    public static void get(String urlString, JsonHttpResponseHandler res) {
        client.get(urlString, res);

    }

    // 带参数，获取json对象或者数组
    public static void get(String urlString, RequestParams params,
                           JsonHttpResponseHandler res) {

//        client.setEnableRedirects(true);
        client.get(urlString, params, res);

    }



    // 下载数据使用，会返回byte数据
    public static void get(String url, BinaryHttpResponseHandler bHandler) {

        client.get(url, bHandler);

    }

    public static void post(String url, RequestParams params, JsonHttpResponseHandler responseHandler) {

        client.post(AsyncHttpClient.getUrlWithQueryString(client.isUrlEncodingEnabled(), url, params), responseHandler);
    }

    public static void post(String url, RequestParams params, String contentType, JsonHttpResponseHandler responseHandler) {
        if (contentType!=null)
            client.addHeader(AsyncHttpClient.HEADER_CONTENT_TYPE, contentType);

        client.post(AsyncHttpClient.getUrlWithQueryString(client.isUrlEncodingEnabled(), url, params), responseHandler);
    }

    public static void put(String url, JsonHttpResponseHandler responseHandler) {

        client.put( url, responseHandler);
    }

    public static void put(String url, RequestParams params, JsonHttpResponseHandler responseHandler) {

        client.put( AsyncHttpClient.getUrlWithQueryString(client.isUrlEncodingEnabled(), url, params), responseHandler);
    }

    public static void delete(String url, RequestParams params, JsonHttpResponseHandler responseHandler) {

        client.delete(AsyncHttpClient.getUrlWithQueryString(client.isUrlEncodingEnabled(), url, params), responseHandler);
    }

    public static void getDataWithAction(String model ,String action, Type type, RequestParams params,AsyncHttpResponseHandler responseHandler) {

        String url = BASE_URL  + model + "/" + action;
        LeanchatUser user = LeanchatUser.getCurrentUser();
        params.put("username", user.getUsername());
        params.put("password", user.getMyPassword());

        switch (type){
            case GET:{

                client.get(url, params, responseHandler);
                break;
            }


            case POST:{
                client.post(AsyncHttpClient.getUrlWithQueryString(client.isUrlEncodingEnabled(), url, params), responseHandler);
                break;
            }

            case PUT:{
                client.put(AsyncHttpClient.getUrlWithQueryString(client.isUrlEncodingEnabled(), url, params), responseHandler);
                break;
            }

            default:
                url = BASE_URL;
                break;
        }





    }


    public static void closeRequest(){

        client.cancelAllRequests(true);

    }

    public static String getLegalURL(String relativeUrl){
        //String newUrl = URLEncoder.encode(relativeUrl, "UTF-8");
        //String urlEncode = Uri.encode(relativeUrl);
        //return relativeUrl.replaceAll("|","%7C");
        return relativeUrl.replace("|", "%7C");
    }

}
