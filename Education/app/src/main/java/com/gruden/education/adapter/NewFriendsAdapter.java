package com.gruden.education.adapter;


import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gruden.education.R;
import com.gruden.education.activity.discover.contact.FriendDetailActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Friend;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 15/12/31.
 */
public class NewFriendsAdapter extends BaseAdapter implements View.OnClickListener {

    LayoutInflater mInflater;
    Activity  context;
    LeanchatUser user=LeanchatUser.getCurrentUser();

    public NewFriendsAdapter(Activity context){
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    public List<Friend> dataList = new ArrayList<Friend>();

    public List<Friend> getDataList() {
        return dataList;
    }

    public void setDataList(List<Friend> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }

    public void addDataList(List<Friend> datas) {
        dataList.addAll(datas);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Friend getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Friend friend=dataList.get(position);
        FriendHolder holder ;
        if (convertView == null) {
            holder=new FriendHolder();
            convertView = mInflater.inflate(R.layout.contact_add_friend_item, parent, false);
            holder.txtname= (TextView) convertView.findViewById(R.id.name);
            holder.btnadd= (Button) convertView.findViewById(R.id.add);
            holder.agreedView= (TextView) convertView.findViewById(R.id.agreedView);
            holder.imgAvatar= (ImageView) convertView.findViewById(R.id.avatar);
            convertView.setTag(holder);

        } else {
            holder= (FriendHolder) convertView.getTag();
        }
        if (friend.avatarUrl.length()==0) {
            holder.imgAvatar.setImageResource(R.drawable.defaultface);
        }else{
            ImageLoader.getInstance().displayImage(friend.avatarUrl,holder.imgAvatar);
        }
        holder.txtname.setText(friend.nickname);
        holder.btnadd.setTag(holder);
        holder.txtname.setTag(friend);
        holder.agreedView.setTag(position);
        if(friend.state==0&&friend.type==0){
            holder.agreedView.setVisibility(View.GONE);
            holder.btnadd.setVisibility(View.VISIBLE);
            holder.btnadd.setOnClickListener(this);
            holder.btnadd.setText("同意");
        }else if(friend.state==0&&friend.type==1){
            holder.agreedView.setVisibility(View.GONE);
            holder.btnadd.setText("添加");
            holder.btnadd.setVisibility(View.VISIBLE);
            holder.btnadd.setOnClickListener(this);

        }else if(friend.state==2&&friend.type==0){
            holder.btnadd.setVisibility(View.GONE);
            holder.agreedView.setVisibility(View.VISIBLE);
            holder.agreedView.setText("已拒绝");
        }else if(friend.state==2&&friend.type==1){
            holder.btnadd.setVisibility(View.GONE);
            holder.agreedView.setVisibility(View.VISIBLE);
            holder.agreedView.setText("等待验证");
        }else{
            holder.btnadd.setVisibility(View.GONE);
            holder.agreedView.setVisibility(View.VISIBLE);
            holder.agreedView.setText("已同意");
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,FriendDetailActivity.class);
                intent.putExtra("id", friend.userId);
                intent.putExtra("opId",friend.id);
                intent.putExtra("backTitle","好友申请");
                intent.putExtra("position",position);
                intent.putExtra("FriendState",friend.state);
                intent.putExtra("FriendType",friend.type);
                context.startActivityForResult(intent,3);
            }
        });
        return convertView;
    }


    @Override
    public void onClick(View v) {
        FriendHolder holder = (FriendHolder) v.getTag();
        Friend friend= (Friend) holder.txtname.getTag();
        int type=friend.type;
        int position=Integer.parseInt(holder.agreedView.getTag().toString());
        if(type==0){
            String id=friend.id;
            addFriend(id, holder,position);
        }else{

            applyFriend(holder,friend,position);
        }

    }
    private void addFriend(String id, final FriendHolder holder, final int position){
        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("id", id);
        params.put("op",1);
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"friends/verify";
        MyHttpClient.put(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg) {
                Toast.makeText(context, mesg, Toast.LENGTH_SHORT).show();

                    holder.btnadd.setVisibility(View.GONE);
                    holder.agreedView.setVisibility(View.VISIBLE);
                    ArrayList<Friend> changeState= (ArrayList<Friend>) user.getNewFriends();
                    changeState.get(position).state=1;
                    user.setNewFriends(changeState);
                    LeanchatUser.changeCurrentUser(user,true);

            }

            @Override
            public void handleFailure(int error) {
                super.handleFailure(error);
            }
        });

    }
    private void applyFriend( final FriendHolder holder,Friend friend,final int position){

        RequestParams params = new RequestParams();
        params.put("selfId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("friendId", friend.userId);
        params.put("type", 1);
        params.put("remarkName",holder.txtname.getText());
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"friends/apply";
        MyHttpClient.post(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {
                Toast.makeText(context, mesg, Toast.LENGTH_SHORT).show();
                if (code==0) {
                    holder.btnadd.setVisibility(View.GONE);
                    holder.agreedView.setVisibility(View.VISIBLE);
                    holder.agreedView.setText("等待验证");
                    ArrayList<Friend> changeState= (ArrayList<Friend>) user.getNewFriends();
                    changeState.get(position).state = 2;
                    user.setNewFriends(changeState);
                    LeanchatUser.changeCurrentUser(user,true);
                }
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                Toast.makeText(context, mesg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleFailure(int error) {
                super.handleFailure(error);
            }
        });

    }
    class FriendHolder{
        TextView txtname;
        Button btnadd;
        TextView agreedView;
        ImageView imgAvatar;
    }
}


