package com.gruden.education.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gruden.education.R;
import com.gruden.education.activity.classmenu.OpenFileActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.download.DownloadInfos;
import com.gruden.education.download.DownloadManager;
import com.gruden.education.download.DownloadState;
import com.gruden.education.download.DownloadViewHolder;
import com.loopj.android.http.RequestParams;

import org.xutils.common.Callback;
import org.xutils.ex.DbException;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.File;

/**
 * Created by yunjiansun on 15/12/31.
 */
public class FileListAdapter extends BaseAdapter  {

    LayoutInflater mInflater;
    Context context;

    private DownloadManager downloadManager;

    public  FileListAdapter(Context context,DownloadManager downloadManager){

        this.context = context;
        this.downloadManager=downloadManager;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (downloadManager == null) return 0;
        return downloadManager.getDownloadListCount();
    }

    @Override
    public DownloadInfos getItem(int position) {
       return downloadManager.getDownloadInfo(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DownloadItemViewHolder holder = null;
        DownloadInfos downloadInfo = downloadManager.getDownloadInfo(position);
        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.classmenu_filelist_item, parent, false);
            holder = new DownloadItemViewHolder(convertView, downloadInfo);
            convertView.setTag(holder);
            holder.refresh();
        }else{
            holder = (DownloadItemViewHolder) convertView.getTag();
            holder.update(downloadInfo);
        }
//        if (downloadInfo.getState().value() < DownloadState.FINISHED.value()) {
//            try {
//                downloadManager.startDownload(downloadInfo.getFileId(),downloadInfo.getGroupId(),
//                        downloadInfo.getUrl(),
//                        downloadInfo.getLabel(),
//                        downloadInfo.getFileSavePath(),
//                        downloadInfo.isAutoResume(),
//                        downloadInfo.isAutoRename(),
//                        holder);
//            } catch (DbException ex) {
//                Toast.makeText(x.app(), "添加下载失败", Toast.LENGTH_LONG).show();
//            }
//        }
        return convertView;
    }

    public class DownloadItemViewHolder extends DownloadViewHolder {

        @ViewInject(R.id.txtFileName)
        TextView txtFileName;
        @ViewInject(R.id.txtFileTime)
        TextView txtFileTime;
        @ViewInject(R.id.txtFileSize)
        TextView txtFileSize;
        @ViewInject(R.id.txtRemarkName)
        TextView txtRemarkName;
        @ViewInject(R.id.fileImg)
        ImageView fileImg;
        @ViewInject(R.id.download_stop_btn)
        Button stopbtn;
        @ViewInject(R.id.download_finish_btn)
        Button finishBtn;
        @ViewInject(R.id.download_start_btn)
        Button startBtn;
        @ViewInject(R.id.download_pb)
        ProgressBar progressBar;
        @ViewInject(R.id.download_pb_layout)
        LinearLayout pbLayout;
        @ViewInject(R.id.fileinfo_layout)
        LinearLayout fileinfoLayout;

        public DownloadItemViewHolder(View view, DownloadInfos downloadInfo) {
            super(view, downloadInfo);
            refresh();
        }

        @Event(R.id.download_start_btn)
        private void toggleEvent(View view) {
            stopbtn.setVisibility(View.VISIBLE);
            pbLayout.setVisibility(View.VISIBLE);
            startBtn.setVisibility(View.GONE);
            fileinfoLayout.setVisibility(View.GONE);
            if (downloadInfo.getUrl().equals("null")||downloadInfo.getUrl()==null){
                Toast.makeText(context,"文件已失效~",Toast.LENGTH_SHORT).show();
                stopbtn.setVisibility(View.GONE);
                pbLayout.setVisibility(View.GONE);
                startBtn.setVisibility(View.VISIBLE);
                finishBtn.setVisibility(View.GONE);
                fileinfoLayout.setVisibility(View.VISIBLE);
                return ;
            }
            try {

                downloadManager.startDownload(downloadInfo.getFileId(),downloadInfo.getGroupId(),downloadInfo.getUrl(),
//                        downloadInfo.getLabel(), Environment.getExternalStorageDirectory()+"/Education/"+downloadInfo.getLabel(), downloadInfo.isAutoResume(), downloadInfo.isAutoRename(),
                        this);
            } catch (DbException e) {
                e.printStackTrace();
            }
        }

        @Event(R.id.download_stop_btn)
        private void stopEvent(View view) {
            DownloadState state = downloadInfo.getState();
            switch (state) {
                case WAITING:
                case STARTED:
                    downloadManager.stopDownload(downloadInfo);
                    break;
                case ERROR:
                case STOPPED:
                    try {
                        downloadManager.startDownload(downloadInfo.getFileId(),downloadInfo.getGroupId(), downloadInfo.getUrl(),
//                                downloadInfo.getLabel(), downloadInfo.getFileSavePath(), downloadInfo.isAutoResume(), downloadInfo.isAutoRename(),
                                this);
                    } catch (DbException ex) {
                        Toast.makeText(x.app(), "添加下载失败", Toast.LENGTH_LONG).show();
                    }
                    break;
                case FINISHED:
                    Toast.makeText(x.app(), "已经下载完成", Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
        @Event(R.id.download_finish_btn)
        private void finishEvent(View v){
            File file=new File(downloadInfo.getFileSavePath());
            if (file.exists()) {
                Intent intent = new Intent(context, OpenFileActivity.class);
                intent.putExtra("name", downloadInfo.getLabel());
                intent.putExtra("filePath", downloadInfo.getFileSavePath());
                intent.putExtra("url", downloadInfo.getUrl());
                context.startActivity(intent);
            }else{
                Toast.makeText(context,"文件不存在，请重新下载~",Toast.LENGTH_SHORT).show();
                downloadInfo.setState(DownloadState.ERROR);
                try {
                    downloadManager.db.saveOrUpdate(downloadInfo);
                    refresh();
                } catch (DbException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void update(DownloadInfos downloadInfo) {
            super.update(downloadInfo);
            refresh();
        }

        @Override
        public void onWaiting() {
            refresh();
        }

        @Override
        public void onStarted() {
            refresh();
        }

        @Override
        public void onLoading(long total, long current) {
            refresh();
        }

        @Override
        public void onSuccess(File result) {
            refresh();
        }

        @Override
        public void onError(Throwable ex, boolean isOnCallback) {
            refresh();
        }

        @Override
        public void onCancelled(Callback.CancelledException cex) {
            refresh();
        }

        public synchronized void refresh() {
            fileImg.setImageResource(downloadInfo.getImgUrl());
            txtFileName.setText(downloadInfo.getLabel());
            txtFileName.setTag(downloadInfo.getGroupId());
            txtFileTime.setText(downloadInfo.getCreatedAt());
            txtFileSize.setText(downloadInfo.getFileLength()/1024+"K");
            progressBar.setProgress(downloadInfo.getProgress());
            txtRemarkName.setText(downloadInfo.getRemarkName());
            DownloadState state = downloadInfo.getState();
            switch (state) {
                case WAITING:
                    stopbtn.setVisibility(View.GONE);
                    pbLayout.setVisibility(View.GONE);
                    startBtn.setVisibility(View.VISIBLE);
                    finishBtn.setVisibility(View.GONE);
                    fileinfoLayout.setVisibility(View.VISIBLE);
                    break;
                case STARTED:
                    stopbtn.setBackgroundResource(R.drawable.xiazai_start);
                    stopbtn.setVisibility(View.VISIBLE);
                    pbLayout.setVisibility(View.VISIBLE);
                    startBtn.setVisibility(View.GONE);
                    finishBtn.setVisibility(View.GONE);
                    fileinfoLayout.setVisibility(View.GONE);
                    break;
                case ERROR:
//                    Toast.makeText(context,"文件已失效~",Toast.LENGTH_SHORT).show();
                    stopbtn.setVisibility(View.GONE);
                    pbLayout.setVisibility(View.GONE);
                    startBtn.setVisibility(View.VISIBLE);
                    finishBtn.setVisibility(View.GONE);
                    fileinfoLayout.setVisibility(View.VISIBLE);
                    break;
                case STOPPED:
                    stopbtn.setBackgroundResource(R.drawable.xiazai_pause);
                    stopbtn.setVisibility(View.VISIBLE);
                    pbLayout.setVisibility(View.VISIBLE);
                    startBtn.setVisibility(View.GONE);
                    finishBtn.setVisibility(View.GONE);
                    fileinfoLayout.setVisibility(View.GONE);
                    break;
                case FINISHED:
                    finishBtn.setVisibility(View.VISIBLE);
                    fileinfoLayout.setVisibility(View.VISIBLE);
                    stopbtn.setVisibility(View.GONE);
                    pbLayout.setVisibility(View.GONE);
                    startBtn.setVisibility(View.GONE);
                    if(downloadInfo.isDone()){
                        successDowloadFile(downloadInfo.getFileId());
                    }
                    break;
                default:
                    startBtn.setVisibility(View.VISIBLE);
                    fileinfoLayout.setVisibility(View.VISIBLE);
                    finishBtn.setVisibility(View.GONE);
                    stopbtn.setVisibility(View.GONE);
                    pbLayout.setVisibility(View.GONE);
                    break;
            }
        }
    }
    private void  successDowloadFile(String id) {
        RequestParams params = new RequestParams();
        params.put("id", id);
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME + "filelist";
        MyHttpClient.put(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg) {
                Toast.makeText(context, mesg, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void handleFailure(int error) {
                super.handleFailure(error);

            }

        });
    }
}


