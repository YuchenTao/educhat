package com.gruden.education.activity.search;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.SearchAllAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.Search;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by admin on 2016/3/22.
 */
public class SearchActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.searchView)
    SearchView searchView;

    @Bind(R.id.btn_cancel)
    Button btn_cancel;

    @Bind(R.id.contacts_layout)
    LinearLayout contacts_layout;

    @Bind(R.id.qun_layout)
    LinearLayout qun_layout;

    @Bind(R.id.post_layout)
    LinearLayout post_layout;

    @Bind(R.id.layout_bg)
    LinearLayout layout_bg;

    @Bind(R.id.list_search)
    ListView listSearch;

    private SearchAllAdapter adapter;
    private String changeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.activity_search);
        adapter=new SearchAllAdapter(this);
        listSearch.setAdapter(adapter);
        init();
    }

    private void init() {

        layout_bg.getBackground().setAlpha(100);
        searchView.setOnClickListener(this);
        contacts_layout.setOnClickListener(this);
        qun_layout.setOnClickListener(this);
        post_layout.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.dataList.clear();
                changeText = newText;
                if (TextUtils.isEmpty(newText)) {
                    layout_bg.setVisibility(View.VISIBLE);
                    listSearch.setVisibility(View.GONE);
                } else {
                    layout_bg.setVisibility(View.GONE);
                    listSearch.setVisibility(View.VISIBLE);
                }
                adapter.setKeyWords(newText);
                searchInfo(changeText);
                return false;
            }
        });
    }
    private void searchInfo(String newText) {

        if (TextUtils.isEmpty(newText)||newText.length()==0){
            return;
        }
        showSpinnerDialog();
        searchContectInfo(newText);
        searchGroupInfo(newText);
        searchPostInfo(newText);
    }
    private void searchContectInfo(String txt){

        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUserId());
        params.put("content", txt);
        String url = MyHttpClient.BASE_URL+MyHttpClient.MODEL_USER+"friends/list";
        MyHttpClient.get(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length() == 0) {
                    dismissSpinnerDialog();
                    return;
                } else {
                    ArrayList<Search> searchList = new ArrayList<Search>();
                    for (int i = 0; i < array.length() + 1; i++) {
                        Search search = new Search();
                        if (i == 0) {
                            search.title = "联系人";
                        } else if (i >= 5) {
                            search.title = changeText;
                            search.type = 0;
                            search.foot = "查询更多联系人>>";
                            searchList.add(search);
                            break;
                        } else {
                            JSONObject data = array.optJSONObject(i - 1);
                            search.id = data.optString("userId");
                            search.name = data.optString("remarkName");
                            search.content ="昵称 ："+ data.optString("nickname");
                            search.imgUrl = data.optString("avatar");
                        }
                        searchList.add(search);
                    }


                    adapter.setDataList(searchList);


                    adapter.notifyDataSetChanged();


                }
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }
        });


    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            searchInfo(changeText);
        }

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        adapter.dataList.clear();
//        searchInfo(changeText);
//    }

    private void  searchPostInfo(String title ){

        RequestParams params = new RequestParams();
        params.put("order", "lasted");
        params.put("tcode", "all");
        params.put("operat", "query");
        params.put("curPage", 0);
        params.put("title", title);
        String url =MyHttpClient.HOST + "topic/all";
        MyHttpClient.get(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length() == 0) {
                    dismissSpinnerDialog();
                    return;
                } else {
                    ArrayList<Search> searchList = new ArrayList<Search>();
                    for (int i = 0; i < array.length() + 1; i++) {
                        Search search = new Search();
                        if (i == 0) {
                            search.title = "帖子";
                        } else if (i >= 10) {
                            search.title = changeText;
                            search.type = 2;
                            search.foot = "查询更多帖子>>";
                            searchList.add(search);
                            break;
                        } else {
                            JSONObject data = array.optJSONObject(i - 1);
                            search.id = data.optString("id");
                            search.title = data.optString("title");
                            search.imgUrl = data.optString("avatar");
                            search.content = data.optString("content");
                        }
                        searchList.add(search);
                    }

                    adapter.setDataList(searchList);

                    adapter.notifyDataSetChanged();

                }
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }
        });
    }
    private void searchGroupInfo(String txt) {
        RequestParams params = new RequestParams();
        params.put("content", txt);
        params.put("curPage", 0);
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME + "group/list";
        MyHttpClient.get(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length() == 0) {
                    dismissSpinnerDialog();
                    return;
                } else {
                    ArrayList<Search> searchList = new ArrayList<Search>();
                    for (int i = 0; i < array.length() + 1; i++) {
                        Search search = new Search();
                        if (i == 0) {
                            search.title = "群组";
                        } else if (i >= 5) {
                            search.title = changeText;
                            search.type = 1;
                            search.foot = "查询更多群组>>";
                            searchList.add(search);
                            break;
                        } else {
                            JSONObject data = array.optJSONObject(i - 1);
                            search.id = data.optString("groupId");
                            search.title = data.optString("name");
                            search.name = data.optString("type");
                            search.imgUrl=data.optString("avatar");
                            search.remarkName = "(" + data.optString("memberCount") + "人)";
                        }
                        searchList.add(search);
                    }


                    adapter.setDataList(searchList);


                    adapter.notifyDataSetChanged();

                }
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast("群组"+mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }
        });
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.contacts_layout:
                intent = new Intent(this, SearchTypeActivity.class);

                intent.putExtra("type", 0);
                startActivity(intent);
                break;
            case R.id.qun_layout:
                intent = new Intent(this, SearchTypeActivity.class);
              
                intent.putExtra("type", 1);
                startActivity(intent);
                break;
            case R.id.post_layout:
                intent = new Intent(this, SearchTypeActivity.class);

                intent.putExtra("type", 2);
                startActivity(intent);
                break;
        }
    }



}
