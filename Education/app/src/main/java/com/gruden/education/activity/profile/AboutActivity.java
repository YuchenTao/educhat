package com.gruden.education.activity.profile;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;

import butterknife.Bind;

public class AboutActivity extends BaseActivity {
    @Bind(R.id.txtAbout)
    TextView txtAbout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        initActionBar("关于");
        showLeftBackButton();

        PackageManager pm = getPackageManager();

        PackageInfo pi = null;//getPackageName()是你当前类的包名，0代表是获取版本信息
        try {
            pi = pm.getPackageInfo(getPackageName(), 0);
            String name = pi.versionName;
            txtAbout.setText("天天向上 V"+name);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
