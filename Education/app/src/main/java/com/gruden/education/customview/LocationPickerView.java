package com.gruden.education.customview;

import android.content.Context;

import com.bigkoo.pickerview.OptionsPickerView;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by yunjiansun on 16/1/11.
 */
public class LocationPickerView extends OptionsPickerView {

    Context context;
    public boolean isOK;
    public ArrayList<String> arrAreaCode;
    ArrayList<String> options1Items;
    ArrayList<ArrayList<String>> options2Items;
    public ArrayList<ArrayList<ArrayList<String>>> options3Items;

    public LocationPickerView(Context context) {
        super(context);
        this.context = context;

        arrAreaCode = new ArrayList<String>();
        options1Items = new ArrayList<String>();
        options2Items = new ArrayList<ArrayList<String>>();
        options3Items = new ArrayList<ArrayList<ArrayList<String>>>();

        getCityList();

    }

    private void initPicker(){

        isOK = true;
        setPicker(options1Items, options2Items, options3Items, true);
        setTitle("选择城市");
        setCyclic(false, false, false);
        setSelectOptions(0, 0, 0);
    }


    private void getCityList(){

        RequestParams params = new RequestParams();
        params.put("city","015002");

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_DATA +"location";

        MyHttpClient.get(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray data) {

                options1Items.add("山东");

                ArrayList<String> options2Items_01=new ArrayList<String>();
                options2Items_01.add("青岛");
                options2Items.add(options2Items_01);

                ArrayList<ArrayList<String>> options3Items_01 = new ArrayList<ArrayList<String>>();
                ArrayList<String> options3Items_01_01=new ArrayList<String>();
                int length = data.length();
                for (int i=0; i<length; i++){
                    options3Items_01_01.add(data.optJSONObject(i).optString("name"));
                    arrAreaCode.add(data.optJSONObject(i).optString("code"));
                }

                options3Items_01.add(options3Items_01_01);
                options3Items.add(options3Items_01);

                if (options3Items.size()>0) {
                    initPicker();
                }
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                Utils.toast(context,mesg);
            }

            @Override
            public void handleFailure(int error) {
                super.handleFailure(error);
            }

        });
    }
}
