package com.gruden.education.activity.profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.gruden.education.R;
import com.gruden.education.activity.entry.EntryBaseActivity;
import com.gruden.education.activity.entry.EntryLoginActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

/**
 * Created by admin on 2016/3/2.
 */
public class PasswordRevise extends EntryBaseActivity {

    EditText   oldPasswordEdit,newPasswordEdit;
    ImageButton eyeableButton;
    Button btnRevise;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_passwordrevise);

        findView();
        initActionBar("修改密码");
        showLeftBackButton();


        btnRevise.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                revise();
            }
        });
        eyeableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                setEyeable();
            }
        });


    }

    private void revise() {
        final String oldpassword = oldPasswordEdit.getText().toString();
        final String stroldpassword = LeanchatUser.getCurrentUser().getMyPassword();
        final String newpassword = newPasswordEdit.getText().toString();
        if (TextUtils.isEmpty(oldpassword)) {
            Utils.toast(this, R.string.password_can_not_null);
            return;
        }
        if (TextUtils.isEmpty(newpassword)) {
            Utils.toast(this, R.string.password_can_not_null);
            return;
        }
        if (!stroldpassword.equals(oldpassword)){
            toast("您输入的密码有误，请重新输入");
            oldPasswordEdit.setText("");
            return;
        }
        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());
        params.put("oldPwd", oldpassword);
        params.put("newPwd", newpassword);

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "profile";

        MyHttpClient.put(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {


                dismissSpinnerDialog();

                reviseSuccess();


            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();

                reviseSuccess();

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });



    }


    private void findView() {
        oldPasswordEdit= (EditText) findViewById(R.id.oldpasswordEdit);
        newPasswordEdit= (EditText) findViewById(R.id.newpasswordEdit);
        eyeableButton= (ImageButton) findViewById(R.id.btnEyeable);
        btnRevise= (Button) findViewById(R.id.btnRevise);

    }



    private void setEyeable() {
        if(eyeableButton.isSelected()){
            eyeableButton.setSelected(false);
            newPasswordEdit.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }else {
            eyeableButton.setSelected(true);
            newPasswordEdit.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
    }
    private void reviseSuccess(){
        AlertDialog.Builder normalDia=new AlertDialog.Builder(PasswordRevise.this);
        normalDia.setMessage("修改密码成功");

        normalDia.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(PasswordRevise.this, EntryLoginActivity.class);
                startActivity(intent);
            }
        });

        normalDia.create().show();
    }
}
