package com.gruden.education.event;

/**
 * Created by yunjiansun on 15/12/25.
 */


public class ClassItemClickEvent {

    public String conversationId;
    public String conversationName;
    public ClassItemClickEvent(String conversationId, String conversationName) {
        this.conversationId = conversationId;
        this.conversationName = conversationName;
    }
}
