package com.gruden.education.download;

import android.view.View;

import org.xutils.common.Callback;
import org.xutils.x;

import java.io.File;

/**
 * Created by wyouflf on 15/11/10.
 */
public abstract class DownloadViewHolder {

    protected DownloadInfos downloadInfo;

    public DownloadViewHolder(View view, DownloadInfos downloadInfo) {
        this.downloadInfo = downloadInfo;
        x.view().inject(this, view);
    }

    public final DownloadInfos getDownloadInfo() {
        return downloadInfo;
    }

    public void update(DownloadInfos downloadInfo) {
        this.downloadInfo = downloadInfo;
    }

    public abstract void onWaiting();

    public abstract void onStarted();

    public abstract void onLoading(long total, long current);

    public abstract void onSuccess(File result);

    public abstract void onError(Throwable ex, boolean isOnCallback);

    public abstract void onCancelled(Callback.CancelledException cex);
}
