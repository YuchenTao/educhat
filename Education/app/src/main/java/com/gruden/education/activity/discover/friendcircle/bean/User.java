package com.gruden.education.activity.discover.friendcircle.bean;
/**
 * 
* @ClassName: User 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author YuchenTao
* @date 2015-12-28 下午3:45:04 
*
 */
public class User {
	private String id;
	private String name;
	private String avatarUrl;
	public User(String id, String name, String avatarUrl){
		this.id = id;
		this.name = name;
		this.avatarUrl = avatarUrl;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	@Override
	public String toString() {
		return "id = " + id
				+ "; name = " + name
				+ "; avatarUrl = " + avatarUrl;
	}
}
