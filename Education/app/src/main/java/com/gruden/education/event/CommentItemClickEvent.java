package com.gruden.education.event;

import com.gruden.education.model.Comment;

/**
 * Created by yunjiansun on 16/3/23.
 */
public class CommentItemClickEvent {
    public Comment comment;
    public int position;
    public CommentItemClickEvent(Comment comment,int position){
        this.comment = comment;
        this.position = position;
    }
}
