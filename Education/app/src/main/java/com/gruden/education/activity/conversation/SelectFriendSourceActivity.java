package com.gruden.education.activity.conversation;

import android.content.Intent;
import android.os.Bundle;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.activity.discover.contact.ContactActivity;

import butterknife.OnClick;

/**
 * Created by admin on 2016/5/5.
 */
public class SelectFriendSourceActivity extends BaseActivity {

    private String conversationId;
    private String className;
    private String code;
    private int type;
    private int status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_friend_source);
        Intent intent=getIntent();
        conversationId=intent.getStringExtra("groupId");
        className=intent.getStringExtra("name");
        code=intent.getStringExtra("code");
        type=intent.getIntExtra("type",0);
        status=intent.getIntExtra("status",0);
        initActionBar(intent.getStringExtra("title"));
        showLeftBackButton();
    }
    @OnClick(R.id.layoutIdentity)
    public void onIdentityClick(){
        Intent intent=new Intent(this,InviteNewMember.class) ;
        intent.putExtra("type",1);
        intent.putExtra("groupId",conversationId);
        intent.putExtra("code",code);
        intent.putExtra("name",className);
        intent.putExtra("status",status);
        startActivity(intent);
    }
    @OnClick(R.id.layoutFriendList)
    public void onFriendListClick(){
        Intent intent = new Intent(this, ContactActivity.class);
        intent.putExtra("isCheck", true);
        intent.putExtra("type",type);
        intent.putExtra("groupId",conversationId);
        intent.putExtra("code",code);
        intent.putExtra("status",status);
        intent.putExtra("name",className);
        startActivity(intent);
    }
    @OnClick(R.id.layoutContacts)
    public void onContactsClick(){
        Intent intent = new Intent(this, AddFromContactsActivity.class);
        intent.putExtra("isCheck", true);
        intent.putExtra("type",type);
        intent.putExtra("groupId",conversationId);
        intent.putExtra("code",code);
        intent.putExtra("status",status);
        intent.putExtra("name",className);
        startActivity(intent);

    }



}
