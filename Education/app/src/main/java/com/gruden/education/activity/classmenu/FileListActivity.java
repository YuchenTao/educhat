package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.FileListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.download.DownloadManager;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xutils.ex.DbException;
import org.xutils.view.annotation.ContentView;
import org.xutils.x;

import butterknife.Bind;

/**
 * Created by admin on 2016/3/9.
 */
@ContentView(R.layout.filelist_activity)
public class FileListActivity extends BaseActivity {

    @Bind(R.id.file_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.filelist_listView)
    ListView filelist_listView;

    FileListAdapter fileListAdapter;

    private boolean isRefreshing;
    private boolean isLoadMoreing;

    private int curPage=0;
    private String conversationId;
   private DownloadManager downloadManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        x.view().inject(this);
        downloadManager=DownloadManager.getInstance(conversationId);

        initActionBar("群文件");
        showLeftBackButton();
        Intent intent=getIntent();
        conversationId = intent.getStringExtra("id");

        showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(FileListActivity.this, LocalFileUploadActivity.class);
                intent.putExtra("groupId", conversationId);
                startActivity(intent);
            }
        });


        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                downloadManager.downloadInfoList.clear();
                updatePostList(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                updatePostList(curPage + 1);
            }
        });
        filelist_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                toast(view.findViewById(R.id.txtFileName).getTag().toString());
            }
        });
    }

    /**
     * 刷新页面，加载群文件列表
     * @param page  分页页码从page开始
     */
    private  void updatePostList(final int page) {
        RequestParams params = new RequestParams();
        params.put("groupId", conversationId);
        params.put("curPage", page);
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"filelist";

        MyHttpClient.get(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {
                if (array.length() == 0) {
                    toast("已经没有更多文件了~");
                } else {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject data = array.optJSONObject(i);
                        try {
                            downloadManager.showDownloadList(data.optString("id"), conversationId,
                                    data.optString("url"),
                                    data.optString("filename"),
                                    Environment.getExternalStorageDirectory().getPath() + "/Education/" + data.optString("filename"),
                                    Long.parseLong(data.optString("size")),
                                    Integer.parseInt(data.optString("downloadCount")),
                                    data.optString("createdAt"),
                                    data.optString("remarkName"),
                                    true, false);
                        } catch (DbException e) {

                            e.printStackTrace();
                        } finally {
                            fileListAdapter = new FileListAdapter(FileListActivity.this, downloadManager);
                            filelist_listView.setAdapter(fileListAdapter);
                        }
                    }
                    curPage = page;
                }
                dissmissCircle();
            }

            @Override
            public void handleSuccess(int code, String mesg) {

                dissmissCircle();
                toast(mesg);
            }

            @Override
            public void handleFailure(int error) {

                dissmissCircle();
                super.handleFailure(error);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }



    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }

}
