package com.gruden.education.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.gruden.education.R;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.event.PostListClickEvent;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.Topic;
import com.gruden.education.viewholder.TopicItemHolder;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by yunjiansun on 16/1/4.
 */
public class TopicListAdapter extends BaseAdapter {

    ArrayList<Topic> dataList;
    Context context;
    LayoutInflater mInflater;
    String tagCode;

    public void setDataList(ArrayList<Topic> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }
    public TopicListAdapter(Context context , ArrayList<Topic> topicList){
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.dataList = topicList;

    }
    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return dataList.get(position).tag;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Topic topic = dataList.get(position);

        TopicItemHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.bbs_topic_item, parent, false);
//            convertView = mInflater.inflate(R.layout.itemview_request, null);
            holder = new TopicItemHolder();

            holder.txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
            holder.txtDetail = (TextView) convertView.findViewById(R.id.txt_detail);
            holder.imgTopic = (ImageView) convertView.findViewById(R.id.img_topic);
            holder.btnAttention= (Button) convertView.findViewById(R.id.btnAttention);
            convertView.setTag(holder);
        } else {
            holder = (TopicItemHolder) convertView.getTag();
        }

        holder.txtTitle.setText(topic.strTitle);
        holder.txtDetail.setText(topic.strDetail);
        ImageLoader.getInstance().displayImage(topic.imgUrl, holder.imgTopic, PhotoUtils.avatarImageOptions);
        if (topic.isConcerned==0){
            holder.btnAttention.setText("关注");
            holder.btnAttention.setTextColor(0xff82d3cc);
            holder.btnAttention.setBackgroundResource(R.drawable.common_btn_bg_normal);
        }else{
            holder.btnAttention.setText("已关注");
            holder.btnAttention.setTextColor(0xffffffff);
            holder.btnAttention.setBackgroundResource(R.drawable.common_btn_normal);
        }
        holder.btnAttention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button= (Button) v;
                if (topic.isConcerned==0){
                    concerned(button,topic.strTagCode,1,topic);
                }else{
                    concerned(button,topic.strTagCode,0,topic);
                }

            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new PostListClickEvent(topic.strTitle, topic.strTagCode));
            }
        });

        return convertView;
    }
    public void concerned(final Button button, String code, final int type, final Topic topic){
        RequestParams params = new RequestParams();
        params.put("userId",LeanchatUser.getCurrentUser().getObjectId());
        params.put("tagCode", code);
        params.put("type",type);
        System.out.println("用户Id："+LeanchatUser.getCurrentUser().getObjectId());
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_FORUM +"post/concern";
        MyHttpClient.put(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg) {
                Toast.makeText(context,mesg,Toast.LENGTH_SHORT).show();
                if(type==1) {
                    button.setText("已关注");
                    button.setTextColor(0xffffffff);
                    button.setBackgroundResource(R.drawable.common_btn_normal);
                    topic.isConcerned = 1;
                }else{
                    button.setText("关注");
                    button.setTextColor(0xff82d3cc);
                    button.setBackgroundResource(R.drawable.common_btn_bg_normal);
                    topic.isConcerned=0;
                }
            }

            @Override
            public void handleFailure(int error) {
                super.handleFailure(error);

            }

        });
    }
}
