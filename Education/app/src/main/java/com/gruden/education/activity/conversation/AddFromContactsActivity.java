package com.gruden.education.activity.conversation;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.activity.discover.contact.Contact;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * Created by admin on 2016/5/9.
 */
public class AddFromContactsActivity extends BaseActivity{
    @Bind(R.id.listContacts)
    ListView listView;
    @Bind(R.id.btn_invite)
    Button btn_invite;
    ContactsAdapter adapter;

    private String conversationId;
    private String className;
    private String code;
    private int type;
    private int status;

    ArrayList<Map<String,String>> phoneList=new ArrayList<Map<String,String>>();
    ArrayList<String> phone=new ArrayList<String>();
    ArrayList<Contact> contactlist=new ArrayList<Contact>();
    ArrayList<String> addMembersList=new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent=getIntent();
        conversationId=intent.getStringExtra("groupId");
        className=intent.getStringExtra("name");
        code=intent.getStringExtra("code");
        type=intent.getIntExtra("type",0);
        status=intent.getIntExtra("status",0);
        setContentView(R.layout.activity_add_from_contacts);
        initActionBar("手机通讯录");
        showLeftBackButton();
        adapter=new ContactsAdapter(this);
        listView.setAdapter(adapter);
        init();
    }
    private void init() {
        contactlist.clear();
        matchContacts();
        btn_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteNewMembers();
            }
        });
    }
    private void getFriendsInfo(){
        addMembersList.clear();
        for (int i=0;i<adapter.getCount();i++){
            Contact contact=adapter.getItem(i);
            if (contact.isChecked){
                String friend=contact.userId+"::"+contact.name+"::"+contact.identity;
                addMembersList.add(friend);
            }
        }
    }

    private void inviteNewMembers(){
        getFriendsInfo();
        if (addMembersList.size()==0){
            toast("您还没有选择联系人~");
            return ;
        }
        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("groupId", conversationId);
        params.put("type", type);
        params.put("code", code);
        params.put("name", className);
        params.put("nickname", LeanchatUser.getCurrentUser().getNickName());
        params.put("member",addMembersList);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME+"members/invite";

        MyHttpClient.post(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
                finish();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }
    private void matchContacts(){
        showSpinnerDialog();
        readAllContacts();

        System.out.println(phone.toString());

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"contact";
        url = url + "?userId="+ LeanchatUser.getCurrentUser().getObjectId();
        for(int i=0;i<phone.size();i++){
            url+="&phone="+phone.get(i);
        }
        MyHttpClient.get(url, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray data) {

                if (data.length() == 0 || data == null) {
//                    toast("未匹配的用户~");
                } else {
                    for (int j = 0; j < phoneList.size(); j++) {
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.optJSONObject(i);
                            if (phoneList.get(j).containsKey(object.opt("phone"))) {
                                Contact contact = new Contact();
                                contact.userId = object.optString("uid");
                                contact.avatarUrl = object.optString("avatar");
                                contact.name = object.optString("nickname") ;
                                contact.number=object.optString("phone");
                                contact.simpleNumber=phoneList.get(j).get(object.opt("phone"));
                                contact.isChecked=false;

                                List<String> arrRolesCode = new ArrayList<String>();
                                for (int m=0; m<object.optJSONArray("roles").length(); m++){
                                    arrRolesCode.add(object.optJSONArray("roles").optJSONObject(m).optString("code"));
                                }
                                if (status==1&&arrRolesCode.contains("R_TEACHER")) {
                                    contact.identity=1;
                                    contactlist.add(contact);
                                }
                                if (status==2&&arrRolesCode.contains("R_PARENT")){
                                    contact.identity=2;
                                    contactlist.add(contact);
                                }
                            }
                        }
                    }
                    if (contactlist.size()==0&&status==1){
                        toast("未匹配到老师用户~");
                    }else if(contactlist.size()==0&&status==2){
                        toast("未匹配到家长用户~");
                    }
                    adapter.setDataList(contactlist);
                    adapter.notifyDataSetChanged();
                }
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }
    /*
         * 读取联系人的信息
         */
    private static final String[] CONTACTOR_ION = new String[]{
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER
    };
    public void readAllContacts(){
        phone.clear();
        phoneList.clear();
        Cursor phones = null;
        ContentResolver cr = getContentResolver();
        try {
            phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                    , CONTACTOR_ION, null, null, "sort_key");

            if (phones != null) {
                final int contactIdIndex = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
                final int displayNameIndex = phones.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                final int phoneIndex = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String phoneString, displayNameString, contactIdString;
                while (phones.moveToNext()) {
//          LinkManForm linkManForm = new LinkManForm();
                    HashMap<String,String> map = new HashMap<String,String>();
                    phoneString = phones.getString(phoneIndex);
                    displayNameString = phones.getString(displayNameIndex);
                    contactIdString = phones.getString(contactIdIndex);
                    if (TextUtils.isEmpty(phoneString))
                        continue;
                    phone.add(phoneString);
                    map.put(phoneString, displayNameString);
                    phoneList.add(map);

                }
            }
        } catch (Exception e) {

        } finally {
            if (phones != null)
                phones.close();
        }
    }

    class ContactsAdapter extends BaseAdapter {
        Context context;
        ArrayList<Contact> data=new ArrayList<Contact>();

        public ContactsAdapter( Context context) {
            this.context = context;
        }

        public void setDataList(ArrayList<Contact> datas) {
            data.clear();
            if (null != datas) {
                data.addAll(datas);
            }
        }
        public void addDataList(ArrayList<Contact> datas) {
            data.addAll( datas);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Contact getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Contact contact=data.get(position);
            final  ContactHolder holder=new ContactHolder();
                convertView=View.inflate(context,R.layout.item_contact,null);
                holder.tvLetter= (TextView) convertView.findViewById(R.id.tvLetter);
                holder.cbChecked= (CheckBox) convertView.findViewById(R.id.cbChecked);
                holder.title= (TextView) convertView.findViewById(R.id.title);
                holder.imgAvatar= (ImageView) convertView.findViewById(R.id.imgAvatar);
                holder.tvLetter.setVisibility(View.GONE);
                holder.cbChecked.setVisibility(View.VISIBLE);

            if (contact.isChecked){
                holder.cbChecked.setChecked(true);
            }else{
                holder.cbChecked.setChecked(false);
            }
            if(contact.avatarUrl.length()>0){
                ImageLoader.getInstance().displayImage(contact.avatarUrl, holder.imgAvatar);
            }
            holder.title.setText(contact.name+"("+contact.simpleNumber+")");
            holder.cbChecked.setTag(contact.userId+"::"+contact.simpleNumber);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.cbChecked.performClick();
                }
            });
            holder.cbChecked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.cbChecked.isChecked()){
                        holder.cbChecked.setChecked(true);
                        contact.isChecked=true;
                    }else{
                        holder.cbChecked.setChecked(false);
                        contact.isChecked=false;
                    }
                }
            });
            return convertView;
        }
    }

    class ContactHolder{
        TextView tvLetter;
        CheckBox cbChecked;
        ImageView imgAvatar;
        TextView title;

    }
}
