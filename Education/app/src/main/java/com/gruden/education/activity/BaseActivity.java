package com.gruden.education.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.LogUtil;
import com.avoscloud.leanchatlib.activity.AVBaseActivity;
import com.baidu.mapapi.map.Text;
import com.gruden.education.R;
import com.avoscloud.leanchatlib.utils.LogUtils;

import java.util.ArrayList;


public class BaseActivity extends AVBaseActivity {

  protected static final int PERMISSIONS_REQUEST = 301;
  protected ActionBar actionBar;
  protected TextView txtBarTitle;
  protected TextView txtBack;
  protected ImageButton btnBack;
  protected ImageButton btnLeftImage;
  protected Button btnRightTitle;
  protected ImageButton btnRightImage;
  protected ProgressDialog dialog;

  protected void alwaysShowMenuItem(MenuItem add) {
    add.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS
            | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
  }

  protected ProgressDialog showSpinnerDialog() {

    return showSpinnerDialog(null);

  }


  protected void dismissSpinnerDialog() {
    if (dialog != null && dialog.isShowing())
      dialog.dismiss();
  }

  protected ProgressDialog showSpinnerDialog(String message) {

    if (dialog!=null&&dialog.isShowing())
      return dialog;

    if(dialog==null){
      dialog = new ProgressDialog(this);
      dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
      dialog.setCancelable(false);
    }

    if (message ==null)
        message = "加载中";
    dialog.setMessage(message);
    if (!isFinishing()) {
      dialog.show();
    }
    return dialog;
  }



//  @Override
//  public boolean onOptionsItemSelected(MenuItem item) {
//    switch (item.getItemId()) {
//      case R.id.btnBack:
//        super.onBackPressed();
//        return true;
//    }
//    return super.onOptionsItemSelected(item);
//  }

  public void hideSoftInputView() {
    if (getWindow().getAttributes().softInputMode !=
        WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
      InputMethodManager manager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      View currentFocus = getCurrentFocus();
      if (currentFocus != null) {
        manager.hideSoftInputFromWindow(currentFocus.getWindowToken(),
            InputMethodManager.HIDE_NOT_ALWAYS);
      }
    }
  }

  protected void setSoftInputMode() {
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
  }

  private void findBarViews(){

    actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setCustomView(R.layout.base_actionbar);

    txtBarTitle = (TextView) actionBar.getCustomView().findViewById(R.id.txtBarTitle);
    btnBack = (ImageButton) actionBar.getCustomView().findViewById(R.id.btnBack);
    txtBack = (TextView) actionBar.getCustomView().findViewById(R.id.txtBack);
    btnLeftImage = (ImageButton) actionBar.getCustomView().findViewById(R.id.btnLeftImage);
    btnRightTitle = (Button) actionBar.getCustomView().findViewById(R.id.btnRightTitle);
    btnRightImage = (ImageButton) actionBar.getCustomView().findViewById(R.id.btnRightImage);
  }

  protected void initActionBar() {
    initActionBar(null);
  }

  protected void initActionBar(String title) {

    actionBar = getActionBar();


    if (actionBar != null) {
      findBarViews();

      if (title != null) {

        txtBarTitle.setText(title);


      }

    }
  }

  protected void initActionBar(int id) {
    initActionBar(getString(id));
  }


  public void showLeftBackButton(View.OnClickListener listener) {
    showLeftBackButton(null, listener);
  }
  public void showLeftBackButton(String backText) {
    showLeftBackButton(backText, null);
  }

  public void showLeftBackButton() {
    showLeftBackButton(null,null);
  }


  public void showLeftBackButton(String backText, View.OnClickListener listener) {

    btnBack.setVisibility(View.VISIBLE);
    txtBack.setVisibility(View.VISIBLE);

    if (backText!=null)
      txtBack.setText(backText);

    if (listener == null) {
      listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          onBackPressed();
        }
      };
    }

    btnBack.setOnClickListener(listener);
    txtBack.setOnClickListener(listener);
  }

  public void hideLeftBackButton(){
    btnBack.setVisibility(View.INVISIBLE);
    txtBack.setVisibility(View.INVISIBLE);
  }

  public void showLeftImageButton(int rightResId, View.OnClickListener listener) {

    btnLeftImage.setVisibility(View.VISIBLE);

    btnLeftImage.setImageResource(rightResId);
    btnLeftImage.setOnClickListener(listener);
  }

  public void showRightImageButton(int rightResId, View.OnClickListener listener) {

    btnRightImage.setVisibility(View.VISIBLE);
    btnRightTitle.setVisibility(View.INVISIBLE);

    btnRightImage.setImageResource(rightResId);
    btnRightImage.setOnClickListener(listener);
  }

  public void showRightTextButton(String rightText, View.OnClickListener listener) {

    btnRightImage.setVisibility(View.INVISIBLE);
    btnRightTitle.setVisibility(View.VISIBLE);

    btnRightTitle.setText(rightText);
    btnRightTitle.setOnClickListener(listener);
  }

  public void hideRightButton(){
    btnRightImage.setVisibility(View.INVISIBLE);
    btnRightTitle.setVisibility(View.INVISIBLE);
  }


  protected void toast(String str) {
    if (isFinishing())
      return;
    Toast toast = Toast.makeText(this, str, Toast.LENGTH_LONG);
    toast.setGravity(Gravity.CENTER, 0, 0);
    toast.show();
  }

  protected void toast(Exception e){
    if(e!=null){
      toast(e.getMessage());
    }
  }

  protected void toast(int id) {
    Toast toast = Toast.makeText(this, id, Toast.LENGTH_LONG);
    toast.setGravity(Gravity.CENTER, 0, 0);
    toast.show();
  }


  protected int dp2px(int dp) {
    return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
            getResources().getDisplayMetrics());
  }


  protected boolean hasPermission(String permission) {
    if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
      return(PackageManager.PERMISSION_GRANTED == checkSelfPermission(permission));
    else
      return true;
  }

  protected String[]  getUngrantedPermissions(String[] permissions){
    // 一个list，用来存放没有被授权的权限
    ArrayList<String> denidArray = new ArrayList<>();
    // 遍历PERMISSIONS_GROUP，将没有被授权的权限存放进denidArray
    for (String p : permissions) {
      if (hasPermission(p)) {
        Log.d(p,"allow");
      }else {
        Log.e(p,"deny");
        denidArray.add(p);
      }
    }

    return denidArray.toArray(new String[denidArray.size()]);
  }

  @TargetApi(Build.VERSION_CODES.M)
  protected void checkPermission(String[] permissions){

    String [] unGranted = getUngrantedPermissions(permissions);

    Log.e("unGrantedPermission",unGranted.length+"");

    if (unGranted.length > 0) {
      requestPermissions(unGranted,PERMISSIONS_REQUEST);
    }

  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    if (requestCode==PERMISSIONS_REQUEST){
      System.out.println("onRequestPermissionsResult");
      for (int i =0;i<permissions.length;i++){

        Log.e(permissions[i],grantResults[i]+"");

        if (grantResults[i]!=PackageManager.PERMISSION_GRANTED){
          ungrantedPermissionPrompt(permissions[i]);
        }

      }

    }

  }

  protected void ungrantedPermissionPrompt(String permission){

    String strPrompt="";

    switch (permission){
      case Manifest.permission.READ_EXTERNAL_STORAGE:{
        strPrompt = "访问存储空间";
        break;
      }
      case Manifest.permission.WRITE_EXTERNAL_STORAGE:{
        strPrompt = "修改存储空间";
        break;
      }
      case Manifest.permission.CAMERA:{
        strPrompt = "访问相机";
        break;
      }
      case Manifest.permission.READ_CONTACTS:{
        strPrompt = "访问通讯录";
        break;
      }
      case Manifest.permission.CALL_PHONE:{
        strPrompt = "拨打电话";
        break;
      }
      case Manifest.permission.SEND_SMS:{
        strPrompt = "发送短息";
        break;
      }

    }

    if (!TextUtils.isEmpty(strPrompt)){
      new AlertDialog.Builder(this)
              .setMessage("请去系统设置应用程序中修改并允许"+strPrompt+"的权限")
              .setPositiveButton("OK", null)
              .create()
              .show();
    }

  }

}
