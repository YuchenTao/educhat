package com.gruden.education.activity.bbs;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.PostCommentListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.event.CommentItemClickEvent;
import com.gruden.education.model.Comment;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.datafans.android.common.helper.DipHelper;
import net.datafans.android.timeline.view.imagegrid.ImageGridView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

public class PostDetailActivity extends BaseActivity implements TextWatcher {

    @Bind(R.id.post_commment_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.listView)
    ListView listView;

    @Bind(R.id.editComment)
    EditText editComment;

    @Bind(R.id.btnSend)
    Button btnSend;

    private View headerView;
    private FrameLayout layoutContent;
    private ImageView imgAvatar;
    private TextView txtTitle;
    private TextView txtUserName;
    private TextView txtCreatedAt;

    private WebView webViewContent;

    private ImageGridView imageGridView;

    private String id;
    private  boolean isCollected;
    private int curPage;
    private boolean isRefreshing;
    private boolean isLoadMoreing;

    private PostCommentListAdapter listAdapter;

    private Comment commentReply;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        initActionBar();
        showLeftBackButton();
        showRightImageButton(R.drawable.btn_favorite, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnRightImage.isSelected()){
                    deleteFavotite();
                }else {
                    addFavotite();
                }
            }
        });


        Intent intent = getIntent();
        id = intent.getStringExtra("id");

        headerView =  LayoutInflater.from(this).inflate(R.layout.post_header_layout, listView, false);
        txtTitle = (TextView) headerView.findViewById(R.id.txtTitle);
        txtUserName = (TextView) headerView.findViewById(R.id.txtUserName);
        txtCreatedAt = (TextView) headerView.findViewById(R.id.txtCreatedAt);

        imgAvatar = (ImageView) headerView.findViewById(R.id.imgAvatar);
        layoutContent = (FrameLayout) headerView.findViewById(R.id.layoutContent);
        webViewContent= (WebView) headerView.findViewById(R.id.webViewcontent);
        webViewContent.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webViewContent.getSettings().setLoadWithOverviewMode(true);
        webViewContent.getSettings().setSupportZoom(true);
        webViewContent.getSettings().setBuiltInZoomControls(true);
//        webViewContent.getSettings().setDefaultTextEncodingName("UTF-8");

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        Display display = wm.getDefaultDisplay();
        display.getSize(size);
        int width = size.x;
        float scale = DipHelper.px2dip(this, width) / (float) DipHelper.px2dip(this, 1080);
        imageGridView = new ImageGridView(this, DipHelper.dip2px(this, scale * 240));
        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutContent.addView(imageGridView, imageParams);

        listView.addHeaderView(headerView);
        listAdapter = new PostCommentListAdapter(this);
        listView.setAdapter(listAdapter);

        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                updateCommentList(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                updateCommentList(curPage+1);
            }

        });

        editComment.addTextChangedListener(this);

        getPostDetail();

    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }

    void getPostDetail(){
        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("postId",id);
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_FORUM+"post/manage";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {

                ImageLoader.getInstance().displayImage(data.optString("avatar"), imgAvatar, PhotoUtils.avatarImageOptions);
                txtTitle.setText(data.optString("title"));
                txtUserName.setText(data.optString("nickname"));
                txtCreatedAt.setText(data.optString("createdAt"));
                btnRightImage.setSelected(data.optInt("isCollected")==1);
//                webViewContent.loadData(data.optString("content"),"text/html","utf-8");
                webViewContent.loadData(data.optString("content"), "text/html; charset=UTF-8", null);
                JSONArray arrUrls = data.optJSONArray("imgUrl");

                if (arrUrls!=null&&arrUrls.length()>0){

                    List<String> listUrls = new ArrayList<String>();
                    for (int i=0; i<arrUrls.length(); i++){
                        listUrls.add(arrUrls.optString(i));
                    }
                    imageGridView.updateWithImage(listUrls);

                }else {
                    layoutContent.setVisibility(View.GONE);
                }

                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }


    void updateCommentList(final int page){
        RequestParams params = new RequestParams();
        params.put("postId",id);
        params.put("curPage",page);

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_FORUM +"post/comment";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                dissmissCircle();

                if (array.length()==0&&isLoadMoreing){
                    if (page==0)
                        toast("暂时还没有评论~");
                    else
                        toast("已经没有更多评论了~");

                }else {
                    ArrayList<Comment> commentList = new ArrayList<Comment>();

                    for (int i=0; i<array.length(); i++){

                        JSONObject object = array.optJSONObject(i);
                        Comment comment = new Comment();
                        comment.id = object.optString("id");
                        comment.userId = object.optString("userId");
                        comment.avatarUrl = object.optString("avatar");
                        comment.nickname = object.optString("nickname");
                        comment.replyTo = object.optJSONObject("reply_to");
                        comment.content = object.optString("content");
                        comment.createdAt= object.optString("createdAt");
                        comment.replyCount = object.optInt("replyCount");
                        commentList.add(comment);
                    }

                    if (page==0) {
                        listAdapter.setDataList(commentList);
                    }
                    else {
                        listAdapter.addDataList(commentList);
                    }

                    listAdapter.notifyDataSetChanged();
                    curPage = page;
                    dismissSpinnerDialog();
                }



            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }

        });
    }
    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }

    private void addFavotite(){

        showSpinnerDialog();
        LeanchatUser user = LeanchatUser.getCurrentUser();
        RequestParams params = new RequestParams();
        params.put("userId",user.getObjectId());
        params.put("postId",id);

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER+ "collection";
        MyHttpClient.post(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg){
                dismissSpinnerDialog();
                toast(mesg);
                if (code==0)
                    btnRightImage.setSelected(true);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);

            }

        });

    }

    private void deleteFavotite(){

        showSpinnerDialog();
        LeanchatUser user = LeanchatUser.getCurrentUser();
        RequestParams params = new RequestParams();
        params.put("userId",user.getObjectId());
        params.put("postId",id);

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "collection";
        MyHttpClient.delete(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg){
                dismissSpinnerDialog();
                if (code==0) {
                    btnRightImage.setSelected(false);
                    toast("已取消收藏！");
                }
                else
                    toast(mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);

            }

        });

    }

    @OnClick(R.id.btnSend)
    public void sendComment(){

        editComment.clearFocus();

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//隐藏软键盘
        imm.hideSoftInputFromWindow(editComment.getWindowToken(), 0);

        showSpinnerDialog();

        LeanchatUser user = LeanchatUser.getCurrentUser();
        RequestParams params = new RequestParams();
        params.put("userId",user.getObjectId());
        params.put("postId",id);
        params.put("content",editComment.getText().toString());
        params.put("type",0);

        if (commentReply!=null){
            params.put("commentId",commentReply.id);
            params.put("commentCreatorId",commentReply.userId);
        }

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_FORUM+ "post/comment";
        MyHttpClient.post(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg, JSONObject data){

                commentReply = null;
                editComment.setText(null);
                editComment.setHint("我也说一句...");
                btnSend.setEnabled(false);

                refreshLayout.autoRefresh();
            }

            @Override
            public void handleSuccess(int code, String mesg){
                dismissSpinnerDialog();
                toast(mesg);
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);

            }

        });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.isEmpty(editComment.getText().toString()))
            btnSend.setEnabled(false);
        else
            btnSend.setEnabled(true);
    }

    public void onEvent(CommentItemClickEvent event){
        commentReply = event.comment;
        editComment.setHint("回复"+event.comment.nickname+":");
        editComment.requestFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//隐藏软键盘
//        imm.hideSoftInputFromWindow(editComment.getWindowToken(), 0);
//显示软键盘
        imm.showSoftInput(editComment, 0);
//        listView.setSelection(event.position);

//        listView.smoothScrollToPosition(event.position);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        webViewContent.stopLoading();
        webViewContent=null;
    }
}
