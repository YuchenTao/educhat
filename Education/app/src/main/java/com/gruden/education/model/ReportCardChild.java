package com.gruden.education.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/1/15.
 */
public class ReportCardChild {

    public String studentCode;
    public String studentName;
    public int total;

    public List<Subject> subjectList  = new ArrayList<Subject>();


}
