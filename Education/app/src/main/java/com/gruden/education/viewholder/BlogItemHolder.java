package com.gruden.education.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.nereo.multi_image_selector.bean.Image;

/**
 * Created by yunjiansun on 16/2/18.
 */
public class BlogItemHolder {
    public View dateLayout;
    public View blogLayout;
    public View lineBottom;
    public ImageView imageView;
    public TextView txtDate;
    public TextView txtTitle;
    public TextView txtCreateAt;
    public TextView txtContent;
}
