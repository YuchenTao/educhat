package com.gruden.education.activity.entry;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.gruden.education.R;
import com.gruden.education.activity.MainActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Child;
import com.gruden.education.model.Friend;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EntrySplashActivity extends EntryBaseActivity {
  public static final int SPLASH_DURATION_LONG = 2700;
  public static final int SPLASH_DURATION_ShORT = 2200;
  private static final int GO_MAIN_MSG = 1;
  private static final int GO_LOGIN_MSG = 2;

  private Handler handler = new Handler() {
    @Override
    public void handleMessage(Message msg) {
      switch (msg.what) {
        case GO_MAIN_MSG:
          MainActivity.goMainActivityFromActivity(EntrySplashActivity.this);
          finish();
          break;
        case GO_LOGIN_MSG:
          Intent intent = new Intent(EntrySplashActivity.this, EntryLoginActivity.class);
          EntrySplashActivity.this.startActivity(intent);
          finish();
          break;
      }
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    // TODO Auto-generated method stub
    super.onCreate(savedInstanceState);
    setContentView(R.layout.entry_splash_layout);
  }

  @Override
  protected void onResume() {
    super.onResume();
    checkCurrentUser();
  }

  private void checkCurrentUser(){
    if (LeanchatUser.getCurrentUser() != null) {
      LeanchatUser user = LeanchatUser.getCurrentUser();
      final String password = user.getMyPassword();
      final boolean isMatching =user.getMatching();
      final List<Friend> newFriendList=user.getNewFriends();
      final long friend_startTime=user.getFriendStartTime();
      final long class_startTime=user.getClassStartTime();
      RequestParams params = new RequestParams();
      params.put("username",user.getUsername());
      params.put("password", password);

      String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_USER + "login";

      MyHttpClient.get(url,params,new MyJsonResponseHandler(){

        @Override
        public void handleSuccess(int code, String mesg, JSONObject data) {


          int length = data.optJSONArray("roles").length();
          List<String> arrRolesCode = new ArrayList<String>();

          for (int i=0; i<length; i++){
            arrRolesCode.add(data.optJSONArray("roles").optJSONObject(i).optString("code"));
          }

          List<Child> arrChildren = new ArrayList<Child>();
          for (int i=0;i<data.optJSONArray("children").length();i++){
            JSONObject object = data.optJSONArray("children").optJSONObject(i);
            Child child = new Child();
            child.groupId = object.optString("groupId");
            child.childId = object.optString("childId");
            child.studentCode = object.optString("studentCode");
            child.name = object.optString("name");
            arrChildren.add(child);
          }

          LeanchatUser user = new LeanchatUser();
          user.setObjectId(data.optString("userId"));
          user.setUsername(data.optString("username"));
          user.setMyPassword(password);
          user.setNickName(data.optString("nickname"));
          user.setGender(data.optInt("gender",0));
          user.setAvatarUrl(data.optString("avatar"));
          user.setSignture(data.optString("signature"));
          user.setMobilePhoneNumber(data.optString("mobilePhoneNumber"));
          user.setRoles(arrRolesCode);
          user.setPosition(data.optString("position"));
          user.setProvince(data.optString("provinceCode"));
          user.setCity(data.optString("cityCode"));
          user.setArea(data.optString("areaCode"));
          user.setChildren(arrChildren);
          user.setMatching(isMatching);
          user.setNewFriends(newFriendList);
          user.setFriendStartTime(friend_startTime);
          user.setClassStartTime(class_startTime);
          LeanchatUser.logOut();

          LeanchatUser.changeCurrentUser(user, true);

          LeanchatUser.getCurrentUser().updateUserInfo();
          handler.sendEmptyMessageDelayed(GO_MAIN_MSG, SPLASH_DURATION_ShORT);

        }

        @Override
        public void handleSuccess(int code, String mesg) {
          toast(mesg);
          handler.sendEmptyMessageDelayed(GO_LOGIN_MSG, SPLASH_DURATION_ShORT);
        }

        @Override
        public void handleFailure(int error) {
          super.handleFailure(error);
          handler.sendEmptyMessageDelayed(GO_LOGIN_MSG, SPLASH_DURATION_ShORT);
        }

      });

    } else {
      handler.sendEmptyMessageDelayed(GO_LOGIN_MSG, SPLASH_DURATION_LONG);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }
}
