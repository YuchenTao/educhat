package com.gruden.education.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gruden.education.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;


public class RedGridViewAdapter extends BaseAdapter {
//    private RelativeLayout.LayoutParams params;
//    int rowHeight;
//    int screenWidth;

    Context context;
    JSONArray data;


    public RedGridViewAdapter(Context context, JSONArray data) {
        this.context = context;
        this.data = data;
//        screenWidth = ImageUtil.getScreenWidth(context);// 屏幕宽度px
//        int itemPadding = (int) context.getResources().getDimension(R.dimen.gridPadding); // px
//        // 计算每张图片大小：屏幕宽度 - GridView右边距 - GridView每个item的边距 * 4，结果除以4， 就是每张图片的大小（px）
//        int imageSize = (screenWidth - itemPadding - itemPadding * 4) / 4;
//        params = new RelativeLayout.LayoutParams(imageSize, imageSize);
    }

    @Override
    public int getCount() {
        return data.length();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView=View.inflate(context, R.layout.redgridview_item_layout,null);
        ImageView imageChild = (ImageView)convertView.findViewById(R.id.imageChild);
        TextView txtchildName=(TextView)convertView.findViewById(R.id.txtchildName);
        TextView txtredNum=(TextView)convertView.findViewById(R.id.txtredNum);
        RelativeLayout child_layout= (RelativeLayout) convertView.findViewById(R.id.child_layout);
//        child_layout.setLayoutParams(params);
        if(!TextUtils.isEmpty(data.optJSONObject(position).optString("avatar"))){
            ImageLoader.getInstance().displayImage(data.optJSONObject(position).optString("avatar"), imageChild);
        }
        txtchildName.setText(data.optJSONObject(position).optString("childName"));
        txtchildName.setTag(data.optJSONObject(position).opt("childId"));
        txtredNum.setText(data.optJSONObject(position).optString("count"));
        return convertView;
    }
    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


}
