package com.gruden.education.activity.profile;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.BBSPostListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.BBSPost;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by admin on 2016/3/8.
 */
public class MyPostActivity extends BaseActivity {


    @Bind(R.id.collection_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.collection_listView)
    ListView collection_listView;

    @Bind(R.id.txtPrompt)
    TextView txtPrompt;

    BBSPostListAdapter postListAdapter;


    private boolean isRefreshing;
    private boolean isLoadMoreing;
    int curPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mycollection_activity);
        initActionBar("我的帖子");
        showLeftBackButton();
        initData();


        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                updatePostList(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                updatePostList(curPage + 1);
            }
            @Override
            public void onfinish() {
                txtPrompt.setVisibility(postListAdapter.getCount()>0? View.INVISIBLE:View.VISIBLE);
            }

        });
    }
    private void initData() {
        txtPrompt.setText("您还没有发布过帖子~");
        postListAdapter = new BBSPostListAdapter(this);
        collection_listView.setAdapter(postListAdapter);
        updatePostList(0);

    }
    private void updatePostList(final int page) {
        RequestParams params = new RequestParams();
        params.put("curPage",page);
        params.put("userId", LeanchatUser.getCurrentUserId());
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_FORUM +"post/list/latest";

        MyHttpClient.get(url, params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length() == 0) {
                    toast("已经没有更多帖子了~");
                } else {
                    ArrayList<BBSPost> postList = new ArrayList<BBSPost>();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject data = array.optJSONObject(i);
                        BBSPost post = new BBSPost();
                        post.postId = data.optString("postId");
                        post.userId = data.optString("userId");
                        post.nickname = data.optString("nickname");
                        post.urlAvatar = data.optString("avatar");
                        post.title = data.optString("title");
                        post.replyCount = data.optInt("replyCount");
                        post.time = data.optString("createdAt");

                        postList.add(post);
                    }

                    if (page==0) {
                        postListAdapter.setDataList(postList);

                    }
                    else {
                        postListAdapter.addDataList(postList);
                    }
                    postListAdapter.notifyDataSetChanged();
                    curPage = page;

                }

                dissmissCircle();


            }


            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast(mesg);

            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);

            }

        });


    }
    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }


}
