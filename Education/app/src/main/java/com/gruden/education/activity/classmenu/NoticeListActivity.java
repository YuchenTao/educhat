package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.avoscloud.leanchatlib.view.DividerItemDecoration;
import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.adapter.NoticeListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.event.NoticeItemClickEvent;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.Notice;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;

public class NoticeListActivity extends BaseActivity {

    @Bind(R.id.notice_refresh)
    MaterialRefreshLayout refreshLayout;

    @Bind(R.id.notice_recylerview)
    RecyclerView recyclerView;

    @Bind(R.id.txtPrompt)
    TextView txtPrompt;

    private LinearLayoutManager layoutManager;
    private NoticeListAdapter listAdapter;

    private int curPage;
    private String conversationId;

    private boolean isRefreshing;
    private boolean isLoadMoreing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_list);

        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");

        initActionBar("通知");
        showLeftBackButton();
        LeanchatUser user = LeanchatUser.getCurrentUser();
        if(user.getRoles().contains(LeanchatUser.TEACHER)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN)){
            showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(NoticeListActivity.this, SendNoticeActivity.class);
                    intent.putExtra("id", conversationId);
                    startActivity(intent);
                }
            });
        }




        refreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(final MaterialRefreshLayout materialRefreshLayout) {
                //下拉刷新...
                isRefreshing = true;
                updateNoticeList(0);
            }

            @Override
            public void onRefreshLoadMore(MaterialRefreshLayout materialRefreshLayout) {
                //上拉刷新...
                isLoadMoreing = true;
                updateNoticeList(curPage+1);
            }

            @Override
            public void onfinish(){
                txtPrompt.setVisibility(listAdapter.getItemCount()>0?View.INVISIBLE:View.VISIBLE);
            }
        });

        listAdapter= new NoticeListAdapter(conversationId);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        recyclerView.setAdapter(listAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshLayout.autoRefresh();
    }

    public void updateNoticeList(final int page){

        RequestParams params = new RequestParams();
        params.put("groupId",conversationId);
        params.put("curPage",page);

        LeanchatUser user = LeanchatUser.getCurrentUser();
        params.put("userId",user.getObjectId());
        params.put("role",(user.getRoles().contains(LeanchatUser.TEACHER)||user.getRoles().contains(LeanchatUser.SYSTEM_ADMIN))?"1":"0");

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_MESSAGE +"notice";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0&&page>0){

                    toast("已经没有更多通知了~");

                }else {
                    ArrayList<Notice> noticeList = new ArrayList<Notice>();

                    for (int i=0; i<array.length(); i++){

                        JSONObject data = array.optJSONObject(i);
                        Notice notice = new Notice();
                        notice.id = data.optString("id");
                        notice.content = data.optString("content");
                        notice.groupId = data.optString("groupId");
                        notice.isSign = data.optInt("isSign")==0?false:true;
                        notice.creator = data.optString("creator");
                        notice.isCreatedByMe = data.optInt("isCreatedByMe")==0?false:true;
                        notice.createdAt = data.optString("createdAt");

                        noticeList.add(notice);
                    }

                    if (page==0) {
                        listAdapter.setDataList(noticeList);
                        listAdapter.notifyDataSetChanged();
                    }
                    else {

                        int position = listAdapter.getItemCount();
                        listAdapter.addDataList(noticeList);
                        listAdapter.notifyItemRangeChanged(position,noticeList.size());
                    }

                    curPage = page;
                }

                dissmissCircle();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dissmissCircle();
                toast( mesg);
            }

            @Override
            public void handleFailure(int error) {
                dissmissCircle();
                super.handleFailure(error);
            }

        });
    }

    void dissmissCircle(){
        if (isRefreshing) {
            refreshLayout.finishRefresh();
            isRefreshing = false;
        }

        if (isLoadMoreing) {
            refreshLayout.finishRefreshLoadMore();
            isLoadMoreing = false;
        }
    }


    public void onEvent(NoticeItemClickEvent event) {
        if (isLoadMoreing||isRefreshing )
            return;

        if (event.isCreatedByMe){

            Intent intent = new Intent(this, NoticeTeacherActivity.class);
            intent.putExtra("id", event.id);
            intent.putExtra("conversationId", event.conversationId);
            startActivity(intent);
        }else {
            Intent intent = new Intent(this, NoticeParentActivity.class);
            intent.putExtra("id", event.id);
            startActivity(intent);
        }
    }
}
