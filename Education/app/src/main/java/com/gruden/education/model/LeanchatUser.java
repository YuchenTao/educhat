package com.gruden.education.model;


import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.FollowCallback;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.SignUpCallback;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by wli on 15/9/30.
 * 自定义的 AVUser
 */
public class LeanchatUser extends AVUser {

  public static final String USERNAME = "username";
  public static final String PASSWORD = "myPassword";
  public static final String NICKNAME = "nickname";
  public static final String GENDER = "gender";
  public static final String AVATAR = "avatar";
  public static final String SIGNTURE = "signature";
  public static final String USERTYPE = "userType";
  public static final String POSITION = "position";
  public static final String PROVINCE = "provinceCode";
  public static final String CITY = "cityCode";
  public static final String AREA = "areaCode";
  public static final String CHILDREN = "children";
  public static final String LOCATION = "location";
  public static final String INSTALLATION = "installation";
  public static final String FILE_MANAGER= "file_manager";
  public static final String NEWFRIENDS="newFriend";
  public static final String CONTACTS_MATCHING="contacts_matching";
  public static final String PARENT = "R_PARENT";
  public static final String TEACHER = "R_TEACHER";
  public static final String SYSTEM_ADMIN = "R_SYSTEM_ADMIN";
  public static final String FRIEND_STARTTIME="friend_startTime";
  public static final String CLASS_STARTTIME="class_startTime";
  public String myPassword;
  public AVObject fileManager;


  public static String getCurrentUserId () {
    LeanchatUser currentUser = getCurrentUser(LeanchatUser.class);
    return (null != currentUser ? currentUser.getObjectId() : null);
  }

  public String getNickName() {
      return getString(NICKNAME);
  }

  public void setNickName(String nickname) {
    this.put(NICKNAME,nickname);
  }

  public String getGender() {
    return getString(GENDER);
  }

  public void setGender(int gender) {
    this.put(GENDER,gender);
  }

  public String getSignture() {
    return getString(SIGNTURE);
  }

  public void setSignture(String signature) {
    this.put(SIGNTURE,signature);
  }

  public List getRoles() {
    return getList(USERTYPE);
  }

  public void setRoles(List roles) {
    this.put(USERTYPE,roles);
  }

  public void setMyPassword(String password) {
    setPassword(password);
    this.myPassword = password;
    this.put(PASSWORD,password);
  }
  public String getMyPassword() {
      return getString(PASSWORD);

  }

  public long getFriendStartTime() {
    return getLong(FRIEND_STARTTIME);
  }

  public void setFriendStartTime(long startTime) {
    this.put(FRIEND_STARTTIME,startTime);
  }
  public long getClassStartTime() {
    return getLong(CLASS_STARTTIME);
  }

  public void setClassStartTime(long startTime) {
    this.put(CLASS_STARTTIME,startTime);
  }
  public void setNewFriends(List<Friend> newFriends) {
    this.put(NEWFRIENDS,newFriends);
  }
  public List<Friend> getNewFriends(){
    return getList(NEWFRIENDS);
  }


  public boolean getMatching() {
    return getBoolean(CONTACTS_MATCHING);
  }

  public void setMatching(boolean matching) {
    this.put(CONTACTS_MATCHING,matching);
  }

  public String getPosition() {
    return getString(POSITION);
  }

  public void setPosition(String positon) {
    this.put(POSITION,positon);
  }

  public String getProvince() {
    return getString(PROVINCE);
  }

  public void setProvince(String province) {
    this.put(PROVINCE,province);
  }

  public String getCity() {
    return getString(CITY);
  }

  public void setCity(String city) {
    this.put(CITY,city);
  }

  public String getArea() {
    return getString(AREA);
  }

  public void setArea(String area) {
    this.put(AREA,area);
  }

  public List<Child> getChildren() {
    return getList(CHILDREN);
  }

  public void setChildren(List<Child> children) {
    this.put(CHILDREN,children);
  }

  public String getAvatarUrl() {
      return getString(AVATAR);
  }

  public void setAvatarUrl(String url){
    this.put(AVATAR,url);
  }

//  public void saveAvatar(String path, final SaveCallback saveCallback) {
//    final AVFile file;
//    try {
//      file = AVFile.withAbsoluteLocalPath(getUsername(), path);
//      put(AVATAR, file);
//      file.saveInBackground(new SaveCallback() {
//        @Override
//        public void done(AVException e) {
//          if (null == e) {
//            saveInBackground(saveCallback);
//          } else {
//            if (null != saveCallback) {
//              saveCallback.done(e);
//            }
//          }
//        }
//      });
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
//  }

  public void putFile(String objtectId, String path){
    fileManager = getAVObject(FILE_MANAGER);
    if (fileManager == null){
      fileManager = new AVObject();
      this.put(FILE_MANAGER,fileManager);
    }
    fileManager.put(objtectId, path);

  }

  public static LeanchatUser getCurrentUser() {
    return getCurrentUser(LeanchatUser.class);
  }

  public void updateUserInfo() {
    AVInstallation installation = AVInstallation.getCurrentInstallation();
    if (installation != null) {
      put(INSTALLATION, installation);
      saveInBackground();
    }


  }

  public AVGeoPoint getGeoPoint() {
    return getAVGeoPoint(LOCATION);
  }

  public void setGeoPoint(AVGeoPoint point) {
    put(LOCATION, point);
  }

  public static void signUpByNameAndPwd(String name, String password, SignUpCallback callback) {
    AVUser user = new AVUser();
    user.setUsername(name);
    user.setPassword(password);
    user.signUpInBackground(callback);
  }

  public void removeFriend(String friendId, final SaveCallback saveCallback) {
    unfollowInBackground(friendId, new FollowCallback() {
      @Override
      public void done(AVObject object, AVException e) {
        if (saveCallback != null) {
          saveCallback.done(e);
        }
      }
    });
  }

  public void findFriendsWithCachePolicy(AVQuery.CachePolicy cachePolicy, FindCallback<LeanchatUser>
      findCallback) {
    AVQuery<LeanchatUser> q = null;
    try {
      q = followeeQuery(LeanchatUser.class);
    } catch (Exception e) {
    }
    q.setCachePolicy(cachePolicy);
    q.setMaxCacheAge(TimeUnit.MINUTES.toMillis(1));
    q.findInBackground(findCallback);
  }
}
