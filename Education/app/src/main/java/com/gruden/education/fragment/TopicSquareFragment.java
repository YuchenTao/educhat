package com.gruden.education.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import com.gruden.education.App;
import com.gruden.education.R;
import com.gruden.education.activity.MainActivity;
import com.gruden.education.activity.bbs.PublicPostActivity;
import com.gruden.education.activity.search.SearchActivity;
import com.gruden.education.adapter.TopicListAdapter;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.model.Topic;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopicSquareFragment extends BaseFragment {


    @Bind(R.id.tabHost)
    TabHost tabHost;

    @Bind(R.id.layout_search)
    View layout_search;

    ArrayList<String> arrTitleCode = new ArrayList<String>();
    private LeanchatUser user=LeanchatUser.getCurrentUser();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.topic_square_fragment, container, false);
        ButterKnife.bind(this, view);
        tabHost.setup();
        initListener();
        return view;


    }

    private void initListener() {
        layout_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                getActivity().startActivity(intent);
            }
        });

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        headerLayout.showTitle("话题广场");
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();

                transaction.remove(App.ctx.mainActivity.topicSquareFragment);
                transaction.show(App.ctx.mainActivity.bbsFragment);
                transaction.commit();

                App.ctx.mainActivity.topicSquareFragment = null;

            }
        };
        headerLayout.showLeftBackButton("社区",listener);

        headerLayout.showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, PublicPostActivity.class);
                ctx.startActivity(intent);
            }
        });


        getTabTitles();

    }

    @Override
    public void onResume() {
        super.onResume();

    }





    private void getTabTitles(){

        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("userId", user.getObjectId());
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_FORUM +"topics/tag";

        MyHttpClient.get(url, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0){
                    toast( "加载失败");
                }else {

                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    for (int i=0; i<array.length(); i++){

                        View tab = inflater.inflate(R.layout.topic_tab_spec, null);
                        TextView txtTitle = (TextView) tab.findViewById(R.id.txtTitle);
                        txtTitle.setText(array.optJSONObject(i).optString("name"));

                        ListView listView = new ListView(getActivity());
                        listView.setId(100+i);
                        listView.setDivider(null);
                        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                        tabHost.getTabContentView().addView(listView,params);


                        tabHost.addTab(tabHost.newTabSpec("Tab"+i).setIndicator(tab).setContent(listView.getId()));
                        System.out.println("listView id:" + listView.getId());

                        String titileCode = array.optJSONObject(i).optString("code");
                        arrTitleCode.add(titileCode);

                        getTopicList(listView,arrTitleCode.get(i));
                    }


                }

            }

            @Override
            public void handleSuccess(int code, String mesg) {
                toast( mesg);

            }

            @Override
            public void handleFailure(int error) {
                super.handleFailure(error);

            }

        });

    }

    private void getTopicList(final ListView listView, String tagCode){

        RequestParams params = new RequestParams();
        params.put("code", tagCode);
        params.put("userId", user.getObjectId());
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_FORUM +"topics/tag";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0){
                    toast( "加载失败");
                }else {
                    ArrayList<Topic> topicList = new ArrayList<Topic>();
                    for (int i=0; i<array.length(); i++){

                        Topic topic = new Topic();
                        topic.strTitle = array.optJSONObject(i).optString("name");
                        topic.strTagCode = array.optJSONObject(i).optString("code");
                        topic.imgUrl = array.optJSONObject(i).optString("icon");
                        topic.strDetail = array.optJSONObject(i).optString("note");
                        topic.isConcerned=array.optJSONObject(i).optInt("isConcerned");
                        topicList.add(topic);
                    }

                    TopicListAdapter listAdapter = new TopicListAdapter(getActivity(), topicList);
                    listView.setAdapter(listAdapter);

                }

                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);

            }

        });
    }
}
