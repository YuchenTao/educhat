package com.gruden.education.activity.classmenu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import net.datafans.android.common.helper.DipHelper;
import net.datafans.android.timeline.view.imagegrid.ImageGridView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class HomeworkParentActivity extends BaseActivity {

    @Bind(R.id.txtSubject)
    TextView txtSubject;

    @Bind(R.id.txtDate)
    TextView txtDate;

    @Bind(R.id.txtContent)
    TextView txtContent;

    @Bind(R.id.txtPrompt)
    TextView txtPrompt;

    @Bind(R.id.layoutContent)
    FrameLayout layoutContent;

    @Bind(R.id.btnSign)
    Button btnSign;

    private ImageGridView imageGridView;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework_parent);

        initActionBar("作业详情");
        showLeftBackButton();

        Intent intent = getIntent();
        id = intent.getStringExtra("id");

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();
        float scale = DipHelper.px2dip(this, width) / (float) DipHelper.px2dip(this, 1080);
        imageGridView = new ImageGridView(this, DipHelper.dip2px(this, scale * 240));
        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutContent.addView(imageGridView, imageParams);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHomeworkDetail();
    }

    void getHomeworkDetail(){
        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("id",id);
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_MESSAGE+"homework";

        MyHttpClient.get(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg, JSONArray array) {

                if (array.length()==0){

                    toast("请求失败");

                }else {
                    JSONObject object = array.optJSONObject(0);
                    txtSubject.setText(object.optString("subject"));
                    txtDate.setText(object.optString("endDate"));
                    txtContent.setText(object.optString("content"));
                    int sign = object.optInt("isSign");
                    if (sign==1){
                        txtPrompt.setVisibility(View.INVISIBLE);
                        btnSign.setText("已签收");
                        btnSign.setEnabled(false);
                    }

                    JSONArray arrUrls = object.optJSONArray("imgUrl");

                    if (arrUrls!=null&&arrUrls.length()>0){

                        List<String> listUrls = new ArrayList<String>();
                        for (int i=0; i<arrUrls.length(); i++){
                            listUrls.add(arrUrls.optString(i));
                        }
                        imageGridView.updateWithImage(listUrls);

                    }else {
                        layoutContent.setVisibility(View.GONE);
                    }

                }
                dismissSpinnerDialog();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }

    public void signHomework(View v){
        showSpinnerDialog();
        RequestParams params = new RequestParams();
        params.put("type",1);
        params.put("id",id);
        params.put("userId", LeanchatUser.getCurrentUser().getObjectId());

        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_MESSAGE+"sign";

        MyHttpClient.post(url,params, new MyJsonResponseHandler() {

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast( mesg);

                if (code==0){
                    txtPrompt.setVisibility(View.INVISIBLE);
                    btnSign.setText("已签收");
                    btnSign.setEnabled(false);
                }

            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });
    }
}
