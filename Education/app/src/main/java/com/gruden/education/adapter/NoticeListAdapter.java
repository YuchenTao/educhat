package com.gruden.education.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.gruden.education.model.Notice;
import com.gruden.education.viewholder.NoticeItemHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/1/12.
 */
public class NoticeListAdapter extends RecyclerView.Adapter<NoticeItemHolder> {

    String conversationId;
    public NoticeListAdapter(String conversationId){
        super();
        this.conversationId = conversationId;
    }
    public List<Notice> dataList = new ArrayList<Notice>();

    public List<Notice> getDataList() {
        return dataList;
    }

    public void setDataList(List<Notice> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }

    public void addDataList(List<Notice> datas) {
        dataList.addAll(0, datas);
    }

    @Override
    public NoticeItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NoticeItemHolder(parent,conversationId);
    }

    @Override
    public void onBindViewHolder(NoticeItemHolder holder, int position) {
        holder.bindData(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
