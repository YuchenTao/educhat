package com.gruden.education.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.model.Subject;
import com.gruden.education.viewholder.ScheduleItemHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/2/2.
 */
public class ScheduleListAdapter extends BaseAdapter {

    LayoutInflater mInflater;
    Context context;

    public ScheduleListAdapter(Context context){
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);

    }

    public List<Subject> dataList = new ArrayList<Subject>();

    public List<Subject> getDataList() {
        return dataList;
    }

    public void setDataList(List<Subject> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }


    public void addDataList(List<Subject> datas) {
        dataList.addAll( datas);
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Subject getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Subject subject = dataList.get(position);
        ScheduleItemHolder holder;
        if (convertView == null) {
            holder = new ScheduleItemHolder();
            convertView = mInflater.inflate(R.layout.schedule_item, parent, false);
            holder.txtSubject = (TextView)convertView.findViewById(R.id.txtSubject);
            holder.txtTime = (TextView)convertView.findViewById(R.id.txtTime);
            convertView.setTag(holder);

        } else {
            holder = (ScheduleItemHolder) convertView.getTag();
        }

        holder.txtSubject.setText(subject.name);
        holder.txtTime.setText(subject.period);

        return convertView;
    }
}
