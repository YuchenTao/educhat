package com.gruden.education.activity.conversation;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.avoscloud.leanchatlib.controller.ChatManager;
import com.avoscloud.leanchatlib.model.ConversationType;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.Utils;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import butterknife.Bind;

/**
 * Created by admin on 2016/5/5.
 */
public class CreateClassActivity extends BaseActivity {
    @Bind(R.id.btnCreateClass)
    Button btnCreateClass;
    @Bind(R.id.classnameEdit)
    EditText classnameEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_class);
        initActionBar("创建班级");
        showLeftBackButton();
        btnCreateClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createClass();
            }
        });
    }
    //创建班级
    private void createClass() {
        final String className = classnameEdit.getText().toString();
        if (TextUtils.isEmpty(className)) {
            Utils.toast(this, "班级名称不能为空~");
            return;
        }
        showSpinnerDialog();

        RequestParams params = new RequestParams();
        params.put("name", className);
        params.put("type", 1);
        params.put("creator", LeanchatUser.getCurrentUser().getObjectId()+"::"+LeanchatUser.getCurrentUser().getNickName());
        String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_REALTIME +"group";
        MyHttpClient.post(url, params, new MyJsonResponseHandler() {
            @Override
            public void handleSuccess(int code, String mesg, JSONObject data) {
                dismissSpinnerDialog();

                ChatManager.getInstance().getRoomsTable().insertRoom(data.optString("groupId"), ConversationType.Public.getValue());
                Intent intent=new Intent(CreateClassActivity.this,CreateSuccessActivity.class);
                intent.putExtra("groupId",data.optString("groupId"));
                intent.putExtra("code",data.optString("code"));
                intent.putExtra("type",data.optInt("type"));
                intent.putExtra("name",data.optString("name"));
                startActivity(intent);

                finish();
            }

            @Override
            public void handleSuccess(int code, String mesg) {
                dismissSpinnerDialog();
                toast(mesg);
                finish();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);
            }

        });

    }

}
