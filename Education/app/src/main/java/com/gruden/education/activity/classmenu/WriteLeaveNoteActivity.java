package com.gruden.education.activity.classmenu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.Child;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;

public class WriteLeaveNoteActivity extends BaseActivity {

    @Bind(R.id.editContent)
    EditText editContent;




    @Bind(R.id.layoutName)
    View layoutName;

    @Bind(R.id.txtChildName)
    TextView txtChildName;

    @Bind(R.id.layoutDate)
    View layoutDate;

    @Bind(R.id.txtDate)
    TextView txtDate;


    private TimePickerView datePickerView;
    private String child;

    private String conversationId;

    List<Child> childrenList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_note);
        initActionBar("写假条");
        showLeftBackButton();
        showRightTextButton("发送", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publish();
            }
        });


        Intent intent = getIntent();
        conversationId = intent.getStringExtra("id");

        //时间选择器
        datePickerView = new TimePickerView(this, TimePickerView.Type.YEAR_MONTH_DAY);
        //控制时间范围
        Calendar calendar = Calendar.getInstance();
        datePickerView.setRange(calendar.get(Calendar.YEAR), calendar.get(Calendar.YEAR)+10);
        datePickerView.setTime(new Date());
        datePickerView.setCyclic(false);
        datePickerView.setCancelable(true);
        //时间选择后回调
        datePickerView.setOnTimeSelectListener(new TimePickerView.OnTimeSelectListener() {

            @Override
            public void onTimeSelect(Date date) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                txtDate.setText( format.format(date) );
            }
        });

        //弹出时间选择器
        layoutDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                datePickerView.show();
            }
        });
        layoutName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChild();
            }
        });
    }

    private void showChild() {
        if (childrenList.size()==0){
            toast("目前还没有孩子~");
            return;
        }
        final String[] mList=new String[childrenList.size()];
        for (int i =0;i<childrenList.size();i++){
            mList[i]=childrenList.get(i).name;
        }

        AlertDialog.Builder listDia=new AlertDialog.Builder(this);
        listDia.setTitle("我的孩子");
        listDia.setItems(mList, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                child=childrenList.get(which).childId+"::"+childrenList.get(which).name;
//                txtChildName.setText(child.substring(child.indexOf("::") + 1, child.length()));
                child=childrenList.get(which).childId+"::"+childrenList.get(which).name;
                txtChildName.setText(childrenList.get(which).name);
            }
        });
        listDia.create().show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getChildList();
    }

    private void getChildList(){
        LeanchatUser user = LeanchatUser.getCurrentUser();
        childrenList = user.getChildren();
    }

    void publish(){
        String strChild = txtChildName.getText().toString();
        if (TextUtils.isEmpty(strChild)) {
            toast("请假人不能为空");
            return;
        }
        String strDate = txtDate.getText().toString();
        if (TextUtils.isEmpty(strDate)) {
            toast("请假日期不能为空");
            return;
        }
        String strContent = editContent.getText().toString();
        if (TextUtils.isEmpty(strContent)) {
            toast("请假原因不能为空");
            return;
        }

        showSpinnerDialog();
        LeanchatUser user = LeanchatUser.getCurrentUser();
        RequestParams params = new RequestParams();
        params.put("userId",user.getObjectId());
        params.put("groupId",conversationId);
        params.put("startDate",strDate);
        params.put("reason",strContent);
        params.put("child",child);

        String url = MyHttpClient.BASE_URL +  MyHttpClient.MODEL_REALTIME + "leave";
        MyHttpClient.post(url,params,new MyJsonResponseHandler(){

            @Override
            public void handleSuccess(int code, String mesg){
                dismissSpinnerDialog();
                toast(mesg);
                if (code==0)
                    onBackPressed();
            }

            @Override
            public void handleFailure(int error) {
                dismissSpinnerDialog();
                super.handleFailure(error);

            }

        });



    }
}
