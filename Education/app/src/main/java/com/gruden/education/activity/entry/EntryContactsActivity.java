package com.gruden.education.activity.entry;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.activity.MainActivity;
import com.gruden.education.activity.discover.contact.Contact;
import com.gruden.education.base.MyHttpClient;
import com.gruden.education.base.MyJsonResponseHandler;
import com.gruden.education.model.LeanchatUser;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;

public class EntryContactsActivity extends EntryBaseActivity {
	@Bind(R.id.btn_add)
	Button btnAdd;
	@Bind(R.id.btn_pass)
	Button btnPass;
	@Bind(R.id.listContacts)
	ListView listContacts;
	ContactsAdapter adapter;


	ArrayList<Map<String,String>> phoneList=new ArrayList<Map<String,String>>();
	ArrayList<String> phone=new ArrayList<String>();
	ArrayList<Contact> contactlist=new ArrayList<Contact>();
	ArrayList<String> addFriends=new ArrayList<String>();
	private static final String[] CONTACTOR_ION = new String[]{
			ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
			ContactsContract.Contacts.DISPLAY_NAME,
			ContactsContract.CommonDataKinds.Phone.NUMBER
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.entry_contacts_activity);
		initActionBar("通讯录匹配");
		showLeftBackButton();
		adapter=new ContactsAdapter(this);
		listContacts.setAdapter(adapter);
		initListener();
	}

	private void initListener() {
		contactlist.clear();
		matchContacts();
		btnPass.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MainActivity.goMainActivityFromActivity(EntryContactsActivity.this);
			}
		});
		btnAdd.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFriend();
			}
		});
//		listContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				CheckBox checkBox = (CheckBox) view.findViewById(R.id.cbChecked);
//				if (checkBox.isChecked()) {
//					checkBox.setChecked(false);
//				} else {
//					checkBox.setChecked(true);
//				}
//			}
//		});
	}
	private void getFriendsInfo(){
		addFriends.clear();
		for (int i=0;i<adapter.getCount();i++){
			Contact contact=adapter.getItem(i);
			if (contact.isChecked){
				String friend=contact.userId+"::"+contact.simpleNumber;
				addFriends.add(friend);
			}
		}
	}

    private void addFriend(){
		getFriendsInfo();
		if (addFriends.size()==0){
			toast("您还没有选择联系人~");
			return ;
		}
		showSpinnerDialog();
		RequestParams params = new RequestParams();
		params.put("selfId", LeanchatUser.getCurrentUser().getObjectId());
		params.put("type", 1);
		params.put("friend", addFriends);

		String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"friends/apply";

		MyHttpClient.post(url, params, new MyJsonResponseHandler() {
			@Override
			public void handleSuccess(int code, String mesg, JSONArray data) {
				dismissSpinnerDialog();
				toast(mesg);
				MainActivity.goMainActivityFromActivity(EntryContactsActivity.this);
			}
			@Override
			public void handleSuccess(int code, String mesg) {
				dismissSpinnerDialog();
				toast(mesg);
			}

			@Override
			public void handleFailure(int error) {
				dismissSpinnerDialog();
				super.handleFailure(error);
			}

		});
	}
	private void matchContacts(){
		showSpinnerDialog();
		readAllContacts();

		System.out.println(phone.toString());

		String url = MyHttpClient.BASE_URL + MyHttpClient.MODEL_USER +"contact";
		url = url + "?userId="+ LeanchatUser.getCurrentUser().getObjectId();
		for(int i=0;i<phone.size();i++){
			url+="&phone="+phone.get(i);
		}
		MyHttpClient.get(url, new MyJsonResponseHandler() {

			@Override
			public void handleSuccess(int code, String mesg, JSONArray data) {
				if (data.length() == 0 || data == null) {
					toast("通讯录里还没有匹配的用户~");
				} else {

					for (int j = 0; j < phoneList.size(); j++) {
						for (int i = 0; i < data.length(); i++) {
							JSONObject object = data.optJSONObject(i);
							if (phoneList.get(j).containsKey(object.opt("phone"))) {
								Contact contact = new Contact();
								contact.userId = object.optString("uid");
								contact.avatarUrl = object.optString("avatar");
								contact.name = object.optString("nickname")+"(" + phoneList.get(j).get(object.opt("phone")) + ")";
								contact.number=object.optString("phone");
							    contact.simpleNumber=phoneList.get(j).get(object.opt("phone"));
								contact.isChecked=true;
								contactlist.add(contact);
							}
						}
					}
					adapter.setDataList(contactlist);
					adapter.notifyDataSetChanged();
				}
				dismissSpinnerDialog();
			}

			@Override
			public void handleSuccess(int code, String mesg) {
				dismissSpinnerDialog();
				toast(mesg);

			}

			@Override
			public void handleFailure(int error) {
				dismissSpinnerDialog();
				super.handleFailure(error);
			}

		});
	}

//    @Override
//	protected void onResume() {
//		super.onResume();
//		contactlist.clear();
//		matchContacts();
//	}

	/*
         * 读取联系人的信息
         */
	public void readAllContacts(){
		phone.clear();
		phoneList.clear();
		Cursor phones = null;
		ContentResolver cr = getContentResolver();
		try {
			phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI
					, CONTACTOR_ION, null, null, "sort_key");

			if (phones != null) {
				final int contactIdIndex = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
				final int displayNameIndex = phones.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
				final int phoneIndex = phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
				String phoneString, displayNameString, contactIdString;
				while (phones.moveToNext()) {
//          LinkManForm linkManForm = new LinkManForm();
					HashMap<String,String> map = new HashMap<String,String>();
					phoneString = phones.getString(phoneIndex);
					displayNameString = phones.getString(displayNameIndex);
					contactIdString = phones.getString(contactIdIndex);
					if (TextUtils.isEmpty(phoneString))
						continue;
					phone.add(phoneString);
					map.put(phoneString, displayNameString);
					phoneList.add(map);

				}
			}
		} catch (Exception e) {

		} finally {
			if (phones != null)
				phones.close();
		}
	}

    class ContactsAdapter extends BaseAdapter{
		Context context;
		ArrayList<Contact> data=new ArrayList<Contact>();

		public ContactsAdapter( Context context) {
			this.context = context;
		}

		public void setDataList(ArrayList<Contact> datas) {
			data.clear();
			if (null != datas) {
				data.addAll(datas);
			}
		}
		public void addDataList(ArrayList<Contact> datas) {
			data.addAll( datas);
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Contact getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final Contact contact=data.get(position);
			ContactHolder holder=null;
			if (convertView==null){
				holder=new ContactHolder();
				convertView=View.inflate(context,R.layout.item_contact,null);
				holder.tvLetter= (TextView) convertView.findViewById(R.id.tvLetter);
				holder.cbChecked= (CheckBox) convertView.findViewById(R.id.cbChecked);
				holder.title= (TextView) convertView.findViewById(R.id.title);
				holder.imgAvatar= (ImageView) convertView.findViewById(R.id.imgAvatar);
				holder.tvLetter.setVisibility(View.GONE);
				holder.cbChecked.setVisibility(View.VISIBLE);
				convertView.setTag(holder);
			}else{
				holder= (ContactHolder) convertView.getTag();
			}
			if (contact.isChecked){
				holder.cbChecked.setChecked(true);
			}else{
				holder.cbChecked.setChecked(false);
			}
			if(contact.avatarUrl.length()>0){
				ImageLoader.getInstance().displayImage(contact.avatarUrl, holder.imgAvatar);
			}
			holder.title.setText(contact.name);
			holder.cbChecked.setTag(contact.userId+"::"+contact.simpleNumber);
			final ContactHolder finalHolder = holder;
			convertView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					finalHolder.cbChecked.performClick();
				}
			});
			holder.cbChecked.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (finalHolder.cbChecked.isChecked()){
						finalHolder.cbChecked.setChecked(true);
						contact.isChecked=true;
					}else{
						finalHolder.cbChecked.setChecked(false);
						contact.isChecked=false;
					}
				}
			});
			return convertView;
		}
	}

	class ContactHolder{
		TextView tvLetter;
		CheckBox cbChecked;
		ImageView imgAvatar;
		TextView title;

	}


}