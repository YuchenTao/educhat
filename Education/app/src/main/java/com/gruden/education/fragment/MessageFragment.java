package com.gruden.education.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.callback.AVIMConversationCallback;
import com.avos.avoscloud.im.v2.callback.AVIMSingleMessageQueryCallback;
import com.avoscloud.leanchatlib.controller.ChatManager;
import com.avoscloud.leanchatlib.controller.ConversationHelper;
import com.avoscloud.leanchatlib.controller.RoomsTable;
import com.avoscloud.leanchatlib.event.ConnectionChangeEvent;
import com.avoscloud.leanchatlib.event.ImTypeMessageEvent;
import com.avoscloud.leanchatlib.model.ConversationType;
import com.avoscloud.leanchatlib.model.Room;
import com.avoscloud.leanchatlib.utils.Constants;
import com.avoscloud.leanchatlib.utils.ConversationManager;
import com.gruden.education.R;
import com.gruden.education.activity.conversation.ChatRoomActivity;
import com.gruden.education.activity.discover.contact.ContactActivity;
import com.gruden.education.activity.search.SearchActivity;
import com.gruden.education.adapter.ConversationSwipeListAdapter;
import com.gruden.education.customview.swipemenulistview.SwipeMenu;
import com.gruden.education.customview.swipemenulistview.SwipeMenuCreator;
import com.gruden.education.customview.swipemenulistview.SwipeMenuItem;
import com.gruden.education.customview.swipemenulistview.SwipeMenuListView;
import com.gruden.education.event.MessageItemClickEvent;
import com.gruden.education.model.LeanchatUser;
import com.gruden.education.util.UserCacheUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;


public class MessageFragment extends BaseFragment {

  @Bind(R.id.im_client_state_view)
  View imClientStateView;

  @Bind(R.id.fragment_conversation_srl_pullrefresh)
  protected SwipeRefreshLayout refreshLayout;

  @Bind(R.id.fragment_conversation_srl_view)
  protected SwipeMenuListView listView;

  protected ConversationSwipeListAdapter itemAdapter;

  private boolean hidden;
  private ConversationManager conversationManager;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.message_fragment, container, false);
    ButterKnife.bind(this, view);

    conversationManager = ConversationManager.getInstance();
    refreshLayout.setEnabled(false);
    EventBus.getDefault().register(this);

    return view;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    headerLayout.showTitle("消息");

    headerLayout.showLeftImageButton(R.drawable.search, new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(getActivity(), SearchActivity.class);
        getActivity().startActivity(intent);
      }
    });

    headerLayout.showRightImageButton(R.drawable.navbar_tianjia, new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(ctx, ContactActivity.class);
        intent.putExtra("isCheck", true);
        intent.putExtra("type",0);
        ctx.startActivity(intent);
      }
    });

    initListView();

//    updateConversationList();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    EventBus.getDefault().unregister(this);
  }

  @Override
  public void onHiddenChanged(boolean hidden) {
    super.onHiddenChanged(hidden);
    this.hidden = hidden;
    if (!hidden) {
      updateConversationList();
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!hidden) {
      updateConversationList();
    }
  }


  private void initListView(){

    /** 给ListView设置adapter **/
    itemAdapter = new ConversationSwipeListAdapter(ctx);
    listView.setAdapter(itemAdapter);

    SwipeMenuCreator creator = new SwipeMenuCreator() {

      @Override
      public void create(SwipeMenu menu) {
        // create "open" item
        SwipeMenuItem openItem = new SwipeMenuItem(ctx);
        // set item background
        openItem.setBackground(new ColorDrawable(Color.RED));
        // set item width
        openItem.setWidth(dp2px(70));
        // set item title
        openItem.setTitle("删除");
        // set item title fontsize
        openItem.setTitleSize(18);
        // set item title font color
        openItem.setTitleColor(Color.WHITE);
        // add to menu
        menu.addMenuItem(openItem);

      }
    };

    // set creator
    listView.setMenuCreator(creator);

    // step 2. listener item click event
    listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
      @Override
      public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
        new AlertDialog.Builder(ctx).setMessage("您确定要退出该会话吗?")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    showSpinnerDialog();
                    Room room = itemAdapter.getItem(position);
                    final String convid = room.getConversationId();
                    AVIMConversation conversation = room.getConversation();
                    conversation.quit(new AVIMConversationCallback() {
                      @Override
                      public void done(AVIMException e) {
                        dismissSpinnerDialog();
                        if (filterException(e)) {

                          RoomsTable roomsTable = ChatManager.getInstance().getRoomsTable();
                          roomsTable.deleteRoom(convid);
                          itemAdapter.getDataList().remove(position);
                          itemAdapter.notifyDataSetChanged();
                          toast("已退出该会话");
                        }else {
                          toast("退出会话失败");
                        }
                      }
                    });
                  }
                }).setNegativeButton("取消", null).show();

        return false;
      }
    });

  }
  
  
  public void onEvent(MessageItemClickEvent event) {
    Intent intent = new Intent(getActivity(), ChatRoomActivity.class);
    intent.putExtra(Constants.CONVERSATION_ID, event.conversationId);
    startActivity(intent);
  }

  public void onEvent(ImTypeMessageEvent event) {
    updateConversationList();
  }

  private void updateConversationList() {
    conversationManager.findAndCachePrivateRooms(new Room.MultiRoomsCallback() {
      @Override
      public void done(List<Room> roomList, AVException exception) {
        if (filterException(exception)) {

          updateLastMessage(roomList);
          cacheRelatedUsers(roomList);
          setItemType(roomList);

          List<Room> sortedRooms = sortRooms(roomList);
          itemAdapter.updateListView(sortedRooms);
          itemAdapter.notifyDataSetChanged();
        }
      }
    });
  }

  private void updateLastMessage(final List<Room> roomList) {
    for (final Room room : roomList) {
      AVIMConversation conversation = room.getConversation();
      if (null != conversation) {
        conversation.getLastMessage(new AVIMSingleMessageQueryCallback() {
          @Override
          public void done(AVIMMessage avimMessage, AVIMException e) {
            if (filterException(e) && null != avimMessage) {
              room.setLastMessage(avimMessage);
            }
          }
        });
      }
    }
  }

  private void cacheRelatedUsers(List<Room> rooms) {
    List<String> needCacheUsers = new ArrayList<String>();
    for(Room room : rooms) {
      AVIMConversation conversation = room.getConversation();
      if (ConversationHelper.typeOfConversation(conversation) == ConversationType.Private) {
        needCacheUsers.add(ConversationHelper.otherIdOfConversation(conversation));
      }
    }
    UserCacheUtils.fetchUsers(needCacheUsers, new UserCacheUtils.CacheUserCallback() {
      @Override
      public void done(List<LeanchatUser> userList, Exception e) {
        itemAdapter.notifyDataSetChanged();
      }
    });
  }

  private List<Room> sortRooms(final List<Room> roomList) {
    List<Room> sortedList = new ArrayList<Room>();
    if (null != roomList) {
      sortedList.addAll(roomList);
      Collections.sort(sortedList, new Comparator<Room>() {
        @Override
        public int compare(Room lhs, Room rhs) {
          long value = lhs.getLastModifyTime() - rhs.getLastModifyTime();
          if (value > 0) {
            return -1;
          } else if (value < 0) {
            return 1;
          } else {
            return 0;
          }
        }
      });
    }
    return sortedList;
  }

  private void setItemType(final List<Room> roomList) {
    for (Room room : roomList) {
      room.itemType = 0;
    }
  }

  public void onEvent(ConnectionChangeEvent event) {
    if(imClientStateView.getVisibility()==View.VISIBLE||(!event.isConnect)){
      updateConversationList();
    }
    imClientStateView.setVisibility(event.isConnect ? View.GONE : View.VISIBLE);
  }
}
