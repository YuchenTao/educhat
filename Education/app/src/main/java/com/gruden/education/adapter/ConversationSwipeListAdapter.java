package com.gruden.education.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.im.v2.AVIMConversation;
import com.avos.avoscloud.im.v2.AVIMException;
import com.avos.avoscloud.im.v2.AVIMMessage;
import com.avos.avoscloud.im.v2.callback.AVIMSingleMessageQueryCallback;
import com.avoscloud.leanchatlib.controller.ConversationHelper;
import com.avoscloud.leanchatlib.model.Room;
import com.avoscloud.leanchatlib.utils.PhotoUtils;
import com.avoscloud.leanchatlib.utils.ThirdPartUserUtils;
import com.avoscloud.leanchatlib.utils.Utils;
import com.gruden.education.R;
import com.gruden.education.customview.swipemenulistview.BaseSwipListAdapter;
import com.gruden.education.event.ClassDetailItemClickEvent;
import com.gruden.education.event.ClassItemClickEvent;
import com.gruden.education.event.MessageItemClickEvent;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by yunjiansun on 16/5/5.
 */
public class ConversationSwipeListAdapter extends BaseSwipListAdapter {

    private List<Room> listRooms = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context ctx;


    public ConversationSwipeListAdapter(Context ctx){
        super();
        this.ctx = ctx;
        this.mInflater = LayoutInflater.from(ctx);

    }

    public ConversationSwipeListAdapter(Context ctx,List<Room> listRooms){
        super();
        this.ctx = ctx;
        this.mInflater = LayoutInflater.from(ctx);
        this.listRooms = listRooms;

    }

    public List<Room> getDataList(){
        return listRooms;
    }

    public void  updateListView(List<Room> list) {
        if (list == null) {
            this.listRooms.clear();
        } else {
            this.listRooms = list;
        }
    }
    

    @Override
    public int getCount() {
        return listRooms.size();
    }

    @Override
    public Room getItem(int position) {
        return listRooms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ConversationItemHolder holder;
        final Room room = listRooms.get(position);
        final AVIMConversation conversation = room.getConversation();
        if (convertView == null) {
            holder = new ConversationItemHolder();
            convertView = mInflater.inflate(R.layout.conversation_item, parent, false);
            holder.avatarView = (ImageView)convertView.findViewById(R.id.iv_recent_avatar);
            holder.nameView = (TextView)convertView.findViewById(R.id.recent_name_text);
            holder.timeView = (TextView)convertView.findViewById(R.id.recent_time_text);
            holder.unreadView = (TextView)convertView.findViewById(R.id.recent_unread);
            holder.messageView = (TextView)convertView.findViewById(R.id.recent_msg_text);
            convertView.setTag(holder);
        } else {
            holder = (ConversationItemHolder) convertView.getTag();
        }

        if (null != conversation) {
            String avatar;
            if (conversation.getMembers().size()==2) {
                String userId = ConversationHelper.otherIdOfConversation(conversation);
                avatar = ThirdPartUserUtils.getInstance().getUserAvatar(userId);
                ImageLoader.getInstance().displayImage(avatar, holder.avatarView, PhotoUtils.avatarImageOptions);

            } else {
                if (conversation.getAttribute("avatar")==null){
                    avatar = null;
                }else {
                    avatar = conversation.getAttribute("avatar").toString();
                }
                System.out.println("avatar:"+avatar);
                ImageLoader.getInstance().displayImage(avatar, holder.avatarView, PhotoUtils.groupImageOptions);
            }

            final String conversationName = ConversationHelper.nameOfConversation(conversation);
            holder.nameView.setText(conversation.getName());

            int num = room.getUnreadCount();
            holder.unreadView.setText(num + "");
            holder.unreadView.setVisibility(num > 0 ? View.VISIBLE : View.GONE);

            conversation.getLastMessage(new AVIMSingleMessageQueryCallback() {
                @Override
                public void done(AVIMMessage avimMessage, AVIMException e) {
                    if (null != avimMessage) {
                        Date date = new Date(avimMessage.getTimestamp());
                        SimpleDateFormat format = new SimpleDateFormat("MM-dd HH:mm");
                        holder.timeView.setText(format.format(date));
                        holder.messageView.setText(Utils.getMessageeShorthand(ctx, avimMessage));
                    } else {
                        holder.timeView.setText("");
                        holder.messageView.setText("");
                    }
                }
            });

            switch (room.itemType){

                case 0: {
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            EventBus.getDefault().post(new MessageItemClickEvent(room.getConversationId()));
                        }
                    });
                    break;
                }

                case 1:{
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            EventBus.getDefault().post(new ClassItemClickEvent(room.getConversationId(),conversationName));
                        }
                    });
                    break;
                }

                case 2:{
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            EventBus.getDefault().post(new ClassDetailItemClickEvent(room.getConversationId()));
                        }
                    });
                    break;
                }
            }

        }
        return convertView;
    }

    public static class ConversationItemHolder{
        ImageView avatarView;
        TextView unreadView;
        TextView messageView;
        TextView timeView;
        TextView nameView;
    }
}
