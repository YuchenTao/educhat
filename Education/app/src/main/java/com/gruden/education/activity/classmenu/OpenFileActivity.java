package com.gruden.education.activity.classmenu;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;

import com.gruden.education.R;
import com.gruden.education.activity.BaseActivity;

import java.io.File;

import butterknife.Bind;

/**
 * Created by admin on 2016/3/11.
 */
public class OpenFileActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.btnopenfile)
    Button openfilebtn;

    @Bind(R.id.btnLayout)
    View btnLayout;

    @Bind(R.id.layoutCheck)
    View layoutCheck;

    @Bind(R.id.layoutPicture)
    View layoutPicture;

    @Bind(R.id.img)
    ImageView img;

    @Bind(R.id.layoutOffice)
    View layoutOffice;

    @Bind(R.id.webView)
    WebView webView;

    String filePath;
    private  String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.open_file_activity);
        Intent intent=getIntent();
        String title=intent.getStringExtra("name");
        filePath=intent.getStringExtra("filePath");
        url=intent.getStringExtra("url");
        System.out.println("URL:"+url);
        initActionBar(title);
        showLeftBackButton();
        openfilebtn.setOnClickListener(this);
        openFile2(filePath);
    }

    @Override
    public void onClick(View v) {

        Intent intent = openFile(filePath);
        startActivity(intent);
    }
    public void openFile2(String filePath){
        File file = new File(filePath);
        if(!file.exists()) {
            return;
        }
        Uri uri=Uri.fromFile(file);
        String end=file.getName().substring(file.getName().lastIndexOf(".") + 1,file.getName().length()).toLowerCase();
        if(end.equalsIgnoreCase("jpg")||end.equalsIgnoreCase("gif")||end.equalsIgnoreCase("png")||
                end.equalsIgnoreCase("jpeg")||end.equalsIgnoreCase("bmp")) {
            Bitmap bmpDefaultPic = null;
           btnLayout.setVisibility(View.GONE);
            layoutCheck.setVisibility(View.GONE);
            layoutPicture.setVisibility(View.VISIBLE);
            if (bmpDefaultPic == null)
                bmpDefaultPic = BitmapFactory.decodeFile(filePath, null);
            img.setImageBitmap(bmpDefaultPic);
//        }else{
//        String doc="<iframe src='http://docs.google.com/gview?embedded=true&url="+url+"' width='100%' height='100%' style='border: none;'></iframe>";
//        layoutCheck.setVisibility(View.GONE);
//        layoutOffice.setVisibility(View.VISIBLE);
//        webView.getSettings().setUseWideViewPort(true);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setAllowFileAccess(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.getSettings().setSupportZoom(true);
//        webView.loadUrl(doc);
//        webView.loadData(doc, "text/html", "UTF-8");
//            WebSettings webSettings =   webView .getSettings();
//            webSettings.setJavaScriptEnabled(true);
//            webSettings.setBuiltInZoomControls(true);
//            webSettings.setSupportZoom(true);
//            webSettings.setUseWideViewPort(true);//设置此属性，可任意比例缩放
//            webSettings.setLoadWithOverviewMode(true);
//            webSettings.setAllowFileAccess(true);
        }
    }

    public Intent openFile(String filePath){

        File file = new File(filePath);
        if(!file.exists()) return null;
		/* 取得扩展名 */
        String end=file.getName().substring(file.getName().lastIndexOf(".") + 1,file.getName().length()).toLowerCase();
		/* 依扩展名的类型决定MimeType */
        if(end.equalsIgnoreCase("m4a")||end.equalsIgnoreCase("mp3")||end.equalsIgnoreCase("mid")||
                end.equalsIgnoreCase("xmf")||end.equalsIgnoreCase("ogg")||end.equalsIgnoreCase("wav")){
            return getAudioFileIntent(filePath);
        }else if(end.equalsIgnoreCase("3gp")||end.equalsIgnoreCase("mp4")){
            return getAudioFileIntent(filePath);
        }else if(end.equalsIgnoreCase("jpg")||end.equalsIgnoreCase("gif")||end.equalsIgnoreCase("png")||
                end.equalsIgnoreCase("jpeg")||end.equalsIgnoreCase("bmp")){
            return getImageFileIntent(filePath);
        }else if(end.equalsIgnoreCase("apk")){
            return getApkFileIntent(filePath);
        }else if(end.equalsIgnoreCase("ppt")){
            return getPptFileIntent(filePath);
        }else if(end.equalsIgnoreCase("xls")||end.equalsIgnoreCase("xlsx")){
            return getExcelFileIntent(filePath);
        }else if(end.equalsIgnoreCase("doc")||end.equalsIgnoreCase("docx")){
            return getWordFileIntent(filePath);
        }else if(end.equalsIgnoreCase("pdf")){
            return getPdfFileIntent(filePath);
        }else if(end.equalsIgnoreCase("chm")){
            return getChmFileIntent(filePath);
        }else if (end.equalsIgnoreCase("txt")){
            return getTextFileIntent(filePath,false);
        }else{
            return getAllIntent(filePath);
        }
    }

    //Android获取一个用于打开APK文件的intent
    public static Intent getAllIntent( String param ) {

        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.content.Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(new File(param ));
        intent.setDataAndType(uri,"*/*");
        return intent;
    }
    //Android获取一个用于打开APK文件的intent
    public static Intent getApkFileIntent( String param ) {

        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.content.Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(new File(param ));
        intent.setDataAndType(uri,"application/vnd.android.package-archive");
        return intent;
    }

    //Android获取一个用于打开VIDEO文件的intent
    public static Intent getVideoFileIntent( String param ) {

        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("oneshot", 0);
        intent.putExtra("configchange", 0);
        Uri uri = Uri.fromFile(new File(param ));
        intent.setDataAndType(uri, "video/*");
        return intent;
    }

    //Android获取一个用于打开AUDIO文件的intent
    public static Intent getAudioFileIntent( String param ){

        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("oneshot", 0);
        intent.putExtra("configchange", 0);
        Uri uri = Uri.fromFile(new File(param ));
        intent.setDataAndType(uri, "audio/*");
        return intent;
    }

    //Android获取一个用于打开Html文件的intent
    public static Intent getHtmlFileIntent( String param ){

        Uri uri = Uri.parse(param ).buildUpon().encodedAuthority("com.android.htmlfileprovider").scheme("content").encodedPath(param ).build();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(uri, "text/html");
        return intent;
    }

    //Android获取一个用于打开图片文件的intent
    public static Intent getImageFileIntent( String param ) {

        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(new File(param));
        intent.setDataAndType(uri, "image/*");
        return intent;
    }

    //Android获取一个用于打开PPT文件的intent
    public static Intent getPptFileIntent( String param ){

        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(new File(param ));
        intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        return intent;
    }

    //Android获取一个用于打开Excel文件的intent
    public static Intent getExcelFileIntent( String param ){

        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(new File(param ));
        intent.setDataAndType(uri, "application/vnd.ms-excel");
        return intent;
    }

    //Android获取一个用于打开Word文件的intent
    public static Intent getWordFileIntent( String param ){

        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(new File(param ));
        intent.setDataAndType(uri, "application/msword");
        return intent;
    }

    //Android获取一个用于打开CHM文件的intent
    public static Intent getChmFileIntent( String param ){

        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(new File(param ));
        intent.setDataAndType(uri, "application/x-chm");
        return intent;
    }

    //Android获取一个用于打开文本文件的intent
    public static Intent getTextFileIntent( String param, boolean paramBoolean){

        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (paramBoolean){
            Uri uri1 = Uri.parse(param );
            intent.setDataAndType(uri1, "text/plain");
        }else{
            Uri uri2 = Uri.fromFile(new File(param ));
            intent.setDataAndType(uri2, "text/plain");
        }
        return intent;
    }
    //Android获取一个用于打开PDF文件的intent
    public static Intent getPdfFileIntent( String param ){

        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(new File(param ));
        intent.setDataAndType(uri, "application/pdf");
        return intent;
    }
}
