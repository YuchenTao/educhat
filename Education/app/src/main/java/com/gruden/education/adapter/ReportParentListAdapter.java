package com.gruden.education.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gruden.education.R;
import com.gruden.education.model.Exam;
import com.gruden.education.model.Subject;
import com.gruden.education.viewholder.ReportParentItemHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yunjiansun on 16/1/15.
 */
public class ReportParentListAdapter extends BaseAdapter {

    LayoutInflater mInflater;

    public ReportParentListAdapter(Context context){
        super();
        this.mInflater = LayoutInflater.from(context);

    }

    public List<Exam> dataList = new ArrayList<Exam>();

    public List<Exam> getDataList() {
        return dataList;
    }

    public void setDataList(List<Exam> datas) {
        dataList.clear();
        if (null != datas) {
            dataList.addAll(datas);
        }
    }


    public void addDataList(List<Exam> datas) {
        dataList.addAll( datas);
    }
    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Exam exam = dataList.get(position);
        ReportParentItemHolder holder;
        if (convertView == null) {
            holder = new ReportParentItemHolder();
            convertView = mInflater.inflate(R.layout.report_parent_item, parent, false);
            holder.txtTitle = (TextView)convertView.findViewById(R.id.txtTitle);
            holder.txtTotal = (TextView)convertView.findViewById(R.id.txtTotal);
            holder.txtSubject = (TextView)convertView.findViewById(R.id.txtSubject);
            convertView.setTag(holder);
        }else {
            holder = (ReportParentItemHolder) convertView.getTag();
        }

        holder.txtTitle.setText(exam.examName);
        holder.txtTotal.setText("总分："+exam.total);

        String strSubject = "";
        for (int i=0; i<exam.subjectList.size();i++){
            Subject subject = exam.subjectList.get(i);
            strSubject = strSubject + subject.name + "：" + subject.score;
            if ((i+1)>3&&(i+1)/3==1){
                strSubject = "\n"+strSubject;
            }
            if((i+1)%3!=0){
                strSubject = strSubject + " ";
            }
        }

        holder.txtSubject.setText(strSubject);

        return convertView;
    }
}
